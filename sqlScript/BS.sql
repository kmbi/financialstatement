

ALTER PROCEDURE dbo.spBalanceSheet
@month nvarchar(50),
@year int
AS

--DECLARE @month nvarchar(50)
--DECLARE @year int
DECLARE @previousyear int

--SET @month = 'April'
--SET @year = 2021
SET @previousyear = @year - 1

SELECT currYear.Particulars, 
	currYear.accntType, 
	currYear.Amount, 
	prevYear.Amount, 
	currYear.month, 
	currYear.year as currYear, 
	prevYear.year as prevYear 
FROM
(
SELECT 
	Particulars,
	CASE WHEN trim(Particulars) LIKE 'currAssets%' THEN 'Current Assets'
		WHEN  trim(Particulars) LIKE 'noncurrAssets%' THEN 'Noncurrent Assets'
		WHEN  trim(Particulars) LIKE 'currLia%' THEN 'Current Liabilities'
		WHEN  trim(Particulars) LIKE 'noncurrLia%' THEN 'Noncurrent Liabilities'
		WHEN  trim(Particulars) LIKE 'fund%' THEN 'Fund Balance'
	ELSE '' END AS accntType,
	Amount,
	@month [month],
	@year [year]
FROM
(
	SELECT 
		Particulars,
		Amount 
	FROM
	(
		SELECT 
		--TOTAL ASSETS
			SUm(CASE WHEN trim(accntType) = 'Assets' THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) in ('111','') AND trim(accntTitle) LIKE 'Advances%' AND trim(accntType) = 'Assets' THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) = '106' THEN consolidated ELSE 0 END) as 'TotalAssets',
		--TOTAL LIABILITIES
			SUm(CASE WHEN trim(accntType) = 'Liabilities' THEN consolidated ELSE 0 END) * -1
			+ SUM(CASE WHEN trim(accntNo) in ('211','') AND trim(accntTitle) LIKE 'Advances%' AND trim(accntType) = 'Liabilities' THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) = '106' THEN consolidated ELSE 0 END) as 'TotalLiabilities',
		--TOTAL FUND BALANCE
			SUm(CASE WHEN trim(accntType) = 'Fund Balance' THEN consolidated ELSE 0 END) * -1 as 'TotalFundBalance',
		--TOTAL INCOME
			SUm(CASE WHEN trim(accntType) = 'Income' THEN consolidated ELSE 0 END) * -1 as 'TotalIncome',
		--TOTAL EXPENSES
			SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END) as 'TotalExpenses',
		--TOTAL NET INCOME (NET LOSS)
			SUm(CASE WHEN trim(accntType) = 'Income' THEN consolidated ELSE 0 END) * -1
			- SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END)  as 'TotalNetIncome',
		--CURRENT ASSETS
			SUM(CASE WHEN trim(accntNo) in ('101','102','103') THEN consolidated ELSE 0 END) as 'currAssetsCash',
			SUM(CASE WHEN trim(accntNo) in ('104','105') THEN consolidated ELSE 0 END) as 'currAssetsLoanReceivable',
			SUM(CASE WHEN trim(accntNo) in ('108','109') THEN consolidated ELSE 0 END) as 'currAssetsOtherReceivable',
			SUM(CASE WHEN trim(accntNo) in ('107','110','112') THEN consolidated ELSE 0 END) as 'currAssetsOther',
		--NONCURRENT ASSETS
			SUM(CASE WHEN trim(accntNo) in ('113') THEN consolidated ELSE 0 END) as 'noncurrAssetsFinancial',
			SUM(CASE WHEN trim(accntNo) in ('127') THEN consolidated ELSE 0 END) as 'noncurrAssetsRestrictedCash',
			SUM(CASE WHEN trim(accntNo) in ('114') THEN consolidated ELSE 0 END)
			+ SUM(CASE WHEN trim(accntNo) = '124' AND trim(accntTitle) Like '%ROU' THEN consolidated ELSE 0 END) as 'noncurrAssetsROUAsset',
			SUM(CASE WHEN trim(accntNo) in ('115','116','117','118','119','120','121','122','123') THEN consolidated ELSE 0 END)
			+ SUM(CASE WHEN trim(accntNo) = '' AND trim(accntTitle) LIKE 'Accumulated%' THEN consolidated ELSE 0 END)
			+ SUM(CASE WHEN trim(accntNo) = '124' AND trim(accntTitle) NOT LIKE '%ROU' THEN consolidated ELSE 0 END) as 'noncurrAssetsProperty',
			SUM(CASE WHEN trim(accntNo) in ('125','126','128') THEN consolidated ELSE 0 END) as 'noncurrAssetsOther',
		--LIABILITIES AND FUND BALANCE
		--CURRENT LIABILITIES
			SUM(CASE WHEN trim(accntNo) in ('201') THEN consolidated * -1 ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) in ('207') THEN consolidated  ELSE 0 END) as 'currLiaTrade',
			SUM(CASE WHEN trim(accntNo) in ('206','106') THEN consolidated ELSE 0 END) * -1 as 'currLiaUnearned',
			SUM(CASE WHEN trim(accntNo) in ('209') THEN consolidated ELSE 0 END) * -1 as 'currLiaClientCBU',
			SUM(CASE WHEN trim(accntNo) in ('202') THEN consolidated ELSE 0 END) * -1 as 'currLiaTaxPayable',
			SUM(CASE WHEN trim(accntNo) in ('205') THEN consolidated ELSE 0 END * -1)
			- SUM(CASE WHEN trim(accntNo) in ('203') THEN consolidated ELSE 0 END) 
			- SUM(CASE WHEN trim(accntNo) in ('204') THEN consolidated ELSE 0 END) as 'currLiaCGLIPayable',
			SUM(CASE WHEN trim(accntNo) in ('212') THEN consolidated ELSE 0 END) * -1 as 'currLiaProvisional',
		--NONCURRENT LIABILITIES
			SUM(CASE WHEN trim(accntNo) in ('208') THEN consolidated ELSE 0 END) * -1 as 'noncurrLiaLease',
			SUM(CASE WHEN trim(accntNo) in ('210') THEN consolidated ELSE 0 END) * -1 as 'noncurrLiaRetirement',
			SUM(CASE WHEN trim(accntNo) in ('213') THEN consolidated ELSE 0 END) * -1 as 'noncurrLiaDeferred',
		--FUND BALANCE
			SUM(CASE WHEN trim(accntNo) in ('301') THEN consolidated ELSE 0 END) * -1
			+ (SUm(CASE WHEN trim(accntType) = 'Income' THEN consolidated ELSE 0 END) * -1
			- SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END)) as 'fundRetained',
			SUM(CASE WHEN trim(accntNo) in ('303') THEN consolidated ELSE 0 END) * -1 as 'fundCommulative',
			SUM(CASE WHEN trim(accntNo) in ('302') THEN consolidated ELSE 0 END) * -1 as 'fundFairValue'
		FROM dba
		WHERE [month] = @month 
		and [year] = @year
	) AS SOURCE
unpivot
(
  Amount
  for Particulars in (
	TotalAssets, 
	TotalLiabilities, 
	TotalFundBalance,
	TotalIncome,
	TotalExpenses,
	TotalNetIncome,
	currAssetsCash,
	currAssetsLoanReceivable,
	currAssetsOtherReceivable,
	currAssetsOther,
	noncurrAssetsFinancial,
	noncurrAssetsRestrictedCash,
	noncurrAssetsROUAsset,
	noncurrAssetsProperty,
	noncurrAssetsOther,
	currLiaTrade,
	currLiaUnearned,
	currLiaClientCBU,
	currLiaTaxPayable,
	currLiaCGLIPayable,
	currLiaProvisional,
	noncurrLiaLease,
	noncurrLiaRetirement,
	noncurrLiaDeferred,
	fundRetained,
	fundCommulative,
	fundFairValue
	)
) unpiv) AS A) AS currYear

INNER JOIN


(SELECT 
	Particulars,
	CASE WHEN trim(Particulars) LIKE 'currAssets%' THEN 'Current Assets'
		WHEN  trim(Particulars) LIKE 'noncurrAssets%' THEN 'Noncurrent Assets'
		WHEN  trim(Particulars) LIKE 'currLia%' THEN 'Current Liabilities'
		WHEN  trim(Particulars) LIKE 'noncurrLia%' THEN 'Noncurrent Liabilities'
		WHEN  trim(Particulars) LIKE 'fund%' THEN 'Fund Balance'
	ELSE '' END AS accntType,
	Amount,
	@month [month],
	@previousyear [year]
FROM
(
SELECT 
	Particulars,
	Amount 
FROM
(
	SELECT 
	--TOTAL ASSETS
		SUm(CASE WHEN trim(accntType) = 'Assets' THEN consolidated ELSE 0 END)
		- SUM(CASE WHEN trim(accntNo) in ('111','') AND trim(accntTitle) LIKE 'Advances%' AND trim(accntType) = 'Assets' THEN consolidated ELSE 0 END)
		- SUM(CASE WHEN trim(accntNo) = '106' THEN consolidated ELSE 0 END) as 'TotalAssets',
	--TOTAL LIABILITIES
		SUm(CASE WHEN trim(accntType) = 'Liabilities' THEN consolidated ELSE 0 END) * -1
		+ SUM(CASE WHEN trim(accntNo) in ('211','') AND trim(accntTitle) LIKE 'Advances%' AND trim(accntType) = 'Liabilities' THEN consolidated ELSE 0 END)
		- SUM(CASE WHEN trim(accntNo) = '106' THEN consolidated ELSE 0 END) as 'TotalLiabilities',
	--TOTAL FUND BALANCE
		SUm(CASE WHEN trim(accntType) = 'Fund Balance' THEN consolidated ELSE 0 END) * -1 as 'TotalFundBalance',
	--TOTAL INCOME
		SUm(CASE WHEN trim(accntType) = 'Income' THEN consolidated ELSE 0 END) * -1 as 'TotalIncome',
	--TOTAL EXPENSES
		SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END) as 'TotalExpenses',
	--TOTAL NET INCOME (NET LOSS)
		SUm(CASE WHEN trim(accntType) = 'Income' THEN consolidated ELSE 0 END) * -1
		- SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END)  as 'TotalNetIncome',
	--CURRENT ASSETS
		SUM(CASE WHEN trim(accntNo) in ('101','102','103') THEN consolidated ELSE 0 END) as 'currAssetsCash',
		SUM(CASE WHEN trim(accntNo) in ('104','105') THEN consolidated ELSE 0 END) as 'currAssetsLoanReceivable',
		SUM(CASE WHEN trim(accntNo) in ('108','109') THEN consolidated ELSE 0 END) as 'currAssetsOtherReceivable',
		SUM(CASE WHEN trim(accntNo) in ('107','110','112') THEN consolidated ELSE 0 END) as 'currAssetsOther',
	--NONCURRENT ASSETS
		SUM(CASE WHEN trim(accntNo) in ('113') THEN consolidated ELSE 0 END) as 'noncurrAssetsFinancial',
		SUM(CASE WHEN trim(accntNo) in ('127') THEN consolidated ELSE 0 END) as 'noncurrAssetsRestrictedCash',
		SUM(CASE WHEN trim(accntNo) in ('114') THEN consolidated ELSE 0 END)
		+ SUM(CASE WHEN trim(accntNo) = '124' AND trim(accntTitle) Like '%ROU' THEN consolidated ELSE 0 END) as 'noncurrAssetsROUAsset',
		SUM(CASE WHEN trim(accntNo) in ('115','116','117','118','119','120','121','122','123') THEN consolidated ELSE 0 END)
		+ SUM(CASE WHEN trim(accntNo) = '' AND trim(accntTitle) LIKE 'Accumulated%' THEN consolidated ELSE 0 END)
		+ SUM(CASE WHEN trim(accntNo) = '124' AND trim(accntTitle) NOT LIKE '%ROU' THEN consolidated ELSE 0 END) as 'noncurrAssetsProperty',
		SUM(CASE WHEN trim(accntNo) in ('125','126','128') THEN consolidated ELSE 0 END) as 'noncurrAssetsOther',
	--LIABILITIES AND FUND BALANCE
	--CURRENT LIABILITIES
		SUM(CASE WHEN trim(accntNo) in ('201') THEN consolidated * -1 ELSE 0 END)
		- SUM(CASE WHEN trim(accntNo) in ('207') THEN consolidated  ELSE 0 END) as 'currLiaTrade',
		SUM(CASE WHEN trim(accntNo) in ('206','106') THEN consolidated ELSE 0 END) * -1 as 'currLiaUnearned',
		SUM(CASE WHEN trim(accntNo) in ('209') THEN consolidated ELSE 0 END) * -1 as 'currLiaClientCBU',
		SUM(CASE WHEN trim(accntNo) in ('202') THEN consolidated ELSE 0 END) * -1 as 'currLiaTaxPayable',
		SUM(CASE WHEN trim(accntNo) in ('205') THEN consolidated ELSE 0 END * -1)
		- SUM(CASE WHEN trim(accntNo) in ('203') THEN consolidated ELSE 0 END) 
		- SUM(CASE WHEN trim(accntNo) in ('204') THEN consolidated ELSE 0 END) as 'currLiaCGLIPayable',
		SUM(CASE WHEN trim(accntNo) in ('212') THEN consolidated ELSE 0 END) * -1 as 'currLiaProvisional',
	--NONCURRENT LIABILITIES
		SUM(CASE WHEN trim(accntNo) in ('208') THEN consolidated ELSE 0 END) * -1 as 'noncurrLiaLease',
		SUM(CASE WHEN trim(accntNo) in ('210') THEN consolidated ELSE 0 END) * -1 as 'noncurrLiaRetirement',
		SUM(CASE WHEN trim(accntNo) in ('213') THEN consolidated ELSE 0 END) * -1 as 'noncurrLiaDeferred',
	--FUND BALANCE
		SUM(CASE WHEN trim(accntNo) in ('301') THEN consolidated ELSE 0 END) * -1
		+ (SUm(CASE WHEN trim(accntType) = 'Income' THEN consolidated ELSE 0 END) * -1
		- SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END)) as 'fundRetained',
		SUM(CASE WHEN trim(accntNo) in ('303') THEN consolidated ELSE 0 END) * -1 as 'fundCommulative',
		SUM(CASE WHEN trim(accntNo) in ('302') THEN consolidated ELSE 0 END) * -1 as 'fundFairValue'
	FROM dba
	WHERE [month] = @month 
	and [year] = @previousyear
) AS SOURCE
unpivot
(
  Amount
  for Particulars in (
	TotalAssets, 
	TotalLiabilities, 
	TotalFundBalance,
	TotalIncome,
	TotalExpenses,
	TotalNetIncome,
	currAssetsCash,
	currAssetsLoanReceivable,
	currAssetsOtherReceivable,
	currAssetsOther,
	noncurrAssetsFinancial,
	noncurrAssetsRestrictedCash,
	noncurrAssetsROUAsset,
	noncurrAssetsProperty,
	noncurrAssetsOther,
	currLiaTrade,
	currLiaUnearned,
	currLiaClientCBU,
	currLiaTaxPayable,
	currLiaCGLIPayable,
	currLiaProvisional,
	noncurrLiaLease,
	noncurrLiaRetirement,
	noncurrLiaDeferred,
	fundRetained,
	fundCommulative,
	fundFairValue
	)
) unpiv) AS A) as prevYear

ON currYear.Particulars = prevYear.Particulars
AND currYear.month = prevYear.month