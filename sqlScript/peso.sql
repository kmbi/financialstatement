DECLARE @month nvarchar(50)
DECLARE @year int

SET @month = 'June'
SET @year = 2021


SELECT (a.principalBalanceOfLoan / b.LoanReceivableConso) * 100 as PARRatio,
--------------------------------------------------------------------------
------------COOMPUTATION OF LOAN LOSS RESERVE RATIO-----------------------
--------------------------------------------------------------------------
--a.accountWithoutPastDue + b.LoanReceivableHo As currentPrincipal,
--a.oneTOthirtyDays as PAR130, a.thiryOneTOsixtyDays as PAR3160, a.sixtyOneTOninetyDays AS PAR6190, a.nonrOneTOone80Days AS PAR9180, a.over91 AS PARI80,
--(a.accountWithoutPastDue + b.LoanReceivableHo) * 0.01  +
--a.oneTOthirtyDays * 0.02 +
--a.thiryOneTOsixtyDays * 0.2 +
--a.sixtyOneTOninetyDays * 0.5 +
--(a.nonrOneTOone80Days + a.over91) * 1 as totalRequired,
((b.AllowanceForProbable * -1)  / 
((a.accountWithoutPastDue + b.LoanReceivableHo) * 0.01  +
a.oneTOthirtyDays * 0.02 +
a.thiryOneTOsixtyDays * 0.2 +
a.sixtyOneTOninetyDays * 0.5 +
(a.nonrOneTOone80Days + a.over91))) * 100 as LoanLossReserveRatio,
--------------------------------------------------------------------------
------------COOMPUTATION OF OPERATING EXPENSE RATIO-----------------------
--------------------------------------------------------------------------
--b.TotalExpenses-b.TotalExpensesHo-b.ProvisionForLoanLosses-b.InterestExpenseonCBU-b.InternalExpense as operatingCost,
--b.GrossLoanOutsTanding as GrossBegin, b.LoanReceivableConso as GrossEnd,(b.GrossLoanOutsTanding + b.LoanReceivableConso)/2 as average,

(((b.TotalExpenses-b.TotalExpensesHo-b.ProvisionForLoanLosses-b.InterestExpenseonCBU-b.InternalExpense) * 1) / ((b.GrossLoanOutsTanding + b.LoanReceivableConso)/2)) * 100  as OperatingExpenseRatio
FROM
	(select distinct
	SUM(CASE WHEN TRIM(a.particulars) = 'Total Delinquent and Default Accounts' then a.forTheMonth else 0 end) as 'principalBalanceOfLoan',
	SUM(CASE WHEN TRIM(a.particulars) = 'Accounts Without Past Due' then a.forTheMonth else 0 end) as 'accountWithoutPastDue',
	SUM(CASE WHEN TRIM(a.particulars) = '1-30 days' then a.forTheMonth else 0 end) as 'oneTOthirtyDays',
	SUM(CASE WHEN TRIM(a.particulars) = '31-60 days' then a.forTheMonth else 0 end) as 'thiryOneTOsixtyDays',
	SUM(CASE WHEN TRIM(a.particulars) = '61-90 days' then a.forTheMonth else 0 end) as 'sixtyOneTOninetyDays',
	SUM(CASE WHEN TRIM(a.particulars) = '91-180 days' then a.forTheMonth else 0 end) as 'nonrOneTOone80Days',
	SUM(CASE WHEN TRIM(a.particulars) = 'Over 180 Days Past-Due' then a.forTheMonth else 0 end) as 'over91',
	--SUM(CASE WHEN TRIM(a.particulars) = 'Over 180 Days Past-Due' then a.forTheMonth else 0 end) as 'over91',
	[month],[year]
	 from dou a 
	 where a.month = @month and a.year = @year
	 group by [month],[year]
	 ) AS A
	 INNER JOIN
	 (
	 select distinct
		SUM(case when b.accntNo = '104' then b.consolidated else 0 end) as 'LoanReceivableConso',
		SUM(case when b.accntNo = '104' then b.headOffice else 0 end) as 'LoanReceivableHo',
		SUM(case when b.accntNo = '105' then b.consolidated else 0 end) as 'AllowanceForProbable',
		--TOTAL EXPENSES
				SUm(CASE WHEN trim(accntType) = 'Expenses' THEN consolidated ELSE 0 END) as 'TotalExpenses',
				SUm(CASE WHEN trim(accntType) = 'Expenses' THEN headOffice ELSE 0 END) as 'TotalExpensesHo',
				SUM(case when b.accntNo = '541' then b.consolidated else 0 end) as 'ProvisionForLoanLosses',
				SUM(case when b.accntNo = '538' then b.consolidated else 0 end) as 'InterestExpenseonCBU',
				SUM(case when b.accntNo = '539' then b.consolidated else 0 end) as 'InternalExpense',
				( SELECT 
					SUM(case when accntNo = '104' then consolidated else 0 end) as 'LoanReceivableConso'
					FROM dba
				WHERE MONTH = 'December' and year = @year - 1) as 'GrossLoanOutsTanding',
		[month],[year]
	 from dba as b
	 where b.month = @month and b.year = @year
	  group by [month],[year]
	  ) AS B
	  ON A.MONTH = B.MONTH AND A.YEAR = B.YEAR

	  --INNER JOIN
	  --(
		 -- SELECT *
		 --  FROM dba
		 -- WHERE MONTH = 'December' and year = @year - 1
	  --) as C
	  --on a.month = c.month and a.year = c.year


 select * from DOU where month = @month