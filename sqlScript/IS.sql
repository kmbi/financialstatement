
DECLARE @month nvarchar(50)
DECLARE @year int
DECLARE @previousyear int

SET @month = 'April'
SET @year = 2021
SET @previousyear = @year - 1

SELECT currYear.Particulars, 
	currYear.accntType, 
	currYear.Amount as currAmount, 
	prevYear.Amount as prevAmount, 
	currYear.Amount - prevyear.Amount as diff,
	CAST(Round(((currYear.Amount - prevyear.Amount) / prevYear.Amount) * 100,2) AS decimal(18,2)) as per,
	currYear.month, 
	currYear.year as currYear, 
	prevYear.year as prevYear 
FROM
(
SELECT 
	Particulars,
	CASE WHEN trim(Particulars) LIKE 'rev%' THEN 'Revenue'
		WHEN  trim(Particulars) LIKE 'exp%' THEN 'Expenses'
		WHEN  trim(Particulars) LIKE 'income%' THEN 'Income Tax'
	ELSE '' END AS accntType,
	Amount,
	@month [month],
	@year [year]
FROM
(
	SELECT 
		Particulars,
		Amount 
	FROM
	(
		SELECT 
		
		--REVENUE
			SUM(CASE WHEN trim(accntNo) in ('401') THEN consolidated ELSE 0 END) * -1 as 'revIntLoans',
			SUM(CASE WHEN trim(accntNo) in ('402') THEN consolidated ELSE 0 END) * - 1
			- SUM(CASE WHEN trim(accntNo) in ('403') THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) in ('404') THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) in ('405') THEN consolidated ELSE 0 END) as 'revServiceIncome',
			SUM(CASE WHEN trim(accntNo) in ('406') THEN consolidated ELSE 0 END) * -1 as 'revIntDeposits',
			SUM(CASE WHEN trim(accntNo) in ('407') THEN consolidated ELSE 0 END) * -1 as 'revForex',
			SUM(CASE WHEN trim(accntNo) in ('408') THEN consolidated ELSE 0 END) * -1 as 'revOther',
			SUM(CASE WHEN trim(accntNo) in ('539') THEN consolidated ELSE 0 END) as 'revIntExpense',
			SUM(CASE WHEN trim(accntNo) in ('538') THEN consolidated ELSE 0 END) as 'revIntExpenseCBU',
			SUM(CASE WHEN trim(accntNo) in ('541') THEN consolidated ELSE 0 END) as 'revProvision',
		--EXPENSES
			SUM(CASE WHEN trim(accntNo) in ('501','502','503','508','509','510') THEN consolidated ELSE 0 END) as 'expPersonnel',
			SUM(CASE WHEN trim(accntNo) in ('504') THEN consolidated ELSE 0 END) as 'expOutstation',
			SUM(CASE WHEN trim(accntNo) in ('505') THEN consolidated ELSE 0 END) as 'expFringe',
			SUM(CASE WHEN trim(accntNo) in ('511') THEN consolidated ELSE 0 END) as 'expRetirement',
			SUM(CASE WHEN trim(accntNo) in ('513') THEN consolidated ELSE 0 END) as 'expOutside',
			SUM(CASE WHEN trim(accntNo) in ('514') THEN consolidated ELSE 0 END) as 'expProfFees',
			SUM(CASE WHEN trim(accntNo) in ('516','515') THEN consolidated ELSE 0 END) as 'expMeetings',
			SUM(CASE WHEN trim(accntNo) in ('517') THEN consolidated ELSE 0 END) as 'expClient',
			SUM(CASE WHEN trim(accntNo) in ('519') THEN consolidated ELSE 0 END) as 'expLitigation',
			SUM(CASE WHEN trim(accntNo) in ('521','520') THEN consolidated ELSE 0 END) as 'expSupplies',
			SUM(CASE WHEN trim(accntNo) in ('524') THEN consolidated ELSE 0 END) as 'expUtilities',
			SUM(CASE WHEN trim(accntNo) in ('526','525') THEN consolidated ELSE 0 END) as 'expCommunication',
			SUM(CASE WHEN trim(accntNo) in ('529') THEN consolidated ELSE 0 END) as 'expRental',
			SUM(CASE WHEN trim(accntNo) in ('530') THEN consolidated ELSE 0 END) as 'expInsurance',
			SUM(CASE WHEN trim(accntNo) in ('532','531') THEN consolidated ELSE 0 END) as 'expTranspo',
			SUM(CASE WHEN trim(accntNo) in ('534','506') THEN consolidated ELSE 0 END) as 'expTaxes',
			SUM(CASE WHEN trim(accntNo) in ('537') THEN consolidated ELSE 0 END) as 'expRepairs',
			SUM(CASE WHEN trim(accntNo) in ('542','543') THEN consolidated ELSE 0 END)
			+ SUM(CASE WHEN trim(accntNo) = '' AND trim(accntTitle) LIKE 'Depreciation Expense%' THEN consolidated ELSE 0 END) as 'expDepreciation',
			SUM(CASE WHEN trim(accntNo) in ('544') THEN consolidated ELSE 0 END) as 'expProvision',
			SUM(CASE WHEN trim(accntNo) in ('507','512','522','523','528','533','535','536','540','547','527','518') THEN consolidated ELSE 0 END) as 'expOthers',
		--INCOME TAX
			SUM(CASE WHEN trim(accntNo) in ('545','546') THEN consolidated ELSE 0 END) as 'incomeTaxExpense'
		FROM dba
		WHERE [month] = @month 
		and [year] = @year) AS SOURCE
unpivot
(
  Amount
  for Particulars in (
	revIntLoans, revServiceIncome, revIntDeposits, revForex, revOther, revIntExpense, revIntExpenseCBU, revProvision,
	expPersonnel, expOutstation, expFringe, expRetirement, expOutside, expProfFees, expMeetings, expClient, expLitigation,
	expSupplies, expUtilities, expCommunication, expRental, expInsurance, expTranspo, expTaxes, expRepairs,
	expDepreciation, expProvision, expOthers, incomeTaxExpense
	)
) unpiv) AS A) AS currYear

INNER JOIN

(SELECT 
	Particulars,
	CASE WHEN trim(Particulars) LIKE 'rev%' THEN 'Revenue'
		WHEN  trim(Particulars) LIKE 'exp%' THEN 'Expenses'
		WHEN  trim(Particulars) LIKE 'income%' THEN 'Income Tax'
	ELSE '' END AS accntType,
	Amount,
	@month [month],
	@previousyear [year]
FROM
(
	SELECT 
		Particulars,
		Amount 
	FROM
	(
		SELECT 
		
		--REVENUE
			SUM(CASE WHEN trim(accntNo) in ('401') THEN consolidated ELSE 0 END) * -1 as 'revIntLoans',
			SUM(CASE WHEN trim(accntNo) in ('402') THEN consolidated ELSE 0 END) * - 1
			- SUM(CASE WHEN trim(accntNo) in ('403') THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) in ('404') THEN consolidated ELSE 0 END)
			- SUM(CASE WHEN trim(accntNo) in ('405') THEN consolidated ELSE 0 END) as 'revServiceIncome',
			SUM(CASE WHEN trim(accntNo) in ('406') THEN consolidated ELSE 0 END) * -1 as 'revIntDeposits',
			SUM(CASE WHEN trim(accntNo) in ('407') THEN consolidated ELSE 0 END) * -1 as 'revForex',
			SUM(CASE WHEN trim(accntNo) in ('408') THEN consolidated ELSE 0 END) * -1 as 'revOther',
			SUM(CASE WHEN trim(accntNo) in ('539') THEN consolidated ELSE 0 END) as 'revIntExpense',
			SUM(CASE WHEN trim(accntNo) in ('538') THEN consolidated ELSE 0 END) as 'revIntExpenseCBU',
			SUM(CASE WHEN trim(accntNo) in ('541') THEN consolidated ELSE 0 END) as 'revProvision',
		--EXPENSES
			SUM(CASE WHEN trim(accntNo) in ('501','502','503','508','509','510') THEN consolidated ELSE 0 END) as 'expPersonnel',
			SUM(CASE WHEN trim(accntNo) in ('504') THEN consolidated ELSE 0 END) as 'expOutstation',
			SUM(CASE WHEN trim(accntNo) in ('505') THEN consolidated ELSE 0 END) as 'expFringe',
			SUM(CASE WHEN trim(accntNo) in ('511') THEN consolidated ELSE 0 END) as 'expRetirement',
			SUM(CASE WHEN trim(accntNo) in ('513') THEN consolidated ELSE 0 END) as 'expOutside',
			SUM(CASE WHEN trim(accntNo) in ('514') THEN consolidated ELSE 0 END) as 'expProfFees',
			SUM(CASE WHEN trim(accntNo) in ('516','515') THEN consolidated ELSE 0 END) as 'expMeetings',
			SUM(CASE WHEN trim(accntNo) in ('517') THEN consolidated ELSE 0 END) as 'expClient',
			SUM(CASE WHEN trim(accntNo) in ('519') THEN consolidated ELSE 0 END) as 'expLitigation',
			SUM(CASE WHEN trim(accntNo) in ('521','520') THEN consolidated ELSE 0 END) as 'expSupplies',
			SUM(CASE WHEN trim(accntNo) in ('524') THEN consolidated ELSE 0 END) as 'expUtilities',
			SUM(CASE WHEN trim(accntNo) in ('526','525') THEN consolidated ELSE 0 END) as 'expCommunication',
			SUM(CASE WHEN trim(accntNo) in ('529') THEN consolidated ELSE 0 END) as 'expRental',
			SUM(CASE WHEN trim(accntNo) in ('530') THEN consolidated ELSE 0 END) as 'expInsurance',
			SUM(CASE WHEN trim(accntNo) in ('532','531') THEN consolidated ELSE 0 END) as 'expTranspo',
			SUM(CASE WHEN trim(accntNo) in ('534','506') THEN consolidated ELSE 0 END) as 'expTaxes',
			SUM(CASE WHEN trim(accntNo) in ('537') THEN consolidated ELSE 0 END) as 'expRepairs',
			SUM(CASE WHEN trim(accntNo) in ('542','543') THEN consolidated ELSE 0 END)
			+ SUM(CASE WHEN trim(accntNo) = '' AND trim(accntTitle) LIKE 'Depreciation Expense%' THEN consolidated ELSE 0 END) as 'expDepreciation',
			SUM(CASE WHEN trim(accntNo) in ('544') THEN consolidated ELSE 0 END) as 'expProvision',
			SUM(CASE WHEN trim(accntNo) in ('507','512','522','523','528','533','535','536','540','547','527','518') THEN consolidated ELSE 0 END) as 'expOthers',
		--INCOME TAX
			SUM(CASE WHEN trim(accntNo) in ('545','546') THEN consolidated ELSE 0 END) as 'incomeTaxExpense'
		FROM dba
		WHERE [month] = @month 
		and [year] = @previousyear) AS SOURCE
unpivot
(
  Amount
  for Particulars in (
	revIntLoans, revServiceIncome, revIntDeposits, revForex, revOther, revIntExpense, revIntExpenseCBU, revProvision,
	expPersonnel, expOutstation, expFringe, expRetirement, expOutside, expProfFees, expMeetings, expClient, expLitigation,
	expSupplies, expUtilities, expCommunication, expRental, expInsurance, expTranspo, expTaxes, expRepairs,
	expDepreciation, expProvision, expOthers, incomeTaxExpense
	)
) unpiv) AS A) AS prevYear
ON currYear.Particulars = prevYear.Particulars
AND currYear.month = prevYear.month