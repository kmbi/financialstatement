﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using fs.Models;
using fs.Models.IRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace fs.Controllers
{
    [Authorize]
    public class UploadController : Controller
    {
        string conString = SQLConString.consTring();

        private IHostingEnvironment hostingEnv;
        private iUserManager _userManager;
        string empCode = "";
        public UploadController(IHostingEnvironment env, iUserManager userManager)
        {
            hostingEnv = env;
            _userManager = userManager;
            
        }

        public IActionResult Index()
        {
            return View();
        }

        //DBA
        public IActionResult Dba(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0) {
                    DataRow row = dt.Rows[0];
                    var rMonth = row[9];
                    var rYear = row[10];
                    con.Open();
                    string sqlD = "DELETE FROM dba WHERE month = '" + rMonth + "' and year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }
                

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col3;
                    if (columns[3].Trim() == "") { col3 = 0; } else { col3 = Convert.ToDecimal(columns[3]); }
                    decimal col4;
                    if (columns[4].Trim() == "") { col4 = 0; } else { col4 = Convert.ToDecimal(columns[4]); }
                    decimal col5;
                    if (columns[5].Trim() == "") { col5 = 0; } else { col5 = Convert.ToDecimal(columns[5]); }
                    decimal col6;
                    if (columns[6].Trim() == "") { col6 = 0; } else { col6 = Convert.ToDecimal(columns[6]); }
                    decimal col7;
                    if (columns[7].Trim() == "") { col7 = 0; } else { col7 = Convert.ToDecimal(columns[7]); }
                    decimal col8;
                    if (columns[8].Trim() == "") { col8 = 0; } else { col8 = Convert.ToDecimal(columns[8]); }

                    string sql = "INSERT INTO dba(accntNo,accntTitle,accntType,consolidated,headOffice,northLuzon,southLuzon,visayas,mindanao,month,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0] + "','" +
                        columns[1].Replace("'","") + "','" +
                        columns[2].Replace("'", "") + "','" +
                        col3 + "','" +
                        col4 + "','" +
                        col5 + "','" +
                        col6 + "','" +
                        col7 + "','" +
                        col8 + "','" +
                        columns[9] + "','" +
                        columns[10] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetDba()
        {
            List<dba> viewModel = new List<dba>();
            //string conString = @"Server=DESKTOP-6T4065S;Database=payslip;User Id=sa;Password=P@ssw0rd";
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct month,  year, uploadedDate, MONTH([month] + ' 1 ' + cast(year as nvarchar)) as monthNumber from dba order by year desc, MONTH([month] + ' 1 ' + cast(year as nvarchar)) asc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dba
                        {
                            month = reader.GetString(0).Trim().ToUpper(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(2).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteDba(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM dba WHERE month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewDba(string month, string year)
        {
            List<dbaViewModel> viewModel = new List<dbaViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From dba where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dbaViewModel
                        {
                            id = reader.GetInt32(0),
                            accntNo = reader.GetString(1),
                            accntTitle = reader.GetString(2),
                            accntType = reader.GetString(3),
                            consolidated = reader.GetDecimal(4),
                            headOffice = reader.GetDecimal(5),
                            northLuzon = reader.GetDecimal(6),
                            southLuzon = reader.GetDecimal(7),
                            visayas = reader.GetDecimal(8),
                            mindanao = reader.GetDecimal(9),
                            month = reader.GetString(10),
                            year = reader.GetInt32(11).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }

        //DOU
        public IActionResult Dou(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rMonth = row[4];
                    var rYear = row[5];
                    con.Open();
                    string sqlD = "DELETE FROM dou WHERE month = '" + rMonth + "' and year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col1;
                    if (columns[1].Trim() == "") { col1 = 0; } else { col1 = Convert.ToDecimal(columns[1]); }
                    decimal col2;
                    if (columns[2].Trim() == "") { col2 = 0; } else { col2 = Convert.ToDecimal(columns[2]); }
                    decimal col3;
                    if (columns[3].Trim() == "") { col3 = 0; } else { col3 = Convert.ToDecimal(columns[3]); }
                    
                    string sql = "INSERT INTO dou(particulars,forTheDay,cumulative,forTheMonth,month,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        col1 + "','" +
                        col2 + "','" +
                        col3 + "','" +
                        columns[4] + "','" +
                        columns[5] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetDou()
        {
            List<dba> viewModel = new List<dba>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct month,  year, uploadedDate, MONTH([month] + ' 1 ' + cast(year as nvarchar)) as monthNumber from dou order by year desc, MONTH([month] + ' 1 ' + cast(year as nvarchar)) asc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dba
                        {
                            month = reader.GetString(0).Trim().ToUpper(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(2).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteDou(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM dou WHERE month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewDou(string month, string year)
        {
            List<douViewModel> viewModel = new List<douViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From dou where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new douViewModel
                        {
                            id = reader.GetInt32(0),
                            particulars = reader.GetString(1),
                            forTheDay = reader.GetDecimal(2),
                            cumulative = reader.GetDecimal(3),
                            forTheMonth = reader.GetDecimal(4),
                            month = reader.GetString(5),
                            year = reader.GetInt32(6).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }

    

        //BALANCE SHEET
        public IActionResult BalanceSheet(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rYear = row[15];
                    con.Open();
                    string sqlD = "DELETE FROM balanceSheet WHERE year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();
                    
                    decimal col3;
                    if (columns[3].Trim() == "") { col3 = 0; } else { col3 = Convert.ToDecimal(columns[3]); }
                    decimal col4;
                    if (columns[4].Trim() == "") { col4 = 0; } else { col4 = Convert.ToDecimal(columns[4]); }
                    decimal col5;
                    if (columns[5].Trim() == "") { col5 = 0; } else { col5 = Convert.ToDecimal(columns[5]); }
                    decimal col6;
                    if (columns[6].Trim() == "") { col6 = 0; } else { col6 = Convert.ToDecimal(columns[6]); }
                    decimal col7;
                    if (columns[7].Trim() == "") { col7 = 0; } else { col7 = Convert.ToDecimal(columns[7]); }
                    decimal col8;
                    if (columns[8].Trim() == "") { col8 = 0; } else { col8 = Convert.ToDecimal(columns[8]); }
                    decimal col9;
                    if (columns[9].Trim() == "") { col9 = 0; } else { col9 = Convert.ToDecimal(columns[9]); }
                    decimal col10;
                    if (columns[10].Trim() == "") { col10 = 0; } else { col10 = Convert.ToDecimal(columns[10]); }
                    decimal col11;
                    if (columns[11].Trim() == "") { col11 = 0; } else { col11 = Convert.ToDecimal(columns[11]); }
                    decimal col12;
                    if (columns[12].Trim() == "") { col12 = 0; } else { col12 = Convert.ToDecimal(columns[12]); }
                    decimal col13;
                    if (columns[13].Trim() == "") { col13 = 0; } else { col13 = Convert.ToDecimal(columns[13]); }
                    decimal col14;
                    if (columns[14].Trim() == "") { col14 = 0; } else { col14 = Convert.ToDecimal(columns[14]); }

                    string sql = "INSERT INTO balanceSheet(accntTitle,accntType,subAccntType,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        columns[1].Replace("'", "") + "','" +
                        columns[2].Replace("'", "") + "','" +
                        col3 + "','" +
                        col4 + "','" +
                        col5 + "','" +
                        col6 + "','" +
                        col7 + "','" +
                        col8 + "','" +
                        col9 + "','" +
                        col10 + "','" +
                        col11 + "','" +
                        col12 + "','" +
                        col13 + "','" +
                        col14 + "','" +
                        columns[15] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetBalanceSheet()
        {
            List<balanceSheet> viewModel = new List<balanceSheet>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct  year, uploadedDate  from balanceSheet order by year desc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new balanceSheet
                        {
                            year = reader.GetInt32(0).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(1).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteBalanceSheet(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM balanceSheet WHERE year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewBalanceSheet(string month, string year)
        {
            List<balanceSheetViewModel> viewModel = new List<balanceSheetViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From balanceSheet where year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new balanceSheetViewModel
                        {
                            id = reader.GetInt32(0),
                            accntTitle = reader.GetString(1),
                            accntType = reader.GetString(2),
                            subAccntType = reader.GetString(3),
                            m1 = reader.GetDecimal(4),
                            m2 = reader.GetDecimal(5),
                            m3 = reader.GetDecimal(6),
                            m4 = reader.GetDecimal(7),
                            m5 = reader.GetDecimal(8),
                            m6 = reader.GetDecimal(9),
                            m7 = reader.GetDecimal(10),
                            m8 = reader.GetDecimal(11),
                            m9 = reader.GetDecimal(12),
                            m10 = reader.GetDecimal(13),
                            m11 = reader.GetDecimal(14),
                            m12 = reader.GetDecimal(15),
                            year = reader.GetInt32(16).ToString()
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }



        //INCOME STATEMENT
        public IActionResult IncomeStatement(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rYear = row[13];
                    con.Open();
                    string sqlD = "DELETE FROM incomeStatement WHERE year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col1;
                    if (columns[1].Trim() == "") { col1 = 0; } else { col1 = Convert.ToDecimal(columns[1]); }
                    decimal col2;
                    if (columns[2].Trim() == "") { col2 = 0; } else { col2 = Convert.ToDecimal(columns[2]); }
                    decimal col3;
                    if (columns[3].Trim() == "") { col3 = 0; } else { col3 = Convert.ToDecimal(columns[3]); }
                    decimal col4;
                    if (columns[4].Trim() == "") { col4 = 0; } else { col4 = Convert.ToDecimal(columns[4]); }
                    decimal col5;
                    if (columns[5].Trim() == "") { col5 = 0; } else { col5 = Convert.ToDecimal(columns[5]); }
                    decimal col6;
                    if (columns[6].Trim() == "") { col6 = 0; } else { col6 = Convert.ToDecimal(columns[6]); }
                    decimal col7;
                    if (columns[7].Trim() == "") { col7 = 0; } else { col7 = Convert.ToDecimal(columns[7]); }
                    decimal col8;
                    if (columns[8].Trim() == "") { col8 = 0; } else { col8 = Convert.ToDecimal(columns[8]); }
                    decimal col9;
                    if (columns[9].Trim() == "") { col9 = 0; } else { col9 = Convert.ToDecimal(columns[9]); }
                    decimal col10;
                    if (columns[10].Trim() == "") { col10 = 0; } else { col10 = Convert.ToDecimal(columns[10]); }
                    decimal col11;
                    if (columns[11].Trim() == "") { col11 = 0; } else { col11 = Convert.ToDecimal(columns[11]); }
                    decimal col12;
                    if (columns[12].Trim() == "") { col12 = 0; } else { col12 = Convert.ToDecimal(columns[12]); }

                    string sql = "INSERT INTO incomeStatement(accntTitle,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        col1 + "','" +
                        col2 + "','" +
                        col3 + "','" +
                        col4 + "','" +
                        col5 + "','" +
                        col6 + "','" +
                        col7 + "','" +
                        col8 + "','" +
                        col9 + "','" +
                        col10 + "','" +
                        col11 + "','" +
                        col12 + "','" +
                        columns[13] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetIncomeStatement()
        {
            List<incomeStatement> viewModel = new List<incomeStatement>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct  year, uploadedDate  from incomeStatement order by year desc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new incomeStatement
                        {
                            year = reader.GetInt32(0).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(1).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteIncomeStatement(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM incomeStatement WHERE year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewIncomeStatement(string month, string year)
        {
            List<incomeStatementViewModel> viewModel = new List<incomeStatementViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From incomeStatement where year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new incomeStatementViewModel
                        {
                            id = reader.GetInt32(0),
                            accntTitle = reader.GetString(1),
                            m1 = reader.GetDecimal(2),
                            m2 = reader.GetDecimal(3),
                            m3 = reader.GetDecimal(4),
                            m4 = reader.GetDecimal(5),
                            m5 = reader.GetDecimal(6),
                            m6 = reader.GetDecimal(7),
                            m7 = reader.GetDecimal(8),
                            m8 = reader.GetDecimal(9),
                            m9 = reader.GetDecimal(10),
                            m10 = reader.GetDecimal(11),
                            m11 = reader.GetDecimal(12),
                            m12 = reader.GetDecimal(13),
                            year = reader.GetInt32(14).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }


        //CASH IN BANK
        public IActionResult Cib(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rMonth = row[3];
                    var rYear = row[4];
                    con.Open();
                    string sqlD = "DELETE FROM cib WHERE month = '" + rMonth + "' and year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col1;
                    if (columns[1].Trim() == "") { col1 = 0; } else { col1 = Convert.ToDecimal(columns[1]); }
                    decimal col2;
                    if (columns[2].Trim() == "") { col2 = 0; } else { col2 = Convert.ToDecimal(columns[2]); }
                 

                    string sql = "INSERT INTO cib(bank,ho,branches,month,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        col1 + "','" +
                        col2 + "','" +
                        columns[3] + "','" +
                        columns[4] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetCib()
        {
            List<dba> viewModel = new List<dba>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct month,  year, uploadedDate, MONTH([month] + ' 1 ' + cast(year as nvarchar)) as monthNumber from cib order by year desc, MONTH([month] + ' 1 ' + cast(year as nvarchar)) asc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dba
                        {
                            month = reader.GetString(0).Trim().ToUpper(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(2).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteCib(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM cib WHERE month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewCib(string month, string year)
        {
            List<cibViewModel> viewModel = new List<cibViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From cib where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new cibViewModel
                        {
                            id = reader.GetInt32(0),
                            bank = reader.GetString(1),
                            ho = reader.GetDecimal(2),
                            branches = reader.GetDecimal(3),
                            month = reader.GetString(4),
                            year = reader.GetInt32(5).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }


        //SHORT TERM
        public IActionResult ShortTerm(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rMonth = row[2];
                    var rYear = row[3];
                    con.Open();
                    string sqlD = "DELETE FROM shortTerm WHERE month = '" + rMonth + "' and year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col1;
                    if (columns[1].Trim() == "") { col1 = 0; } else { col1 = Convert.ToDecimal(columns[1]); }
                   

                    string sql = "INSERT INTO shortTerm(particulars,amount,month,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        col1 + "','" +
                        columns[2] + "','" +
                        columns[3] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetShortTerm()
        {
            List<dba> viewModel = new List<dba>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct month,  year, uploadedDate, MONTH([month] + ' 1 ' + cast(year as nvarchar)) as monthNumber from shortTerm order by year desc, MONTH([month] + ' 1 ' + cast(year as nvarchar)) asc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dba
                        {
                            month = reader.GetString(0).Trim().ToUpper(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(2).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteShortTerm(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM shortTerm WHERE month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewShortTerm(string month, string year)
        {
            List<shortTermViewModel> viewModel = new List<shortTermViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From shortTerm where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new shortTermViewModel
                        {
                            id = reader.GetInt32(0),
                            particulars = reader.GetString(1),
                            amount = reader.GetDecimal(2),
                            month = reader.GetString(3),
                            year = reader.GetInt32(4).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }


        //FINANCIAL ASSETS
        public IActionResult FinancialAssets(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rMonth = row[6];
                    var rYear = row[7];
                    con.Open();
                    string sqlD = "DELETE FROM financialAssets WHERE month = '" + rMonth + "' and year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col3;
                    if (columns[3].Trim() == "") { col3 = 0; } else { col3 = Convert.ToInt32(columns[3]); }
                    decimal col4;
                    if (columns[4].Trim() == "") { col4 = 0; } else { col4 = Convert.ToDecimal(columns[4]); }
                    decimal col5;
                    if (columns[5].Trim() == "") { col5 = 0; } else { col5 = Convert.ToDecimal(columns[5]); }


                    string sql = "INSERT INTO financialAssets(particulars,entity,dateIssued,noShares,closingRate,marketValue,month,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        columns[1] + "','" +
                        columns[2] + "','" +
                        col3 + "','" +
                        col4 + "','" +
                        col5 + "','" +
                         columns[6] + "','" +
                        columns[7] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetFinancialAssets()
        {
            List<dba> viewModel = new List<dba>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct month,  year, uploadedDate, MONTH([month] + ' 1 ' + cast(year as nvarchar)) as monthNumber from financialAssets order by year desc, MONTH([month] + ' 1 ' + cast(year as nvarchar)) asc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dba
                        {
                            month = reader.GetString(0).Trim().ToUpper(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(2).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteFinancialAssets(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM financialAssets WHERE month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewFinancialAssets(string month, string year)
        {
            List<financialAssetsViewModel> viewModel = new List<financialAssetsViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From financialAssets where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new financialAssetsViewModel
                        {
                            id = reader.GetInt32(0),
                            particulars = reader.GetString(1),
                            entity = reader.GetString(2),
                            dateIssued = reader.GetDateTime(3).ToString("MM/dd/yyyy"),
                            noShares = reader.GetInt32(4),
                            closingRate = reader.GetDecimal(5),
                            marketValue = reader.GetDecimal(6),
                            month = reader.GetString(7),
                            year = reader.GetInt32(8).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }


        //RESTRICTED CASH
        public IActionResult RestrictedCash(IFormFile file)
        {
            empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();
            var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var FileDic = "Files";
            string FilePath = Path.Combine(hostingEnv.WebRootPath, FileDic);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            var fileName = file.FileName;
            var filePath = Path.Combine(FilePath, fileName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
            }

            //get extension
            string extension = Path.GetExtension(filename);


            string exConString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    exConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    exConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            DataTable dt = new DataTable();
            exConString = string.Format(exConString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(exConString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            //your database connection string

            using (SqlConnection con = new SqlConnection(conString))
            {
                //DELETE RECORD
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    var rMonth = row[3];
                    var rYear = row[4];
                    con.Open();
                    string sqlD = "DELETE FROM restrictedCash WHERE month = '" + rMonth + "' and year = '" + rYear + "'";
                    SqlCommand cmdD = new SqlCommand(sqlD, con);
                    cmdD.ExecuteNonQuery();
                    con.Close();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    int columnCount = dt.Columns.Count;
                    string[] columns = new string[columnCount];
                    for (int j = 0; j < columnCount; j++)
                    {
                        columns[j] = row[j].ToString();
                    }
                    con.Open();

                    decimal col1;
                    if (columns[1].Trim() == "") { col1 = 0; } else { col1 = Convert.ToDecimal(columns[1]); }


                    string sql = "INSERT INTO restrictedCash(particulars,amount,description,month,year,uploadedBy,uploadedDate)";
                    sql += "VALUES('" + columns[0].Replace("'", "") + "','" +
                        col1 + "','" +
                        columns[2] + "','" +
                        columns[3] + "','" +
                        columns[4] + "','" +
                        empCode + "','" +
                        DateTime.Now + "')";
                    SqlCommand cmd = new SqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    System.IO.File.Delete(filePath);
                }

            }
            //if the code reach here means everthing goes fine and excel data is imported into database
            ViewBag.Message = "File Imported and excel data saved into database";
            //return View("Index");

            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult GetRestrictedCash()
        {
            List<dba> viewModel = new List<dba>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select Distinct month,  year, uploadedDate, MONTH([month] + ' 1 ' + cast(year as nvarchar)) as monthNumber from restrictedCash order by year desc, MONTH([month] + ' 1 ' + cast(year as nvarchar)) asc";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dba
                        {
                            month = reader.GetString(0).Trim().ToUpper(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            uploadedDate = reader.GetDateTime(2).ToString("MMM dd, yyyy")
                        });
                    }
                }

                con.Close();
            }
            return Json(new { data = viewModel });
            // return View(viewModel);
        }

        [HttpDelete]
        public void DeleteRestrictedCash(string month, string year)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM restrictedCash WHERE month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpGet]
        public IActionResult ViewRestrictedCash(string month, string year)
        {
            List<restrictedCashViewModel> viewModel = new List<restrictedCashViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * From restrictedCash where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new restrictedCashViewModel
                        {
                            id = reader.GetInt32(0),
                            particulars = reader.GetString(1),
                            amount = reader.GetDecimal(2),
                            description = reader.GetString(3),
                            month = reader.GetString(4),
                            year = reader.GetInt32(5).ToString()
                        });
                    }
                }

                con.Close();
            }
            //return Json(new { data = viewModel });
            return View(viewModel);
        }
    }
}