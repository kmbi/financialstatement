﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using fs.Models;
using fs.Models.IRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

namespace fs.Controllers
{
    [Authorize]
    public class FinancialStatementController : Controller
    {
        string conString = SQLConString.consTring();
        private iUserManager _userManager;
        string empCode = "";

        private NotesController _notesController;
        private readonly IHostingEnvironment _hostingEnvironment;

        public FinancialStatementController(iUserManager userManager, IHostingEnvironment hostingEnvironment, NotesController notesController)
        {
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
            _notesController = notesController;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public List<fsBalanceSheetViewModel> GetBalanceSheet(string month, int year)
        {
            List<fsBalanceSheetViewModel> viewModel = new List<fsBalanceSheetViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spBalanceSheet";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsBalanceSheetViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            currAmount = reader.GetDecimal(2),
                            prevAmount = reader.GetDecimal(3),
                            diff = reader.GetDecimal(4),
                            per = reader.GetDecimal(5),
                            month = reader.GetString(6).Trim(),
                            currYear = reader.GetInt32(7),
                            prevYear = reader.GetInt32(8),
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }


        

        [HttpGet]
        public IActionResult BSPDF(string month, int year)
        {
            var model = GetBalanceSheet(month, year);
            var prevYear = year - 1;

            //ASSETS
            //CURRENT ASSETS
            decimal currAssetsCashCurrentYear = 0;
            decimal currAssetsCashPreviousYear = 0;
            decimal currAssetsDiff = 0;
            decimal currAssetsPercent = 0;

            decimal currAssetsLoanReceivableCurrentYear=0;
            decimal currAssetsLoanReceivablePreviousYear=0;
            decimal currAssetsLoanReceivableDiff=0;
            decimal currAssetsLoanReceivablePercent=0;

            decimal currAssetsOtherReceivableCurrentYear = 0;
            decimal currAssetsOtherReceivablePreviousYear = 0;
            decimal currAssetsOtherReceivableDiff = 0;
            decimal currAssetsOtherReceivablePercent = 0;

            decimal currAssetsOtherCurrentYear = 0;
            decimal currAssetsOtherPreviousYear = 0;
            decimal currAssetsOtherDiff = 0;
            decimal currAssetsOtherPercent = 0;

            decimal totalCurrAssetsCurrYear = 0;
            decimal totalCurrAssetsPrevYear = 0;
            decimal totalCurrAssetsDiff = 0;
            decimal totalCurrAssetsPer = 0;

            //NONCURRENT ASSETS
            decimal noncurrAssetsFinancialCurrentYear = 0;
            decimal noncurrAssetsFinancialPreviousYear = 0;
            decimal noncurrAssetsFinancialDiff = 0;
            decimal noncurrAssetsFinancialPercent = 0;

            decimal noncurrAssetsRestrictedCashCurrentYear = 0;
            decimal noncurrAssetsRestrictedCashPreviousYear = 0;
            decimal noncurrAssetsRestrictedCashDiff = 0;
            decimal noncurrAssetsRestrictedCashPercent = 0;

            decimal noncurrAssetsROUAssetCurrentYear = 0;
            decimal noncurrAssetsROUAssetPreviousYear = 0;
            decimal noncurrAssetsROUAssetDiff = 0;
            decimal noncurrAssetsROUAssetPercent = 0;

            decimal noncurrAssetsPropertyCurrentYear = 0;
            decimal noncurrAssetsPropertyPreviousYear = 0;
            decimal noncurrAssetsPropertyDiff = 0;
            decimal noncurrAssetsPropertyPercent = 0;

            decimal noncurrAssetsOtherCurrentYear = 0;
            decimal noncurrAssetsOtherPreviousYear = 0;
            decimal noncurrAssetsOtherDiff = 0;
            decimal noncurrAssetsOtherPercent = 0;

            decimal totalNonCurrAssetsCurrYear = 0;
            decimal totalNonCurrAssetsPrevYear = 0;
            decimal totalNonCurrAssetsDiff = 0;
            decimal totalNonCurrAssetsPer = 0;

            decimal totalAssetsCurrYear = 0;
            decimal totalAssetsPrevYear = 0;
            decimal totalAssetsDiff = 0;
            decimal totalAssetsPer = 0;

            //LIABILITIES
            //CURRENT LIA
            decimal currLiaTradeCurrentYear = 0;
            decimal currLiaTradePreviousYear = 0;
            decimal currLiaTradeDiff = 0;
            decimal currLiaTradePercent = 0;

            decimal currLiaUnearnedCurrentYear = 0;
            decimal currLiaUnearnedPreviousYear = 0;
            decimal currLiaUnearnedDiff = 0;
            decimal currLiaUnearnedPercent = 0;

            decimal currLiaClientCBUCurrentYear = 0;
            decimal currLiaClientCBUPreviousYear = 0;
            decimal currLiaClientCBUDiff = 0;
            decimal currLiaClientCBUPercent = 0;

            decimal currLiaTaxPayableCurrentYear = 0;
            decimal currLiaTaxPayablePreviousYear = 0;
            decimal currLiaTaxPayableDiff = 0;
            decimal currLiaTaxPayablePercent = 0;

            decimal currLiaCGLIPayableCurrentYear = 0;
            decimal currLiaCGLIPayablePreviousYear = 0;
            decimal currLiaCGLIPayableDiff = 0;
            decimal currLiaCGLIPayablePercent = 0;

            decimal currLiaProvisionalCurrentYear = 0;
            decimal currLiaProvisionalPreviousYear = 0;
            decimal currLiaProvisionalDiff = 0;
            decimal currLiaProvisionalPercent = 0;

            decimal totalCurrLiaCurrYear = 0;
            decimal totalCurrLiaPrevYear = 0;
            decimal totalCurrLiaDiff = 0;
            decimal totalCurrLiaPer = 0;

            //NONCURRENT LIA
            decimal noncurrLiaLeaseCurrentYear = 0;
            decimal noncurrLiaLeasePreviousYear = 0;
            decimal noncurrLiaLeaseDiff = 0;
            decimal noncurrLiaLeasePercent = 0;

            decimal noncurrLiaRetirementCurrentYear = 0;
            decimal noncurrLiaRetirementPreviousYear = 0;
            decimal noncurrLiaRetirementDiff = 0;
            decimal noncurrLiaRetirementPercent = 0;

            decimal noncurrLiaDeferredCurrentYear = 0;
            decimal noncurrLiaDeferredPreviousYear = 0;
            decimal noncurrLiaDeferredDiff = 0;
            decimal noncurrLiaDeferredPercent = 0;

            decimal totalNonCurrLiaCurrYear = 0;
            decimal totalNonCurrLiaPrevYear = 0;
            decimal totalNonCurrLiaDiff = 0;
            decimal totalNonCurrLiaPer = 0;

            decimal totalLiaCurrYear = 0;
            decimal totalLiaPrevYear = 0;
            decimal totalLiaDiff = 0;
            decimal totalLiaPer = 0;

            //FUND BALANCE
            decimal fundRetainedCurrentYear = 0;
            decimal fundRetainedPreviousYear = 0;
            decimal fundRetainedDiff = 0;
            decimal fundRetainedPercent = 0;

            decimal fundCommulativeCurrentYear = 0;
            decimal fundCommulativePreviousYear = 0;
            decimal fundCommulativeDiff = 0;
            decimal fundCommulativePercent = 0;

            decimal fundFairValueCurrentYear = 0;
            decimal fundFairValuePreviousYear = 0;
            decimal fundFairValueDiff = 0;
            decimal fundFairValuePercent = 0;

            decimal totalFundCurrYear = 0;
            decimal totalFundPrevYear = 0;
            decimal totalFundDiff = 0;
            decimal totalFundPer = 0;

            //TOTAL FUND BALANCE AND LIABILITIES
            decimal totalFundLiaCurrYear = 0;
            decimal totalFundLiaPrevYear = 0;
            decimal totalFundLiaDiff = 0;
            decimal totalFundLiaPer = 0;

            if (model.Count > 0) {
                foreach (var r in model)
                {
                    //CURRENT ASSERTS
                    if (r.particulars == "currAssetsCash") {
                        currAssetsCashCurrentYear = r.currAmount;
                        currAssetsCashPreviousYear=r.prevAmount;
                        currAssetsDiff=r.diff;
                        currAssetsPercent=r.per;
                    }
                    if (r.particulars == "currAssetsLoanReceivable")
                    {
                        currAssetsLoanReceivableCurrentYear=r.currAmount;
                        currAssetsLoanReceivablePreviousYear=r.prevAmount;
                        currAssetsLoanReceivableDiff=r.diff;
                        currAssetsLoanReceivablePercent=r.per;
                    }
                    if (r.particulars == "currAssetsOtherReceivable")
                    {
                        currAssetsOtherReceivableCurrentYear=r.currAmount;
                        currAssetsOtherReceivablePreviousYear=r.prevAmount;
                        currAssetsOtherReceivableDiff=r.diff;
                        currAssetsOtherReceivablePercent=r.per;
                    }
                    if (r.particulars == "currAssetsOther")
                    {
                        currAssetsOtherCurrentYear=r.currAmount;
                        currAssetsOtherPreviousYear=r.prevAmount;
                        currAssetsOtherDiff=r.diff;
                        currAssetsOtherPercent=r.per;
                    }
                    if (r.accntType == "Current Assets")
                    {
                        totalCurrAssetsCurrYear += r.currAmount;
                        totalCurrAssetsPrevYear += r.prevAmount;
                        totalCurrAssetsDiff += r.diff;
                    }

                    //NONCURRENT ASSETS
                    if (r.particulars == "noncurrAssetsFinancial") {
                        noncurrAssetsFinancialCurrentYear=r.currAmount;
                        noncurrAssetsFinancialPreviousYear=r.prevAmount;
                        noncurrAssetsFinancialDiff=r.diff;
                        noncurrAssetsFinancialPercent=r.per;
                    }
                    if (r.particulars == "noncurrAssetsRestrictedCash") {
                        noncurrAssetsRestrictedCashCurrentYear=r.currAmount;
                        noncurrAssetsRestrictedCashPreviousYear=r.prevAmount;
                        noncurrAssetsRestrictedCashDiff=r.diff;
                        noncurrAssetsRestrictedCashPercent=r.per;
                    }
                    if (r.particulars == "noncurrAssetsROUAsset") {
                        noncurrAssetsROUAssetCurrentYear=r.currAmount;
                        noncurrAssetsROUAssetPreviousYear=r.prevAmount;
                        noncurrAssetsROUAssetDiff=r.diff;
                        noncurrAssetsROUAssetPercent=r.per;
                    }
                    if (r.particulars == "noncurrAssetsProperty") {
                        noncurrAssetsPropertyCurrentYear=r.currAmount;
                        noncurrAssetsPropertyPreviousYear=r.prevAmount;
                        noncurrAssetsPropertyDiff=r.diff;
                        noncurrAssetsPropertyPercent=r.per;
                    }
                    if (r.particulars == "noncurrAssetsOther") {
                        noncurrAssetsOtherCurrentYear=r.currAmount;
                        noncurrAssetsOtherPreviousYear=r.prevAmount;
                        noncurrAssetsOtherDiff=r.diff;
                        noncurrAssetsOtherPercent=r.per;
                    }
                    if (r.accntType == "Noncurrent Assets") {
                        totalNonCurrAssetsCurrYear += r.currAmount;
                        totalNonCurrAssetsPrevYear += r.prevAmount;
                        totalNonCurrAssetsDiff += r.diff;
                    }

                    //CURRENT LIABILITIES
                    if (r.particulars == "currLiaTrade") {
                        currLiaTradeCurrentYear=r.currAmount;
                        currLiaTradePreviousYear=r.prevAmount;
                        currLiaTradeDiff=r.diff;
                        currLiaTradePercent=r.per;
                    }
                    if (r.particulars == "currLiaUnearned") {
                        currLiaUnearnedCurrentYear=r.currAmount;
                        currLiaUnearnedPreviousYear=r.prevAmount;
                        currLiaUnearnedDiff=r.diff;
                        currLiaUnearnedPercent=r.per;
                    }
                    if (r.particulars == "currLiaClientCBU") {
                        currLiaClientCBUCurrentYear=r.currAmount;
                        currLiaClientCBUPreviousYear=r.prevAmount;
                        currLiaClientCBUDiff=r.diff;
                        currLiaClientCBUPercent=r.per;
                    }
                    if (r.particulars == "currLiaTaxPayable") {
                        currLiaTaxPayableCurrentYear=r.currAmount;
                        currLiaTaxPayablePreviousYear=r.prevAmount;
                        currLiaTaxPayableDiff=r.diff;
                        currLiaTaxPayablePercent=r.per;
                    }
                    if (r.particulars == "currLiaCGLIPayable") {
                        currLiaCGLIPayableCurrentYear=r.currAmount;
                        currLiaCGLIPayablePreviousYear=r.prevAmount;
                        currLiaCGLIPayableDiff=r.diff;
                        currLiaCGLIPayablePercent=r.per;
                    }
                    if (r.particulars == "currLiaProvisional") {
                        currLiaProvisionalCurrentYear=r.currAmount;
                        currLiaProvisionalPreviousYear=r.prevAmount;
                        currLiaProvisionalDiff=r.diff;
                        currLiaProvisionalPercent=r.per;
                    }
                    if (r.accntType == "Current Liabilities") {
                        totalCurrLiaCurrYear += r.currAmount;
                        totalCurrLiaPrevYear += r.prevAmount;
                        totalCurrLiaDiff += r.diff;
                    }

                    //NONCURRENT LIABILITIES
                    if (r.particulars == "noncurrLiaLease") {
                        noncurrLiaLeaseCurrentYear=r.currAmount;
                        noncurrLiaLeasePreviousYear=r.prevAmount;
                        noncurrLiaLeaseDiff=r.diff;
                        noncurrLiaLeasePercent=r.per;
                    }
                    if (r.particulars == "noncurrLiaRetirement") {
                        noncurrLiaRetirementCurrentYear=r.currAmount;
                        noncurrLiaRetirementPreviousYear=r.prevAmount;
                        noncurrLiaRetirementDiff=r.diff;
                        noncurrLiaRetirementPercent=r.per;
                    }
                    if (r.particulars == "noncurrLiaDeferred") {
                        noncurrLiaDeferredCurrentYear=r.currAmount;
                        noncurrLiaDeferredPreviousYear=r.prevAmount;
                        noncurrLiaDeferredDiff=r.diff;
                        noncurrLiaDeferredPercent=r.per;
                    }
                    if (r.accntType == "Noncurrent Liabilities") {
                        totalNonCurrLiaCurrYear += r.currAmount;
                        totalNonCurrLiaPrevYear += r.prevAmount;
                        totalNonCurrLiaDiff += r.diff;
                    }

                    //FUND BALANCE
                    if (r.particulars == "fundRetained") {
                        fundRetainedCurrentYear=r.currAmount;
                        fundRetainedPreviousYear=r.prevAmount;
                        fundRetainedDiff=r.diff;
                        fundRetainedPercent=r.per;
                    }
                    if (r.particulars == "fundCommulative") {
                        fundCommulativeCurrentYear=r.currAmount;
                        fundCommulativePreviousYear=r.prevAmount;
                        fundCommulativeDiff=r.diff;
                        fundCommulativePercent=r.per;
                    }
                    if (r.particulars == "fundFairValue") {
                        fundFairValueCurrentYear=r.currAmount;
                        fundFairValuePreviousYear=r.prevAmount;
                        fundFairValueDiff=r.diff;
                        fundFairValuePercent=r.per;
                    }
                    if (r.accntType == "Fund Balance") {
                        totalFundCurrYear += r.currAmount;
                        totalFundPrevYear += r.prevAmount;
                        totalFundDiff += r.diff;
                    }
                }
                totalCurrAssetsPer = (totalCurrAssetsDiff / totalCurrAssetsPrevYear) * 100;
                totalNonCurrAssetsPer = (totalNonCurrAssetsDiff / totalNonCurrAssetsPrevYear) * 100;
                totalAssetsCurrYear = totalCurrAssetsCurrYear + totalNonCurrAssetsCurrYear;
                totalAssetsPrevYear = totalCurrAssetsPrevYear + totalNonCurrAssetsPrevYear;
                totalAssetsDiff = totalCurrAssetsDiff + totalNonCurrAssetsDiff;
                totalAssetsPer = (totalAssetsDiff / totalAssetsPrevYear) * 100;

                totalCurrLiaPer = (totalCurrLiaDiff / totalCurrLiaPrevYear) * 100;
                totalNonCurrLiaPer = (totalNonCurrLiaDiff / totalNonCurrLiaPrevYear) * 100;
                totalLiaCurrYear = totalCurrLiaCurrYear + totalNonCurrLiaCurrYear;
                totalLiaPrevYear = totalCurrLiaPrevYear + totalNonCurrLiaPrevYear;
                totalLiaDiff = totalCurrLiaDiff + totalNonCurrLiaDiff;
                totalLiaPer = (totalLiaDiff / totalLiaPrevYear) * 100;

                totalFundPer = (totalFundDiff / totalFundPrevYear) * 100;
                totalFundLiaCurrYear = totalLiaCurrYear + totalFundCurrYear;
                totalFundLiaPrevYear = totalLiaPrevYear + totalFundPrevYear;
                totalFundLiaDiff = totalLiaDiff + totalFundDiff;
                totalFundLiaPer = (totalFundLiaDiff / totalFundLiaPrevYear) * 100;
            }

            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.Blink);

            BlinkConverterSettings blinkConverterSettings = new BlinkConverterSettings();
            string HTMLContent = "<html lang='en'><head>" +
                    "<title>Balance Sheet</title>" +
                    "<link href=" + Path.Combine(_hostingEnvironment.ContentRootPath, "ReportCss/bs.css") + " rel='stylesheet' />" +
                    "</head>" +
                    "<body>" +
                    //"<div class='container'>" +

                    "<table width='100%'>" +
                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><b>KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)</b></td>" +
                        "</tr>" +

                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><b>(A Non-Stock, Non-Profit Organization)</b></td>" +
                        "</tr>" +

                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><b>STATEMENT OF ASSETS, LIABILITIES AND FUND BALANCE</b></td>" +
                        "</tr>" +

                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><div id='bsMonthYear' style='font-weight:bold'>AS OF " + month + " " + year + "</div></td>" +
                        "</tr>" +

                         "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr>" +
                                    "<td>&nbsp;</td>" +
                                    "<td style='text-align:center'><b>NOTES</b></td>" +
                                    "<td style='text-align:center'><div id='bsDetailCurrentYear' style='font-weight:bold'>" + year + "</div></td>" +
                                    "<td style='text-align:center'><div id='bsDetailPreviousYear' style='font-weight:bold'>" + prevYear + "</div></td>" +
                                    "<td style='text-align:center'><b>INCREASE <br />(DECREASE) </b></td>" +
                                    "<td style='text-align:center'><b>%</b></td>" +
                                "</tr>" +
                        "<tr>" +
                            "<td><b>ASSETS</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                         "<tr>" +
                            "<td><b>Current Assets</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +

                        "<tr>" +
                            "<td>Cash and Cash Equivalents</td>" +
                            "<td style='text-align:center'>1</td>" +
                            "<td style='text-align:right'><div id='currAssetsCashCurrentYear'>" + currAssetsCashCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsCashPreviousYear'>" + currAssetsCashPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsDiff'>" + currAssetsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currAssetsPercent'>" + currAssetsPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Loans Receivable - Net</td>" +
                            "<td style='text-align:center'>2</td>" +
                            "<td style='text-align:right'><div id='currAssetsLoanReceivableCurrentYear'>" + currAssetsLoanReceivableCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsLoanReceivablePreviousYear'>" + currAssetsLoanReceivablePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsLoanReceivableDiff'>" + currAssetsLoanReceivableDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currAssetsLoanReceivablePercent'>" + currAssetsLoanReceivablePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Other Receivable - Net</td>" +
                            "<td style='text-align:center'>3</td>" +
                            "<td style='text-align:right'><div id='currAssetsOtherReceivableCurrentYear'>" + currAssetsOtherReceivableCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsOtherReceivablePreviousYear'>" + currAssetsOtherReceivablePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsOtherReceivableDiff'>" + currAssetsOtherReceivableDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currAssetsOtherReceivablePercent'>" + currAssetsOtherReceivablePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>Other Current Assets</td>" +
                            "<td style='text-align:center'>4</td>" +
                            "<td style='text-align:right'><div id='currAssetsOtherCurrentYear'>" + currAssetsOtherCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsOtherPreviousYear'>" + currAssetsOtherPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currAssetsOtherDiff'>" + currAssetsOtherDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currAssetsOtherPercent'>" + currAssetsOtherPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Current Assets</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totCurrAssetsCurrentYear'>" + totalCurrAssetsCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totCurrAssetsPreviousYear'>" + totalCurrAssetsPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totCurrAssetsDiff'>" + totalCurrAssetsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totCurrAssetsPercent'>" + totalCurrAssetsPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr>" +
                            "<td><b>Noncurrent Assets</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Financial Assets at FVOCI</td>" +
                            "<td style='text-align:center'>5</td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsFinancialCurrentYear'>" + noncurrAssetsFinancialCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsFinancialPreviousYear'>" + noncurrAssetsFinancialPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsFinancialDiff'>" + noncurrAssetsFinancialDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrAssetsFinancialPercent'>" + noncurrAssetsFinancialPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Restricted Cash</td>" +
                            "<td style='text-align:center'>6</td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsRestrictedCashCurrentYear'>" + noncurrAssetsRestrictedCashCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsRestrictedCashPreviousYear'>" + noncurrAssetsRestrictedCashPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsRestrictedCashDiff'>" + noncurrAssetsRestrictedCashDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrAssetsRestrictedCashPercent'>" + noncurrAssetsRestrictedCashPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>ROU  Asset</td>" +
                            "<td style='text-align:center'>7</td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsROUAssetCurrentYear'>" + noncurrAssetsROUAssetCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsROUAssetPreviousYear'>" + noncurrAssetsROUAssetPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsROUAssetDiff'>" + noncurrAssetsROUAssetDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrAssetsROUAssetPercent'>" + noncurrAssetsROUAssetPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Property and Equipment</td>" +
                            "<td style='text-align:center'>8</td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsPropertyCurrentYear'>" + noncurrAssetsPropertyCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsPropertyPreviousYear'>" + noncurrAssetsPropertyPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsPropertyDiff'>" + noncurrAssetsPropertyDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrAssetsPropertyPercent'>" + noncurrAssetsPropertyPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                         "<tr class='total'>" +
                            "<td>Other Noncurrent Assets</td>" +
                            "<td style='text-align:center'>9</td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsOtherCurrentYear'>" + noncurrAssetsOtherCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsOtherPreviousYear'>" + noncurrAssetsOtherPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrAssetsOtherDiff'>" + noncurrAssetsOtherDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrAssetsOtherPercent'>" + noncurrAssetsOtherPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Noncurrent Assets</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totNonCurrAssetsCurrentYear'>" + totalNonCurrAssetsCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNonCurrAssetsPreviousYear'>" + totalNonCurrAssetsPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNonCurrAssetsDiff'>" + totalNonCurrAssetsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totNonCurrAssetsPercent'>" + totalNonCurrAssetsPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr class='grandTotal'>" +
                            "<td style='text-align:left'>TOTAL ASSETS</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totAssetsCurrentYear'>" + totalAssetsCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totAssetsPreviousYear'>" + totalAssetsPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totAssetsDiff'>" + totalAssetsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totAssetsPercent'>" + totalAssetsPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                         "<tr>" +
                            "<td><b>LIABILITIES AND FUND BALANCE</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td><b>Current Liabilities</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Trade and Other Payables</td>" +
                            "<td style='text-align:center'>10</td>" +
                            "<td style='text-align:right'><div id='currLiaTradeCurrentYear'>" + currLiaTradeCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaTradePreviousYear'>" + currLiaTradePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaTradeDiff'>" + currLiaTradeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currLiaTradePercent'>" + currLiaTradePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Unearned Interest Income</td>" +
                            "<td style='text-align:center'>11</td>" +
                            "<td style='text-align:right'><div id='currLiaUnearnedCurrentYear'>" + currLiaUnearnedCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaUnearnedPreviousYear'>" + currLiaUnearnedPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaUnearnedDiff'>" + currLiaUnearnedDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currLiaUnearnedPercent'>" + currLiaUnearnedPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Clients' CBU</td>" +
                            "<td style='text-align:center'>12</td>" +
                            "<td style='text-align:right'><div id='currLiaClientCBUCurrentYear'>" + currLiaClientCBUCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaClientCBUPreviousYear'>" + currLiaClientCBUPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaClientCBUDiff'>" + currLiaClientCBUDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currLiaClientCBUPercent'>" + currLiaClientCBUPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Tax Payable</td>" +
                            "<td style='text-align:center'>13</td>" +
                            "<td style='text-align:right'><div id='currLiaTaxPayableCurrentYear'>" + currLiaTaxPayableCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaTaxPayablePreviousYear'>" + currLiaTaxPayablePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaTaxPayableDiff'>" + currLiaTaxPayableDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currLiaTaxPayablePercent'>" + currLiaTaxPayablePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>MI/CGLI Payables</td>" +
                            "<td style='text-align:center'>14</td>" +
                            "<td style='text-align:right'><div id='currLiaCGLIPayableCurrentYear'>" + currLiaCGLIPayableCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaCGLIPayablePreviousYear'>" + currLiaCGLIPayablePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaCGLIPayableDiff'>" + currLiaCGLIPayableDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currLiaCGLIPayablePercent'>" + currLiaCGLIPayablePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>Provision for Contingencies</td>" +
                            "<td style='text-align:center'>15</td>" +
                            "<td style='text-align:right'><div id='currLiaProvisionalCurrentYear'>" + currLiaProvisionalCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaProvisionalPreviousYear'>" + currLiaProvisionalPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='currLiaProvisionalDiff'>" + currLiaProvisionalDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='currLiaProvisionalPercent'>" + currLiaProvisionalPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Current Liabilities</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totCurrLiaCurrentYear'>" + totalCurrLiaCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totCurrLiaPreviousYear'>" + totalCurrLiaPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totCurrLiaDiff'>" + totalCurrLiaDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totCurrLiaPercent'>" + totalCurrLiaPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td><b>Noncurrent Liabilities</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Lease Liability</td>" +
                            "<td style='text-align:center'>16</td>" +
                            "<td style='text-align:right'><div id='noncurrLiaLeaseCurrentYear'>" + noncurrLiaLeaseCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrLiaLeasePreviousYear'>" + noncurrLiaLeasePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrLiaLeaseDiff'>" + noncurrLiaLeaseDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrLiaLeasePercent'>" + noncurrLiaLeasePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Retirement Benefit Liability</td>" +
                            "<td style='text-align:center'>17</td>" +
                            "<td style='text-align:right'><div id='noncurrLiaRetirementCurrentYear'>" + noncurrLiaRetirementCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrLiaRetirementPreviousYear'>" + noncurrLiaRetirementPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrLiaRetirementDiff'>" + noncurrLiaRetirementDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrLiaRetirementPercent'>" + noncurrLiaRetirementPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td> Deferred Tax Liability</td>" +
                            "<td style='text-align:center'>18</td>" +
                            "<td style='text-align:right'><div id='noncurrLiaDeferredCurrentYear'>" + noncurrLiaDeferredCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrLiaDeferredPreviousYear'>" + noncurrLiaDeferredPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='noncurrLiaDeferredDiff'>" + noncurrLiaDeferredDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='noncurrLiaDeferredPercent'>" + noncurrLiaDeferredPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Noncurrent Liabilities</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totNonCurrLiaCurrentYear'>" + totalNonCurrLiaCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNonCurrLiaPreviousYear'>" + totalNonCurrLiaPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNonCurrLiaDiff'>" + totalNonCurrLiaDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totNonCurrLiaPercent'>" + totalNonCurrLiaPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Liabilities</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totLiaCurrentYear'>" + totalLiaCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totLiaPreviousYear'>" + totalLiaPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totLiaDiff'>" + totalLiaDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totLiaPercent'>" + totalLiaPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td><b>Fund Balance</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Retained Earnings</td>" +
                            "<td style='text-align:center'>19</td>" +
                            "<td style='text-align:right'><div id='fundRetainedCurrentYear'>" + fundRetainedCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='fundRetainedPreviousYear'>" + fundRetainedPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='fundRetainedDiff'>" + fundRetainedDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='fundRetainedPercent'>" + fundRetainedPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Cumulative Remeasurement Gains on Retirement Benefit Liability</td>" +
                            "<td style='text-align:center'>20</td>" +
                            "<td style='text-align:right'><div id='fundCommulativeCurrentYear'>" + fundCommulativeCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='fundCommulativePreviousYear'>" + fundCommulativePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='fundCommulativeDiff'>" + fundCommulativeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='fundCommulativePercent'>" + fundCommulativePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>Fair Value Reserve on FVTOCI Financial Assets</td>" +
                            "<td style='text-align:center'>21</td>" +
                            "<td style='text-align:right'><div id='fundFairValueCurrentYear'>" + fundFairValueCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='fundFairValuePreviousYear'>" + fundFairValuePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='fundFairValueDiff'>" + fundFairValueDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='fundFairValuePercent'>" + fundFairValuePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Fund Balance</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totFundCurrentYear'>" + totalFundCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totFundPreviousYear'>" + totalFundPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totFundDiff'></div>" + totalFundDiff.ToString("#,##0.00;(#,##0.00);-") + "</td>" +
                            "<td style='text-align:center'><div id='totFundPercent'>" + totalFundPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr class='grandTotal'>" +
                            "<td style='text-align:left'>TOTAL LIABILITIES AND FUND BALANCE</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totFundLiaCurrentYear'>" + totalFundLiaCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totFundLiaPreviousYear'>" + totalFundLiaPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totFundLiaDiff'>" + totalFundLiaDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totFundLiaPercent'>" + totalFundLiaPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                    "</table>" +
                    //"</div>" +
                    "</body></html>";
            string baseUrl = @"D:/Hey";
            //Set the BlinkBinaries folder path
            blinkConverterSettings.BlinkPath = Path.Combine(_hostingEnvironment.ContentRootPath, "BlinkBinariesWindows");

            //Assign Blink converter settings to HTML converter
            htmlConverter.ConverterSettings = blinkConverterSettings;
            blinkConverterSettings.Margin = new PdfMargins { All = 5 };
            //Convert URL to PDF
            PdfDocument document = htmlConverter.Convert(HTMLContent, baseUrl);
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            string mimeType = "application/pdf";
            Response.Headers.Add("content-disposition", "inline;filename=" + "BS" + month + year + ".pdf");
            return File(stream.ToArray(), mimeType);
        }

        public List<fsIncomeStatementViewModel> GetIncomeStatement(string month, int year)
        {
            List<fsIncomeStatementViewModel> viewModel = new List<fsIncomeStatementViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spIncomeStatement";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsIncomeStatementViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            currAmount = reader.GetDecimal(2),
                            prevAmount = reader.GetDecimal(3),
                            diff = reader.GetDecimal(4),
                            per = reader.GetDecimal(5),
                            month = reader.GetString(6).Trim(),
                            currYear = reader.GetInt32(7),
                            prevYear = reader.GetInt32(8),
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<fsIncomeStatementMonthlyViewModel> GetMonthlyIncomeStatement(string month, int year)
        {
            List<fsIncomeStatementMonthlyViewModel> viewModel = new List<fsIncomeStatementMonthlyViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spIncomeStatementMonthly";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsIncomeStatementMonthlyViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            currAmount = reader.GetDecimal(2),
                            prevAmount = reader.GetDecimal(3),
                            diff = reader.GetDecimal(4),
                            per = reader.GetDecimal(5),
                            currMonth = reader.GetString(6).Trim(),
                            prevMonth = reader.GetString(7).Trim(),
                            currYear = reader.GetInt32(8),
                            prevYear = reader.GetInt32(9),
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<fsRetainedEarningsViewModel> GetRetainedEarnings(string month, int year)
        {
            List<fsRetainedEarningsViewModel> viewModel = new List<fsRetainedEarningsViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spRetainedEarnings";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsRetainedEarningsViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            currAmount = reader.GetDecimal(2),
                            prevAmount = reader.GetDecimal(3),
                            diff = reader.GetDecimal(4),
                            per = reader.GetDecimal(5),
                            month = reader.GetString(6).Trim(),
                            currYear = reader.GetInt32(7),
                            prevYear = reader.GetInt32(8),
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }
        
        public List<fsMonthlyISvsBudgetViewModel> GetMonthlyIncomeStatementVSBudget(string month, int year)
        {
            List<fsMonthlyISvsBudgetViewModel> viewModel = new List<fsMonthlyISvsBudgetViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spMonthlyISvsBudget";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year.ToString()));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsMonthlyISvsBudgetViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            budgetMonthly = reader.GetDecimal(2),
                            budgetCumulative = reader.GetDecimal(3),
                            month = reader.GetString(4).Trim(),
                            year = reader.GetInt32(5).ToString().Trim(),
                            actual = reader.GetDecimal(6),
                            diff = reader.GetDecimal(8),
                            per = reader.GetDecimal(9),
                            
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<fsMonthlyISvsBudgetViewModel> GetCumulativeIncomeStatementVSBudget(string month, int year)
        {
            List<fsMonthlyISvsBudgetViewModel> viewModel = new List<fsMonthlyISvsBudgetViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spCumulativeISvsBudget";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year.ToString()));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsMonthlyISvsBudgetViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            budgetMonthly = reader.GetDecimal(2),
                            budgetCumulative = reader.GetDecimal(3),
                            month = reader.GetString(4).Trim(),
                            year = reader.GetInt32(5).ToString().Trim(),
                            actual = reader.GetDecimal(6),
                            diff = reader.GetDecimal(8),
                            per = reader.GetDecimal(9),

                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<fsMonthlyBSvsBudgetViewModel> GetBalanceSheetVSBudget(string month, int year)
        {
            List<fsMonthlyBSvsBudgetViewModel> viewModel = new List<fsMonthlyBSvsBudgetViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spMonthlyBSvsBudget";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsMonthlyBSvsBudgetViewModel
                        {
                            particulars = reader.GetString(0).Trim(),
                            accntType = reader.GetString(1).ToString().Trim(),
                            budgetMonthly = reader.GetDecimal(2),
                            budgetCumulative = reader.GetDecimal(3),
                            month = reader.GetString(4).Trim(),
                            year = reader.GetInt32(5).ToString().Trim(),
                            actual = reader.GetDecimal(6),
                            diff = reader.GetDecimal(8),
                            per = reader.GetDecimal(9),
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        //GETNOTES
        public fsNotes GetFSNotes(string month, int year, int notesId)
        {
            fsNotes viewModel = new fsNotes();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * from fsNotes where month = '" + month + "' and year = '" + year + "' and notesId = '" + notesId + "'";
                SqlCommand cmd = new SqlCommand(sql, con);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.id = reader.GetInt32(0);
                        viewModel.notesId = reader.GetInt32(1);
                        viewModel.remarks = reader.GetString(2);
                        viewModel.month = reader.GetString(3);
                        viewModel.year = reader.GetInt32(4);
                    }
                }
                con.Close();
            }
            return viewModel;
            //return viewModel;
        }


        [HttpGet]
        public IActionResult BSXLS(string month, int year)
        {

            #region

            var model = GetBalanceSheet(month, year);
            var prevYear = year - 1;

            //ASSETS
            //CURRENT ASSETS
            decimal currAssetsCashCurrentYear = 0;
            decimal currAssetsCashPreviousYear = 0;
            decimal currAssetsDiff = 0;
            decimal currAssetsPercent = 0;

            decimal currAssetsLoanReceivableCurrentYear = 0;
            decimal currAssetsLoanReceivablePreviousYear = 0;
            decimal currAssetsLoanReceivableDiff = 0;
            decimal currAssetsLoanReceivablePercent = 0;

            decimal currAssetsOtherReceivableCurrentYear = 0;
            decimal currAssetsOtherReceivablePreviousYear = 0;
            decimal currAssetsOtherReceivableDiff = 0;
            decimal currAssetsOtherReceivablePercent = 0;

            decimal currAssetsOtherCurrentYear = 0;
            decimal currAssetsOtherPreviousYear = 0;
            decimal currAssetsOtherDiff = 0;
            decimal currAssetsOtherPercent = 0;

            decimal totalCurrAssetsCurrYear = 0;
            decimal totalCurrAssetsPrevYear = 0;
            decimal totalCurrAssetsDiff = 0;
            decimal totalCurrAssetsPer = 0;

            //NONCURRENT ASSETS
            decimal noncurrAssetsFinancialCurrentYear = 0;
            decimal noncurrAssetsFinancialPreviousYear = 0;
            decimal noncurrAssetsFinancialDiff = 0;
            decimal noncurrAssetsFinancialPercent = 0;

            decimal noncurrAssetsRestrictedCashCurrentYear = 0;
            decimal noncurrAssetsRestrictedCashPreviousYear = 0;
            decimal noncurrAssetsRestrictedCashDiff = 0;
            decimal noncurrAssetsRestrictedCashPercent = 0;

            decimal noncurrAssetsROUAssetCurrentYear = 0;
            decimal noncurrAssetsROUAssetPreviousYear = 0;
            decimal noncurrAssetsROUAssetDiff = 0;
            decimal noncurrAssetsROUAssetPercent = 0;

            decimal noncurrAssetsPropertyCurrentYear = 0;
            decimal noncurrAssetsPropertyPreviousYear = 0;
            decimal noncurrAssetsPropertyDiff = 0;
            decimal noncurrAssetsPropertyPercent = 0;

            decimal noncurrAssetsOtherCurrentYear = 0;
            decimal noncurrAssetsOtherPreviousYear = 0;
            decimal noncurrAssetsOtherDiff = 0;
            decimal noncurrAssetsOtherPercent = 0;

            decimal totalNonCurrAssetsCurrYear = 0;
            decimal totalNonCurrAssetsPrevYear = 0;
            decimal totalNonCurrAssetsDiff = 0;
            decimal totalNonCurrAssetsPer = 0;

            decimal totalAssetsCurrYear = 0;
            decimal totalAssetsPrevYear = 0;
            decimal totalAssetsDiff = 0;
            decimal totalAssetsPer = 0;

            //LIABILITIES
            //CURRENT LIA
            decimal currLiaTradeCurrentYear = 0;
            decimal currLiaTradePreviousYear = 0;
            decimal currLiaTradeDiff = 0;
            decimal currLiaTradePercent = 0;

            decimal currLiaUnearnedCurrentYear = 0;
            decimal currLiaUnearnedPreviousYear = 0;
            decimal currLiaUnearnedDiff = 0;
            decimal currLiaUnearnedPercent = 0;

            decimal currLiaClientCBUCurrentYear = 0;
            decimal currLiaClientCBUPreviousYear = 0;
            decimal currLiaClientCBUDiff = 0;
            decimal currLiaClientCBUPercent = 0;

            decimal currLiaTaxPayableCurrentYear = 0;
            decimal currLiaTaxPayablePreviousYear = 0;
            decimal currLiaTaxPayableDiff = 0;
            decimal currLiaTaxPayablePercent = 0;

            decimal currLiaCGLIPayableCurrentYear = 0;
            decimal currLiaCGLIPayablePreviousYear = 0;
            decimal currLiaCGLIPayableDiff = 0;
            decimal currLiaCGLIPayablePercent = 0;

            decimal currLiaProvisionalCurrentYear = 0;
            decimal currLiaProvisionalPreviousYear = 0;
            decimal currLiaProvisionalDiff = 0;
            decimal currLiaProvisionalPercent = 0;

            decimal totalCurrLiaCurrYear = 0;
            decimal totalCurrLiaPrevYear = 0;
            decimal totalCurrLiaDiff = 0;
            decimal totalCurrLiaPer = 0;

            //NONCURRENT LIA
            decimal noncurrLiaLeaseCurrentYear = 0;
            decimal noncurrLiaLeasePreviousYear = 0;
            decimal noncurrLiaLeaseDiff = 0;
            decimal noncurrLiaLeasePercent = 0;

            decimal noncurrLiaRetirementCurrentYear = 0;
            decimal noncurrLiaRetirementPreviousYear = 0;
            decimal noncurrLiaRetirementDiff = 0;
            decimal noncurrLiaRetirementPercent = 0;

            decimal noncurrLiaDeferredCurrentYear = 0;
            decimal noncurrLiaDeferredPreviousYear = 0;
            decimal noncurrLiaDeferredDiff = 0;
            decimal noncurrLiaDeferredPercent = 0;

            decimal totalNonCurrLiaCurrYear = 0;
            decimal totalNonCurrLiaPrevYear = 0;
            decimal totalNonCurrLiaDiff = 0;
            decimal totalNonCurrLiaPer = 0;

            decimal totalLiaCurrYear = 0;
            decimal totalLiaPrevYear = 0;
            decimal totalLiaDiff = 0;
            decimal totalLiaPer = 0;

            //FUND BALANCE
            decimal fundRetainedCurrentYear = 0;
            decimal fundRetainedPreviousYear = 0;
            decimal fundRetainedDiff = 0;
            decimal fundRetainedPercent = 0;

            decimal fundCommulativeCurrentYear = 0;
            decimal fundCommulativePreviousYear = 0;
            decimal fundCommulativeDiff = 0;
            decimal fundCommulativePercent = 0;

            decimal fundFairValueCurrentYear = 0;
            decimal fundFairValuePreviousYear = 0;
            decimal fundFairValueDiff = 0;
            decimal fundFairValuePercent = 0;

            decimal totalFundCurrYear = 0;
            decimal totalFundPrevYear = 0;
            decimal totalFundDiff = 0;
            decimal totalFundPer = 0;

            //TOTAL FUND BALANCE AND LIABILITIES
            decimal totalFundLiaCurrYear = 0;
            decimal totalFundLiaPrevYear = 0;
            decimal totalFundLiaDiff = 0;
            decimal totalFundLiaPer = 0;
            #endregion  

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    //CURRENT ASSERTS
                    if (r.particulars == "currAssetsCash")
                    {
                        currAssetsCashCurrentYear = r.currAmount;
                        currAssetsCashPreviousYear = r.prevAmount;
                        currAssetsDiff = r.diff;
                        currAssetsPercent = r.per;
                    }
                    if (r.particulars == "currAssetsLoanReceivable")
                    {
                        currAssetsLoanReceivableCurrentYear = r.currAmount;
                        currAssetsLoanReceivablePreviousYear = r.prevAmount;
                        currAssetsLoanReceivableDiff = r.diff;
                        currAssetsLoanReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOtherReceivable")
                    {
                        currAssetsOtherReceivableCurrentYear = r.currAmount;
                        currAssetsOtherReceivablePreviousYear = r.prevAmount;
                        currAssetsOtherReceivableDiff = r.diff;
                        currAssetsOtherReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOther")
                    {
                        currAssetsOtherCurrentYear = r.currAmount;
                        currAssetsOtherPreviousYear = r.prevAmount;
                        currAssetsOtherDiff = r.diff;
                        currAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Current Assets")
                    {
                        totalCurrAssetsCurrYear += r.currAmount;
                        totalCurrAssetsPrevYear += r.prevAmount;
                        totalCurrAssetsDiff += r.diff;
                    }

                    //NONCURRENT ASSETS
                    if (r.particulars == "noncurrAssetsFinancial")
                    {
                        noncurrAssetsFinancialCurrentYear = r.currAmount;
                        noncurrAssetsFinancialPreviousYear = r.prevAmount;
                        noncurrAssetsFinancialDiff = r.diff;
                        noncurrAssetsFinancialPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsRestrictedCash")
                    {
                        noncurrAssetsRestrictedCashCurrentYear = r.currAmount;
                        noncurrAssetsRestrictedCashPreviousYear = r.prevAmount;
                        noncurrAssetsRestrictedCashDiff = r.diff;
                        noncurrAssetsRestrictedCashPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsROUAsset")
                    {
                        noncurrAssetsROUAssetCurrentYear = r.currAmount;
                        noncurrAssetsROUAssetPreviousYear = r.prevAmount;
                        noncurrAssetsROUAssetDiff = r.diff;
                        noncurrAssetsROUAssetPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsProperty")
                    {
                        noncurrAssetsPropertyCurrentYear = r.currAmount;
                        noncurrAssetsPropertyPreviousYear = r.prevAmount;
                        noncurrAssetsPropertyDiff = r.diff;
                        noncurrAssetsPropertyPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsOther")
                    {
                        noncurrAssetsOtherCurrentYear = r.currAmount;
                        noncurrAssetsOtherPreviousYear = r.prevAmount;
                        noncurrAssetsOtherDiff = r.diff;
                        noncurrAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Assets")
                    {
                        totalNonCurrAssetsCurrYear += r.currAmount;
                        totalNonCurrAssetsPrevYear += r.prevAmount;
                        totalNonCurrAssetsDiff += r.diff;
                    }

                    //CURRENT LIABILITIES
                    if (r.particulars == "currLiaTrade")
                    {
                        currLiaTradeCurrentYear = r.currAmount;
                        currLiaTradePreviousYear = r.prevAmount;
                        currLiaTradeDiff = r.diff;
                        currLiaTradePercent = r.per;
                    }
                    if (r.particulars == "currLiaUnearned")
                    {
                        currLiaUnearnedCurrentYear = r.currAmount;
                        currLiaUnearnedPreviousYear = r.prevAmount;
                        currLiaUnearnedDiff = r.diff;
                        currLiaUnearnedPercent = r.per;
                    }
                    if (r.particulars == "currLiaClientCBU")
                    {
                        currLiaClientCBUCurrentYear = r.currAmount;
                        currLiaClientCBUPreviousYear = r.prevAmount;
                        currLiaClientCBUDiff = r.diff;
                        currLiaClientCBUPercent = r.per;
                    }
                    if (r.particulars == "currLiaTaxPayable")
                    {
                        currLiaTaxPayableCurrentYear = r.currAmount;
                        currLiaTaxPayablePreviousYear = r.prevAmount;
                        currLiaTaxPayableDiff = r.diff;
                        currLiaTaxPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaCGLIPayable")
                    {
                        currLiaCGLIPayableCurrentYear = r.currAmount;
                        currLiaCGLIPayablePreviousYear = r.prevAmount;
                        currLiaCGLIPayableDiff = r.diff;
                        currLiaCGLIPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaProvisional")
                    {
                        currLiaProvisionalCurrentYear = r.currAmount;
                        currLiaProvisionalPreviousYear = r.prevAmount;
                        currLiaProvisionalDiff = r.diff;
                        currLiaProvisionalPercent = r.per;
                    }
                    if (r.accntType == "Current Liabilities")
                    {
                        totalCurrLiaCurrYear += r.currAmount;
                        totalCurrLiaPrevYear += r.prevAmount;
                        totalCurrLiaDiff += r.diff;
                    }

                    //NONCURRENT LIABILITIES
                    if (r.particulars == "noncurrLiaLease")
                    {
                        noncurrLiaLeaseCurrentYear = r.currAmount;
                        noncurrLiaLeasePreviousYear = r.prevAmount;
                        noncurrLiaLeaseDiff = r.diff;
                        noncurrLiaLeasePercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaRetirement")
                    {
                        noncurrLiaRetirementCurrentYear = r.currAmount;
                        noncurrLiaRetirementPreviousYear = r.prevAmount;
                        noncurrLiaRetirementDiff = r.diff;
                        noncurrLiaRetirementPercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaDeferred")
                    {
                        noncurrLiaDeferredCurrentYear = r.currAmount;
                        noncurrLiaDeferredPreviousYear = r.prevAmount;
                        noncurrLiaDeferredDiff = r.diff;
                        noncurrLiaDeferredPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Liabilities")
                    {
                        totalNonCurrLiaCurrYear += r.currAmount;
                        totalNonCurrLiaPrevYear += r.prevAmount;
                        totalNonCurrLiaDiff += r.diff;
                    }

                    //FUND BALANCE
                    if (r.particulars == "fundRetained")
                    {
                        fundRetainedCurrentYear = r.currAmount;
                        fundRetainedPreviousYear = r.prevAmount;
                        fundRetainedDiff = r.diff;
                        fundRetainedPercent = r.per;
                    }
                    if (r.particulars == "fundCommulative")
                    {
                        fundCommulativeCurrentYear = r.currAmount;
                        fundCommulativePreviousYear = r.prevAmount;
                        fundCommulativeDiff = r.diff;
                        fundCommulativePercent = r.per;
                    }
                    if (r.particulars == "fundFairValue")
                    {
                        fundFairValueCurrentYear = r.currAmount;
                        fundFairValuePreviousYear = r.prevAmount;
                        fundFairValueDiff = r.diff;
                        fundFairValuePercent = r.per;
                    }
                    if (r.accntType == "Fund Balance")
                    {
                        totalFundCurrYear += r.currAmount;
                        totalFundPrevYear += r.prevAmount;
                        totalFundDiff += r.diff;
                    }
                }
                totalCurrAssetsPer = (totalCurrAssetsDiff / totalCurrAssetsPrevYear) * 100;
                totalNonCurrAssetsPer = (totalNonCurrAssetsDiff / totalNonCurrAssetsPrevYear) * 100;
                totalAssetsCurrYear = totalCurrAssetsCurrYear + totalNonCurrAssetsCurrYear;
                totalAssetsPrevYear = totalCurrAssetsPrevYear + totalNonCurrAssetsPrevYear;
                totalAssetsDiff = totalCurrAssetsDiff + totalNonCurrAssetsDiff;
                totalAssetsPer = (totalAssetsDiff / totalAssetsPrevYear) * 100;

                totalCurrLiaPer = (totalCurrLiaDiff / totalCurrLiaPrevYear) * 100;
                totalNonCurrLiaPer = (totalNonCurrLiaDiff / totalNonCurrLiaPrevYear) * 100;
                totalLiaCurrYear = totalCurrLiaCurrYear + totalNonCurrLiaCurrYear;
                totalLiaPrevYear = totalCurrLiaPrevYear + totalNonCurrLiaPrevYear;
                totalLiaDiff = totalCurrLiaDiff + totalNonCurrLiaDiff;
                totalLiaPer = (totalLiaDiff / totalLiaPrevYear) * 100;

                totalFundPer = (totalFundDiff / totalFundPrevYear) * 100;
                totalFundLiaCurrYear = totalLiaCurrYear + totalFundCurrYear;
                totalFundLiaPrevYear = totalLiaPrevYear + totalFundPrevYear;
                totalFundLiaDiff = totalLiaDiff + totalFundDiff;
                totalFundLiaPer = (totalFundLiaDiff / totalFundLiaPrevYear) * 100;
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("BS" + month + year);
                //HEADER
                ew.Cells["A1:F1"].Merge = true;
                ew.Cells["A1:F1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:F1"].Style.Font.Bold = true;

                ew.Cells["A2:F2"].Merge = true;
                ew.Cells["A2:F2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:F2"].Style.Font.Bold = true;

                ew.Cells["A4:F4"].Merge = true;
                ew.Cells["A4:F4"].Value = "STATEMENT OF ASSETS, LIABILITIES AND FUND BALANCE";
                ew.Cells["A4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:F4"].Style.Font.Bold = true;

                ew.Cells["A5:F5"].Merge = true;
                ew.Cells["A5:F5"].Value = "AS OF " + month + " " + year;
                ew.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:F5"].Style.Font.Bold = true;

                ew.Cells["A:F"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:F7"].Style.Font.Size = 12;
                ew.Cells["A8:F53"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B7"].Value = "NOTE";
                ew.Cells["C7"].Value = year;
                ew.Cells["D7"].Value = prevYear;
                ew.Cells["E7"].Value = "INCREASE (DECREASE)";
                ew.Cells["E7"].Style.WrapText = true;
                ew.Cells["F7"].Value = "%";
                ew.Cells["A7:F7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A7:F7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A7:F7"].Style.Font.Bold = true;
                ew.Cells["B11:B53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B11:B53"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["B11:B53"].Style.Numberformat.Format = "0";

                ew.Cells["A8"].Value = "ASSETS";
                ew.Cells["A8"].Style.Font.Bold = true;

                ew.Cells["A10"].Value = "Current Assets";
                ew.Cells["A10"].Style.Font.Bold = true;

                ew.Cells["C11:D53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["F11:F53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C11:E53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* '-'??_);_(@_)";
                ew.Cells["F11:F53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                ew.Cells["A11"].Value = "Cash and Cash Equivalents";
                ew.Cells["B11"].Value = 1;
                ew.Cells["C11"].Value = currAssetsCashCurrentYear;
                ew.Cells["D11"].Value = currAssetsCashPreviousYear;
                ew.Cells["E11"].Value = currAssetsDiff;
                ew.Cells["F11"].Value = currAssetsPercent / 100;

                ew.Cells["A12"].Value = "Loans Receivable - Net";
                ew.Cells["B12"].Value = 2;
                ew.Cells["C12"].Value = currAssetsLoanReceivableCurrentYear;
                ew.Cells["D12"].Value = currAssetsLoanReceivablePreviousYear;
                ew.Cells["E12"].Value = currAssetsLoanReceivableDiff;
                ew.Cells["F12"].Value = currAssetsLoanReceivablePercent / 100;

                ew.Cells["A13"].Value = "Other receivables - net";
                ew.Cells["B13"].Value = 3;
                ew.Cells["C13"].Value = currAssetsOtherReceivableCurrentYear;
                ew.Cells["D13"].Value = currAssetsOtherReceivablePreviousYear;
                ew.Cells["E13"].Value = currAssetsOtherReceivableDiff;
                ew.Cells["F13"].Value = currAssetsOtherReceivablePercent / 100;

                ew.Cells["A14"].Value = "Other Current Assets";
                ew.Cells["B14"].Value = 4;
                ew.Cells["C14"].Value = currAssetsOtherCurrentYear;
                ew.Cells["D14"].Value = currAssetsOtherPreviousYear;
                ew.Cells["E14"].Value = currAssetsOtherDiff;
                ew.Cells["F14"].Value = currAssetsOtherPercent / 100;
                ew.Cells["A14:F14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Current Assets";
                ew.Cells["B15"].Value = "";
                ew.Cells["C15"].Value = totalCurrAssetsCurrYear;
                ew.Cells["D15"].Value = totalCurrAssetsPrevYear;
                ew.Cells["E15"].Value = totalCurrAssetsDiff;
                ew.Cells["F15"].Value = totalCurrAssetsPer / 100;
                ew.Cells["A15:F15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT ASSETS
                ew.Cells["A17"].Value = "Noncurrent Assets";
                ew.Cells["A17"].Style.Font.Bold = true;

                ew.Cells["A18"].Value = "Financial Assets at FVOCI";
                ew.Cells["B18"].Value = 5;
                ew.Cells["C18"].Value = noncurrAssetsFinancialCurrentYear;
                ew.Cells["D18"].Value = noncurrAssetsFinancialPreviousYear;
                ew.Cells["E18"].Value = noncurrAssetsFinancialDiff;
                ew.Cells["F18"].Value = noncurrAssetsFinancialPercent / 100;

                ew.Cells["A19"].Value = "Restricted Cash";
                ew.Cells["B19"].Value = 6;
                ew.Cells["C19"].Value = noncurrAssetsRestrictedCashCurrentYear;
                ew.Cells["D19"].Value = noncurrAssetsRestrictedCashPreviousYear;
                ew.Cells["E19"].Value = noncurrAssetsRestrictedCashDiff;
                ew.Cells["F19"].Value = noncurrAssetsRestrictedCashPercent / 100;

                ew.Cells["A20"].Value = "ROU Asset";
                ew.Cells["B20"].Value = 7;
                ew.Cells["C20"].Value = noncurrAssetsROUAssetCurrentYear;
                ew.Cells["D20"].Value = noncurrAssetsROUAssetPreviousYear;
                ew.Cells["E20"].Value = noncurrAssetsROUAssetDiff;
                ew.Cells["F20"].Value = noncurrAssetsROUAssetPercent / 100;

                ew.Cells["A21"].Value = "Property and Equipment";
                ew.Cells["B21"].Value = 8;
                ew.Cells["C21"].Value = noncurrAssetsPropertyCurrentYear;
                ew.Cells["D21"].Value = noncurrAssetsPropertyPreviousYear;
                ew.Cells["E21"].Value = noncurrAssetsPropertyDiff;
                ew.Cells["F21"].Value = noncurrAssetsPropertyPercent / 100;

                ew.Cells["A22"].Value = "Other Noncurrent Assets";
                ew.Cells["B22"].Value = 9;
                ew.Cells["C22"].Value = noncurrAssetsOtherCurrentYear;
                ew.Cells["D22"].Value = noncurrAssetsOtherPreviousYear;
                ew.Cells["E22"].Value = noncurrAssetsOtherDiff;
                ew.Cells["F22"].Value = noncurrAssetsOtherPercent / 100;
                ew.Cells["A22:F22"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A23"].Value = "     Total Noncurrent Assets";
                ew.Cells["B23"].Value = "";
                ew.Cells["C23"].Value = totalNonCurrAssetsCurrYear;
                ew.Cells["D23"].Value = totalNonCurrAssetsPrevYear;
                ew.Cells["E23"].Value = totalNonCurrAssetsDiff;
                ew.Cells["F23"].Value = totalNonCurrAssetsPer / 100;
                ew.Cells["A23:F23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //TOTAL ASSETS
                ew.Cells["A25"].Value = "TOTAL ASSETS";
                ew.Cells["B25"].Value = "";
                ew.Cells["C25"].Value = Math.Round(totalAssetsCurrYear,0);
                ew.Cells["D25"].Value = Math.Round(totalAssetsPrevYear,0);
                ew.Cells["E25"].Value = Math.Round(totalAssetsDiff,0);
                ew.Cells["F25"].Value = totalAssetsPer / 100;
                ew.Cells["A25:F25"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A25:F25"].Style.Font.Bold = true;

                //LIABILITIES and FUND BALANCE
                ew.Cells["A28"].Value = "LIABILITIES AND FUND BALANCE";
                ew.Cells["A28"].Style.Font.Bold = true;

                ew.Cells["A30"].Value = "Current Liabilities";
                ew.Cells["A30"].Style.Font.Bold = true;

                //CURRENT LIABILITIES
                ew.Cells["A31"].Value = "Trade and Other Payables";
                ew.Cells["B31"].Value = 10;
                ew.Cells["C31"].Value = currLiaTradeCurrentYear;
                ew.Cells["D31"].Value = currLiaTradePreviousYear;
                ew.Cells["E31"].Value = currLiaTradeDiff;
                ew.Cells["F31"].Value = currLiaTradePercent / 100;

                ew.Cells["A32"].Value = "Unearned Interest Incomeh";
                ew.Cells["B32"].Value = 11;
                ew.Cells["C32"].Value = currLiaUnearnedCurrentYear;
                ew.Cells["D32"].Value = currLiaUnearnedPreviousYear;
                ew.Cells["E32"].Value = currLiaUnearnedDiff;
                ew.Cells["F32"].Value = currLiaUnearnedPercent / 100;

                ew.Cells["A33"].Value = "Clients' CBU";
                ew.Cells["B33"].Value = 12;
                ew.Cells["C33"].Value = currLiaClientCBUCurrentYear;
                ew.Cells["D33"].Value = currLiaClientCBUPreviousYear;
                ew.Cells["E33"].Value = currLiaClientCBUDiff;
                ew.Cells["F33"].Value = currLiaClientCBUPercent / 100;

                ew.Cells["A34"].Value = "Tax Payable";
                ew.Cells["B34"].Value = 13;
                ew.Cells["C34"].Value = currLiaTaxPayableCurrentYear;
                ew.Cells["D34"].Value = currLiaTaxPayablePreviousYear;
                ew.Cells["E34"].Value = currLiaTaxPayableDiff;
                ew.Cells["F34"].Value = currLiaTaxPayablePercent / 100;

                ew.Cells["A35"].Value = "MI/CGLI Payables";
                ew.Cells["B35"].Value = 14;
                ew.Cells["C35"].Value = currLiaCGLIPayableCurrentYear;
                ew.Cells["D35"].Value = currLiaCGLIPayablePreviousYear;
                ew.Cells["E35"].Value = currLiaCGLIPayableDiff;
                ew.Cells["F35"].Value = currLiaCGLIPayablePercent / 100;

                ew.Cells["A36"].Value = "Provision for contingenciess";
                ew.Cells["B36"].Value = 15;
                ew.Cells["C36"].Value = currLiaProvisionalCurrentYear;
                ew.Cells["D36"].Value = currLiaProvisionalPreviousYear;
                ew.Cells["E36"].Value = currLiaProvisionalDiff;
                ew.Cells["F36"].Value = currLiaProvisionalPercent / 100;
                ew.Cells["A36:F36"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A37"].Value = "     Total Current Liabilities";
                ew.Cells["B37"].Value = "";
                ew.Cells["C37"].Value = totalCurrLiaCurrYear;
                ew.Cells["D37"].Value = totalCurrLiaPrevYear;
                ew.Cells["E37"].Value = totalCurrLiaDiff;
                ew.Cells["F37"].Value = totalCurrLiaPer / 100;
                ew.Cells["A37:F37"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT LIABILITIES
                ew.Cells["A39"].Value = "Noncurrent Liabilities";
                ew.Cells["A39"].Style.Font.Bold = true;

                ew.Cells["A40"].Value = "Lease Liability";
                ew.Cells["B40"].Value = 16;
                ew.Cells["C40"].Value = noncurrLiaLeaseCurrentYear;
                ew.Cells["D40"].Value = noncurrLiaLeasePreviousYear;
                ew.Cells["E40"].Value = noncurrLiaLeaseDiff;
                ew.Cells["F40"].Value = noncurrLiaLeasePercent / 100;

                ew.Cells["A41"].Value = "Retirement Benefit Liability";
                ew.Cells["B41"].Value = 17;
                ew.Cells["C41"].Value = noncurrLiaRetirementCurrentYear;
                ew.Cells["D41"].Value = noncurrLiaRetirementPreviousYear;
                ew.Cells["E41"].Value = noncurrLiaRetirementDiff;
                ew.Cells["F41"].Value = noncurrLiaRetirementPercent / 100;

                ew.Cells["A42"].Value = "Deferred Tax Liability";
                ew.Cells["B42"].Value = 18;
                ew.Cells["C42"].Value = noncurrLiaDeferredCurrentYear;
                ew.Cells["D42"].Value = noncurrLiaDeferredPreviousYear;
                ew.Cells["E42"].Value = noncurrLiaDeferredDiff;
                ew.Cells["F42"].Value = noncurrLiaDeferredPercent / 100;
                ew.Cells["A42:F42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A43"].Value = "     Total Noncurrent Liabilities";
                ew.Cells["B43"].Value = "";
                ew.Cells["C43"].Value = totalNonCurrLiaCurrYear;
                ew.Cells["D43"].Value = totalNonCurrLiaPrevYear;
                ew.Cells["E43"].Value = totalNonCurrLiaDiff;
                ew.Cells["F43"].Value = totalNonCurrLiaPer / 100;
                ew.Cells["A43:F43"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A44"].Value = "     Total Liabilities";
                ew.Cells["B44"].Value = "";
                ew.Cells["C44"].Value = totalLiaCurrYear;
                ew.Cells["D44"].Value = totalLiaPrevYear;
                ew.Cells["E44"].Value = totalLiaDiff;
                ew.Cells["F44"].Value = totalLiaPer / 100;
                ew.Cells["A44:F44"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //FUND BALANCE
                ew.Cells["A46"].Value = "Fund Balance";
                ew.Cells["A46"].Style.Font.Bold = true;

                ew.Cells["A47"].Value = "Retained Earnings";
                ew.Cells["B47"].Value = 19;
                ew.Cells["C47"].Value = fundRetainedCurrentYear;
                ew.Cells["D47"].Value = fundRetainedPreviousYear;
                ew.Cells["E47"].Value = fundRetainedDiff;
                ew.Cells["F47"].Value = fundRetainedPercent / 100;

                ew.Cells["A48"].Value = "Cumulative remeasurement gains on";
                ew.Cells["A49"].Value = "     retirement benefit liability";
                ew.Cells["B49"].Value = 20;
                ew.Cells["C49"].Value = fundCommulativeCurrentYear;
                ew.Cells["D49"].Value = fundCommulativePreviousYear;
                ew.Cells["E49"].Value = fundCommulativeDiff;
                ew.Cells["F49"].Value = fundCommulativePercent / 100;

                ew.Cells["A50"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                ew.Cells["B50"].Value = 21;
                ew.Cells["C50"].Value = fundFairValueCurrentYear;
                ew.Cells["D50"].Value = fundFairValuePreviousYear;
                ew.Cells["E50"].Value = fundFairValueDiff;
                ew.Cells["F50"].Value = fundFairValuePercent / 100;
                ew.Cells["A50:F50"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A51"].Value = "     Total Fund Balance";
                ew.Cells["B51"].Value = "";
                ew.Cells["C51"].Value = totalFundCurrYear;
                ew.Cells["D51"].Value = totalFundPrevYear;
                ew.Cells["E51"].Value = totalFundDiff;
                ew.Cells["F51"].Value = totalFundPer / 100;
                ew.Cells["A51:F51"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A53"].Value = "TOTAL LIABILITIES AND FUND BALANCE";
                ew.Cells["B53"].Value = "";
                ew.Cells["C53"].Value = Math.Round(totalFundLiaCurrYear,0);
                ew.Cells["D53"].Value = Math.Round(totalFundLiaPrevYear,0);
                ew.Cells["E53"].Value = Math.Round(totalFundLiaDiff,0);
                ew.Cells["F53"].Value = totalFundLiaPer / 100;
                ew.Cells["A53:F53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:F53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();
                ew.Column(6).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "BS" + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult ISXLS(string month, int year)
        {
            var model = GetIncomeStatement(month, year);
            var prevYear = year - 1;

            //REVENUE
            decimal revIntLoansCurrentYear = 0;
            decimal revIntLoansPreviousYear = 0;
            decimal revIntLoansDiff = 0;
            decimal revIntLoansPercent = 0;

            decimal revServiceIncomeCurrentYear = 0;
            decimal revServiceIncomePreviousYear = 0;
            decimal revServiceIncomeDiff = 0;
            decimal revServiceIncomePercent = 0;

            decimal revIntDepositsCurrentYear = 0;
            decimal revIntDepositsPreviousYear = 0;
            decimal revIntDepositsDiff = 0;
            decimal revIntDepositsPercent = 0;

            decimal revForexCurrentYear = 0;
            decimal revForexPreviousYear = 0;
            decimal revForexDiff = 0;
            decimal revForexPercent = 0;

            decimal revOtherCurrentYear = 0;
            decimal revOtherPreviousYear = 0;
            decimal revOtherDiff = 0;
            decimal revOtherPercent = 0;

            decimal revIntExpenseCBUCurrentYear = 0;
            decimal revIntExpenseCBUPreviousYear = 0;
            decimal revIntExpenseCBUDiff = 0;
            decimal revIntExpenseCBUPercent = 0;

            decimal totGrossIncomeCurrYear = 0;
            decimal totGrossIncomePrevYear = 0;
            decimal totGrossIncomeDiff = 0;
            decimal totGrossIncomePer = 0;

            decimal revIntExpenseCurrentYear = 0;
            decimal revIntExpensePreviousYear = 0;
            decimal revIntExpenseDiff = 0;
            decimal revIntExpensePercent = 0;

            decimal totFinancialCostCurrYear = 0;
            decimal totFinancialCostPrevYear = 0;
            decimal totFinancialCostDiff = 0;
            decimal totFinancialCostPer = 0;

            decimal totProvisionLoanCurrYear = 0;
            decimal totProvisionLoanPrevYear = 0;
            decimal totProvisionLoanDiff = 0;
            decimal totProvisionLoanPer = 0;

            decimal revProvisionCurrentYear = 0;
            decimal revProvisionPreviousYear = 0;
            decimal revProvisionDiff = 0;
            decimal revProvisionPercent = 0;

            decimal totNetMarginCurrYear = 0;
            decimal totNetMarginPrevYear = 0;
            decimal totNetMarginDiff = 0;
            decimal totNetMarginPer = 0;

            //EXPENSES
            decimal expPersonnelCurrentYear = 0;
            decimal expPersonnelPreviousYear = 0;
            decimal expPersonnelDiff = 0;
            decimal expPersonnelPercent = 0;

            decimal expOutstationCurrentYear = 0;
            decimal expOutstationPreviousYear = 0;
            decimal expOutstationDiff = 0;
            decimal expOutstationPercent = 0;

            decimal expFringeCurrentYear =0;
            decimal expFringePreviousYear =0;
            decimal expFringeDiff =0;
            decimal expFringePercent =0;

            decimal expRetirementCurrentYear =0;
            decimal expRetirementPreviousYear =0;
            decimal expRetirementDiff =0;
            decimal expRetirementPercent =0;

            decimal expOutsideCurrentYear =0;
            decimal expOutsidePreviousYear =0;
            decimal expOutsideDiff =0;
            decimal expOutsidePercent =0;

            decimal expProfFeesCurrentYear =0;
            decimal expProfFeesPreviousYear =0;
            decimal expProfFeesDiff =0;
            decimal expProfFeesPercent =0;

            decimal expMeetingsCurrentYear =0;
            decimal expMeetingsPreviousYear =0;
            decimal expMeetingsDiff =0;
            decimal expMeetingsPercent =0;

            decimal expClientCurrentYear =0;
            decimal expClientPreviousYear =0;
            decimal expClientDiff =0;
            decimal expClientPercent =0;

            decimal expLitigationCurrentYear =0;
            decimal expLitigationPreviousYear =0;
            decimal expLitigationDiff =0;
            decimal expLitigationPercent =0;

            decimal expSuppliesCurrentYear =0;
            decimal expSuppliesPreviousYear =0;
            decimal expSuppliesDiff =0;
            decimal expSuppliesPercent =0;

            decimal expUtilitiesCurrentYear =0;
            decimal expUtilitiesPreviousYear =0;
            decimal expUtilitiesDiff =0;
            decimal expUtilitiesPercent =0;

            decimal expCommunicationCurrentYear =0;
            decimal expCommunicationPreviousYear =0;
            decimal expCommunicationDiff =0;
            decimal expCommunicationPercent =0;

            decimal expRentalCurrentYear =0;
            decimal expRentalPreviousYear =0;
            decimal expRentalDiff =0;
            decimal expRentalPercent =0;

            decimal expInsuranceCurrentYear =0;
            decimal expInsurancePreviousYear =0;
            decimal expInsuranceDiff =0;
            decimal expInsurancePercent =0;

            decimal expTranspoCurrentYear =0;
            decimal expTranspoPreviousYear =0;
            decimal expTranspoDiff =0;
            decimal expTranspoPercent =0;

            decimal expTaxesCurrentYear =0;
            decimal expTaxesPreviousYear =0;
            decimal expTaxesDiff =0;
            decimal expTaxesPercent =0;

            decimal expRepairsCurrentYear =0;
            decimal expRepairsPreviousYear =0;
            decimal expRepairsDiff =0;
            decimal expRepairsPercent =0;

            decimal expDepreciationCurrentYear =0;
            decimal expDepreciationPreviousYear =0;
            decimal expDepreciationDiff =0;
            decimal expDepreciationPercent =0;

            decimal expProvisionCurrentYear =0;
            decimal expProvisionPreviousYear =0;
            decimal expProvisionDiff =0;
            decimal expProvisionPercent =0;

            decimal expOthersCurrentYear =0;
            decimal expOthersPreviousYear =0;
            decimal expOthersDiff =0;
            decimal expOthersPercent =0;

            //INCOME TAX

            decimal incomeTaxExpenseCurrentYear =0;
            decimal incomeTaxExpensePreviousYear =0;
            decimal incomeTaxExpenseDiff =0;
            decimal incomeTaxExpensePercent =0;

            decimal totExpensesCurrYear = 0;
            decimal totExpensesPrevYear = 0;
            decimal totExpensesDiff = 0;
            decimal totExpensesPer = 0;

            decimal totIncomeCurrYear = 0;
            decimal totIncomePrevYear = 0;
            decimal totIncomeDiff = 0;
            decimal totIncomePer = 0;

            decimal totIncomeTaxCurrYear = 0;
            decimal totIncomeTaxPrevYear = 0;
            decimal totIncomeTaxDiff = 0;
            decimal totIncomeTaxPer = 0;

            decimal totNetIncomeCurrYear = 0;
            decimal totNetIncomePrevYear = 0;
            decimal totNetIncomeDiff = 0;
            decimal totNetIncomePer = 0;

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    if (r.particulars == "revIntLoans") {
                        revIntLoansCurrentYear=r.currAmount;
                        revIntLoansPreviousYear=r.prevAmount;
                        revIntLoansDiff=r.diff;
                        revIntLoansPercent=r.per;
                    }
                    if (r.particulars == "revServiceIncome") {
                        revServiceIncomeCurrentYear=r.currAmount;
                        revServiceIncomePreviousYear=r.prevAmount;
                        revServiceIncomeDiff=r.diff;
                        revServiceIncomePercent=r.per;
                    }
                    if (r.particulars == "revIntDeposits") {
                        revIntDepositsCurrentYear=r.currAmount;
                        revIntDepositsPreviousYear=r.prevAmount;
                        revIntDepositsDiff=r.diff;
                        revIntDepositsPercent=r.per;
                    }
                    if (r.particulars == "revForex") {
                        revForexCurrentYear=r.currAmount;
                        revForexPreviousYear=r.prevAmount;
                        revForexDiff=r.diff;
                        revForexPercent=r.per;
                    }
                    if (r.particulars == "revOther") {
                        revOtherCurrentYear=r.currAmount;
                        revOtherPreviousYear=r.prevAmount;
                        revOtherDiff=r.diff;
                        revOtherPercent=r.per;
                    }
                    if (r.accntType == "GrossIncome") {
                        totGrossIncomeCurrYear += r.currAmount;
                        totGrossIncomePrevYear += r.prevAmount;
                        totGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense") {
                        revIntExpenseCurrentYear=r.currAmount;
                        revIntExpensePreviousYear=r.prevAmount;
                        revIntExpenseDiff=r.diff;
                        revIntExpensePercent=r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU") {
                        revIntExpenseCBUCurrentYear=r.currAmount;
                        revIntExpenseCBUPreviousYear=r.prevAmount;
                        revIntExpenseCBUDiff=r.diff;
                        revIntExpenseCBUPercent=r.per;
                    }
                    if (r.accntType == "FinancialCost") {
                        totFinancialCostCurrYear += r.currAmount;
                        totFinancialCostPrevYear += r.prevAmount;
                        totFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision") {
                        revProvisionCurrentYear=r.currAmount;
                        revProvisionPreviousYear=r.prevAmount;
                        revProvisionDiff=r.diff;
                        revProvisionPercent=r.per;
                    }
                    if (r.accntType == "ProvisionLoan") {
                        totProvisionLoanCurrYear += r.currAmount;
                        totProvisionLoanPrevYear += r.prevAmount;
                        totProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel") {
                        expPersonnelCurrentYear=r.currAmount;
                        expPersonnelPreviousYear=r.prevAmount;
                        expPersonnelDiff=r.diff;
                        expPersonnelPercent=r.per;
                    }
                    if (r.particulars == "expOutstation") {
                        expOutstationCurrentYear=r.currAmount;
                        expOutstationPreviousYear=r.prevAmount;
                        expOutstationDiff=r.diff;
                        expOutstationPercent=r.per;
                    }
                    if (r.particulars == "expFringe") {
                        expFringeCurrentYear=r.currAmount;
                        expFringePreviousYear=r.prevAmount;
                        expFringeDiff=r.diff;
                        expFringePercent=r.per;
                    }
                    if (r.particulars == "expRetirement") {
                        expRetirementCurrentYear=r.currAmount;
                        expRetirementPreviousYear=r.prevAmount;
                        expRetirementDiff=r.diff;
                        expRetirementPercent=r.per;
                    }
                    if (r.particulars == "expOutside") {
                        expOutsideCurrentYear=r.currAmount;
                        expOutsidePreviousYear=r.prevAmount;
                        expOutsideDiff=r.diff;
                        expOutsidePercent=r.per;
                    }
                    if (r.particulars == "expProfFees") {
                        expProfFeesCurrentYear=r.currAmount;
                        expProfFeesPreviousYear=r.prevAmount;
                        expProfFeesDiff=r.diff;
                        expProfFeesPercent=r.per;
                    }
                    if (r.particulars == "expMeetings") {
                        expMeetingsCurrentYear=r.currAmount;
                        expMeetingsPreviousYear=r.prevAmount;
                        expMeetingsDiff=r.diff;
                        expMeetingsPercent=r.per;
                    }
                    if (r.particulars == "expClient") {
                        expClientCurrentYear=r.currAmount;
                        expClientPreviousYear=r.prevAmount;
                        expClientDiff=r.diff;
                        expClientPercent=r.per;
                    }
                    if (r.particulars == "expLitigation") {
                        expLitigationCurrentYear=r.currAmount;
                        expLitigationPreviousYear=r.prevAmount;
                        expLitigationDiff=r.diff;
                        expLitigationPercent=r.per;
                    }
                    if (r.particulars == "expSupplies") {
                        expSuppliesCurrentYear=r.currAmount;
                        expSuppliesPreviousYear=r.prevAmount;
                        expSuppliesDiff=r.diff;
                        expSuppliesPercent=r.per;
                    }
                    if (r.particulars == "expUtilities") {
                        expUtilitiesCurrentYear=r.currAmount;
                        expUtilitiesPreviousYear=r.prevAmount;
                        expUtilitiesDiff=r.diff;
                        expUtilitiesPercent=r.per;
                    }
                    if (r.particulars == "expCommunication") {
                        expCommunicationCurrentYear=r.currAmount;
                        expCommunicationPreviousYear=r.prevAmount;
                        expCommunicationDiff=r.diff;
                        expCommunicationPercent=r.per;
                    }
                    if (r.particulars == "expRental") {
                        expRentalCurrentYear=r.currAmount;
                        expRentalPreviousYear=r.prevAmount;
                        expRentalDiff=r.diff;
                        expRentalPercent=r.per;
                    }
                    if (r.particulars == "expInsurance") {
                        expInsuranceCurrentYear=r.currAmount;
                        expInsurancePreviousYear=r.prevAmount;
                        expInsuranceDiff=r.diff;
                        expInsurancePercent=r.per;
                    }
                    if (r.particulars == "expTranspo") {
                        expTranspoCurrentYear=r.currAmount;
                        expTranspoPreviousYear=r.prevAmount;
                        expTranspoDiff=r.diff;
                        expTranspoPercent=r.per;
                    }
                    if (r.particulars == "expTaxes") {
                        expTaxesCurrentYear=r.currAmount;
                        expTaxesPreviousYear=r.prevAmount;
                        expTaxesDiff=r.diff;
                        expTaxesPercent=r.per;
                    }
                    if (r.particulars == "expRepairs") {
                        expRepairsCurrentYear=r.currAmount;
                        expRepairsPreviousYear=r.prevAmount;
                        expRepairsDiff=r.diff;
                        expRepairsPercent=r.per;
                    }
                    if (r.particulars == "expDepreciation") {
                        expDepreciationCurrentYear=r.currAmount;
                        expDepreciationPreviousYear=r.prevAmount;
                        expDepreciationDiff=r.diff;
                        expDepreciationPercent=r.per;
                    }
                    if (r.particulars == "expProvision") {
                        expProvisionCurrentYear=r.currAmount;
                        expProvisionPreviousYear=r.prevAmount;
                        expProvisionDiff=r.diff;
                        expProvisionPercent=r.per;
                    }
                    if (r.particulars == "expOthers") {
                        expOthersCurrentYear=r.currAmount;
                        expOthersPreviousYear=r.prevAmount;
                        expOthersDiff=r.diff;
                        expOthersPercent=r.per;
                    }

                    if (r.accntType == "Expenses") {
                        totExpensesCurrYear += r.currAmount;
                        totExpensesPrevYear += r.prevAmount;
                        totExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense") {
                        incomeTaxExpenseCurrentYear=r.currAmount;
                        incomeTaxExpensePreviousYear=r.prevAmount;
                        incomeTaxExpenseDiff=r.diff;
                        incomeTaxExpensePercent=r.per;
                    }

                    if (r.accntType == "Income Tax") {
                        totIncomeTaxCurrYear += r.currAmount;
                        totIncomeTaxPrevYear += r.prevAmount;
                        totIncomeTaxDiff += r.diff;
                    }
                }
                totGrossIncomePer = (totGrossIncomeDiff / totGrossIncomePrevYear) * 100;
                totFinancialCostPer = (totFinancialCostDiff / totFinancialCostPrevYear) * 100;

                totNetMarginCurrYear = totGrossIncomeCurrYear - totFinancialCostCurrYear - totProvisionLoanCurrYear;
                totNetMarginPrevYear = totGrossIncomePrevYear - totFinancialCostPrevYear - totProvisionLoanPrevYear;
                totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                totNetMarginPer = (totNetMarginDiff / totNetMarginPrevYear) * 100;


                totExpensesPer = (totExpensesDiff / totExpensesPrevYear) * 100;
                //TOTAL INCOME
                totIncomeCurrYear = totNetMarginCurrYear - totExpensesCurrYear;
                totIncomePrevYear = totNetMarginPrevYear - totExpensesPrevYear;
                totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                totIncomePer = (totIncomeDiff / totIncomePrevYear * 100);

                //NET INCOME
                totNetIncomeCurrYear = totIncomeCurrYear - totIncomeTaxCurrYear;
                totNetIncomePrevYear = totIncomePrevYear - totIncomeTaxPrevYear;
                totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                totNetIncomePer = (totNetIncomeDiff / totNetIncomePrevYear) * 100;
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("IS" + month + year);
                //HEADER
                ew.Cells["A1:F1"].Merge = true;
                ew.Cells["A1:F1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:F1"].Style.Font.Bold = true;

                ew.Cells["A2:F2"].Merge = true;
                ew.Cells["A2:F2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:F2"].Style.Font.Bold = true;

                ew.Cells["A4:F4"].Merge = true;
                ew.Cells["A4:F4"].Value = "STATEMENT OF COMPREHENSIVE INCOME";
                ew.Cells["A4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:F4"].Style.Font.Bold = true;

                ew.Cells["A5:F5"].Merge = true;
                ew.Cells["A5:F5"].Value = "AS OF " + month + " " + year;
                ew.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:F5"].Style.Font.Bold = true;

                ew.Cells["A:F"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:F8"].Style.Font.Size = 12;
                ew.Cells["A8:F53"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B8"].Value = "NOTES";
                ew.Cells["C8"].Value = year;
                ew.Cells["D8"].Value = prevYear;
                ew.Cells["E8"].Value = "INCREASE (DECREASE)";
                ew.Cells["E8"].Style.WrapText = true;
                ew.Cells["F8"].Value = "%";
                ew.Cells["A8:F8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A8:F8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A8:F8"].Style.Font.Bold = true;

                ew.Cells["A9"].Value = "REVENUE";
                ew.Cells["A9"].Style.Font.Bold = true;

                ew.Cells["C10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["F10:F53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C10:E53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["F10:F53"].Style.Numberformat.Format = "0.00%_);(0.00%)";
                ew.Cells["B10:B53"].Style.Numberformat.Format = "0";
                ew.Cells["B10:B53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B10:B53"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                ew.Cells["A10"].Value = "Interest on Loans";
                ew.Cells["B10"].Value = 22;
                ew.Cells["C10"].Value = revIntLoansCurrentYear;
                ew.Cells["D10"].Value = revIntLoansPreviousYear;
                ew.Cells["E10"].Value = revIntLoansDiff;
                ew.Cells["F10"].Value = revIntLoansPercent / 100;

                ew.Cells["A11"].Value = "Service Income";
                ew.Cells["B11"].Value = 23;
                ew.Cells["C11"].Value = revServiceIncomeCurrentYear;
                ew.Cells["D11"].Value = revServiceIncomePreviousYear;
                ew.Cells["E11"].Value = revServiceIncomeDiff;
                ew.Cells["F11"].Value = revServiceIncomePercent / 100;

                ew.Cells["A12"].Value = "Interest on Deposit and Placements";
                ew.Cells["B12"].Value = 24;
                ew.Cells["C12"].Value = revIntDepositsCurrentYear;
                ew.Cells["D12"].Value = revIntDepositsPreviousYear;
                ew.Cells["E12"].Value = revIntDepositsDiff;
                ew.Cells["F12"].Value = revIntDepositsPercent / 100;

                ew.Cells["A13"].Value = "Forex Gain or Loss";
                ew.Cells["B13"].Value = 25;
                ew.Cells["C13"].Value = revForexCurrentYear;
                ew.Cells["D13"].Value = revForexPreviousYear;
                ew.Cells["E13"].Value = revForexDiff;
                ew.Cells["F13"].Value = revForexPercent / 100;

                ew.Cells["A14"].Value = "Other Income";
                ew.Cells["B14"].Value = 26;
                ew.Cells["C14"].Value = revOtherCurrentYear;
                ew.Cells["D14"].Value = revOtherPreviousYear;
                ew.Cells["E14"].Value = revOtherDiff;
                ew.Cells["F14"].Value = revOtherPercent / 100;
                ew.Cells["A14:F14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Gross Income";
                ew.Cells["B15"].Value = "";
                ew.Cells["C15"].Value = totGrossIncomeCurrYear;
                ew.Cells["D15"].Value = totGrossIncomePrevYear;
                ew.Cells["E15"].Value = totGrossIncomeDiff;
                ew.Cells["F15"].Value = totGrossIncomePer / 100;
                ew.Cells["A15:F15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

               
                ew.Cells["A17"].Value = "Interest Expense";
                ew.Cells["C17"].Value = 27;
                ew.Cells["D17"].Value = revIntExpenseCurrentYear;
                ew.Cells["D17"].Value = revIntExpensePreviousYear;
                ew.Cells["E17"].Value = revIntExpenseDiff;
                ew.Cells["F17"].Value = revIntExpensePercent / 100;

                ew.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                ew.Cells["B18"].Value = 28;
                ew.Cells["C18"].Value = revIntExpenseCBUCurrentYear;
                ew.Cells["D18"].Value = revIntExpenseCBUPreviousYear;
                ew.Cells["E18"].Value = revIntExpenseCBUDiff;
                ew.Cells["F18"].Value = revIntExpenseCBUPercent / 100;
                ew.Cells["A18:F18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A19"].Value = "     Total Financial Costs";
                ew.Cells["B19"].Value = "";
                ew.Cells["C19"].Value = totFinancialCostCurrYear;
                ew.Cells["D19"].Value = totFinancialCostPrevYear;
                ew.Cells["E19"].Value = totFinancialCostDiff;
                ew.Cells["F19"].Value = totFinancialCostPer / 100;
                ew.Cells["A19:F19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A21"].Value = "Provision for Loan Losses";
                ew.Cells["B21"].Value = 29;
                ew.Cells["C21"].Value = revProvisionCurrentYear;
                ew.Cells["D21"].Value = revProvisionPreviousYear;
                ew.Cells["E21"].Value = revProvisionDiff;
                ew.Cells["F21"].Value = revProvisionPercent / 100;
                ew.Cells["A21:F21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A23"].Value = "     Net Margin";
                ew.Cells["B23"].Value = "";
                ew.Cells["C23"].Value = totNetMarginCurrYear;
                ew.Cells["D23"].Value = totNetMarginPrevYear;
                ew.Cells["E23"].Value = totNetMarginDiff;
                ew.Cells["F23"].Value = totNetMarginPer / 100;
                ew.Cells["A23:F23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                ew.Cells["A25"].Value = "EXPENSES";
                ew.Cells["A25:F25"].Style.Font.Bold = true;

                
                ew.Cells["A26"].Value = "Personnel costs";
                ew.Cells["B26"].Value = 30;
                ew.Cells["C26"].Value = expPersonnelCurrentYear;
                ew.Cells["D26"].Value = expPersonnelPreviousYear;
                ew.Cells["E26"].Value = expPersonnelDiff;
                ew.Cells["F26"].Value = expPersonnelPercent / 100;

                ew.Cells["A27"].Value = "Outstation";
                ew.Cells["B27"].Value = 31;
                ew.Cells["C27"].Value = expOutstationCurrentYear;
                ew.Cells["D27"].Value = expOutstationPreviousYear;
                ew.Cells["E27"].Value = expOutstationDiff;
                ew.Cells["F27"].Value = expOutstationPercent / 100;

                ew.Cells["A28"].Value = "Fringe Benefit";
                ew.Cells["B28"].Value = 32;
                ew.Cells["C28"].Value = expFringeCurrentYear;
                ew.Cells["D28"].Value = expFringePreviousYear;
                ew.Cells["E28"].Value = expFringeDiff;
                ew.Cells["F28"].Value = expFringePercent / 100;

                ew.Cells["A29"].Value = "Retirement";
                ew.Cells["B29"].Value = 33;
                ew.Cells["C29"].Value = expRetirementCurrentYear;
                ew.Cells["D29"].Value = expRetirementPreviousYear;
                ew.Cells["E29"].Value = expRetirementDiff;
                ew.Cells["F29"].Value = expRetirementPercent / 100;

                ew.Cells["A30"].Value = "Outside Services";
                ew.Cells["B30"].Value = 34;
                ew.Cells["C30"].Value = expOutsideCurrentYear;
                ew.Cells["D30"].Value = expOutsidePreviousYear;
                ew.Cells["E30"].Value = expOutsideDiff;
                ew.Cells["F30"].Value = expOutsidePercent / 100;

                ew.Cells["A31"].Value = "Professional fees";
                ew.Cells["B31"].Value = 35;
                ew.Cells["C31"].Value = expProfFeesCurrentYear;
                ew.Cells["D31"].Value = expProfFeesPreviousYear;
                ew.Cells["E31"].Value = expProfFeesDiff;
                ew.Cells["F31"].Value = expProfFeesPercent / 100;

                ew.Cells["A32"].Value = "Meetings, Training and Conferences";
                ew.Cells["B32"].Value = 36;
                ew.Cells["C32"].Value = expMeetingsCurrentYear;
                ew.Cells["D32"].Value = expMeetingsPreviousYear;
                ew.Cells["E32"].Value = expMeetingsDiff;
                ew.Cells["F32"].Value = expMeetingsPercent / 100;

                ew.Cells["A33"].Value = "Client and Community Services";
                ew.Cells["B33"].Value = 37;
                ew.Cells["C33"].Value = expClientCurrentYear;
                ew.Cells["D33"].Value = expClientPreviousYear;
                ew.Cells["E33"].Value = expClientDiff;
                ew.Cells["F33"].Value = expClientPercent / 100;

                ew.Cells["A34"].Value = "Litigation";
                ew.Cells["B34"].Value = 38;
                ew.Cells["C34"].Value = expLitigationCurrentYear;
                ew.Cells["D34"].Value = expLitigationPreviousYear;
                ew.Cells["E34"].Value = expLitigationDiff;
                ew.Cells["F34"].Value = expLitigationPercent / 100;

                ew.Cells["A35"].Value = "Supplies";
                ew.Cells["B35"].Value = 39;
                ew.Cells["C35"].Value = expSuppliesCurrentYear;
                ew.Cells["D35"].Value = expSuppliesPreviousYear;
                ew.Cells["E35"].Value = expSuppliesDiff;
                ew.Cells["F35"].Value = expSuppliesPercent / 100;

                ew.Cells["A36"].Value = "Utilities";
                ew.Cells["B36"].Value = 40;
                ew.Cells["C36"].Value = expUtilitiesCurrentYear;
                ew.Cells["D36"].Value = expUtilitiesPreviousYear;
                ew.Cells["E36"].Value = expUtilitiesDiff;
                ew.Cells["F36"].Value = expUtilitiesPercent / 100;

                ew.Cells["A37"].Value = "Communication and Courier";
                ew.Cells["B37"].Value = 41;
                ew.Cells["C37"].Value = expCommunicationCurrentYear;
                ew.Cells["D37"].Value = expCommunicationPreviousYear;
                ew.Cells["E37"].Value = expCommunicationDiff;
                ew.Cells["F37"].Value = expCommunicationPercent / 100;

               
                ew.Cells["A38"].Value = "Rental";
                ew.Cells["B38"].Value = 42;
                ew.Cells["C38"].Value = expRentalCurrentYear;
                ew.Cells["D38"].Value = expRentalPreviousYear;
                ew.Cells["E38"].Value = expRentalDiff;
                ew.Cells["F38"].Value = expRentalPercent / 100;

                ew.Cells["A39"].Value = "Insurance";
                ew.Cells["B39"].Value = 43;
                ew.Cells["C39"].Value = expInsuranceCurrentYear;
                ew.Cells["D39"].Value = expInsurancePreviousYear;
                ew.Cells["E39"].Value = expInsuranceDiff;
                ew.Cells["F39"].Value = expInsurancePercent / 100;

                ew.Cells["A40"].Value = "Transportation and Travel";
                ew.Cells["B40"].Value = 44;
                ew.Cells["C40"].Value = expTranspoCurrentYear;
                ew.Cells["D40"].Value = expTranspoPreviousYear;
                ew.Cells["E40"].Value = expTranspoDiff;
                ew.Cells["F40"].Value = expTranspoPercent / 100;

                ew.Cells["A41"].Value = "Taxes and Licenses";
                ew.Cells["B41"].Value = 45;
                ew.Cells["C41"].Value = expTaxesCurrentYear;
                ew.Cells["D41"].Value = expTaxesPreviousYear;
                ew.Cells["E41"].Value = expTaxesDiff;
                ew.Cells["F41"].Value = expTaxesPercent / 100;

                ew.Cells["A42"].Value = "Repairs and Maintenance";
                ew.Cells["B42"].Value = 46;
                ew.Cells["C42"].Value = expRepairsCurrentYear;
                ew.Cells["D42"].Value = expRepairsPreviousYear;
                ew.Cells["E42"].Value = expRepairsDiff;
                ew.Cells["F42"].Value = expRepairsPercent / 100;

                ew.Cells["A43"].Value = "Depreciation and Amortization";
                ew.Cells["B43"].Value = 47;
                ew.Cells["C43"].Value = expDepreciationCurrentYear;
                ew.Cells["D43"].Value = expDepreciationPreviousYear;
                ew.Cells["E43"].Value = expDepreciationDiff;
                ew.Cells["F43"].Value = expDepreciationPercent / 100;

                ew.Cells["A44"].Value = "Provision for Other Losses";
                ew.Cells["B44"].Value = 48;
                ew.Cells["C44"].Value = expProvisionCurrentYear;
                ew.Cells["D44"].Value = expProvisionPreviousYear;
                ew.Cells["E44"].Value = expProvisionDiff;
                ew.Cells["F44"].Value = expProvisionPercent / 100;

                ew.Cells["A45"].Value = "Others";
                ew.Cells["B45"].Value = 49;
                ew.Cells["C45"].Value = expOthersCurrentYear;
                ew.Cells["D45"].Value = expOthersPreviousYear;
                ew.Cells["E45"].Value = expOthersDiff;
                ew.Cells["F45"].Value = expOthersPercent / 100;
                ew.Cells["A45:F45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A46"].Value = "     Total Expenses";
                ew.Cells["B46"].Value = "";
                ew.Cells["C46"].Value = totExpensesCurrYear;
                ew.Cells["D46"].Value = totExpensesPrevYear;
                ew.Cells["E46"].Value = totExpensesDiff;
                ew.Cells["F46"].Value = totExpensesPer / 100;
                ew.Cells["A46:F46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                ew.Cells["B48"].Value = "";
                ew.Cells["C48"].Value = totIncomeCurrYear;
                ew.Cells["D48"].Value = totIncomePrevYear;
                ew.Cells["E48"].Value = totIncomeDiff;
                ew.Cells["F48"].Value = totIncomePer / 100;
                ew.Cells["A48:F48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["A48:F48"].Style.Font.Bold = true;

                ew.Cells["A50"].Value = "INCOME TAX EXPENSE";
                ew.Cells["B50"].Value = 50;
                ew.Cells["C50"].Value = incomeTaxExpenseCurrentYear;
                ew.Cells["D50"].Value = incomeTaxExpensePreviousYear;
                ew.Cells["E50"].Value = incomeTaxExpenseDiff;
                ew.Cells["F50"].Value = incomeTaxExpensePercent / 100;
                ew.Cells["A50:F50"].Style.Font.Bold = true;

                ew.Cells["A53"].Value = "NET  INCOME (LOSS)";
                ew.Cells["B53"].Value = "";
                ew.Cells["C53"].Value = totNetIncomeCurrYear;
                ew.Cells["D53"].Value = totNetIncomePrevYear;
                ew.Cells["E53"].Value = totNetIncomeDiff;
                ew.Cells["F53"].Value = totNetIncomePer / 100;
                ew.Cells["A53:F53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:F53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ew.Cells["A53:F53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();
                ew.Column(6).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "IS" + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult REXLS(string month, int year)
        {

            #region

            var model = GetRetainedEarnings(month, year);
            var prevYear = year - 1;

            decimal initBalanceCurrentYear = 0;
            decimal initBalancePreviousYear = 0;
            decimal initNetIncomeCurrentYear = 0;
            decimal initNetIncomePreviousYear = 0;
            decimal cumuBalanceCurrentYear = 0;
            decimal cumuBalancePreviousYear = 0;
            decimal cumuRemeasureCurrentYear = 0;
            decimal cumuRemeasurePreviousYear = 0;
            decimal fairBalanceCurrentYear = 0;
            decimal fairBalancePreviousYear = 0;
            decimal fairFairValueCurrentYear = 0;
            decimal fairFairValuePreviousYear = 0;

            decimal initTotalCurrentYear = 0;
            decimal initTotalPreviousYear = 0;

            decimal cumuTotalCurrentYear = 0;
            decimal cumuTotalPreviousYear = 0;

            decimal fairTotalCurrentYear = 0;
            decimal fairTotalPreviousYear = 0;

            decimal TotalCurrentYear = 0;
            decimal TotalPreviousYear = 0;

            #endregion  

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    if (r.particulars == "initBalance")
                    {
                        initBalanceCurrentYear = r.currAmount;
                        initBalancePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "initNetIncome")
                    {
                        initNetIncomeCurrentYear = r.currAmount;
                        initNetIncomePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "cumuBalance")
                    {
                        cumuBalanceCurrentYear = r.currAmount;
                        cumuBalancePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "cumuRemeasure")
                    {
                        cumuRemeasureCurrentYear = r.currAmount;
                        cumuRemeasurePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "fairBalance")
                    {
                        fairBalanceCurrentYear = r.currAmount;
                        fairBalancePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "fairFairValue")
                    {
                        fairFairValueCurrentYear = r.currAmount;
                        fairFairValuePreviousYear = r.prevAmount;
                    }
                    if (r.accntType == "Initial")
                    {
                        initTotalCurrentYear += r.currAmount;
                        initTotalPreviousYear += r.prevAmount;
                    }
                    if (r.accntType == "Cumulative")
                    {
                        cumuTotalCurrentYear += r.currAmount;
                        cumuTotalPreviousYear += r.prevAmount;
                    }
                    if (r.accntType == "FairValue")
                    {
                        fairTotalCurrentYear += r.currAmount;
                        fairTotalPreviousYear += r.prevAmount;
                    }
                }
                TotalCurrentYear = initTotalCurrentYear + cumuTotalCurrentYear + fairTotalCurrentYear;
                TotalPreviousYear = initTotalPreviousYear + cumuTotalPreviousYear + fairTotalPreviousYear;
                //INIT
                
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("RE" + month + year);
                //HEADER
                ew.Cells["A1:D1"].Merge = true;
                ew.Cells["A1:D1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:D1"].Style.Font.Bold = true;

                ew.Cells["A2:D2"].Merge = true;
                ew.Cells["A2:D2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:D2"].Style.Font.Bold = true;

                ew.Cells["A4:D4"].Merge = true;
                ew.Cells["A4:D4"].Value = "STATEMENT OF CHANGES IN FUND BALANCE";
                ew.Cells["A4:D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:D4"].Style.Font.Bold = true;

                ew.Cells["A5:D5"].Merge = true;
                ew.Cells["A5:D5"].Value = "FOR THE YEAR ENDED " + month + " " + year;
                ew.Cells["A5:D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:D5"].Style.Font.Bold = true;

                ew.Cells["A6:D6"].Merge = true;
                ew.Cells["A6:D6"].Value = "(With Comparative Figures for " + prevYear + ")";
                ew.Cells["A6:D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A6:D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A6:D6"].Style.Font.Bold = true;

                ew.Cells["A:D"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:D7"].Style.Font.Size = 12;
                ew.Cells["A8:D41"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B8"].Value = "NOTE";
                ew.Cells["C8"].Value = year;
                ew.Cells["D8"].Value = prevYear;
               
                ew.Cells["A8:D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A8:D8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A8:D8"].Style.Font.Bold = true;
                ew.Cells["B11:B41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B11:B41"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["B11:B41"].Style.Numberformat.Format = "0";

                ew.Cells["A10"].Value = "Initial Contribution and Retained Earnings (Deficit)";
                ew.Cells["A10"].Style.Font.Bold = true;

                ew.Cells["C11:D41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["C11:D41"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* '-'??_);_(@_)";

                ew.Cells["A11"].Value = "Balance at Beginning of Year";
                ew.Cells["B11"].Value = "";
                ew.Cells["C11"].Value = initBalanceCurrentYear;
                ew.Cells["D11"].Value = initBalancePreviousYear;

                ew.Cells["A12"].Value = "Net Income (Loss)";
                ew.Cells["B12"].Value = "";
                ew.Cells["C12"].Value = initNetIncomeCurrentYear;
                ew.Cells["D12"].Value = initNetIncomePreviousYear;
                ew.Cells["A12:D12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A13"].Value = "Balance at End of Year";
                ew.Cells["B13"].Value = "";
                ew.Cells["C13"].Value = initTotalCurrentYear;
                ew.Cells["D13"].Value = initTotalPreviousYear;
                ew.Cells["A13:D13"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                ew.Cells["A15"].Value = "Cumulative Remeasurement Gains on Retirement Benefit Liability";
                ew.Cells["A15"].Style.Font.Bold = true;

                ew.Cells["A16"].Value = "Balance of Beginning of Year";
                ew.Cells["B16"].Value = "";
                ew.Cells["C16"].Value = cumuBalanceCurrentYear;
                ew.Cells["D16"].Value = cumuBalancePreviousYear;

                ew.Cells["A17"].Value = "Remeasurement Gains (Losses) for the Year";
                ew.Cells["B17"].Value = "";
                ew.Cells["C17"].Value = cumuRemeasureCurrentYear;
                ew.Cells["D17"].Value = cumuRemeasurePreviousYear;
                ew.Cells["A17:D17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A18"].Value = "Balance at End of Year";
                ew.Cells["B18"].Value = "";
                ew.Cells["C18"].Value = cumuTotalCurrentYear;
                ew.Cells["D18"].Value = cumuTotalPreviousYear;
                ew.Cells["A18:D18"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                ew.Cells["A20"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                ew.Cells["A20"].Style.Font.Bold = true;

                ew.Cells["A21"].Value = "Balance of Beginning of Year";
                ew.Cells["B21"].Value = "";
                ew.Cells["C21"].Value = fairBalanceCurrentYear;
                ew.Cells["D21"].Value = fairBalancePreviousYear;

                ew.Cells["A22"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                ew.Cells["B22"].Value = "";
                ew.Cells["C22"].Value = fairFairValueCurrentYear;
                ew.Cells["D22"].Value = fairFairValuePreviousYear;
                ew.Cells["A22:D22"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A23"].Value = "Balance at End of Year";
                ew.Cells["B23"].Value = "";
                ew.Cells["C23"].Value = fairTotalCurrentYear;
                ew.Cells["D23"].Value = fairTotalPreviousYear;
                ew.Cells["A23:D23"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                ew.Cells["A25"].Value = "";
                ew.Cells["B25"].Value = "";
                ew.Cells["C25"].Value = TotalCurrentYear;
                ew.Cells["D25"].Value = TotalPreviousYear;
                ew.Cells["A25:D25"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A25"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "RE" + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult MonthlyComparativeISXLS(string month, int year)
        {
            var model = GetMonthlyIncomeStatement(month, year);
            var prevYear = year - 1;
            var prevMonth = "";
            if (month == "JANUARY")
            {
                prevMonth = "DECEMBER";
            }
            else if (month == "FEBRUARY")
            {
                prevMonth = "JANUARY";
            }
            else if (month == "MARCH")
            {
                prevMonth = "FEBRUARY";
            }
            else if (month == "APRIL")
            {
                prevMonth = "MARCH";
            }
            else if (month == "MAY")
            {
                prevMonth = "APRIL";
            }
            else if (month == "JUNE")
            {
                prevMonth = "MAY";
            }
            else if (month == "JULY")
            {
                prevMonth = "JUNE";
            }
            else if (month == "AUGUST")
            {
                prevMonth = "JULY";
            }
            else if (month == "SEPTEMBER")
            {
                prevMonth = "AUGUST";
            }
            else if (month == "OCTOBER")
            {
                prevMonth = "SEPTEMBER";
            }
            else if (month == "NOVEMBER")
            {
                prevMonth = "OCTOBER";
            }
            else if (month == "DECEMBER")
            {
                prevMonth = "NOVEMBER";
            }

            //REVENUE
            decimal revIntLoansCurrentYear = 0;
            decimal revIntLoansPreviousYear = 0;
            decimal revIntLoansDiff = 0;
            decimal revIntLoansPercent = 0;

            decimal revServiceIncomeCurrentYear = 0;
            decimal revServiceIncomePreviousYear = 0;
            decimal revServiceIncomeDiff = 0;
            decimal revServiceIncomePercent = 0;

            decimal revIntDepositsCurrentYear = 0;
            decimal revIntDepositsPreviousYear = 0;
            decimal revIntDepositsDiff = 0;
            decimal revIntDepositsPercent = 0;

            decimal revForexCurrentYear = 0;
            decimal revForexPreviousYear = 0;
            decimal revForexDiff = 0;
            decimal revForexPercent = 0;

            decimal revOtherCurrentYear = 0;
            decimal revOtherPreviousYear = 0;
            decimal revOtherDiff = 0;
            decimal revOtherPercent = 0;

            decimal revIntExpenseCBUCurrentYear = 0;
            decimal revIntExpenseCBUPreviousYear = 0;
            decimal revIntExpenseCBUDiff = 0;
            decimal revIntExpenseCBUPercent = 0;

            decimal totGrossIncomeCurrYear = 0;
            decimal totGrossIncomePrevYear = 0;
            decimal totGrossIncomeDiff = 0;
            decimal totGrossIncomePer = 0;

            decimal revIntExpenseCurrentYear = 0;
            decimal revIntExpensePreviousYear = 0;
            decimal revIntExpenseDiff = 0;
            decimal revIntExpensePercent = 0;

            decimal totFinancialCostCurrYear = 0;
            decimal totFinancialCostPrevYear = 0;
            decimal totFinancialCostDiff = 0;
            decimal totFinancialCostPer = 0;

            decimal totProvisionLoanCurrYear = 0;
            decimal totProvisionLoanPrevYear = 0;
            decimal totProvisionLoanDiff = 0;
            decimal totProvisionLoanPer = 0;

            decimal revProvisionCurrentYear = 0;
            decimal revProvisionPreviousYear = 0;
            decimal revProvisionDiff = 0;
            decimal revProvisionPercent = 0;

            decimal totNetMarginCurrYear = 0;
            decimal totNetMarginPrevYear = 0;
            decimal totNetMarginDiff = 0;
            decimal totNetMarginPer = 0;

            //EXPENSES
            decimal expPersonnelCurrentYear = 0;
            decimal expPersonnelPreviousYear = 0;
            decimal expPersonnelDiff = 0;
            decimal expPersonnelPercent = 0;

            decimal expOutstationCurrentYear = 0;
            decimal expOutstationPreviousYear = 0;
            decimal expOutstationDiff = 0;
            decimal expOutstationPercent = 0;

            decimal expFringeCurrentYear = 0;
            decimal expFringePreviousYear = 0;
            decimal expFringeDiff = 0;
            decimal expFringePercent = 0;

            decimal expRetirementCurrentYear = 0;
            decimal expRetirementPreviousYear = 0;
            decimal expRetirementDiff = 0;
            decimal expRetirementPercent = 0;

            decimal expOutsideCurrentYear = 0;
            decimal expOutsidePreviousYear = 0;
            decimal expOutsideDiff = 0;
            decimal expOutsidePercent = 0;

            decimal expProfFeesCurrentYear = 0;
            decimal expProfFeesPreviousYear = 0;
            decimal expProfFeesDiff = 0;
            decimal expProfFeesPercent = 0;

            decimal expMeetingsCurrentYear = 0;
            decimal expMeetingsPreviousYear = 0;
            decimal expMeetingsDiff = 0;
            decimal expMeetingsPercent = 0;

            decimal expClientCurrentYear = 0;
            decimal expClientPreviousYear = 0;
            decimal expClientDiff = 0;
            decimal expClientPercent = 0;

            decimal expLitigationCurrentYear = 0;
            decimal expLitigationPreviousYear = 0;
            decimal expLitigationDiff = 0;
            decimal expLitigationPercent = 0;

            decimal expSuppliesCurrentYear = 0;
            decimal expSuppliesPreviousYear = 0;
            decimal expSuppliesDiff = 0;
            decimal expSuppliesPercent = 0;

            decimal expUtilitiesCurrentYear = 0;
            decimal expUtilitiesPreviousYear = 0;
            decimal expUtilitiesDiff = 0;
            decimal expUtilitiesPercent = 0;

            decimal expCommunicationCurrentYear = 0;
            decimal expCommunicationPreviousYear = 0;
            decimal expCommunicationDiff = 0;
            decimal expCommunicationPercent = 0;

            decimal expRentalCurrentYear = 0;
            decimal expRentalPreviousYear = 0;
            decimal expRentalDiff = 0;
            decimal expRentalPercent = 0;

            decimal expInsuranceCurrentYear = 0;
            decimal expInsurancePreviousYear = 0;
            decimal expInsuranceDiff = 0;
            decimal expInsurancePercent = 0;

            decimal expTranspoCurrentYear = 0;
            decimal expTranspoPreviousYear = 0;
            decimal expTranspoDiff = 0;
            decimal expTranspoPercent = 0;

            decimal expTaxesCurrentYear = 0;
            decimal expTaxesPreviousYear = 0;
            decimal expTaxesDiff = 0;
            decimal expTaxesPercent = 0;

            decimal expRepairsCurrentYear = 0;
            decimal expRepairsPreviousYear = 0;
            decimal expRepairsDiff = 0;
            decimal expRepairsPercent = 0;

            decimal expDepreciationCurrentYear = 0;
            decimal expDepreciationPreviousYear = 0;
            decimal expDepreciationDiff = 0;
            decimal expDepreciationPercent = 0;

            decimal expProvisionCurrentYear = 0;
            decimal expProvisionPreviousYear = 0;
            decimal expProvisionDiff = 0;
            decimal expProvisionPercent = 0;

            decimal expOthersCurrentYear = 0;
            decimal expOthersPreviousYear = 0;
            decimal expOthersDiff = 0;
            decimal expOthersPercent = 0;

            //INCOME TAX

            decimal incomeTaxExpenseCurrentYear = 0;
            decimal incomeTaxExpensePreviousYear = 0;
            decimal incomeTaxExpenseDiff = 0;
            decimal incomeTaxExpensePercent = 0;

            decimal totExpensesCurrYear = 0;
            decimal totExpensesPrevYear = 0;
            decimal totExpensesDiff = 0;
            decimal totExpensesPer = 0;

            decimal totIncomeCurrYear = 0;
            decimal totIncomePrevYear = 0;
            decimal totIncomeDiff = 0;
            decimal totIncomePer = 0;

            decimal totIncomeTaxCurrYear = 0;
            decimal totIncomeTaxPrevYear = 0;
            decimal totIncomeTaxDiff = 0;
            decimal totIncomeTaxPer = 0;

            decimal totNetIncomeCurrYear = 0;
            decimal totNetIncomePrevYear = 0;
            decimal totNetIncomeDiff = 0;
            decimal totNetIncomePer = 0;

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        revIntLoansCurrentYear = r.currAmount;
                        revIntLoansPreviousYear = r.prevAmount;
                        revIntLoansDiff = r.diff;
                        revIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        revServiceIncomeCurrentYear = r.currAmount;
                        revServiceIncomePreviousYear = r.prevAmount;
                        revServiceIncomeDiff = r.diff;
                        revServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        revIntDepositsCurrentYear = r.currAmount;
                        revIntDepositsPreviousYear = r.prevAmount;
                        revIntDepositsDiff = r.diff;
                        revIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        revForexCurrentYear = r.currAmount;
                        revForexPreviousYear = r.prevAmount;
                        revForexDiff = r.diff;
                        revForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        revOtherCurrentYear = r.currAmount;
                        revOtherPreviousYear = r.prevAmount;
                        revOtherDiff = r.diff;
                        revOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        totGrossIncomeCurrYear += r.currAmount;
                        totGrossIncomePrevYear += r.prevAmount;
                        totGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        revIntExpenseCurrentYear = r.currAmount;
                        revIntExpensePreviousYear = r.prevAmount;
                        revIntExpenseDiff = r.diff;
                        revIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        revIntExpenseCBUCurrentYear = r.currAmount;
                        revIntExpenseCBUPreviousYear = r.prevAmount;
                        revIntExpenseCBUDiff = r.diff;
                        revIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        totFinancialCostCurrYear += r.currAmount;
                        totFinancialCostPrevYear += r.prevAmount;
                        totFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        revProvisionCurrentYear = r.currAmount;
                        revProvisionPreviousYear = r.prevAmount;
                        revProvisionDiff = r.diff;
                        revProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        totProvisionLoanCurrYear += r.currAmount;
                        totProvisionLoanPrevYear += r.prevAmount;
                        totProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        expPersonnelCurrentYear = r.currAmount;
                        expPersonnelPreviousYear = r.prevAmount;
                        expPersonnelDiff = r.diff;
                        expPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        expOutstationCurrentYear = r.currAmount;
                        expOutstationPreviousYear = r.prevAmount;
                        expOutstationDiff = r.diff;
                        expOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        expFringeCurrentYear = r.currAmount;
                        expFringePreviousYear = r.prevAmount;
                        expFringeDiff = r.diff;
                        expFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        expRetirementCurrentYear = r.currAmount;
                        expRetirementPreviousYear = r.prevAmount;
                        expRetirementDiff = r.diff;
                        expRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        expOutsideCurrentYear = r.currAmount;
                        expOutsidePreviousYear = r.prevAmount;
                        expOutsideDiff = r.diff;
                        expOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        expProfFeesCurrentYear = r.currAmount;
                        expProfFeesPreviousYear = r.prevAmount;
                        expProfFeesDiff = r.diff;
                        expProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        expMeetingsCurrentYear = r.currAmount;
                        expMeetingsPreviousYear = r.prevAmount;
                        expMeetingsDiff = r.diff;
                        expMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        expClientCurrentYear = r.currAmount;
                        expClientPreviousYear = r.prevAmount;
                        expClientDiff = r.diff;
                        expClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        expLitigationCurrentYear = r.currAmount;
                        expLitigationPreviousYear = r.prevAmount;
                        expLitigationDiff = r.diff;
                        expLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        expSuppliesCurrentYear = r.currAmount;
                        expSuppliesPreviousYear = r.prevAmount;
                        expSuppliesDiff = r.diff;
                        expSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        expUtilitiesCurrentYear = r.currAmount;
                        expUtilitiesPreviousYear = r.prevAmount;
                        expUtilitiesDiff = r.diff;
                        expUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        expCommunicationCurrentYear = r.currAmount;
                        expCommunicationPreviousYear = r.prevAmount;
                        expCommunicationDiff = r.diff;
                        expCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        expRentalCurrentYear = r.currAmount;
                        expRentalPreviousYear = r.prevAmount;
                        expRentalDiff = r.diff;
                        expRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        expInsuranceCurrentYear = r.currAmount;
                        expInsurancePreviousYear = r.prevAmount;
                        expInsuranceDiff = r.diff;
                        expInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        expTranspoCurrentYear = r.currAmount;
                        expTranspoPreviousYear = r.prevAmount;
                        expTranspoDiff = r.diff;
                        expTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        expTaxesCurrentYear = r.currAmount;
                        expTaxesPreviousYear = r.prevAmount;
                        expTaxesDiff = r.diff;
                        expTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        expRepairsCurrentYear = r.currAmount;
                        expRepairsPreviousYear = r.prevAmount;
                        expRepairsDiff = r.diff;
                        expRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        expDepreciationCurrentYear = r.currAmount;
                        expDepreciationPreviousYear = r.prevAmount;
                        expDepreciationDiff = r.diff;
                        expDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        expProvisionCurrentYear = r.currAmount;
                        expProvisionPreviousYear = r.prevAmount;
                        expProvisionDiff = r.diff;
                        expProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        expOthersCurrentYear = r.currAmount;
                        expOthersPreviousYear = r.prevAmount;
                        expOthersDiff = r.diff;
                        expOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        totExpensesCurrYear += r.currAmount;
                        totExpensesPrevYear += r.prevAmount;
                        totExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        incomeTaxExpenseCurrentYear = r.currAmount;
                        incomeTaxExpensePreviousYear = r.prevAmount;
                        incomeTaxExpenseDiff = r.diff;
                        incomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        totIncomeTaxCurrYear += r.currAmount;
                        totIncomeTaxPrevYear += r.prevAmount;
                        totIncomeTaxDiff += r.diff;
                    }
                }
                totGrossIncomePer = (totGrossIncomeDiff / totGrossIncomePrevYear) * 100;
                totFinancialCostPer = (totFinancialCostDiff / totFinancialCostPrevYear) * 100;

                totNetMarginCurrYear = totGrossIncomeCurrYear - totFinancialCostCurrYear - totProvisionLoanCurrYear;
                totNetMarginPrevYear = totGrossIncomePrevYear - totFinancialCostPrevYear - totProvisionLoanPrevYear;
                totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                totNetMarginPer = (totNetMarginDiff / totNetMarginPrevYear) * 100;


                totExpensesPer = (totExpensesDiff / totExpensesPrevYear) * 100;
                //TOTAL INCOME
                totIncomeCurrYear = totNetMarginCurrYear - totExpensesCurrYear;
                totIncomePrevYear = totNetMarginPrevYear - totExpensesPrevYear;
                totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                totIncomePer = (totIncomeDiff / totIncomePrevYear * 100);

                //NET INCOME
                totNetIncomeCurrYear = totIncomeCurrYear - totIncomeTaxCurrYear;
                totNetIncomePrevYear = totIncomePrevYear - totIncomeTaxPrevYear;
                totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                totNetIncomePer = (totNetIncomeDiff / totNetIncomePrevYear) * 100;
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Comparative Monthly IS");
                //HEADER
                ew.Cells["A1:E1"].Merge = true;
                ew.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:E1"].Style.Font.Bold = true;

                ew.Cells["A2:E2"].Merge = true;
                ew.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:E2"].Style.Font.Bold = true;

                ew.Cells["A4:E4"].Merge = true;
                ew.Cells["A4:E4"].Value = "COMPARATIVE MONTHLY INCOME STATEMENT";
                ew.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:E4"].Style.Font.Bold = true;

                ew.Cells["A5:E5"].Merge = true;
                ew.Cells["A5:E5"].Value = "FOR THE MONTH ENDED " + month + " " + year;
                ew.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:E5"].Style.Font.Bold = true;

                ew.Cells["A:E"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:E8"].Style.Font.Size = 12;
                ew.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B8"].Value = month.ToUpper();
                ew.Cells["C8"].Value = prevMonth.ToUpper();
                ew.Cells["D8"].Value = "INCREASE (DECREASE)";
                ew.Cells["D8"].Style.WrapText = true;
                ew.Cells["E8"].Value = "%";
                ew.Cells["A8:E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A8:E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A8:E8"].Style.Font.Bold = true;

                ew.Cells["A9"].Value = "REVENUE";
                ew.Cells["A9"].Style.Font.Bold = true;

                ew.Cells["B10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["E10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B10:D53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["E10:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                ew.Cells["A10"].Value = "Interest on Loans";
                ew.Cells["B10"].Value = revIntLoansCurrentYear;
                ew.Cells["C10"].Value = revIntLoansPreviousYear;
                ew.Cells["D10"].Value = revIntLoansDiff;
                ew.Cells["E10"].Value = revIntLoansPercent / 100;

                ew.Cells["A11"].Value = "Service Income";
                ew.Cells["B11"].Value = revServiceIncomeCurrentYear;
                ew.Cells["C11"].Value = revServiceIncomePreviousYear;
                ew.Cells["D11"].Value = revServiceIncomeDiff;
                ew.Cells["E11"].Value = revServiceIncomePercent / 100;

                ew.Cells["A12"].Value = "Interest on Deposit and Placements";
                ew.Cells["B12"].Value = revIntDepositsCurrentYear;
                ew.Cells["C12"].Value = revIntDepositsPreviousYear;
                ew.Cells["D12"].Value = revIntDepositsDiff;
                ew.Cells["E12"].Value = revIntDepositsPercent / 100;

                ew.Cells["A13"].Value = "Forex Gain or Loss";
                ew.Cells["B13"].Value = revForexCurrentYear;
                ew.Cells["C13"].Value = revForexPreviousYear;
                ew.Cells["D13"].Value = revForexDiff;
                ew.Cells["E13"].Value = revForexPercent / 100;

                ew.Cells["A14"].Value = "Other Income";
                ew.Cells["B14"].Value = revOtherCurrentYear;
                ew.Cells["C14"].Value = revOtherPreviousYear;
                ew.Cells["D14"].Value = revOtherDiff;
                ew.Cells["E14"].Value = revOtherPercent / 100;
                ew.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Gross Income";
                ew.Cells["B15"].Value = totGrossIncomeCurrYear;
                ew.Cells["C15"].Value = totGrossIncomePrevYear;
                ew.Cells["D15"].Value = totGrossIncomeDiff;
                ew.Cells["E15"].Value = totGrossIncomePer / 100;
                ew.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A17"].Value = "Interest Expense";
                ew.Cells["B17"].Value = revIntExpenseCurrentYear;
                ew.Cells["C17"].Value = revIntExpensePreviousYear;
                ew.Cells["D17"].Value = revIntExpenseDiff;
                ew.Cells["E17"].Value = revIntExpensePercent / 100;

                ew.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                ew.Cells["B18"].Value = revIntExpenseCBUCurrentYear;
                ew.Cells["C18"].Value = revIntExpenseCBUPreviousYear;
                ew.Cells["D18"].Value = revIntExpenseCBUDiff;
                ew.Cells["E18"].Value = revIntExpenseCBUPercent / 100;
                ew.Cells["A18:E18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A19"].Value = "     Total Financial Costs";
                ew.Cells["B19"].Value = totFinancialCostCurrYear;
                ew.Cells["C19"].Value = totFinancialCostPrevYear;
                ew.Cells["D19"].Value = totFinancialCostDiff;
                ew.Cells["E19"].Value = totFinancialCostPer / 100;
                ew.Cells["A19:E19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A21"].Value = "Provision for Loan Losses";
                ew.Cells["B21"].Value = revProvisionCurrentYear;
                ew.Cells["C21"].Value = revProvisionPreviousYear;
                ew.Cells["D21"].Value = revProvisionDiff;
                ew.Cells["E21"].Value = revProvisionPercent / 100;
                ew.Cells["A21:E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A23"].Value = "     Net Margin";
                ew.Cells["B23"].Value = totNetMarginCurrYear;
                ew.Cells["C23"].Value = totNetMarginPrevYear;
                ew.Cells["D23"].Value = totNetMarginDiff;
                ew.Cells["E23"].Value = totNetMarginPer / 100;
                ew.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                ew.Cells["A25"].Value = "EXPENSES";
                ew.Cells["A25:E25"].Style.Font.Bold = true;


                ew.Cells["A26"].Value = "Personnel Costs";
                ew.Cells["B26"].Value = expPersonnelCurrentYear;
                ew.Cells["C26"].Value = expPersonnelPreviousYear;
                ew.Cells["D26"].Value = expPersonnelDiff;
                ew.Cells["E26"].Value = expPersonnelPercent / 100;

                ew.Cells["A27"].Value = "Outstation";
                ew.Cells["B27"].Value = expOutstationCurrentYear;
                ew.Cells["C27"].Value = expOutstationPreviousYear;
                ew.Cells["D27"].Value = expOutstationDiff;
                ew.Cells["E27"].Value = expOutstationPercent / 100;

                ew.Cells["A28"].Value = "Fringe Benefit";
                ew.Cells["B28"].Value = expFringeCurrentYear;
                ew.Cells["C28"].Value = expFringePreviousYear;
                ew.Cells["D28"].Value = expFringeDiff;
                ew.Cells["E28"].Value = expFringePercent / 100;

                ew.Cells["A29"].Value = "Retirement";
                ew.Cells["B29"].Value = expRetirementCurrentYear;
                ew.Cells["C29"].Value = expRetirementPreviousYear;
                ew.Cells["D29"].Value = expRetirementDiff;
                ew.Cells["E29"].Value = expRetirementPercent / 100;

                ew.Cells["A30"].Value = "Outside Services";
                ew.Cells["B30"].Value = expOutsideCurrentYear;
                ew.Cells["C30"].Value = expOutsidePreviousYear;
                ew.Cells["D30"].Value = expOutsideDiff;
                ew.Cells["E30"].Value = expOutsidePercent / 100;

                ew.Cells["A31"].Value = "Professional fees";
                ew.Cells["B31"].Value = expProfFeesCurrentYear;
                ew.Cells["C31"].Value = expProfFeesPreviousYear;
                ew.Cells["D31"].Value = expProfFeesDiff;
                ew.Cells["E31"].Value = expProfFeesPercent / 100;

                ew.Cells["A32"].Value = "Meetings, Training and Conferences";
                ew.Cells["B32"].Value = expMeetingsCurrentYear;
                ew.Cells["C32"].Value = expMeetingsPreviousYear;
                ew.Cells["D32"].Value = expMeetingsDiff;
                ew.Cells["E32"].Value = expMeetingsPercent / 100;

                ew.Cells["A33"].Value = "Client and Community Services";
                ew.Cells["B33"].Value = expClientCurrentYear;
                ew.Cells["C33"].Value = expClientPreviousYear;
                ew.Cells["D33"].Value = expClientDiff;
                ew.Cells["E33"].Value = expClientPercent / 100;

                ew.Cells["A34"].Value = "Litigation";
                ew.Cells["B34"].Value = expLitigationCurrentYear;
                ew.Cells["C34"].Value = expLitigationPreviousYear;
                ew.Cells["D34"].Value = expLitigationDiff;
                ew.Cells["E34"].Value = expLitigationPercent / 100;

                ew.Cells["A35"].Value = "Supplies";
                ew.Cells["B35"].Value = expSuppliesCurrentYear;
                ew.Cells["C35"].Value = expSuppliesPreviousYear;
                ew.Cells["D35"].Value = expSuppliesDiff;
                ew.Cells["E35"].Value = expSuppliesPercent / 100;

                ew.Cells["A36"].Value = "Utilities";
                ew.Cells["B36"].Value = expUtilitiesCurrentYear;
                ew.Cells["C36"].Value = expUtilitiesPreviousYear;
                ew.Cells["D36"].Value = expUtilitiesDiff;
                ew.Cells["E36"].Value = expUtilitiesPercent / 100;

                ew.Cells["A37"].Value = "Communication and Courier";
                ew.Cells["B37"].Value = expCommunicationCurrentYear;
                ew.Cells["C37"].Value = expCommunicationPreviousYear;
                ew.Cells["D37"].Value = expCommunicationDiff;
                ew.Cells["E37"].Value = expCommunicationPercent / 100;


                ew.Cells["A38"].Value = "Rental";
                ew.Cells["B38"].Value = expRentalCurrentYear;
                ew.Cells["C38"].Value = expRentalPreviousYear;
                ew.Cells["D38"].Value = expRentalDiff;
                ew.Cells["E38"].Value = expRentalPercent / 100;

                ew.Cells["A39"].Value = "Insurance";
                ew.Cells["B39"].Value = expInsuranceCurrentYear;
                ew.Cells["C39"].Value = expInsurancePreviousYear;
                ew.Cells["D39"].Value = expInsuranceDiff;
                ew.Cells["E39"].Value = expInsurancePercent / 100;

                ew.Cells["A40"].Value = "Transportation and Travel";
                ew.Cells["B40"].Value = expTranspoCurrentYear;
                ew.Cells["C40"].Value = expTranspoPreviousYear;
                ew.Cells["D40"].Value = expTranspoDiff;
                ew.Cells["E40"].Value = expTranspoPercent / 100;

                ew.Cells["A41"].Value = "Taxes and Licenses";
                ew.Cells["B41"].Value = expTaxesCurrentYear;
                ew.Cells["C41"].Value = expTaxesPreviousYear;
                ew.Cells["D41"].Value = expTaxesDiff;
                ew.Cells["E41"].Value = expTaxesPercent / 100;

                ew.Cells["A42"].Value = "Repairs and Maintenance";
                ew.Cells["B42"].Value = expRepairsCurrentYear;
                ew.Cells["C42"].Value = expRepairsPreviousYear;
                ew.Cells["D42"].Value = expRepairsDiff;
                ew.Cells["E42"].Value = expRepairsPercent / 100;

                ew.Cells["A43"].Value = "Depreciation and Amortization";
                ew.Cells["B43"].Value = expDepreciationCurrentYear;
                ew.Cells["C43"].Value = expDepreciationPreviousYear;
                ew.Cells["D43"].Value = expDepreciationDiff;
                ew.Cells["E43"].Value = expDepreciationPercent / 100;

                ew.Cells["A44"].Value = "Provision for Other Losses";
                ew.Cells["B44"].Value = expProvisionCurrentYear;
                ew.Cells["C44"].Value = expProvisionPreviousYear;
                ew.Cells["D44"].Value = expProvisionDiff;
                ew.Cells["E44"].Value = expProvisionPercent / 100;

                ew.Cells["A45"].Value = "Others";
                ew.Cells["B45"].Value = expOthersCurrentYear;
                ew.Cells["C45"].Value = expOthersPreviousYear;
                ew.Cells["D45"].Value = expOthersDiff;
                ew.Cells["E45"].Value = expOthersPercent / 100;
                ew.Cells["A45:E45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A46"].Value = "     Total Expenses";
                ew.Cells["B46"].Value = totExpensesCurrYear;
                ew.Cells["C46"].Value = totExpensesPrevYear;
                ew.Cells["D46"].Value = totExpensesDiff;
                ew.Cells["E46"].Value = totExpensesPer / 100;
                ew.Cells["A46:E46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                ew.Cells["B48"].Value = totIncomeCurrYear;
                ew.Cells["C48"].Value = totIncomePrevYear;
                ew.Cells["D48"].Value = totIncomeDiff;
                ew.Cells["E48"].Value = totIncomePer / 100;
                ew.Cells["A48:E48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["A48:E48"].Style.Font.Bold = true;

                ew.Cells["A50"].Value = "INCOME TAX EXPENSE";
                ew.Cells["B50"].Value = incomeTaxExpenseCurrentYear;
                ew.Cells["C50"].Value = incomeTaxExpensePreviousYear;
                ew.Cells["D50"].Value = incomeTaxExpenseDiff;
                ew.Cells["E50"].Value = incomeTaxExpensePercent / 100;
                ew.Cells["A50:E50"].Style.Font.Bold = true;

                ew.Cells["A53"].Value = "NET  INCOME (LOSS)";
                ew.Cells["B53"].Value = totNetIncomeCurrYear;
                ew.Cells["C53"].Value = totNetIncomePrevYear;
                ew.Cells["D53"].Value = totNetIncomeDiff;
                ew.Cells["E53"].Value = totNetIncomePer / 100;
                ew.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:E53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ew.Cells["A53:E53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "Comparative Monthly IS - " + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult MonthlyISBudgetXLS(string month, int year)
        {
            var model = GetMonthlyIncomeStatementVSBudget(month, year);
            var prevYear = year - 1;

            //REVENUE
            decimal revIntLoansCurrentYear = 0;
            decimal revIntLoansPreviousYear = 0;
            decimal revIntLoansDiff = 0;
            decimal revIntLoansPercent = 0;

            decimal revServiceIncomeCurrentYear = 0;
            decimal revServiceIncomePreviousYear = 0;
            decimal revServiceIncomeDiff = 0;
            decimal revServiceIncomePercent = 0;

            decimal revIntDepositsCurrentYear = 0;
            decimal revIntDepositsPreviousYear = 0;
            decimal revIntDepositsDiff = 0;
            decimal revIntDepositsPercent = 0;

            decimal revForexCurrentYear = 0;
            decimal revForexPreviousYear = 0;
            decimal revForexDiff = 0;
            decimal revForexPercent = 0;

            decimal revOtherCurrentYear = 0;
            decimal revOtherPreviousYear = 0;
            decimal revOtherDiff = 0;
            decimal revOtherPercent = 0;

            decimal revIntExpenseCBUCurrentYear = 0;
            decimal revIntExpenseCBUPreviousYear = 0;
            decimal revIntExpenseCBUDiff = 0;
            decimal revIntExpenseCBUPercent = 0;

            decimal totGrossIncomeCurrYear = 0;
            decimal totGrossIncomePrevYear = 0;
            decimal totGrossIncomeDiff = 0;
            decimal totGrossIncomePer = 0;

            decimal revIntExpenseCurrentYear = 0;
            decimal revIntExpensePreviousYear = 0;
            decimal revIntExpenseDiff = 0;
            decimal revIntExpensePercent = 0;

            decimal totFinancialCostCurrYear = 0;
            decimal totFinancialCostPrevYear = 0;
            decimal totFinancialCostDiff = 0;
            decimal totFinancialCostPer = 0;

            decimal totProvisionLoanCurrYear = 0;
            decimal totProvisionLoanPrevYear = 0;
            decimal totProvisionLoanDiff = 0;
            decimal totProvisionLoanPer = 0;

            decimal revProvisionCurrentYear = 0;
            decimal revProvisionPreviousYear = 0;
            decimal revProvisionDiff = 0;
            decimal revProvisionPercent = 0;

            decimal totNetMarginCurrYear = 0;
            decimal totNetMarginPrevYear = 0;
            decimal totNetMarginDiff = 0;
            decimal totNetMarginPer = 0;

            //EXPENSES
            decimal expPersonnelCurrentYear = 0;
            decimal expPersonnelPreviousYear = 0;
            decimal expPersonnelDiff = 0;
            decimal expPersonnelPercent = 0;

            decimal expOutstationCurrentYear = 0;
            decimal expOutstationPreviousYear = 0;
            decimal expOutstationDiff = 0;
            decimal expOutstationPercent = 0;

            decimal expFringeCurrentYear = 0;
            decimal expFringePreviousYear = 0;
            decimal expFringeDiff = 0;
            decimal expFringePercent = 0;

            decimal expRetirementCurrentYear = 0;
            decimal expRetirementPreviousYear = 0;
            decimal expRetirementDiff = 0;
            decimal expRetirementPercent = 0;

            decimal expOutsideCurrentYear = 0;
            decimal expOutsidePreviousYear = 0;
            decimal expOutsideDiff = 0;
            decimal expOutsidePercent = 0;

            decimal expProfFeesCurrentYear = 0;
            decimal expProfFeesPreviousYear = 0;
            decimal expProfFeesDiff = 0;
            decimal expProfFeesPercent = 0;

            decimal expMeetingsCurrentYear = 0;
            decimal expMeetingsPreviousYear = 0;
            decimal expMeetingsDiff = 0;
            decimal expMeetingsPercent = 0;

            decimal expClientCurrentYear = 0;
            decimal expClientPreviousYear = 0;
            decimal expClientDiff = 0;
            decimal expClientPercent = 0;

            decimal expLitigationCurrentYear = 0;
            decimal expLitigationPreviousYear = 0;
            decimal expLitigationDiff = 0;
            decimal expLitigationPercent = 0;

            decimal expSuppliesCurrentYear = 0;
            decimal expSuppliesPreviousYear = 0;
            decimal expSuppliesDiff = 0;
            decimal expSuppliesPercent = 0;

            decimal expUtilitiesCurrentYear = 0;
            decimal expUtilitiesPreviousYear = 0;
            decimal expUtilitiesDiff = 0;
            decimal expUtilitiesPercent = 0;

            decimal expCommunicationCurrentYear = 0;
            decimal expCommunicationPreviousYear = 0;
            decimal expCommunicationDiff = 0;
            decimal expCommunicationPercent = 0;

            decimal expRentalCurrentYear = 0;
            decimal expRentalPreviousYear = 0;
            decimal expRentalDiff = 0;
            decimal expRentalPercent = 0;

            decimal expInsuranceCurrentYear = 0;
            decimal expInsurancePreviousYear = 0;
            decimal expInsuranceDiff = 0;
            decimal expInsurancePercent = 0;

            decimal expTranspoCurrentYear = 0;
            decimal expTranspoPreviousYear = 0;
            decimal expTranspoDiff = 0;
            decimal expTranspoPercent = 0;

            decimal expTaxesCurrentYear = 0;
            decimal expTaxesPreviousYear = 0;
            decimal expTaxesDiff = 0;
            decimal expTaxesPercent = 0;

            decimal expRepairsCurrentYear = 0;
            decimal expRepairsPreviousYear = 0;
            decimal expRepairsDiff = 0;
            decimal expRepairsPercent = 0;

            decimal expDepreciationCurrentYear = 0;
            decimal expDepreciationPreviousYear = 0;
            decimal expDepreciationDiff = 0;
            decimal expDepreciationPercent = 0;

            decimal expProvisionCurrentYear = 0;
            decimal expProvisionPreviousYear = 0;
            decimal expProvisionDiff = 0;
            decimal expProvisionPercent = 0;

            decimal expOthersCurrentYear = 0;
            decimal expOthersPreviousYear = 0;
            decimal expOthersDiff = 0;
            decimal expOthersPercent = 0;

            //INCOME TAX

            decimal incomeTaxExpenseCurrentYear = 0;
            decimal incomeTaxExpensePreviousYear = 0;
            decimal incomeTaxExpenseDiff = 0;
            decimal incomeTaxExpensePercent = 0;

            decimal totExpensesCurrYear = 0;
            decimal totExpensesPrevYear = 0;
            decimal totExpensesDiff = 0;
            decimal totExpensesPer = 0;

            decimal totIncomeCurrYear = 0;
            decimal totIncomePrevYear = 0;
            decimal totIncomeDiff = 0;
            decimal totIncomePer = 0;

            decimal totIncomeTaxCurrYear = 0;
            decimal totIncomeTaxPrevYear = 0;
            decimal totIncomeTaxDiff = 0;
            decimal totIncomeTaxPer = 0;

            decimal totNetIncomeCurrYear = 0;
            decimal totNetIncomePrevYear = 0;
            decimal totNetIncomeDiff = 0;
            decimal totNetIncomePer = 0;

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        revIntLoansCurrentYear = r.budgetMonthly;
                        revIntLoansPreviousYear = r.actual;
                        revIntLoansDiff = r.diff;
                        revIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        revServiceIncomeCurrentYear = r.budgetMonthly;
                        revServiceIncomePreviousYear = r.actual;
                        revServiceIncomeDiff = r.diff;
                        revServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        revIntDepositsCurrentYear = r.budgetMonthly;
                        revIntDepositsPreviousYear = r.actual;
                        revIntDepositsDiff = r.diff;
                        revIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        revForexCurrentYear = r.budgetMonthly;
                        revForexPreviousYear = r.actual;
                        revForexDiff = r.diff;
                        revForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        revOtherCurrentYear = r.budgetMonthly;
                        revOtherPreviousYear = r.actual;
                        revOtherDiff = r.diff;
                        revOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        totGrossIncomeCurrYear += r.budgetMonthly;
                        totGrossIncomePrevYear += r.actual;
                        totGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        revIntExpenseCurrentYear = r.budgetMonthly;
                        revIntExpensePreviousYear = r.actual;
                        revIntExpenseDiff = r.diff;
                        revIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        revIntExpenseCBUCurrentYear = r.budgetMonthly;
                        revIntExpenseCBUPreviousYear = r.actual;
                        revIntExpenseCBUDiff = r.diff;
                        revIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        totFinancialCostCurrYear += r.budgetMonthly;
                        totFinancialCostPrevYear += r.actual;
                        totFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        revProvisionCurrentYear = r.budgetMonthly;
                        revProvisionPreviousYear = r.actual;
                        revProvisionDiff = r.diff;
                        revProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        totProvisionLoanCurrYear += r.budgetMonthly;
                        totProvisionLoanPrevYear += r.actual;
                        totProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        expPersonnelCurrentYear = r.budgetMonthly;
                        expPersonnelPreviousYear = r.actual;
                        expPersonnelDiff = r.diff;
                        expPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        expOutstationCurrentYear = r.budgetMonthly;
                        expOutstationPreviousYear = r.actual;
                        expOutstationDiff = r.diff;
                        expOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        expFringeCurrentYear = r.budgetMonthly;
                        expFringePreviousYear = r.actual;
                        expFringeDiff = r.diff;
                        expFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        expRetirementCurrentYear = r.budgetMonthly;
                        expRetirementPreviousYear = r.actual;
                        expRetirementDiff = r.diff;
                        expRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        expOutsideCurrentYear = r.budgetMonthly;
                        expOutsidePreviousYear = r.actual;
                        expOutsideDiff = r.diff;
                        expOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        expProfFeesCurrentYear = r.budgetMonthly;
                        expProfFeesPreviousYear = r.actual;
                        expProfFeesDiff = r.diff;
                        expProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        expMeetingsCurrentYear = r.budgetMonthly;
                        expMeetingsPreviousYear = r.actual;
                        expMeetingsDiff = r.diff;
                        expMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        expClientCurrentYear = r.budgetMonthly;
                        expClientPreviousYear = r.actual;
                        expClientDiff = r.diff;
                        expClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        expLitigationCurrentYear = r.budgetMonthly;
                        expLitigationPreviousYear = r.actual;
                        expLitigationDiff = r.diff;
                        expLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        expSuppliesCurrentYear = r.budgetMonthly;
                        expSuppliesPreviousYear = r.actual;
                        expSuppliesDiff = r.diff;
                        expSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        expUtilitiesCurrentYear = r.budgetMonthly;
                        expUtilitiesPreviousYear = r.actual;
                        expUtilitiesDiff = r.diff;
                        expUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        expCommunicationCurrentYear = r.budgetMonthly;
                        expCommunicationPreviousYear = r.actual;
                        expCommunicationDiff = r.diff;
                        expCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        expRentalCurrentYear = r.budgetMonthly;
                        expRentalPreviousYear = r.actual;
                        expRentalDiff = r.diff;
                        expRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        expInsuranceCurrentYear = r.budgetMonthly;
                        expInsurancePreviousYear = r.actual;
                        expInsuranceDiff = r.diff;
                        expInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        expTranspoCurrentYear = r.budgetMonthly;
                        expTranspoPreviousYear = r.actual;
                        expTranspoDiff = r.diff;
                        expTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        expTaxesCurrentYear = r.budgetMonthly;
                        expTaxesPreviousYear = r.actual;
                        expTaxesDiff = r.diff;
                        expTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        expRepairsCurrentYear = r.budgetMonthly;
                        expRepairsPreviousYear = r.actual;
                        expRepairsDiff = r.diff;
                        expRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        expDepreciationCurrentYear = r.budgetMonthly;
                        expDepreciationPreviousYear = r.actual;
                        expDepreciationDiff = r.diff;
                        expDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        expProvisionCurrentYear = r.budgetMonthly;
                        expProvisionPreviousYear = r.actual;
                        expProvisionDiff = r.diff;
                        expProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        expOthersCurrentYear = r.budgetMonthly;
                        expOthersPreviousYear = r.actual;
                        expOthersDiff = r.diff;
                        expOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        totExpensesCurrYear += r.budgetMonthly;
                        totExpensesPrevYear += r.actual;
                        totExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        incomeTaxExpenseCurrentYear = r.budgetMonthly;
                        incomeTaxExpensePreviousYear = r.actual;
                        incomeTaxExpenseDiff = r.diff;
                        incomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        totIncomeTaxCurrYear += r.budgetMonthly;
                        totIncomeTaxPrevYear += r.actual;
                        totIncomeTaxDiff += r.diff;
                    }
                }
                totGrossIncomePer = (totGrossIncomeDiff / totGrossIncomeCurrYear) * 100;
                totFinancialCostPer = (totFinancialCostDiff / totFinancialCostCurrYear) * 100;

                totNetMarginCurrYear = totGrossIncomeCurrYear - totFinancialCostCurrYear - totProvisionLoanCurrYear;
                totNetMarginPrevYear = totGrossIncomePrevYear - totFinancialCostPrevYear - totProvisionLoanPrevYear;
                //totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                totNetMarginDiff = totNetMarginPrevYear - totNetMarginCurrYear;
                totNetMarginPer = (totNetMarginDiff / totNetMarginCurrYear) * 100;


                totExpensesPer = (totExpensesDiff / totExpensesCurrYear) * 100;
                //TOTAL INCOME
                totIncomeCurrYear = totNetMarginCurrYear - totExpensesCurrYear;
                totIncomePrevYear = totNetMarginPrevYear - totExpensesPrevYear;
                //totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                totIncomeDiff = totIncomePrevYear - totIncomeCurrYear;
                totIncomePer = (totIncomeDiff / totIncomeCurrYear * 100);

                //NET INCOME
                totNetIncomeCurrYear = totIncomeCurrYear - totIncomeTaxCurrYear;
                totNetIncomePrevYear = totIncomePrevYear - totIncomeTaxPrevYear;
                //totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                totNetIncomeDiff = totNetIncomePrevYear - totNetIncomeCurrYear;
                totNetIncomePer = (totNetIncomeDiff / totNetIncomeCurrYear) * 100;
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Monthly IS vs Budget");
                //HEADER
                ew.Cells["A1:E1"].Merge = true;
                ew.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:E1"].Style.Font.Bold = true;

                ew.Cells["A2:E2"].Merge = true;
                ew.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:E2"].Style.Font.Bold = true;

                ew.Cells["A4:E4"].Merge = true;
                ew.Cells["A4:E4"].Value = "COMPARATIVE MONTHLY INCOME STATEMENT-ACTUAL VS BUDGET";
                ew.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:E4"].Style.Font.Bold = true;

                ew.Cells["A5:E5"].Merge = true;
                ew.Cells["A5:E5"].Value = "FOR THE MONTH ENDED " + month + " " + year;
                ew.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:E5"].Style.Font.Bold = true;

                ew.Cells["A:E"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:E8"].Style.Font.Size = 12;
                ew.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B8"].Value = "BUDGET";
                ew.Cells["C8"].Value = "ACTUAL";
                ew.Cells["D8"].Value = "VARIANCE";
                ew.Cells["E8"].Value = "%";
                ew.Cells["A8:E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A8:E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A8:E8"].Style.Font.Bold = true;

                ew.Cells["A9"].Value = "REVENUE";
                ew.Cells["A9"].Style.Font.Bold = true;

                ew.Cells["B10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["E10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B10:D53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["E10:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                ew.Cells["A10"].Value = "Interest on Loans";
                ew.Cells["B10"].Value = revIntLoansCurrentYear;
                ew.Cells["C10"].Value = revIntLoansPreviousYear;
                ew.Cells["D10"].Value = revIntLoansDiff;
                ew.Cells["E10"].Value = revIntLoansPercent / 100;

                ew.Cells["A11"].Value = "Service Income";
                ew.Cells["B11"].Value = revServiceIncomeCurrentYear;
                ew.Cells["C11"].Value = revServiceIncomePreviousYear;
                ew.Cells["D11"].Value = revServiceIncomeDiff;
                ew.Cells["E11"].Value = revServiceIncomePercent / 100;

                ew.Cells["A12"].Value = "Interest on Deposit and Placements";
                ew.Cells["B12"].Value = revIntDepositsCurrentYear;
                ew.Cells["C12"].Value = revIntDepositsPreviousYear;
                ew.Cells["D12"].Value = revIntDepositsDiff;
                ew.Cells["E12"].Value = revIntDepositsPercent / 100;

                ew.Cells["A13"].Value = "Forex Gain or Loss";
                ew.Cells["B13"].Value = revForexCurrentYear;
                ew.Cells["C13"].Value = revForexPreviousYear;
                ew.Cells["D13"].Value = revForexDiff;
                ew.Cells["E13"].Value = revForexPercent / 100;

                ew.Cells["A14"].Value = "Other Income";
                ew.Cells["B14"].Value = revOtherCurrentYear;
                ew.Cells["C14"].Value = revOtherPreviousYear;
                ew.Cells["D14"].Value = revOtherDiff;
                ew.Cells["E14"].Value = revOtherPercent / 100;
                ew.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Gross Income";
                ew.Cells["B15"].Value = totGrossIncomeCurrYear;
                ew.Cells["C15"].Value = totGrossIncomePrevYear;
                ew.Cells["D15"].Value = totGrossIncomeDiff;
                ew.Cells["E15"].Value = totGrossIncomePer / 100;
                ew.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A17"].Value = "Interest Expense";
                ew.Cells["B17"].Value = revIntExpenseCurrentYear;
                ew.Cells["C17"].Value = revIntExpensePreviousYear;
                ew.Cells["D17"].Value = revIntExpenseDiff;
                ew.Cells["E17"].Value = revIntExpensePercent / 100;

                ew.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                ew.Cells["B18"].Value = revIntExpenseCBUCurrentYear;
                ew.Cells["C18"].Value = revIntExpenseCBUPreviousYear;
                ew.Cells["D18"].Value = revIntExpenseCBUDiff;
                ew.Cells["E18"].Value = revIntExpenseCBUPercent / 100;
                ew.Cells["A18:E18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A19"].Value = "     Total Financial Costs";
                ew.Cells["B19"].Value = totFinancialCostCurrYear;
                ew.Cells["C19"].Value = totFinancialCostPrevYear;
                ew.Cells["D19"].Value = totFinancialCostDiff;
                ew.Cells["E19"].Value = totFinancialCostPer / 100;
                ew.Cells["A19:E19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A21"].Value = "Provision for Loan Losses";
                ew.Cells["B21"].Value = revProvisionCurrentYear;
                ew.Cells["C21"].Value = revProvisionPreviousYear;
                ew.Cells["D21"].Value = revProvisionDiff;
                ew.Cells["E21"].Value = revProvisionPercent / 100;
                ew.Cells["A21:E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A23"].Value = "     Net Margin";
                ew.Cells["B23"].Value = totNetMarginCurrYear;
                ew.Cells["C23"].Value = totNetMarginPrevYear;
                ew.Cells["D23"].Value = totNetMarginDiff;
                ew.Cells["E23"].Value = totNetMarginPer / 100;
                ew.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                ew.Cells["A25"].Value = "EXPENSES";
                ew.Cells["A25:E25"].Style.Font.Bold = true;


                ew.Cells["A26"].Value = "Personnel Costs";
                ew.Cells["B26"].Value = expPersonnelCurrentYear;
                ew.Cells["C26"].Value = expPersonnelPreviousYear;
                ew.Cells["D26"].Value = expPersonnelDiff;
                ew.Cells["E26"].Value = expPersonnelPercent / 100;

                ew.Cells["A27"].Value = "Outstation";
                ew.Cells["B27"].Value = expOutstationCurrentYear;
                ew.Cells["C27"].Value = expOutstationPreviousYear;
                ew.Cells["D27"].Value = expOutstationDiff;
                ew.Cells["E27"].Value = expOutstationPercent / 100;

                ew.Cells["A28"].Value = "Fringe Benefit";
                ew.Cells["B28"].Value = expFringeCurrentYear;
                ew.Cells["C28"].Value = expFringePreviousYear;
                ew.Cells["D28"].Value = expFringeDiff;
                ew.Cells["E28"].Value = expFringePercent / 100;

                ew.Cells["A29"].Value = "Retirement";
                ew.Cells["B29"].Value = expRetirementCurrentYear;
                ew.Cells["C29"].Value = expRetirementPreviousYear;
                ew.Cells["D29"].Value = expRetirementDiff;
                ew.Cells["E29"].Value = expRetirementPercent / 100;

                ew.Cells["A30"].Value = "Outside Services";
                ew.Cells["B30"].Value = expOutsideCurrentYear;
                ew.Cells["C30"].Value = expOutsidePreviousYear;
                ew.Cells["D30"].Value = expOutsideDiff;
                ew.Cells["E30"].Value = expOutsidePercent / 100;

                ew.Cells["A31"].Value = "Professional fees";
                ew.Cells["B31"].Value = expProfFeesCurrentYear;
                ew.Cells["C31"].Value = expProfFeesPreviousYear;
                ew.Cells["D31"].Value = expProfFeesDiff;
                ew.Cells["E31"].Value = expProfFeesPercent / 100;

                ew.Cells["A32"].Value = "Meetings, Training and Conferences";
                ew.Cells["B32"].Value = expMeetingsCurrentYear;
                ew.Cells["C32"].Value = expMeetingsPreviousYear;
                ew.Cells["D32"].Value = expMeetingsDiff;
                ew.Cells["E32"].Value = expMeetingsPercent / 100;

                ew.Cells["A33"].Value = "Client and Community Services";
                ew.Cells["B33"].Value = expClientCurrentYear;
                ew.Cells["C33"].Value = expClientPreviousYear;
                ew.Cells["D33"].Value = expClientDiff;
                ew.Cells["E33"].Value = expClientPercent / 100;

                ew.Cells["A34"].Value = "Litigation";
                ew.Cells["B34"].Value = expLitigationCurrentYear;
                ew.Cells["C34"].Value = expLitigationPreviousYear;
                ew.Cells["D34"].Value = expLitigationDiff;
                ew.Cells["E34"].Value = expLitigationPercent / 100;

                ew.Cells["A35"].Value = "Supplies";
                ew.Cells["B35"].Value = expSuppliesCurrentYear;
                ew.Cells["C35"].Value = expSuppliesPreviousYear;
                ew.Cells["D35"].Value = expSuppliesDiff;
                ew.Cells["E35"].Value = expSuppliesPercent / 100;

                ew.Cells["A36"].Value = "Utilities";
                ew.Cells["B36"].Value = expUtilitiesCurrentYear;
                ew.Cells["C36"].Value = expUtilitiesPreviousYear;
                ew.Cells["D36"].Value = expUtilitiesDiff;
                ew.Cells["E36"].Value = expUtilitiesPercent / 100;

                ew.Cells["A37"].Value = "Communication and Courier";
                ew.Cells["B37"].Value = expCommunicationCurrentYear;
                ew.Cells["C37"].Value = expCommunicationPreviousYear;
                ew.Cells["D37"].Value = expCommunicationDiff;
                ew.Cells["E37"].Value = expCommunicationPercent / 100;


                ew.Cells["A38"].Value = "Rental";
                ew.Cells["B38"].Value = expRentalCurrentYear;
                ew.Cells["C38"].Value = expRentalPreviousYear;
                ew.Cells["D38"].Value = expRentalDiff;
                ew.Cells["E38"].Value = expRentalPercent / 100;

                ew.Cells["A39"].Value = "Insurance";
                ew.Cells["B39"].Value = expInsuranceCurrentYear;
                ew.Cells["C39"].Value = expInsurancePreviousYear;
                ew.Cells["D39"].Value = expInsuranceDiff;
                ew.Cells["E39"].Value = expInsurancePercent / 100;

                ew.Cells["A40"].Value = "Transportation and Travel";
                ew.Cells["B40"].Value = expTranspoCurrentYear;
                ew.Cells["C40"].Value = expTranspoPreviousYear;
                ew.Cells["D40"].Value = expTranspoDiff;
                ew.Cells["E40"].Value = expTranspoPercent / 100;

                ew.Cells["A41"].Value = "Taxes and Licenses";
                ew.Cells["B41"].Value = expTaxesCurrentYear;
                ew.Cells["C41"].Value = expTaxesPreviousYear;
                ew.Cells["D41"].Value = expTaxesDiff;
                ew.Cells["E41"].Value = expTaxesPercent / 100;

                ew.Cells["A42"].Value = "Repairs and Maintenance";
                ew.Cells["B42"].Value = expRepairsCurrentYear;
                ew.Cells["C42"].Value = expRepairsPreviousYear;
                ew.Cells["D42"].Value = expRepairsDiff;
                ew.Cells["E42"].Value = expRepairsPercent / 100;

                ew.Cells["A43"].Value = "Depreciation and Amortization";
                ew.Cells["B43"].Value = expDepreciationCurrentYear;
                ew.Cells["C43"].Value = expDepreciationPreviousYear;
                ew.Cells["D43"].Value = expDepreciationDiff;
                ew.Cells["E43"].Value = expDepreciationPercent / 100;

                ew.Cells["A44"].Value = "Provision for Other Losses";
                ew.Cells["B44"].Value = expProvisionCurrentYear;
                ew.Cells["C44"].Value = expProvisionPreviousYear;
                ew.Cells["D44"].Value = expProvisionDiff;
                ew.Cells["E44"].Value = expProvisionPercent / 100;

                ew.Cells["A45"].Value = "Others";
                ew.Cells["B45"].Value = expOthersCurrentYear;
                ew.Cells["C45"].Value = expOthersPreviousYear;
                ew.Cells["D45"].Value = expOthersDiff;
                ew.Cells["E45"].Value = expOthersPercent / 100;
                ew.Cells["A45:E45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A46"].Value = "     Total Expenses";
                ew.Cells["B46"].Value = totExpensesCurrYear;
                ew.Cells["C46"].Value = totExpensesPrevYear;
                ew.Cells["D46"].Value = totExpensesDiff;
                ew.Cells["E46"].Value = totExpensesPer / 100;
                ew.Cells["A46:E46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                ew.Cells["B48"].Value = totIncomeCurrYear;
                ew.Cells["C48"].Value = totIncomePrevYear;
                ew.Cells["D48"].Value = totIncomeDiff;
                ew.Cells["E48"].Value = totIncomePer / 100;
                ew.Cells["A48:E48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["A48:E48"].Style.Font.Bold = true;

                ew.Cells["A50"].Value = "INCOME TAX EXPENSE";
                ew.Cells["B50"].Value = incomeTaxExpenseCurrentYear;
                ew.Cells["C50"].Value = incomeTaxExpensePreviousYear;
                ew.Cells["D50"].Value = incomeTaxExpenseDiff;
                ew.Cells["E50"].Value = incomeTaxExpensePercent / 100;
                ew.Cells["A50:E50"].Style.Font.Bold = true;

                ew.Cells["A53"].Value = "NET  INCOME (LOSS)";
                ew.Cells["B53"].Value = totNetIncomeCurrYear;
                ew.Cells["C53"].Value = totNetIncomePrevYear;
                ew.Cells["D53"].Value = totNetIncomeDiff;
                ew.Cells["E53"].Value = totNetIncomePer / 100;
                ew.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:E53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ew.Cells["A53:E53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "Monthly IS vs Budget - " + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult ComulativeISBudgetXLS(string month, int year)
        {
            var model = GetCumulativeIncomeStatementVSBudget(month, year);
            var prevYear = year - 1;

            //REVENUE
            decimal revIntLoansCurrentYear = 0;
            decimal revIntLoansPreviousYear = 0;
            decimal revIntLoansDiff = 0;
            decimal revIntLoansPercent = 0;

            decimal revServiceIncomeCurrentYear = 0;
            decimal revServiceIncomePreviousYear = 0;
            decimal revServiceIncomeDiff = 0;
            decimal revServiceIncomePercent = 0;

            decimal revIntDepositsCurrentYear = 0;
            decimal revIntDepositsPreviousYear = 0;
            decimal revIntDepositsDiff = 0;
            decimal revIntDepositsPercent = 0;

            decimal revForexCurrentYear = 0;
            decimal revForexPreviousYear = 0;
            decimal revForexDiff = 0;
            decimal revForexPercent = 0;

            decimal revOtherCurrentYear = 0;
            decimal revOtherPreviousYear = 0;
            decimal revOtherDiff = 0;
            decimal revOtherPercent = 0;

            decimal revIntExpenseCBUCurrentYear = 0;
            decimal revIntExpenseCBUPreviousYear = 0;
            decimal revIntExpenseCBUDiff = 0;
            decimal revIntExpenseCBUPercent = 0;

            decimal totGrossIncomeCurrYear = 0;
            decimal totGrossIncomePrevYear = 0;
            decimal totGrossIncomeDiff = 0;
            decimal totGrossIncomePer = 0;

            decimal revIntExpenseCurrentYear = 0;
            decimal revIntExpensePreviousYear = 0;
            decimal revIntExpenseDiff = 0;
            decimal revIntExpensePercent = 0;

            decimal totFinancialCostCurrYear = 0;
            decimal totFinancialCostPrevYear = 0;
            decimal totFinancialCostDiff = 0;
            decimal totFinancialCostPer = 0;

            decimal totProvisionLoanCurrYear = 0;
            decimal totProvisionLoanPrevYear = 0;
            decimal totProvisionLoanDiff = 0;
            decimal totProvisionLoanPer = 0;

            decimal revProvisionCurrentYear = 0;
            decimal revProvisionPreviousYear = 0;
            decimal revProvisionDiff = 0;
            decimal revProvisionPercent = 0;

            decimal totNetMarginCurrYear = 0;
            decimal totNetMarginPrevYear = 0;
            decimal totNetMarginDiff = 0;
            decimal totNetMarginPer = 0;

            //EXPENSES
            decimal expPersonnelCurrentYear = 0;
            decimal expPersonnelPreviousYear = 0;
            decimal expPersonnelDiff = 0;
            decimal expPersonnelPercent = 0;

            decimal expOutstationCurrentYear = 0;
            decimal expOutstationPreviousYear = 0;
            decimal expOutstationDiff = 0;
            decimal expOutstationPercent = 0;

            decimal expFringeCurrentYear = 0;
            decimal expFringePreviousYear = 0;
            decimal expFringeDiff = 0;
            decimal expFringePercent = 0;

            decimal expRetirementCurrentYear = 0;
            decimal expRetirementPreviousYear = 0;
            decimal expRetirementDiff = 0;
            decimal expRetirementPercent = 0;

            decimal expOutsideCurrentYear = 0;
            decimal expOutsidePreviousYear = 0;
            decimal expOutsideDiff = 0;
            decimal expOutsidePercent = 0;

            decimal expProfFeesCurrentYear = 0;
            decimal expProfFeesPreviousYear = 0;
            decimal expProfFeesDiff = 0;
            decimal expProfFeesPercent = 0;

            decimal expMeetingsCurrentYear = 0;
            decimal expMeetingsPreviousYear = 0;
            decimal expMeetingsDiff = 0;
            decimal expMeetingsPercent = 0;

            decimal expClientCurrentYear = 0;
            decimal expClientPreviousYear = 0;
            decimal expClientDiff = 0;
            decimal expClientPercent = 0;

            decimal expLitigationCurrentYear = 0;
            decimal expLitigationPreviousYear = 0;
            decimal expLitigationDiff = 0;
            decimal expLitigationPercent = 0;

            decimal expSuppliesCurrentYear = 0;
            decimal expSuppliesPreviousYear = 0;
            decimal expSuppliesDiff = 0;
            decimal expSuppliesPercent = 0;

            decimal expUtilitiesCurrentYear = 0;
            decimal expUtilitiesPreviousYear = 0;
            decimal expUtilitiesDiff = 0;
            decimal expUtilitiesPercent = 0;

            decimal expCommunicationCurrentYear = 0;
            decimal expCommunicationPreviousYear = 0;
            decimal expCommunicationDiff = 0;
            decimal expCommunicationPercent = 0;

            decimal expRentalCurrentYear = 0;
            decimal expRentalPreviousYear = 0;
            decimal expRentalDiff = 0;
            decimal expRentalPercent = 0;

            decimal expInsuranceCurrentYear = 0;
            decimal expInsurancePreviousYear = 0;
            decimal expInsuranceDiff = 0;
            decimal expInsurancePercent = 0;

            decimal expTranspoCurrentYear = 0;
            decimal expTranspoPreviousYear = 0;
            decimal expTranspoDiff = 0;
            decimal expTranspoPercent = 0;

            decimal expTaxesCurrentYear = 0;
            decimal expTaxesPreviousYear = 0;
            decimal expTaxesDiff = 0;
            decimal expTaxesPercent = 0;

            decimal expRepairsCurrentYear = 0;
            decimal expRepairsPreviousYear = 0;
            decimal expRepairsDiff = 0;
            decimal expRepairsPercent = 0;

            decimal expDepreciationCurrentYear = 0;
            decimal expDepreciationPreviousYear = 0;
            decimal expDepreciationDiff = 0;
            decimal expDepreciationPercent = 0;

            decimal expProvisionCurrentYear = 0;
            decimal expProvisionPreviousYear = 0;
            decimal expProvisionDiff = 0;
            decimal expProvisionPercent = 0;

            decimal expOthersCurrentYear = 0;
            decimal expOthersPreviousYear = 0;
            decimal expOthersDiff = 0;
            decimal expOthersPercent = 0;

            //INCOME TAX

            decimal incomeTaxExpenseCurrentYear = 0;
            decimal incomeTaxExpensePreviousYear = 0;
            decimal incomeTaxExpenseDiff = 0;
            decimal incomeTaxExpensePercent = 0;

            decimal totExpensesCurrYear = 0;
            decimal totExpensesPrevYear = 0;
            decimal totExpensesDiff = 0;
            decimal totExpensesPer = 0;

            decimal totIncomeCurrYear = 0;
            decimal totIncomePrevYear = 0;
            decimal totIncomeDiff = 0;
            decimal totIncomePer = 0;

            decimal totIncomeTaxCurrYear = 0;
            decimal totIncomeTaxPrevYear = 0;
            decimal totIncomeTaxDiff = 0;
            decimal totIncomeTaxPer = 0;

            decimal totNetIncomeCurrYear = 0;
            decimal totNetIncomePrevYear = 0;
            decimal totNetIncomeDiff = 0;
            decimal totNetIncomePer = 0;

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        revIntLoansCurrentYear = r.budgetCumulative;
                        revIntLoansPreviousYear = r.actual;
                        revIntLoansDiff = r.diff;
                        revIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        revServiceIncomeCurrentYear = r.budgetCumulative;
                        revServiceIncomePreviousYear = r.actual;
                        revServiceIncomeDiff = r.diff;
                        revServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        revIntDepositsCurrentYear = r.budgetCumulative;
                        revIntDepositsPreviousYear = r.actual;
                        revIntDepositsDiff = r.diff;
                        revIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        revForexCurrentYear = r.budgetCumulative;
                        revForexPreviousYear = r.actual;
                        revForexDiff = r.diff;
                        revForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        revOtherCurrentYear = r.budgetCumulative;
                        revOtherPreviousYear = r.actual;
                        revOtherDiff = r.diff;
                        revOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        totGrossIncomeCurrYear += r.budgetCumulative;
                        totGrossIncomePrevYear += r.actual;
                        totGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        revIntExpenseCurrentYear = r.budgetCumulative;
                        revIntExpensePreviousYear = r.actual;
                        revIntExpenseDiff = r.diff;
                        revIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        revIntExpenseCBUCurrentYear = r.budgetCumulative;
                        revIntExpenseCBUPreviousYear = r.actual;
                        revIntExpenseCBUDiff = r.diff;
                        revIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        totFinancialCostCurrYear += r.budgetCumulative;
                        totFinancialCostPrevYear += r.actual;
                        totFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        revProvisionCurrentYear = r.budgetCumulative;
                        revProvisionPreviousYear = r.actual;
                        revProvisionDiff = r.diff;
                        revProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        totProvisionLoanCurrYear += r.budgetCumulative;
                        totProvisionLoanPrevYear += r.actual;
                        totProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        expPersonnelCurrentYear = r.budgetCumulative;
                        expPersonnelPreviousYear = r.actual;
                        expPersonnelDiff = r.diff;
                        expPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        expOutstationCurrentYear = r.budgetCumulative;
                        expOutstationPreviousYear = r.actual;
                        expOutstationDiff = r.diff;
                        expOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        expFringeCurrentYear = r.budgetCumulative;
                        expFringePreviousYear = r.actual;
                        expFringeDiff = r.diff;
                        expFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        expRetirementCurrentYear = r.budgetCumulative;
                        expRetirementPreviousYear = r.actual;
                        expRetirementDiff = r.diff;
                        expRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        expOutsideCurrentYear = r.budgetCumulative;
                        expOutsidePreviousYear = r.actual;
                        expOutsideDiff = r.diff;
                        expOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        expProfFeesCurrentYear = r.budgetCumulative;
                        expProfFeesPreviousYear = r.actual;
                        expProfFeesDiff = r.diff;
                        expProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        expMeetingsCurrentYear = r.budgetCumulative;
                        expMeetingsPreviousYear = r.actual;
                        expMeetingsDiff = r.diff;
                        expMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        expClientCurrentYear = r.budgetCumulative;
                        expClientPreviousYear = r.actual;
                        expClientDiff = r.diff;
                        expClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        expLitigationCurrentYear = r.budgetCumulative;
                        expLitigationPreviousYear = r.actual;
                        expLitigationDiff = r.diff;
                        expLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        expSuppliesCurrentYear = r.budgetCumulative;
                        expSuppliesPreviousYear = r.actual;
                        expSuppliesDiff = r.diff;
                        expSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        expUtilitiesCurrentYear = r.budgetCumulative;
                        expUtilitiesPreviousYear = r.actual;
                        expUtilitiesDiff = r.diff;
                        expUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        expCommunicationCurrentYear = r.budgetCumulative;
                        expCommunicationPreviousYear = r.actual;
                        expCommunicationDiff = r.diff;
                        expCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        expRentalCurrentYear = r.budgetCumulative;
                        expRentalPreviousYear = r.actual;
                        expRentalDiff = r.diff;
                        expRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        expInsuranceCurrentYear = r.budgetCumulative;
                        expInsurancePreviousYear = r.actual;
                        expInsuranceDiff = r.diff;
                        expInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        expTranspoCurrentYear = r.budgetCumulative;
                        expTranspoPreviousYear = r.actual;
                        expTranspoDiff = r.diff;
                        expTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        expTaxesCurrentYear = r.budgetCumulative;
                        expTaxesPreviousYear = r.actual;
                        expTaxesDiff = r.diff;
                        expTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        expRepairsCurrentYear = r.budgetCumulative;
                        expRepairsPreviousYear = r.actual;
                        expRepairsDiff = r.diff;
                        expRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        expDepreciationCurrentYear = r.budgetCumulative;
                        expDepreciationPreviousYear = r.actual;
                        expDepreciationDiff = r.diff;
                        expDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        expProvisionCurrentYear = r.budgetCumulative;
                        expProvisionPreviousYear = r.actual;
                        expProvisionDiff = r.diff;
                        expProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        expOthersCurrentYear = r.budgetCumulative;
                        expOthersPreviousYear = r.actual;
                        expOthersDiff = r.diff;
                        expOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        totExpensesCurrYear += r.budgetCumulative;
                        totExpensesPrevYear += r.actual;
                        totExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        incomeTaxExpenseCurrentYear = r.budgetCumulative;
                        incomeTaxExpensePreviousYear = r.actual;
                        incomeTaxExpenseDiff = r.diff;
                        incomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        totIncomeTaxCurrYear += r.budgetCumulative;
                        totIncomeTaxPrevYear += r.actual;
                        totIncomeTaxDiff += r.diff;
                    }
                }
                totGrossIncomePer = (totGrossIncomeDiff / totGrossIncomeCurrYear) * 100;
                totFinancialCostPer = (totFinancialCostDiff / totFinancialCostCurrYear) * 100;

                totNetMarginCurrYear = totGrossIncomeCurrYear - totFinancialCostCurrYear - totProvisionLoanCurrYear;
                totNetMarginPrevYear = totGrossIncomePrevYear - totFinancialCostPrevYear - totProvisionLoanPrevYear;
                //totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                totNetMarginDiff = totNetMarginPrevYear - totNetMarginCurrYear;
                totNetMarginPer = (totNetMarginDiff / totNetMarginCurrYear) * 100;


                totExpensesPer = (totExpensesDiff / totExpensesCurrYear) * 100;
                //TOTAL INCOME
                totIncomeCurrYear = totNetMarginCurrYear - totExpensesCurrYear;
                totIncomePrevYear = totNetMarginPrevYear - totExpensesPrevYear;
                //totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                totIncomeDiff = totIncomePrevYear - totIncomeCurrYear;
                totIncomePer = (totIncomeDiff / totIncomeCurrYear * 100);

                //NET INCOME
                totNetIncomeCurrYear = totIncomeCurrYear - totIncomeTaxCurrYear;
                totNetIncomePrevYear = totIncomePrevYear - totIncomeTaxPrevYear;
                //totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                totNetIncomeDiff = totNetIncomePrevYear - totNetIncomeCurrYear;
                totNetIncomePer = (totNetIncomeDiff / totNetIncomeCurrYear) * 100;
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Compare Commu IS vs Budget");
                //HEADER
                ew.Cells["A1:E1"].Merge = true;
                ew.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:E1"].Style.Font.Bold = true;

                ew.Cells["A2:E2"].Merge = true;
                ew.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:E2"].Style.Font.Bold = true;

                ew.Cells["A4:E4"].Merge = true;
                ew.Cells["A4:E4"].Value = "COMPARATIVE STATEMENT OF COMPREHENSIVE INCOME";
                ew.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:E4"].Style.Font.Bold = true;

                ew.Cells["A5:E5"].Merge = true;
                ew.Cells["A5:E5"].Value = "FOR THE MONTH ENDED " + month + " " + year;
                ew.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:E5"].Style.Font.Bold = true;

                ew.Cells["A:E"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:E8"].Style.Font.Size = 12;
                ew.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B8"].Value = "BUDGET";
                ew.Cells["C8"].Value = "ACTUAL";
                ew.Cells["D8"].Value = "VARIANCE";
                ew.Cells["E8"].Value = "%";
                ew.Cells["A8:E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A8:E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A8:E8"].Style.Font.Bold = true;

                ew.Cells["A9"].Value = "REVENUE";
                ew.Cells["A9"].Style.Font.Bold = true;

                ew.Cells["B10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["E10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B10:D53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["E10:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                ew.Cells["A10"].Value = "Interest on Loans";
                ew.Cells["B10"].Value = revIntLoansCurrentYear;
                ew.Cells["C10"].Value = revIntLoansPreviousYear;
                ew.Cells["D10"].Value = revIntLoansDiff;
                ew.Cells["E10"].Value = revIntLoansPercent / 100;

                ew.Cells["A11"].Value = "Service Income";
                ew.Cells["B11"].Value = revServiceIncomeCurrentYear;
                ew.Cells["C11"].Value = revServiceIncomePreviousYear;
                ew.Cells["D11"].Value = revServiceIncomeDiff;
                ew.Cells["E11"].Value = revServiceIncomePercent / 100;

                ew.Cells["A12"].Value = "Interest on Deposit and Placements";
                ew.Cells["B12"].Value = revIntDepositsCurrentYear;
                ew.Cells["C12"].Value = revIntDepositsPreviousYear;
                ew.Cells["D12"].Value = revIntDepositsDiff;
                ew.Cells["E12"].Value = revIntDepositsPercent / 100;

                ew.Cells["A13"].Value = "Forex Gain or Loss";
                ew.Cells["B13"].Value = revForexCurrentYear;
                ew.Cells["C13"].Value = revForexPreviousYear;
                ew.Cells["D13"].Value = revForexDiff;
                ew.Cells["E13"].Value = revForexPercent / 100;

                ew.Cells["A14"].Value = "Other Income";
                ew.Cells["B14"].Value = revOtherCurrentYear;
                ew.Cells["C14"].Value = revOtherPreviousYear;
                ew.Cells["D14"].Value = revOtherDiff;
                ew.Cells["E14"].Value = revOtherPercent / 100;
                ew.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Gross Income";
                ew.Cells["B15"].Value = totGrossIncomeCurrYear;
                ew.Cells["C15"].Value = totGrossIncomePrevYear;
                ew.Cells["D15"].Value = totGrossIncomeDiff;
                ew.Cells["E15"].Value = totGrossIncomePer / 100;
                ew.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A17"].Value = "Interest Expense";
                ew.Cells["B17"].Value = revIntExpenseCurrentYear;
                ew.Cells["C17"].Value = revIntExpensePreviousYear;
                ew.Cells["D17"].Value = revIntExpenseDiff;
                ew.Cells["E17"].Value = revIntExpensePercent / 100;

                ew.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                ew.Cells["B18"].Value = revIntExpenseCBUCurrentYear;
                ew.Cells["C18"].Value = revIntExpenseCBUPreviousYear;
                ew.Cells["D18"].Value = revIntExpenseCBUDiff;
                ew.Cells["E18"].Value = revIntExpenseCBUPercent / 100;
                ew.Cells["A18:E18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A19"].Value = "     Total Financial Costs";
                ew.Cells["B19"].Value = totFinancialCostCurrYear;
                ew.Cells["C19"].Value = totFinancialCostPrevYear;
                ew.Cells["D19"].Value = totFinancialCostDiff;
                ew.Cells["E19"].Value = totFinancialCostPer / 100;
                ew.Cells["A19:E19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A21"].Value = "Provision for Loan Losses";
                ew.Cells["B21"].Value = revProvisionCurrentYear;
                ew.Cells["C21"].Value = revProvisionPreviousYear;
                ew.Cells["D21"].Value = revProvisionDiff;
                ew.Cells["E21"].Value = revProvisionPercent / 100;
                ew.Cells["A21:E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                ew.Cells["A23"].Value = "     Net Margin";
                ew.Cells["B23"].Value = totNetMarginCurrYear;
                ew.Cells["C23"].Value = totNetMarginPrevYear;
                ew.Cells["D23"].Value = totNetMarginDiff;
                ew.Cells["E23"].Value = totNetMarginPer / 100;
                ew.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                ew.Cells["A25"].Value = "EXPENSES";
                ew.Cells["A25:E25"].Style.Font.Bold = true;


                ew.Cells["A26"].Value = "Personnel Costs";
                ew.Cells["B26"].Value = expPersonnelCurrentYear;
                ew.Cells["C26"].Value = expPersonnelPreviousYear;
                ew.Cells["D26"].Value = expPersonnelDiff;
                ew.Cells["E26"].Value = expPersonnelPercent / 100;

                ew.Cells["A27"].Value = "Outstation";
                ew.Cells["B27"].Value = expOutstationCurrentYear;
                ew.Cells["C27"].Value = expOutstationPreviousYear;
                ew.Cells["D27"].Value = expOutstationDiff;
                ew.Cells["E27"].Value = expOutstationPercent / 100;

                ew.Cells["A28"].Value = "Fringe Benefit";
                ew.Cells["B28"].Value = expFringeCurrentYear;
                ew.Cells["C28"].Value = expFringePreviousYear;
                ew.Cells["D28"].Value = expFringeDiff;
                ew.Cells["E28"].Value = expFringePercent / 100;

                ew.Cells["A29"].Value = "Retirement";
                ew.Cells["B29"].Value = expRetirementCurrentYear;
                ew.Cells["C29"].Value = expRetirementPreviousYear;
                ew.Cells["D29"].Value = expRetirementDiff;
                ew.Cells["E29"].Value = expRetirementPercent / 100;

                ew.Cells["A30"].Value = "Outside Services";
                ew.Cells["B30"].Value = expOutsideCurrentYear;
                ew.Cells["C30"].Value = expOutsidePreviousYear;
                ew.Cells["D30"].Value = expOutsideDiff;
                ew.Cells["E30"].Value = expOutsidePercent / 100;

                ew.Cells["A31"].Value = "Professional fees";
                ew.Cells["B31"].Value = expProfFeesCurrentYear;
                ew.Cells["C31"].Value = expProfFeesPreviousYear;
                ew.Cells["D31"].Value = expProfFeesDiff;
                ew.Cells["E31"].Value = expProfFeesPercent / 100;

                ew.Cells["A32"].Value = "Meetings, Training and Conferences";
                ew.Cells["B32"].Value = expMeetingsCurrentYear;
                ew.Cells["C32"].Value = expMeetingsPreviousYear;
                ew.Cells["D32"].Value = expMeetingsDiff;
                ew.Cells["E32"].Value = expMeetingsPercent / 100;

                ew.Cells["A33"].Value = "Client and Community Services";
                ew.Cells["B33"].Value = expClientCurrentYear;
                ew.Cells["C33"].Value = expClientPreviousYear;
                ew.Cells["D33"].Value = expClientDiff;
                ew.Cells["E33"].Value = expClientPercent / 100;

                ew.Cells["A34"].Value = "Litigation";
                ew.Cells["B34"].Value = expLitigationCurrentYear;
                ew.Cells["C34"].Value = expLitigationPreviousYear;
                ew.Cells["D34"].Value = expLitigationDiff;
                ew.Cells["E34"].Value = expLitigationPercent / 100;

                ew.Cells["A35"].Value = "Supplies";
                ew.Cells["B35"].Value = expSuppliesCurrentYear;
                ew.Cells["C35"].Value = expSuppliesPreviousYear;
                ew.Cells["D35"].Value = expSuppliesDiff;
                ew.Cells["E35"].Value = expSuppliesPercent / 100;

                ew.Cells["A36"].Value = "Utilities";
                ew.Cells["B36"].Value = expUtilitiesCurrentYear;
                ew.Cells["C36"].Value = expUtilitiesPreviousYear;
                ew.Cells["D36"].Value = expUtilitiesDiff;
                ew.Cells["E36"].Value = expUtilitiesPercent / 100;

                ew.Cells["A37"].Value = "Communication and Courier";
                ew.Cells["B37"].Value = expCommunicationCurrentYear;
                ew.Cells["C37"].Value = expCommunicationPreviousYear;
                ew.Cells["D37"].Value = expCommunicationDiff;
                ew.Cells["E37"].Value = expCommunicationPercent / 100;


                ew.Cells["A38"].Value = "Rental";
                ew.Cells["B38"].Value = expRentalCurrentYear;
                ew.Cells["C38"].Value = expRentalPreviousYear;
                ew.Cells["D38"].Value = expRentalDiff;
                ew.Cells["E38"].Value = expRentalPercent / 100;

                ew.Cells["A39"].Value = "Insurance";
                ew.Cells["B39"].Value = expInsuranceCurrentYear;
                ew.Cells["C39"].Value = expInsurancePreviousYear;
                ew.Cells["D39"].Value = expInsuranceDiff;
                ew.Cells["E39"].Value = expInsurancePercent / 100;

                ew.Cells["A40"].Value = "Transportation and Travel";
                ew.Cells["B40"].Value = expTranspoCurrentYear;
                ew.Cells["C40"].Value = expTranspoPreviousYear;
                ew.Cells["D40"].Value = expTranspoDiff;
                ew.Cells["E40"].Value = expTranspoPercent / 100;

                ew.Cells["A41"].Value = "Taxes and Licenses";
                ew.Cells["B41"].Value = expTaxesCurrentYear;
                ew.Cells["C41"].Value = expTaxesPreviousYear;
                ew.Cells["D41"].Value = expTaxesDiff;
                ew.Cells["E41"].Value = expTaxesPercent / 100;

                ew.Cells["A42"].Value = "Repairs and Maintenance";
                ew.Cells["B42"].Value = expRepairsCurrentYear;
                ew.Cells["C42"].Value = expRepairsPreviousYear;
                ew.Cells["D42"].Value = expRepairsDiff;
                ew.Cells["E42"].Value = expRepairsPercent / 100;

                ew.Cells["A43"].Value = "Depreciation and Amortization";
                ew.Cells["B43"].Value = expDepreciationCurrentYear;
                ew.Cells["C43"].Value = expDepreciationPreviousYear;
                ew.Cells["D43"].Value = expDepreciationDiff;
                ew.Cells["E43"].Value = expDepreciationPercent / 100;

                ew.Cells["A44"].Value = "Provision for Other Losses";
                ew.Cells["B44"].Value = expProvisionCurrentYear;
                ew.Cells["C44"].Value = expProvisionPreviousYear;
                ew.Cells["D44"].Value = expProvisionDiff;
                ew.Cells["E44"].Value = expProvisionPercent / 100;

                ew.Cells["A45"].Value = "Others";
                ew.Cells["B45"].Value = expOthersCurrentYear;
                ew.Cells["C45"].Value = expOthersPreviousYear;
                ew.Cells["D45"].Value = expOthersDiff;
                ew.Cells["E45"].Value = expOthersPercent / 100;
                ew.Cells["A45:E45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A46"].Value = "     Total Expenses";
                ew.Cells["B46"].Value = totExpensesCurrYear;
                ew.Cells["C46"].Value = totExpensesPrevYear;
                ew.Cells["D46"].Value = totExpensesDiff;
                ew.Cells["E46"].Value = totExpensesPer / 100;
                ew.Cells["A46:E46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                ew.Cells["B48"].Value = totIncomeCurrYear;
                ew.Cells["C48"].Value = totIncomePrevYear;
                ew.Cells["D48"].Value = totIncomeDiff;
                ew.Cells["E48"].Value = totIncomePer / 100;
                ew.Cells["A48:E48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["A48:E48"].Style.Font.Bold = true;

                ew.Cells["A50"].Value = "INCOME TAX EXPENSE";
                ew.Cells["B50"].Value = incomeTaxExpenseCurrentYear;
                ew.Cells["C50"].Value = incomeTaxExpensePreviousYear;
                ew.Cells["D50"].Value = incomeTaxExpenseDiff;
                ew.Cells["E50"].Value = incomeTaxExpensePercent / 100;
                ew.Cells["A50:E50"].Style.Font.Bold = true;

                ew.Cells["A53"].Value = "NET  INCOME (LOSS)";
                ew.Cells["B53"].Value = totNetIncomeCurrYear;
                ew.Cells["C53"].Value = totNetIncomePrevYear;
                ew.Cells["D53"].Value = totNetIncomeDiff;
                ew.Cells["E53"].Value = totNetIncomePer / 100;
                ew.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:E53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ew.Cells["A53:E53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "Compare Commu IS vs Budget - " + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult BalanceSheetBudgetXLS(string month, int year)
        {

            #region

            var model = GetBalanceSheetVSBudget(month, year);
            var prevYear = year - 1;

            //ASSETS
            //CURRENT ASSETS
            decimal currAssetsCashCurrentYear = 0;
            decimal currAssetsCashPreviousYear = 0;
            decimal currAssetsDiff = 0;
            decimal currAssetsPercent = 0;

            decimal currAssetsLoanReceivableCurrentYear = 0;
            decimal currAssetsLoanReceivablePreviousYear = 0;
            decimal currAssetsLoanReceivableDiff = 0;
            decimal currAssetsLoanReceivablePercent = 0;

            decimal currAssetsOtherReceivableCurrentYear = 0;
            decimal currAssetsOtherReceivablePreviousYear = 0;
            decimal currAssetsOtherReceivableDiff = 0;
            decimal currAssetsOtherReceivablePercent = 0;

            decimal currAssetsOtherCurrentYear = 0;
            decimal currAssetsOtherPreviousYear = 0;
            decimal currAssetsOtherDiff = 0;
            decimal currAssetsOtherPercent = 0;

            decimal totalCurrAssetsCurrYear = 0;
            decimal totalCurrAssetsPrevYear = 0;
            decimal totalCurrAssetsDiff = 0;
            decimal totalCurrAssetsPer = 0;

            //NONCURRENT ASSETS
            decimal noncurrAssetsFinancialCurrentYear = 0;
            decimal noncurrAssetsFinancialPreviousYear = 0;
            decimal noncurrAssetsFinancialDiff = 0;
            decimal noncurrAssetsFinancialPercent = 0;

            decimal noncurrAssetsRestrictedCashCurrentYear = 0;
            decimal noncurrAssetsRestrictedCashPreviousYear = 0;
            decimal noncurrAssetsRestrictedCashDiff = 0;
            decimal noncurrAssetsRestrictedCashPercent = 0;

            decimal noncurrAssetsROUAssetCurrentYear = 0;
            decimal noncurrAssetsROUAssetPreviousYear = 0;
            decimal noncurrAssetsROUAssetDiff = 0;
            decimal noncurrAssetsROUAssetPercent = 0;

            decimal noncurrAssetsPropertyCurrentYear = 0;
            decimal noncurrAssetsPropertyPreviousYear = 0;
            decimal noncurrAssetsPropertyDiff = 0;
            decimal noncurrAssetsPropertyPercent = 0;

            decimal noncurrAssetsOtherCurrentYear = 0;
            decimal noncurrAssetsOtherPreviousYear = 0;
            decimal noncurrAssetsOtherDiff = 0;
            decimal noncurrAssetsOtherPercent = 0;

            decimal totalNonCurrAssetsCurrYear = 0;
            decimal totalNonCurrAssetsPrevYear = 0;
            decimal totalNonCurrAssetsDiff = 0;
            decimal totalNonCurrAssetsPer = 0;

            decimal totalAssetsCurrYear = 0;
            decimal totalAssetsPrevYear = 0;
            decimal totalAssetsDiff = 0;
            decimal totalAssetsPer = 0;

            //LIABILITIES
            //CURRENT LIA
            decimal currLiaTradeCurrentYear = 0;
            decimal currLiaTradePreviousYear = 0;
            decimal currLiaTradeDiff = 0;
            decimal currLiaTradePercent = 0;

            decimal currLiaUnearnedCurrentYear = 0;
            decimal currLiaUnearnedPreviousYear = 0;
            decimal currLiaUnearnedDiff = 0;
            decimal currLiaUnearnedPercent = 0;

            decimal currLiaClientCBUCurrentYear = 0;
            decimal currLiaClientCBUPreviousYear = 0;
            decimal currLiaClientCBUDiff = 0;
            decimal currLiaClientCBUPercent = 0;

            decimal currLiaTaxPayableCurrentYear = 0;
            decimal currLiaTaxPayablePreviousYear = 0;
            decimal currLiaTaxPayableDiff = 0;
            decimal currLiaTaxPayablePercent = 0;

            decimal currLiaCGLIPayableCurrentYear = 0;
            decimal currLiaCGLIPayablePreviousYear = 0;
            decimal currLiaCGLIPayableDiff = 0;
            decimal currLiaCGLIPayablePercent = 0;

            decimal currLiaProvisionalCurrentYear = 0;
            decimal currLiaProvisionalPreviousYear = 0;
            decimal currLiaProvisionalDiff = 0;
            decimal currLiaProvisionalPercent = 0;

            decimal totalCurrLiaCurrYear = 0;
            decimal totalCurrLiaPrevYear = 0;
            decimal totalCurrLiaDiff = 0;
            decimal totalCurrLiaPer = 0;

            //NONCURRENT LIA
            decimal noncurrLiaLeaseCurrentYear = 0;
            decimal noncurrLiaLeasePreviousYear = 0;
            decimal noncurrLiaLeaseDiff = 0;
            decimal noncurrLiaLeasePercent = 0;

            decimal noncurrLiaRetirementCurrentYear = 0;
            decimal noncurrLiaRetirementPreviousYear = 0;
            decimal noncurrLiaRetirementDiff = 0;
            decimal noncurrLiaRetirementPercent = 0;

            decimal noncurrLiaDeferredCurrentYear = 0;
            decimal noncurrLiaDeferredPreviousYear = 0;
            decimal noncurrLiaDeferredDiff = 0;
            decimal noncurrLiaDeferredPercent = 0;

            decimal totalNonCurrLiaCurrYear = 0;
            decimal totalNonCurrLiaPrevYear = 0;
            decimal totalNonCurrLiaDiff = 0;
            decimal totalNonCurrLiaPer = 0;

            decimal totalLiaCurrYear = 0;
            decimal totalLiaPrevYear = 0;
            decimal totalLiaDiff = 0;
            decimal totalLiaPer = 0;

            //FUND BALANCE
            decimal fundRetainedCurrentYear = 0;
            decimal fundRetainedPreviousYear = 0;
            decimal fundRetainedDiff = 0;
            decimal fundRetainedPercent = 0;

            decimal fundCommulativeCurrentYear = 0;
            decimal fundCommulativePreviousYear = 0;
            decimal fundCommulativeDiff = 0;
            decimal fundCommulativePercent = 0;

            decimal fundFairValueCurrentYear = 0;
            decimal fundFairValuePreviousYear = 0;
            decimal fundFairValueDiff = 0;
            decimal fundFairValuePercent = 0;

            decimal totalFundCurrYear = 0;
            decimal totalFundPrevYear = 0;
            decimal totalFundDiff = 0;
            decimal totalFundPer = 0;

            //TOTAL FUND BALANCE AND LIABILITIES
            decimal totalFundLiaCurrYear = 0;
            decimal totalFundLiaPrevYear = 0;
            decimal totalFundLiaDiff = 0;
            decimal totalFundLiaPer = 0;
            #endregion  

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    //CURRENT ASSERTS
                    if (r.particulars == "currAssetsCash")
                    {
                        currAssetsCashCurrentYear = r.budgetMonthly;
                        currAssetsCashPreviousYear = r.actual;
                        currAssetsDiff = r.diff;
                        currAssetsPercent = r.per;
                    }
                    if (r.particulars == "currAssetsLoanReceivable")
                    {
                        currAssetsLoanReceivableCurrentYear = r.budgetMonthly;
                        currAssetsLoanReceivablePreviousYear = r.actual;
                        currAssetsLoanReceivableDiff = r.diff;
                        currAssetsLoanReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOtherReceivable")
                    {
                        currAssetsOtherReceivableCurrentYear = r.budgetMonthly;
                        currAssetsOtherReceivablePreviousYear = r.actual;
                        currAssetsOtherReceivableDiff = r.diff;
                        currAssetsOtherReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOther")
                    {
                        currAssetsOtherCurrentYear = r.budgetMonthly;
                        currAssetsOtherPreviousYear = r.actual;
                        currAssetsOtherDiff = r.diff;
                        currAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Current Assets")
                    {
                        totalCurrAssetsCurrYear += r.budgetMonthly;
                        totalCurrAssetsPrevYear += r.actual;
                        totalCurrAssetsDiff += r.diff;
                    }

                    //NONCURRENT ASSETS
                    if (r.particulars == "noncurrAssetsFinancial")
                    {
                        noncurrAssetsFinancialCurrentYear = r.budgetMonthly;
                        noncurrAssetsFinancialPreviousYear = r.actual;
                        noncurrAssetsFinancialDiff = r.diff;
                        noncurrAssetsFinancialPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsRestrictedCash")
                    {
                        noncurrAssetsRestrictedCashCurrentYear = r.budgetMonthly;
                        noncurrAssetsRestrictedCashPreviousYear = r.actual;
                        noncurrAssetsRestrictedCashDiff = r.diff;
                        noncurrAssetsRestrictedCashPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsROUAsset")
                    {
                        noncurrAssetsROUAssetCurrentYear = r.budgetMonthly;
                        noncurrAssetsROUAssetPreviousYear = r.actual;
                        noncurrAssetsROUAssetDiff = r.diff;
                        noncurrAssetsROUAssetPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsProperty")
                    {
                        noncurrAssetsPropertyCurrentYear = r.budgetMonthly;
                        noncurrAssetsPropertyPreviousYear = r.actual;
                        noncurrAssetsPropertyDiff = r.diff;
                        noncurrAssetsPropertyPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsOther")
                    {
                        noncurrAssetsOtherCurrentYear = r.budgetMonthly;
                        noncurrAssetsOtherPreviousYear = r.actual;
                        noncurrAssetsOtherDiff = r.diff;
                        noncurrAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Assets")
                    {
                        totalNonCurrAssetsCurrYear += r.budgetMonthly;
                        totalNonCurrAssetsPrevYear += r.actual;
                        totalNonCurrAssetsDiff += r.diff;
                    }

                    //CURRENT LIABILITIES
                    if (r.particulars == "currLiaTrade")
                    {
                        currLiaTradeCurrentYear = r.budgetMonthly;
                        currLiaTradePreviousYear = r.actual;
                        currLiaTradeDiff = r.diff;
                        currLiaTradePercent = r.per;
                    }
                    if (r.particulars == "currLiaUnearned")
                    {
                        currLiaUnearnedCurrentYear = r.budgetMonthly;
                        currLiaUnearnedPreviousYear = r.actual;
                        currLiaUnearnedDiff = r.diff;
                        currLiaUnearnedPercent = r.per;
                    }
                    if (r.particulars == "currLiaClientCBU")
                    {
                        currLiaClientCBUCurrentYear = r.budgetMonthly;
                        currLiaClientCBUPreviousYear = r.actual;
                        currLiaClientCBUDiff = r.diff;
                        currLiaClientCBUPercent = r.per;
                    }
                    if (r.particulars == "currLiaTaxPayable")
                    {
                        currLiaTaxPayableCurrentYear = r.budgetMonthly;
                        currLiaTaxPayablePreviousYear = r.actual;
                        currLiaTaxPayableDiff = r.diff;
                        currLiaTaxPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaCGLIPayable")
                    {
                        currLiaCGLIPayableCurrentYear = r.budgetMonthly;
                        currLiaCGLIPayablePreviousYear = r.actual;
                        currLiaCGLIPayableDiff = r.diff;
                        currLiaCGLIPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaProvisional")
                    {
                        currLiaProvisionalCurrentYear = r.budgetMonthly;
                        currLiaProvisionalPreviousYear = r.actual;
                        currLiaProvisionalDiff = r.diff;
                        currLiaProvisionalPercent = r.per;
                    }
                    if (r.accntType == "Current Liabilities")
                    {
                        totalCurrLiaCurrYear += r.budgetMonthly;
                        totalCurrLiaPrevYear += r.actual;
                        totalCurrLiaDiff += r.diff;
                    }

                    //NONCURRENT LIABILITIES
                    if (r.particulars == "noncurrLiaLease")
                    {
                        noncurrLiaLeaseCurrentYear = r.budgetMonthly;
                        noncurrLiaLeasePreviousYear = r.actual;
                        noncurrLiaLeaseDiff = r.diff;
                        noncurrLiaLeasePercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaRetirement")
                    {
                        noncurrLiaRetirementCurrentYear = r.budgetMonthly;
                        noncurrLiaRetirementPreviousYear = r.actual;
                        noncurrLiaRetirementDiff = r.diff;
                        noncurrLiaRetirementPercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaDeferred")
                    {
                        noncurrLiaDeferredCurrentYear = r.budgetMonthly;
                        noncurrLiaDeferredPreviousYear = r.actual;
                        noncurrLiaDeferredDiff = r.diff;
                        noncurrLiaDeferredPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Liabilities")
                    {
                        totalNonCurrLiaCurrYear += r.budgetMonthly;
                        totalNonCurrLiaPrevYear += r.actual;
                        totalNonCurrLiaDiff += r.diff;
                    }

                    //FUND BALANCE
                    if (r.particulars == "fundRetained")
                    {
                        fundRetainedCurrentYear = r.budgetMonthly;
                        fundRetainedPreviousYear = r.actual;
                        fundRetainedDiff = r.diff;
                        fundRetainedPercent = r.per;
                    }
                    if (r.particulars == "fundCommulative")
                    {
                        fundCommulativeCurrentYear = r.budgetMonthly;
                        fundCommulativePreviousYear = r.actual;
                        fundCommulativeDiff = r.diff;
                        fundCommulativePercent = r.per;
                    }
                    if (r.particulars == "fundFairValue")
                    {
                        fundFairValueCurrentYear = r.budgetMonthly;
                        fundFairValuePreviousYear = r.actual;
                        fundFairValueDiff = r.diff;
                        fundFairValuePercent = r.per;
                    }
                    if (r.accntType == "Fund Balance")
                    {
                        totalFundCurrYear += r.budgetMonthly;
                        totalFundPrevYear += r.actual;
                        totalFundDiff += r.diff;
                    }
                }
                totalCurrAssetsPer = (totalCurrAssetsDiff / totalCurrAssetsPrevYear) * 100;
                totalNonCurrAssetsPer = (totalNonCurrAssetsDiff / totalNonCurrAssetsPrevYear) * 100;
                totalAssetsCurrYear = totalCurrAssetsCurrYear + totalNonCurrAssetsCurrYear;
                totalAssetsPrevYear = totalCurrAssetsPrevYear + totalNonCurrAssetsPrevYear;
                totalAssetsDiff = totalCurrAssetsDiff + totalNonCurrAssetsDiff;
                totalAssetsPer = (totalAssetsDiff / totalAssetsCurrYear) * 100;

                totalCurrLiaPer = (totalCurrLiaDiff / totalCurrLiaPrevYear) * 100;
                totalNonCurrLiaPer = (totalNonCurrLiaDiff / totalNonCurrLiaPrevYear) * 100;
                totalLiaCurrYear = totalCurrLiaCurrYear + totalNonCurrLiaCurrYear;
                totalLiaPrevYear = totalCurrLiaPrevYear + totalNonCurrLiaPrevYear;
                totalLiaDiff = totalCurrLiaDiff + totalNonCurrLiaDiff;
                totalLiaPer = (totalLiaDiff / totalLiaCurrYear) * 100;

                totalFundPer = (totalFundDiff / totalFundPrevYear) * 100;
                totalFundLiaCurrYear = totalLiaCurrYear + totalFundCurrYear;
                totalFundLiaPrevYear = totalLiaPrevYear + totalFundPrevYear;
                totalFundLiaDiff = totalLiaDiff + totalFundDiff;
                totalFundLiaPer = (totalFundLiaDiff / totalFundLiaCurrYear) * 100;
            }

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("BS Actual VS Budget");
                //HEADER
                ew.Cells["A1:E1"].Merge = true;
                ew.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:E1"].Style.Font.Bold = true;

                ew.Cells["A2:E2"].Merge = true;
                ew.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:E2"].Style.Font.Bold = true;

                ew.Cells["A4:E4"].Merge = true;
                ew.Cells["A4:E4"].Value = "STATEMENT OF ASSETS, LIABILITIES AND FUND BALANCE";
                ew.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:E4"].Style.Font.Bold = true;

                ew.Cells["A5:E5"].Merge = true;
                ew.Cells["A5:E5"].Value = "AS OF " + month + " " + year;
                ew.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:E5"].Style.Font.Bold = true;

                ew.Cells["A:E"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:E7"].Style.Font.Size = 12;
                ew.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                //ew.Cells["B7"].Value = "NOTE";
                ew.Cells["B7"].Value = "BUDGET";
                ew.Cells["C7"].Value = "ACTUAL";
                ew.Cells["D7"].Value = "INCREASE (DECREASE)";
                ew.Cells["D7"].Style.WrapText = true;
                ew.Cells["E7"].Value = "%";
                ew.Cells["A7:E7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A7:E7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A7:E7"].Style.Font.Bold = true;
                //ew.Cells["B11:B53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //ew.Cells["B11:B53"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //ew.Cells["B11:B53"].Style.Numberformat.Format = "0";

                ew.Cells["A8"].Value = "ASSETS";
                ew.Cells["A8"].Style.Font.Bold = true;

                ew.Cells["A10"].Value = "Current Assets";
                ew.Cells["A10"].Style.Font.Bold = true;

                ew.Cells["B11:D53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["E11:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C11:E53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* '-'??_);_(@_)";
                ew.Cells["E11:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                ew.Cells["A11"].Value = "Cash and Cash Equivalents";
                ew.Cells["B11"].Value = currAssetsCashCurrentYear;
                ew.Cells["C11"].Value = currAssetsCashPreviousYear;
                ew.Cells["D11"].Value = currAssetsDiff;
                ew.Cells["E11"].Value = currAssetsPercent / 100;

                ew.Cells["A12"].Value = "Loans Receivable - Net";
                ew.Cells["B12"].Value = currAssetsLoanReceivableCurrentYear;
                ew.Cells["C12"].Value = currAssetsLoanReceivablePreviousYear;
                ew.Cells["D12"].Value = currAssetsLoanReceivableDiff;
                ew.Cells["E12"].Value = currAssetsLoanReceivablePercent / 100;

                ew.Cells["A13"].Value = "Other receivables - net";
                ew.Cells["B13"].Value = currAssetsOtherReceivableCurrentYear;
                ew.Cells["C13"].Value = currAssetsOtherReceivablePreviousYear;
                ew.Cells["D13"].Value = currAssetsOtherReceivableDiff;
                ew.Cells["E13"].Value = currAssetsOtherReceivablePercent / 100;

                ew.Cells["A14"].Value = "Other Current Assets";
                ew.Cells["B14"].Value = currAssetsOtherCurrentYear;
                ew.Cells["C14"].Value = currAssetsOtherPreviousYear;
                ew.Cells["D14"].Value = currAssetsOtherDiff;
                ew.Cells["E14"].Value = currAssetsOtherPercent / 100;
                ew.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Current Assets";
                ew.Cells["B15"].Value = totalCurrAssetsCurrYear;
                ew.Cells["C15"].Value = totalCurrAssetsPrevYear;
                ew.Cells["D15"].Value = totalCurrAssetsDiff;
                ew.Cells["E15"].Value = totalCurrAssetsPer / 100;
                ew.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT ASSETS
                ew.Cells["A17"].Value = "Noncurrent Assets";
                ew.Cells["A17"].Style.Font.Bold = true;

                ew.Cells["A18"].Value = "Financial Assets at FVOCI";
                ew.Cells["B18"].Value = noncurrAssetsFinancialCurrentYear;
                ew.Cells["C18"].Value = noncurrAssetsFinancialPreviousYear;
                ew.Cells["D18"].Value = noncurrAssetsFinancialDiff;
                ew.Cells["E18"].Value = noncurrAssetsFinancialPercent / 100;

                ew.Cells["A19"].Value = "Restricted Cash";
                ew.Cells["B19"].Value = noncurrAssetsRestrictedCashCurrentYear;
                ew.Cells["C19"].Value = noncurrAssetsRestrictedCashPreviousYear;
                ew.Cells["D19"].Value = noncurrAssetsRestrictedCashDiff;
                ew.Cells["E19"].Value = noncurrAssetsRestrictedCashPercent / 100;

                ew.Cells["A20"].Value = "ROU Asset";
                ew.Cells["B20"].Value = noncurrAssetsROUAssetCurrentYear;
                ew.Cells["C20"].Value = noncurrAssetsROUAssetPreviousYear;
                ew.Cells["D20"].Value = noncurrAssetsROUAssetDiff;
                ew.Cells["E20"].Value = noncurrAssetsROUAssetPercent / 100;

                ew.Cells["A21"].Value = "Property and Equipment";
                ew.Cells["B21"].Value = noncurrAssetsPropertyCurrentYear;
                ew.Cells["C21"].Value = noncurrAssetsPropertyPreviousYear;
                ew.Cells["D21"].Value = noncurrAssetsPropertyDiff;
                ew.Cells["E21"].Value = noncurrAssetsPropertyPercent / 100;

                ew.Cells["A22"].Value = "Other Noncurrent Assets";
                ew.Cells["B22"].Value = noncurrAssetsOtherCurrentYear;
                ew.Cells["C22"].Value = noncurrAssetsOtherPreviousYear;
                ew.Cells["D22"].Value = noncurrAssetsOtherDiff;
                ew.Cells["E22"].Value = noncurrAssetsOtherPercent / 100;
                ew.Cells["A22:E22"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A23"].Value = "     Total Noncurrent Assets";
                ew.Cells["B23"].Value = totalNonCurrAssetsCurrYear;
                ew.Cells["C23"].Value = totalNonCurrAssetsPrevYear;
                ew.Cells["D23"].Value = totalNonCurrAssetsDiff;
                ew.Cells["E23"].Value = totalNonCurrAssetsPer / 100;
                ew.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //TOTAL ASSETS
                ew.Cells["A25"].Value = "TOTAL ASSETS";
                ew.Cells["B25"].Value = Math.Round(totalAssetsCurrYear, 0);
                ew.Cells["C25"].Value = Math.Round(totalAssetsPrevYear, 0);
                ew.Cells["D25"].Value = Math.Round(totalAssetsDiff, 0);
                ew.Cells["E25"].Value = totalAssetsPer / 100;
                ew.Cells["A25:E25"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A25:E25"].Style.Font.Bold = true;

                //LIABILITIES and FUND BALANCE
                ew.Cells["A28"].Value = "LIABILITIES AND FUND BALANCE";
                ew.Cells["A28"].Style.Font.Bold = true;

                ew.Cells["A30"].Value = "Current Liabilities";
                ew.Cells["A30"].Style.Font.Bold = true;

                //CURRENT LIABILITIES
                ew.Cells["A31"].Value = "Trade and Other Payables";
                ew.Cells["B31"].Value = currLiaTradeCurrentYear;
                ew.Cells["C31"].Value = currLiaTradePreviousYear;
                ew.Cells["D31"].Value = currLiaTradeDiff;
                ew.Cells["E31"].Value = currLiaTradePercent / 100;

                ew.Cells["A32"].Value = "Unearned Interest Income";
                ew.Cells["B32"].Value = currLiaUnearnedCurrentYear;
                ew.Cells["C32"].Value = currLiaUnearnedPreviousYear;
                ew.Cells["D32"].Value = currLiaUnearnedDiff;
                ew.Cells["E32"].Value = currLiaUnearnedPercent / 100;

                ew.Cells["A33"].Value = "Clients' CBU";
                ew.Cells["B33"].Value = currLiaClientCBUCurrentYear;
                ew.Cells["C33"].Value = currLiaClientCBUPreviousYear;
                ew.Cells["D33"].Value = currLiaClientCBUDiff;
                ew.Cells["E33"].Value = currLiaClientCBUPercent / 100;

                ew.Cells["A34"].Value = "Tax Payable";
                ew.Cells["B34"].Value = currLiaTaxPayableCurrentYear;
                ew.Cells["C34"].Value = currLiaTaxPayablePreviousYear;
                ew.Cells["D34"].Value = currLiaTaxPayableDiff;
                ew.Cells["E34"].Value = currLiaTaxPayablePercent / 100;

                ew.Cells["A35"].Value = "MI/CGLI Payables";
                ew.Cells["B35"].Value = currLiaCGLIPayableCurrentYear;
                ew.Cells["C35"].Value = currLiaCGLIPayablePreviousYear;
                ew.Cells["D35"].Value = currLiaCGLIPayableDiff;
                ew.Cells["E35"].Value = currLiaCGLIPayablePercent / 100;

                ew.Cells["A36"].Value = "Provision for contingenciess";
                ew.Cells["B36"].Value = currLiaProvisionalCurrentYear;
                ew.Cells["C36"].Value = currLiaProvisionalPreviousYear;
                ew.Cells["D36"].Value = currLiaProvisionalDiff;
                ew.Cells["E36"].Value = currLiaProvisionalPercent / 100;
                ew.Cells["A36:E36"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A37"].Value = "     Total Current Liabilities";
                ew.Cells["B37"].Value = totalCurrLiaCurrYear;
                ew.Cells["C37"].Value = totalCurrLiaPrevYear;
                ew.Cells["D37"].Value = totalCurrLiaDiff;
                ew.Cells["E37"].Value = totalCurrLiaPer / 100;
                ew.Cells["A37:E37"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT LIABILITIES
                ew.Cells["A39"].Value = "Noncurrent Liabilities";
                ew.Cells["A39"].Style.Font.Bold = true;

                ew.Cells["A40"].Value = "Lease Liability";
                ew.Cells["B40"].Value = noncurrLiaLeaseCurrentYear;
                ew.Cells["C40"].Value = noncurrLiaLeasePreviousYear;
                ew.Cells["D40"].Value = noncurrLiaLeaseDiff;
                ew.Cells["E40"].Value = noncurrLiaLeasePercent / 100;

                ew.Cells["A41"].Value = "Retirement Benefit Liability";
                ew.Cells["B41"].Value = noncurrLiaRetirementCurrentYear;
                ew.Cells["C41"].Value = noncurrLiaRetirementPreviousYear;
                ew.Cells["D41"].Value = noncurrLiaRetirementDiff;
                ew.Cells["E41"].Value = noncurrLiaRetirementPercent / 100;

                ew.Cells["A42"].Value = "Deferred Tax Liability";
                ew.Cells["B42"].Value = noncurrLiaDeferredCurrentYear;
                ew.Cells["C42"].Value = noncurrLiaDeferredPreviousYear;
                ew.Cells["D42"].Value = noncurrLiaDeferredDiff;
                ew.Cells["E42"].Value = noncurrLiaDeferredPercent / 100;
                ew.Cells["A42:E42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A43"].Value = "     Total Noncurrent Liabilities";
                ew.Cells["B43"].Value = totalNonCurrLiaCurrYear;
                ew.Cells["C43"].Value = totalNonCurrLiaPrevYear;
                ew.Cells["D43"].Value = totalNonCurrLiaDiff;
                ew.Cells["E43"].Value = totalNonCurrLiaPer / 100;
                ew.Cells["A43:E43"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A44"].Value = "     Total Liabilities";
                ew.Cells["B44"].Value = totalLiaCurrYear;
                ew.Cells["C44"].Value = totalLiaPrevYear;
                ew.Cells["D44"].Value = totalLiaDiff;
                ew.Cells["E44"].Value = totalLiaPer / 100;
                ew.Cells["A44:E44"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //FUND BALANCE
                ew.Cells["A46"].Value = "Fund Balance";
                ew.Cells["A46"].Style.Font.Bold = true;

                ew.Cells["A47"].Value = "Retained Earnings";
                ew.Cells["B47"].Value = fundRetainedCurrentYear;
                ew.Cells["C47"].Value = fundRetainedPreviousYear;
                ew.Cells["D47"].Value = fundRetainedDiff;
                ew.Cells["E47"].Value = fundRetainedPercent / 100;

                ew.Cells["A48"].Value = "Cumulative remeasurement gains on";
                ew.Cells["A49"].Value = "     retirement benefit liability";
                ew.Cells["B49"].Value = fundCommulativeCurrentYear;
                ew.Cells["C49"].Value = fundCommulativePreviousYear;
                ew.Cells["D49"].Value = fundCommulativeDiff;
                ew.Cells["E49"].Value = fundCommulativePercent / 100;

                ew.Cells["A50"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                ew.Cells["B50"].Value = fundFairValueCurrentYear;
                ew.Cells["C50"].Value = fundFairValuePreviousYear;
                ew.Cells["D50"].Value = fundFairValueDiff;
                ew.Cells["E50"].Value = fundFairValuePercent / 100;
                ew.Cells["A50:E50"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A51"].Value = "     Total Fund Balance";
                ew.Cells["B51"].Value = totalFundCurrYear;
                ew.Cells["C51"].Value = totalFundPrevYear;
                ew.Cells["D51"].Value = totalFundDiff;
                ew.Cells["E51"].Value = totalFundPer / 100;
                ew.Cells["A51:E51"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A53"].Value = "TOTAL LIABILITIES AND FUND BALANCE";
                ew.Cells["B53"].Value = Math.Round(totalFundLiaCurrYear, 0);
                ew.Cells["C53"].Value = Math.Round(totalFundLiaPrevYear, 0);
                ew.Cells["D53"].Value = Math.Round(totalFundLiaDiff, 0);
                ew.Cells["E53"].Value = totalFundLiaPer / 100;
                ew.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:E53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();

                ew.View.FreezePanes(8, 2);

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "BS Actual VS Budget " + month + year + ".xlsx";

                return fileStreamResult;
            }

        }

        [HttpGet]
        public IActionResult ISPDF(string month, int year)
        {
            var model = GetIncomeStatement(month, year);
            var prevYear = year - 1;

            //REVENUE
            decimal revIntLoansCurrentYear = 0;
            decimal revIntLoansPreviousYear = 0;
            decimal revIntLoansDiff = 0;
            decimal revIntLoansPercent = 0;

            decimal revServiceIncomeCurrentYear = 0;
            decimal revServiceIncomePreviousYear = 0;
            decimal revServiceIncomeDiff = 0;
            decimal revServiceIncomePercent = 0;

            decimal revIntDepositsCurrentYear = 0;
            decimal revIntDepositsPreviousYear = 0;
            decimal revIntDepositsDiff = 0;
            decimal revIntDepositsPercent = 0;

            decimal revForexCurrentYear = 0;
            decimal revForexPreviousYear = 0;
            decimal revForexDiff = 0;
            decimal revForexPercent = 0;

            decimal revOtherCurrentYear = 0;
            decimal revOtherPreviousYear = 0;
            decimal revOtherDiff = 0;
            decimal revOtherPercent = 0;

            decimal revIntExpenseCBUCurrentYear = 0;
            decimal revIntExpenseCBUPreviousYear = 0;
            decimal revIntExpenseCBUDiff = 0;
            decimal revIntExpenseCBUPercent = 0;

            decimal totGrossIncomeCurrYear = 0;
            decimal totGrossIncomePrevYear = 0;
            decimal totGrossIncomeDiff = 0;
            decimal totGrossIncomePer = 0;

            decimal revIntExpenseCurrentYear = 0;
            decimal revIntExpensePreviousYear = 0;
            decimal revIntExpenseDiff = 0;
            decimal revIntExpensePercent = 0;

            decimal totFinancialCostCurrYear = 0;
            decimal totFinancialCostPrevYear = 0;
            decimal totFinancialCostDiff = 0;
            decimal totFinancialCostPer = 0;

            decimal totProvisionLoanCurrYear = 0;
            decimal totProvisionLoanPrevYear = 0;
            decimal totProvisionLoanDiff = 0;
            decimal totProvisionLoanPer = 0;

            decimal revProvisionCurrentYear = 0;
            decimal revProvisionPreviousYear = 0;
            decimal revProvisionDiff = 0;
            decimal revProvisionPercent = 0;

            decimal totNetMarginCurrYear = 0;
            decimal totNetMarginPrevYear = 0;
            decimal totNetMarginDiff = 0;
            decimal totNetMarginPer = 0;

            //EXPENSES
            decimal expPersonnelCurrentYear = 0;
            decimal expPersonnelPreviousYear = 0;
            decimal expPersonnelDiff = 0;
            decimal expPersonnelPercent = 0;

            decimal expOutstationCurrentYear = 0;
            decimal expOutstationPreviousYear = 0;
            decimal expOutstationDiff = 0;
            decimal expOutstationPercent = 0;

            decimal expFringeCurrentYear = 0;
            decimal expFringePreviousYear = 0;
            decimal expFringeDiff = 0;
            decimal expFringePercent = 0;

            decimal expRetirementCurrentYear = 0;
            decimal expRetirementPreviousYear = 0;
            decimal expRetirementDiff = 0;
            decimal expRetirementPercent = 0;

            decimal expOutsideCurrentYear = 0;
            decimal expOutsidePreviousYear = 0;
            decimal expOutsideDiff = 0;
            decimal expOutsidePercent = 0;

            decimal expProfFeesCurrentYear = 0;
            decimal expProfFeesPreviousYear = 0;
            decimal expProfFeesDiff = 0;
            decimal expProfFeesPercent = 0;

            decimal expMeetingsCurrentYear = 0;
            decimal expMeetingsPreviousYear = 0;
            decimal expMeetingsDiff = 0;
            decimal expMeetingsPercent = 0;

            decimal expClientCurrentYear = 0;
            decimal expClientPreviousYear = 0;
            decimal expClientDiff = 0;
            decimal expClientPercent = 0;

            decimal expLitigationCurrentYear = 0;
            decimal expLitigationPreviousYear = 0;
            decimal expLitigationDiff = 0;
            decimal expLitigationPercent = 0;

            decimal expSuppliesCurrentYear = 0;
            decimal expSuppliesPreviousYear = 0;
            decimal expSuppliesDiff = 0;
            decimal expSuppliesPercent = 0;

            decimal expUtilitiesCurrentYear = 0;
            decimal expUtilitiesPreviousYear = 0;
            decimal expUtilitiesDiff = 0;
            decimal expUtilitiesPercent = 0;

            decimal expCommunicationCurrentYear = 0;
            decimal expCommunicationPreviousYear = 0;
            decimal expCommunicationDiff = 0;
            decimal expCommunicationPercent = 0;

            decimal expRentalCurrentYear = 0;
            decimal expRentalPreviousYear = 0;
            decimal expRentalDiff = 0;
            decimal expRentalPercent = 0;

            decimal expInsuranceCurrentYear = 0;
            decimal expInsurancePreviousYear = 0;
            decimal expInsuranceDiff = 0;
            decimal expInsurancePercent = 0;

            decimal expTranspoCurrentYear = 0;
            decimal expTranspoPreviousYear = 0;
            decimal expTranspoDiff = 0;
            decimal expTranspoPercent = 0;

            decimal expTaxesCurrentYear = 0;
            decimal expTaxesPreviousYear = 0;
            decimal expTaxesDiff = 0;
            decimal expTaxesPercent = 0;

            decimal expRepairsCurrentYear = 0;
            decimal expRepairsPreviousYear = 0;
            decimal expRepairsDiff = 0;
            decimal expRepairsPercent = 0;

            decimal expDepreciationCurrentYear = 0;
            decimal expDepreciationPreviousYear = 0;
            decimal expDepreciationDiff = 0;
            decimal expDepreciationPercent = 0;

            decimal expProvisionCurrentYear = 0;
            decimal expProvisionPreviousYear = 0;
            decimal expProvisionDiff = 0;
            decimal expProvisionPercent = 0;

            decimal expOthersCurrentYear = 0;
            decimal expOthersPreviousYear = 0;
            decimal expOthersDiff = 0;
            decimal expOthersPercent = 0;

            //INCOME TAX

            decimal incomeTaxExpenseCurrentYear = 0;
            decimal incomeTaxExpensePreviousYear = 0;
            decimal incomeTaxExpenseDiff = 0;
            decimal incomeTaxExpensePercent = 0;

            decimal totExpensesCurrYear = 0;
            decimal totExpensesPrevYear = 0;
            decimal totExpensesDiff = 0;
            decimal totExpensesPer = 0;

            decimal totIncomeCurrYear = 0;
            decimal totIncomePrevYear = 0;
            decimal totIncomeDiff = 0;
            decimal totIncomePer = 0;

            decimal totIncomeTaxCurrYear = 0;
            decimal totIncomeTaxPrevYear = 0;
            decimal totIncomeTaxDiff = 0;
            decimal totIncomeTaxPer = 0;

            decimal totNetIncomeCurrYear = 0;
            decimal totNetIncomePrevYear = 0;
            decimal totNetIncomeDiff = 0;
            decimal totNetIncomePer = 0;

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        revIntLoansCurrentYear = r.currAmount;
                        revIntLoansPreviousYear = r.prevAmount;
                        revIntLoansDiff = r.diff;
                        revIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        revServiceIncomeCurrentYear = r.currAmount;
                        revServiceIncomePreviousYear = r.prevAmount;
                        revServiceIncomeDiff = r.diff;
                        revServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        revIntDepositsCurrentYear = r.currAmount;
                        revIntDepositsPreviousYear = r.prevAmount;
                        revIntDepositsDiff = r.diff;
                        revIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        revForexCurrentYear = r.currAmount;
                        revForexPreviousYear = r.prevAmount;
                        revForexDiff = r.diff;
                        revForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        revOtherCurrentYear = r.currAmount;
                        revOtherPreviousYear = r.prevAmount;
                        revOtherDiff = r.diff;
                        revOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        totGrossIncomeCurrYear += r.currAmount;
                        totGrossIncomePrevYear += r.prevAmount;
                        totGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        revIntExpenseCurrentYear = r.currAmount;
                        revIntExpensePreviousYear = r.prevAmount;
                        revIntExpenseDiff = r.diff;
                        revIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        revIntExpenseCBUCurrentYear = r.currAmount;
                        revIntExpenseCBUPreviousYear = r.prevAmount;
                        revIntExpenseCBUDiff = r.diff;
                        revIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        totFinancialCostCurrYear += r.currAmount;
                        totFinancialCostPrevYear += r.prevAmount;
                        totFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        revProvisionCurrentYear = r.currAmount;
                        revProvisionPreviousYear = r.prevAmount;
                        revProvisionDiff = r.diff;
                        revProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        totProvisionLoanCurrYear += r.currAmount;
                        totProvisionLoanPrevYear += r.prevAmount;
                        totProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        expPersonnelCurrentYear = r.currAmount;
                        expPersonnelPreviousYear = r.prevAmount;
                        expPersonnelDiff = r.diff;
                        expPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        expOutstationCurrentYear = r.currAmount;
                        expOutstationPreviousYear = r.prevAmount;
                        expOutstationDiff = r.diff;
                        expOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        expFringeCurrentYear = r.currAmount;
                        expFringePreviousYear = r.prevAmount;
                        expFringeDiff = r.diff;
                        expFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        expRetirementCurrentYear = r.currAmount;
                        expRetirementPreviousYear = r.prevAmount;
                        expRetirementDiff = r.diff;
                        expRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        expOutsideCurrentYear = r.currAmount;
                        expOutsidePreviousYear = r.prevAmount;
                        expOutsideDiff = r.diff;
                        expOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        expProfFeesCurrentYear = r.currAmount;
                        expProfFeesPreviousYear = r.prevAmount;
                        expProfFeesDiff = r.diff;
                        expProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        expMeetingsCurrentYear = r.currAmount;
                        expMeetingsPreviousYear = r.prevAmount;
                        expMeetingsDiff = r.diff;
                        expMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        expClientCurrentYear = r.currAmount;
                        expClientPreviousYear = r.prevAmount;
                        expClientDiff = r.diff;
                        expClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        expLitigationCurrentYear = r.currAmount;
                        expLitigationPreviousYear = r.prevAmount;
                        expLitigationDiff = r.diff;
                        expLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        expSuppliesCurrentYear = r.currAmount;
                        expSuppliesPreviousYear = r.prevAmount;
                        expSuppliesDiff = r.diff;
                        expSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        expUtilitiesCurrentYear = r.currAmount;
                        expUtilitiesPreviousYear = r.prevAmount;
                        expUtilitiesDiff = r.diff;
                        expUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        expCommunicationCurrentYear = r.currAmount;
                        expCommunicationPreviousYear = r.prevAmount;
                        expCommunicationDiff = r.diff;
                        expCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        expRentalCurrentYear = r.currAmount;
                        expRentalPreviousYear = r.prevAmount;
                        expRentalDiff = r.diff;
                        expRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        expInsuranceCurrentYear = r.currAmount;
                        expInsurancePreviousYear = r.prevAmount;
                        expInsuranceDiff = r.diff;
                        expInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        expTranspoCurrentYear = r.currAmount;
                        expTranspoPreviousYear = r.prevAmount;
                        expTranspoDiff = r.diff;
                        expTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        expTaxesCurrentYear = r.currAmount;
                        expTaxesPreviousYear = r.prevAmount;
                        expTaxesDiff = r.diff;
                        expTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        expRepairsCurrentYear = r.currAmount;
                        expRepairsPreviousYear = r.prevAmount;
                        expRepairsDiff = r.diff;
                        expRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        expDepreciationCurrentYear = r.currAmount;
                        expDepreciationPreviousYear = r.prevAmount;
                        expDepreciationDiff = r.diff;
                        expDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        expProvisionCurrentYear = r.currAmount;
                        expProvisionPreviousYear = r.prevAmount;
                        expProvisionDiff = r.diff;
                        expProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        expOthersCurrentYear = r.currAmount;
                        expOthersPreviousYear = r.prevAmount;
                        expOthersDiff = r.diff;
                        expOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        totExpensesCurrYear += r.currAmount;
                        totExpensesPrevYear += r.prevAmount;
                        totExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        incomeTaxExpenseCurrentYear = r.currAmount;
                        incomeTaxExpensePreviousYear = r.prevAmount;
                        incomeTaxExpenseDiff = r.diff;
                        incomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        totIncomeTaxCurrYear += r.currAmount;
                        totIncomeTaxPrevYear += r.prevAmount;
                        totIncomeTaxDiff += r.diff;
                    }
                }
                totGrossIncomePer = (totGrossIncomeDiff / totGrossIncomePrevYear) * 100;
                totFinancialCostPer = (totFinancialCostDiff / totFinancialCostPrevYear) * 100;

                totNetMarginCurrYear = totGrossIncomeCurrYear - totFinancialCostCurrYear - totProvisionLoanCurrYear;
                totNetMarginPrevYear = totGrossIncomePrevYear - totFinancialCostPrevYear - totProvisionLoanPrevYear;
                totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                totNetMarginPer = (totNetMarginDiff / totNetMarginPrevYear) * 100;


                totExpensesPer = (totExpensesDiff / totExpensesPrevYear) * 100;
                //TOTAL INCOME
                totIncomeCurrYear = totNetMarginCurrYear - totExpensesCurrYear;
                totIncomePrevYear = totNetMarginPrevYear - totExpensesPrevYear;
                totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                totIncomePer = (totIncomeDiff / totIncomePrevYear * 100);

                //NET INCOME
                totNetIncomeCurrYear = totIncomeCurrYear - totIncomeTaxCurrYear;
                totNetIncomePrevYear = totIncomePrevYear - totIncomeTaxPrevYear;
                totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                totNetIncomePer = (totNetIncomeDiff / totNetIncomePrevYear) * 100;
            }

            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.Blink);

            BlinkConverterSettings blinkConverterSettings = new BlinkConverterSettings();
            string HTMLContent = "<html lang='en'><head>" +
                    "<title>Balance Sheet</title>" +
                    "<link href=" + Path.Combine(_hostingEnvironment.ContentRootPath, "ReportCss/bs.css") + " rel='stylesheet' />" +
                    "</head>" +
                    "<body>" +
                    //"<div class='container'>" +

                    "<table width='100%'>" +
                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><b>KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)</b></td>" +
                        "</tr>" +

                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><b>(A Non-Stock, Non-Profit Organization)</b></td>" +
                        "</tr>" +

                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><b>STATEMENT OF COMPREHENSIVE INCOME</b></td>" +
                        "</tr>" +

                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><div id='bsMonthYear' style='font-weight:bold'>AS OF " + month + " " + year + "</div></td>" +
                        "</tr>" +
                        "<tr style='text-align:center'>" +
                            "<td colspan='6'><div id='isComparative' style='font-weight:bold'></div></td> " +
                        "</tr>" +
                         "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr>" +
                                    "<td>&nbsp;</td>" +
                                    "<td><b>NOTES</b></td>" +
                                    "<td style='text-align:center'><div id='bsDetailCurrentYear' style='font-weight:bold'>" + year + "</div></td>" +
                                    "<td style='text-align:center'><div id='bsDetailPreviousYear' style='font-weight:bold'>" + prevYear + "</div></td>" +
                                    "<td style='text-align:center'><b>INCREASE <br />(DECREASE) </b></td>" +
                                    "<td style='text-align:center'><b>%</b></td>" +
                                "</tr>" +
                        "<tr>" +
                            "<td><b>REVENUE</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                        
                        "<tr>" +
                            "<td>Interest on Loans</td>" +
                            "<td style='text-align:center'>22</td>" +
                            "<td style='text-align:right'><div id='revIntLoansCurrentYear'>" + revIntLoansCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntLoansPreviousYear'>" + revIntLoansPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntLoansDiff'>" + revIntLoansDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revIntLoansPercent'>" + revIntLoansPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Service Income</td>" +
                            "<td style='text-align:center'>23</td>" +
                            "<td style='text-align:right'><div id='revServiceIncomeCurrentYear'>" + revServiceIncomeCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revServiceIncomePreviousYear'>" + revServiceIncomePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revServiceIncomeDiff'>" + revServiceIncomeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revServiceIncomePercent'>" + revServiceIncomePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Interest on Deposit and Placements</td>" +
                            "<td style='text-align:center'>24</td>" +
                            "<td style='text-align:right'><div id='revIntDepositsCurrentYear'>" + revIntDepositsCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntDepositsPreviousYear'>" + revIntDepositsPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntDepositsDiff'>" + revIntDepositsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revIntDepositsPercent'>" + revIntDepositsPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Forex Gain or Loss</td>" +
                            "<td style='text-align:center'>25</td>" +
                            "<td style='text-align:right'><div id='revForexCurrentYear'>" + revForexCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revForexPreviousYear'>" + revForexPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revForexDiff'>" + revForexDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revForexPercent'>" + revForexPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>Other Income</td>" +
                            "<td style='text-align:center'>26</td>" +
                            "<td style='text-align:right'><div id='revOtherCurrentYear'>" + revOtherCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revOtherPreviousYear'>" + revOtherPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revOtherDiff'>" + revOtherDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revOtherPercent'>" + revOtherPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Gross Income</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totGrossIncomeCurrentYear'>" + totGrossIncomeCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totGrossIncomePreviousYear'>" + totGrossIncomePrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totGrossIncomeDiff'>" + totGrossIncomeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totGrossIncomePercent'>" + totGrossIncomePer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +

                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Interest Expense</td>" +
                            "<td style='text-align:center'>27</td>" +
                            "<td style='text-align:right'><div id='revIntExpenseCurrentYear'>" + revIntExpenseCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntExpensePreviousYear'>" + revIntExpensePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntExpenseDiff'>" + revIntExpenseDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revIntExpensePercent'>" + revIntExpensePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>Interest Expense on Clients' CBU</td>" +
                            "<td style='text-align:center'>28</td>" +
                            "<td style='text-align:right'><div id='revIntExpenseCBUCurrentYear'>" + revIntExpenseCBUCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntExpenseCBUPreviousYear'>" + revIntExpenseCBUPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revIntExpenseCBUDiff'>" + revIntExpenseCBUDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revIntExpenseCBUPercent'>" + revIntExpenseCBUPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Financial Cost</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totFinancialCostCurrentYear'>" + totFinancialCostCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totFinancialCostPreviousYear'>" + totFinancialCostPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totFinancialCostDiff'>" + totFinancialCostDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totFinancialCostPercent'>" + totFinancialCostPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                         "<tr class='total'>" +
                            "<td>Provision for Loan Losses</td>" +
                            "<td style='text-align:center'>29</td>" +
                            "<td style='text-align:right'><div id='revProvisionCurrentYear'>" + revProvisionCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revProvisionPreviousYear'>" + revProvisionPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='revProvisionDiff'>" + revProvisionDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='revProvisionPercent'>" + revProvisionPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Net Margin</td>" +
                            "<td></td>" +
                            "<td style='text-align:right'><div id='totNetMarginCurrentYear'>" + totNetMarginCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNetMarginPreviousYear'>" + totNetMarginPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNetMarginDiff'>" + totNetMarginDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totNetMarginPercent'>" + totNetMarginPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td><b>EXPENSES</b></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                        "</tr>" +
                         "<tr>" +
                            "<td>Personnel Cost</td>" +
                            "<td style='text-align:center'>30</td>" +
                            "<td style='text-align:right'><div id='expPersonnelCurrentYear'>" + expPersonnelCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expPersonnelPreviousYear'>" + expPersonnelPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expPersonnelDiff'>" + expPersonnelDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expPersonnelPercent'>" + expPersonnelPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Outstation</td>" +
                            "<td style='text-align:center'>31</td>" +
                            "<td style='text-align:right'><div id='expOutstationCurrentYear'>" + expOutstationCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expOutstationPreviousYear'>" + expOutstationPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expOutstationDiff'>" + expOutstationDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expOutstationPercent'>" + expOutstationPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Fringe Benefit</td>" +
                            "<td style='text-align:center'>32</td>" +
                            "<td style='text-align:right'><div id='expFringeCurrentYear'>" + expFringeCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expFringePreviousYear'>" + expFringePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expFringeDiff'>" + expFringeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expFringePercent'>" + expFringePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Retirement</td>" +
                            "<td style='text-align:center'>33</td>" +
                            "<td style='text-align:right'><div id='expRetirementCurrentYear'>" + expRetirementCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expRetirementPreviousYear'>" + expRetirementPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expRetirementDiff'>" + expRetirementDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expRetirementPercent'>" + expRetirementPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                         "<tr>" +
                            "<td>Outside Services</td>" +
                            "<td style='text-align:center'>34</td>" +
                            "<td style='text-align:right'><div id='expOutsideCurrentYear'>" + expOutsideCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expOutsidePreviousYear'>" + expOutsidePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expOutsideDiff'>" + expOutsideDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expOutsidePercent'>" + expOutsidePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Professional Fees</td>" +
                            "<td style='text-align:center'>35</td>" +
                            "<td style='text-align:right'><div id='expProfFeesCurrentYear'>" + expProfFeesCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expProfFeesPreviousYear'>" + expProfFeesPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expProfFeesDiff'>" + expProfFeesDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expProfFeesPercent'>" + expProfFeesPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Meetings, Training and Conferences</td>" +
                            "<td style='text-align:center'>36</td>" +
                            "<td style='text-align:right'><div id='expMeetingsCurrentYear'>" + expMeetingsCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expMeetingsPreviousYear'>" + expMeetingsPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expMeetingsDiff'>" + expMeetingsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expMeetingsPercent'>" + expMeetingsPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Client and Community Services</td>" +
                            "<td style='text-align:center'>37</td>" +
                            "<td style='text-align:right'><div id='expClientCurrentYear'>" + expClientCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expClientPreviousYear'>" + expClientPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expClientDiff'>" + expClientDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expClientPercent'>" + expClientPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Litigation</td>" +
                            "<td style='text-align:center'>38</td>" +
                            "<td style='text-align:right'><div id='expLitigationCurrentYear'>" + expLitigationCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expLitigationPreviousYear'>" + expLitigationPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expLitigationDiff'>" + expLitigationDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expLitigationPercent'>" + expLitigationPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Supplies</td>" +
                            "<td style='text-align:center'>39</td>" +
                            "<td style='text-align:right'><div id='expSuppliesCurrentYear'>" + expSuppliesCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expSuppliesPreviousYear'>" + expSuppliesPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expSuppliesDiff'>" + expSuppliesDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expSuppliesPercent'>" + expSuppliesPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Utilities</td>" +
                            "<td style='text-align:center'>40</td>" +
                            "<td style='text-align:right'><div id='expUtilitiesCurrentYear'>" + expUtilitiesCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expUtilitiesPreviousYear'>" + expUtilitiesPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expUtilitiesDiff'>" + expUtilitiesDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expUtilitiesPercent'>" + expUtilitiesPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Communication and Courier</td>" +
                            "<td style='text-align:center'>41</td>" +
                            "<td style='text-align:right'><div id='expCommunicationCurrentYear'>" + expCommunicationCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expCommunicationPreviousYear'>" + expCommunicationPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expCommunicationDiff'>" + expCommunicationDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expCommunicationPercent'>" + expCommunicationPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                         "<tr>" +
                            "<td>Rental</td>" +
                            "<td style='text-align:center'>42</td>" +
                            "<td style='text-align:right'><div id='expRentalCurrentYear'>" + expRentalCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expRentalPreviousYear'>" + expRentalPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expRentalDiff'>" + expRentalDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expRentalPercent'>" + expRentalPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Insurance</td>" +
                            "<td style='text-align:center'>43</td>" +
                            "<td style='text-align:right'><div id='expInsuranceCurrentYear'>" + expInsuranceCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expInsurancePreviousYear'>" + expInsurancePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expInsuranceDiff'>" + expInsuranceDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expInsurancePercent'>" + expInsurancePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Transportation and Travel</td>" +
                            "<td style='text-align:center'>44</td>" +
                            "<td style='text-align:right'><div id='expTranspoCurrentYear'>" + expTranspoCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expTranspoPreviousYear'>" + expTranspoPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expTranspoDiff'>" + expTranspoDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expTranspoPercent'>" + expTranspoPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Taxes and Licenses</td>" +
                            "<td style='text-align:center'>45</td>" +
                            "<td style='text-align:right'><div id='expTaxesCurrentYear'>" + expTaxesCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expTaxesPreviousYear'>" + expTaxesPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expTaxesDiff'>" + expTaxesDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expTaxesPercent'>" + expTaxesPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                         "<tr>" +
                            "<td>Repairs and Maintenance</td>" +
                            "<td style='text-align:center'>46</td>" +
                            "<td style='text-align:right'><div id='expRepairsCurrentYear'>" + expRepairsCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expRepairsPreviousYear'>" + expRepairsPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expRepairsDiff'>" + expRepairsDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expRepairsPercent'>" + expRepairsPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Depreciation and Amortization</td>" +
                            "<td style='text-align:center'>47</td>" +
                            "<td style='text-align:right'><div id='expDepreciationCurrentYear'>" + expDepreciationCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expDepreciationPreviousYear'>" + expDepreciationPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expDepreciationDiff'>" + expDepreciationDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expDepreciationPercent'>" + expDepreciationPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Provision for Other Losses</td>" +
                            "<td style='text-align:center'>48</td>" +
                            "<td style='text-align:right'><div id='expProvisionCurrentYear'>" + expProvisionCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expProvisionPreviousYear'>" + expProvisionPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expProvisionDiff'>" + expProvisionDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expProvisionPercent'>" + expProvisionPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>Others</td>" +
                            "<td style='text-align:center'>49</td>" +
                            "<td style='text-align:right'><div id='expOthersCurrentYear'>" + expOthersCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expOthersPreviousYear'>" + expOthersPreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='expOthersDiff'>" + expOthersDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='expOthersPercent'>" + expOthersPercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Expense</td>" +
                            "<td style='text-align:center'></td>" +
                            "<td style='text-align:right'><div id='totExpensesCurrentYear'>" + totExpensesCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totExpensesPreviousYear'>" + totExpensesPrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totExpensesDiff'>" + totExpensesDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totExpensesPercent'>" + totExpensesPer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr style='font-weight:bold;' class='total'>" +
                            "<td>INCOME (LOSS) BEFORE TAX</td>" +
                            "<td style='text-align:center'></td>" +
                            "<td style='text-align:right'><div id='totIncomeCurrentYear'>" + totIncomeCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totIncomePreviousYear'>" + totIncomePrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totIncomeDiff'>" + totIncomeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totIncomePercent'>" + totIncomePer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr  style='font-weight:bold;'>" +
                            "<td>INCOME TAX EXPENSE</td>" +
                            "<td style='text-align:center'>50</td>" +
                            "<td style='text-align:right'><div id='incomeTaxExpenseCurrentYear'>" + incomeTaxExpenseCurrentYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='incomeTaxExpensePreviousYear'>" + incomeTaxExpensePreviousYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='incomeTaxExpenseDiff'>" + incomeTaxExpenseDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='incomeTaxExpensePercent'>" + incomeTaxExpensePercent.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr class='total'>" +
                            "<td colspan='6'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr class='grandtotal'>" +
                            "<td>NET INCOME</td>" +
                            "<td style='text-align:center'></td>" +
                            "<td style='text-align:right'><div id='totNetIncomeCurrentYear'>" + totNetIncomeCurrYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNetIncomePreviousYear'>" + totNetIncomePrevYear.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:right'><div id='totNetIncomeDiff'>" + totNetIncomeDiff.ToString("#,##0.00;(#,##0.00);-") + "</div></td>" +
                            "<td style='text-align:center'><div id='totNetIncomePercent'>" + totNetIncomePer.ToString("#,##0.00;(#,##0.00);-") + "%</div></td>" +
                        "</tr>" +
                    "</table>" +
                    //"</div>" +
                    "</body></html>";
            string baseUrl = @"D:/Hey";
            //Set the BlinkBinaries folder path
            blinkConverterSettings.BlinkPath = Path.Combine(_hostingEnvironment.ContentRootPath, "BlinkBinariesWindows");

            //Assign Blink converter settings to HTML converter
            htmlConverter.ConverterSettings = blinkConverterSettings;
            blinkConverterSettings.Margin = new PdfMargins { All = 10 };
            //Convert URL to PDF
            PdfDocument document = htmlConverter.Convert(HTMLContent, baseUrl);
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            string mimeType = "application/pdf";
            Response.Headers.Add("content-disposition", "inline;filename=" + "IS" + month + year + ".pdf");
            return File(stream.ToArray(), mimeType);
        }
        
        [HttpGet]
        public IActionResult ExcelReports(string month, int year)
        {
            //BALANCE SHEET
            #region

            var model = GetBalanceSheet(month, year);
            var prevYear = year - 1;

            //ASSETS
            //CURRENT ASSETS
            decimal currAssetsCashCurrentYear = 0;
            decimal currAssetsCashPreviousYear = 0;
            decimal currAssetsDiff = 0;
            decimal currAssetsPercent = 0;

            decimal currAssetsLoanReceivableCurrentYear = 0;
            decimal currAssetsLoanReceivablePreviousYear = 0;
            decimal currAssetsLoanReceivableDiff = 0;
            decimal currAssetsLoanReceivablePercent = 0;

            decimal currAssetsOtherReceivableCurrentYear = 0;
            decimal currAssetsOtherReceivablePreviousYear = 0;
            decimal currAssetsOtherReceivableDiff = 0;
            decimal currAssetsOtherReceivablePercent = 0;

            decimal currAssetsOtherCurrentYear = 0;
            decimal currAssetsOtherPreviousYear = 0;
            decimal currAssetsOtherDiff = 0;
            decimal currAssetsOtherPercent = 0;

            decimal totalCurrAssetsCurrYear = 0;
            decimal totalCurrAssetsPrevYear = 0;
            decimal totalCurrAssetsDiff = 0;
            decimal totalCurrAssetsPer = 0;

            //NONCURRENT ASSETS
            decimal noncurrAssetsFinancialCurrentYear = 0;
            decimal noncurrAssetsFinancialPreviousYear = 0;
            decimal noncurrAssetsFinancialDiff = 0;
            decimal noncurrAssetsFinancialPercent = 0;

            decimal noncurrAssetsRestrictedCashCurrentYear = 0;
            decimal noncurrAssetsRestrictedCashPreviousYear = 0;
            decimal noncurrAssetsRestrictedCashDiff = 0;
            decimal noncurrAssetsRestrictedCashPercent = 0;

            decimal noncurrAssetsROUAssetCurrentYear = 0;
            decimal noncurrAssetsROUAssetPreviousYear = 0;
            decimal noncurrAssetsROUAssetDiff = 0;
            decimal noncurrAssetsROUAssetPercent = 0;

            decimal noncurrAssetsPropertyCurrentYear = 0;
            decimal noncurrAssetsPropertyPreviousYear = 0;
            decimal noncurrAssetsPropertyDiff = 0;
            decimal noncurrAssetsPropertyPercent = 0;

            decimal noncurrAssetsOtherCurrentYear = 0;
            decimal noncurrAssetsOtherPreviousYear = 0;
            decimal noncurrAssetsOtherDiff = 0;
            decimal noncurrAssetsOtherPercent = 0;

            decimal totalNonCurrAssetsCurrYear = 0;
            decimal totalNonCurrAssetsPrevYear = 0;
            decimal totalNonCurrAssetsDiff = 0;
            decimal totalNonCurrAssetsPer = 0;

            decimal totalAssetsCurrYear = 0;
            decimal totalAssetsPrevYear = 0;
            decimal totalAssetsDiff = 0;
            decimal totalAssetsPer = 0;

            //LIABILITIES
            //CURRENT LIA
            decimal currLiaTradeCurrentYear = 0;
            decimal currLiaTradePreviousYear = 0;
            decimal currLiaTradeDiff = 0;
            decimal currLiaTradePercent = 0;

            decimal currLiaUnearnedCurrentYear = 0;
            decimal currLiaUnearnedPreviousYear = 0;
            decimal currLiaUnearnedDiff = 0;
            decimal currLiaUnearnedPercent = 0;

            decimal currLiaClientCBUCurrentYear = 0;
            decimal currLiaClientCBUPreviousYear = 0;
            decimal currLiaClientCBUDiff = 0;
            decimal currLiaClientCBUPercent = 0;

            decimal currLiaTaxPayableCurrentYear = 0;
            decimal currLiaTaxPayablePreviousYear = 0;
            decimal currLiaTaxPayableDiff = 0;
            decimal currLiaTaxPayablePercent = 0;

            decimal currLiaCGLIPayableCurrentYear = 0;
            decimal currLiaCGLIPayablePreviousYear = 0;
            decimal currLiaCGLIPayableDiff = 0;
            decimal currLiaCGLIPayablePercent = 0;

            decimal currLiaProvisionalCurrentYear = 0;
            decimal currLiaProvisionalPreviousYear = 0;
            decimal currLiaProvisionalDiff = 0;
            decimal currLiaProvisionalPercent = 0;

            decimal totalCurrLiaCurrYear = 0;
            decimal totalCurrLiaPrevYear = 0;
            decimal totalCurrLiaDiff = 0;
            decimal totalCurrLiaPer = 0;

            //NONCURRENT LIA
            decimal noncurrLiaLeaseCurrentYear = 0;
            decimal noncurrLiaLeasePreviousYear = 0;
            decimal noncurrLiaLeaseDiff = 0;
            decimal noncurrLiaLeasePercent = 0;

            decimal noncurrLiaRetirementCurrentYear = 0;
            decimal noncurrLiaRetirementPreviousYear = 0;
            decimal noncurrLiaRetirementDiff = 0;
            decimal noncurrLiaRetirementPercent = 0;

            decimal noncurrLiaDeferredCurrentYear = 0;
            decimal noncurrLiaDeferredPreviousYear = 0;
            decimal noncurrLiaDeferredDiff = 0;
            decimal noncurrLiaDeferredPercent = 0;

            decimal totalNonCurrLiaCurrYear = 0;
            decimal totalNonCurrLiaPrevYear = 0;
            decimal totalNonCurrLiaDiff = 0;
            decimal totalNonCurrLiaPer = 0;

            decimal totalLiaCurrYear = 0;
            decimal totalLiaPrevYear = 0;
            decimal totalLiaDiff = 0;
            decimal totalLiaPer = 0;

            //FUND BALANCE
            decimal fundRetainedCurrentYear = 0;
            decimal fundRetainedPreviousYear = 0;
            decimal fundRetainedDiff = 0;
            decimal fundRetainedPercent = 0;

            decimal fundCommulativeCurrentYear = 0;
            decimal fundCommulativePreviousYear = 0;
            decimal fundCommulativeDiff = 0;
            decimal fundCommulativePercent = 0;

            decimal fundFairValueCurrentYear = 0;
            decimal fundFairValuePreviousYear = 0;
            decimal fundFairValueDiff = 0;
            decimal fundFairValuePercent = 0;

            decimal totalFundCurrYear = 0;
            decimal totalFundPrevYear = 0;
            decimal totalFundDiff = 0;
            decimal totalFundPer = 0;

            //TOTAL FUND BALANCE AND LIABILITIES
            decimal totalFundLiaCurrYear = 0;
            decimal totalFundLiaPrevYear = 0;
            decimal totalFundLiaDiff = 0;
            decimal totalFundLiaPer = 0;
            

            if (model.Count > 0)
            {
                foreach (var r in model)
                {
                    //CURRENT ASSERTS
                    if (r.particulars == "currAssetsCash")
                    {
                        currAssetsCashCurrentYear = r.currAmount;
                        currAssetsCashPreviousYear = r.prevAmount;
                        currAssetsDiff = r.diff;
                        currAssetsPercent = r.per;
                    }
                    if (r.particulars == "currAssetsLoanReceivable")
                    {
                        currAssetsLoanReceivableCurrentYear = r.currAmount;
                        currAssetsLoanReceivablePreviousYear = r.prevAmount;
                        currAssetsLoanReceivableDiff = r.diff;
                        currAssetsLoanReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOtherReceivable")
                    {
                        currAssetsOtherReceivableCurrentYear = r.currAmount;
                        currAssetsOtherReceivablePreviousYear = r.prevAmount;
                        currAssetsOtherReceivableDiff = r.diff;
                        currAssetsOtherReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOther")
                    {
                        currAssetsOtherCurrentYear = r.currAmount;
                        currAssetsOtherPreviousYear = r.prevAmount;
                        currAssetsOtherDiff = r.diff;
                        currAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Current Assets")
                    {
                        totalCurrAssetsCurrYear += r.currAmount;
                        totalCurrAssetsPrevYear += r.prevAmount;
                        totalCurrAssetsDiff += r.diff;
                    }

                    //NONCURRENT ASSETS
                    if (r.particulars == "noncurrAssetsFinancial")
                    {
                        noncurrAssetsFinancialCurrentYear = r.currAmount;
                        noncurrAssetsFinancialPreviousYear = r.prevAmount;
                        noncurrAssetsFinancialDiff = r.diff;
                        noncurrAssetsFinancialPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsRestrictedCash")
                    {
                        noncurrAssetsRestrictedCashCurrentYear = r.currAmount;
                        noncurrAssetsRestrictedCashPreviousYear = r.prevAmount;
                        noncurrAssetsRestrictedCashDiff = r.diff;
                        noncurrAssetsRestrictedCashPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsROUAsset")
                    {
                        noncurrAssetsROUAssetCurrentYear = r.currAmount;
                        noncurrAssetsROUAssetPreviousYear = r.prevAmount;
                        noncurrAssetsROUAssetDiff = r.diff;
                        noncurrAssetsROUAssetPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsProperty")
                    {
                        noncurrAssetsPropertyCurrentYear = r.currAmount;
                        noncurrAssetsPropertyPreviousYear = r.prevAmount;
                        noncurrAssetsPropertyDiff = r.diff;
                        noncurrAssetsPropertyPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsOther")
                    {
                        noncurrAssetsOtherCurrentYear = r.currAmount;
                        noncurrAssetsOtherPreviousYear = r.prevAmount;
                        noncurrAssetsOtherDiff = r.diff;
                        noncurrAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Assets")
                    {
                        totalNonCurrAssetsCurrYear += r.currAmount;
                        totalNonCurrAssetsPrevYear += r.prevAmount;
                        totalNonCurrAssetsDiff += r.diff;
                    }

                    //CURRENT LIABILITIES
                    if (r.particulars == "currLiaTrade")
                    {
                        currLiaTradeCurrentYear = r.currAmount;
                        currLiaTradePreviousYear = r.prevAmount;
                        currLiaTradeDiff = r.diff;
                        currLiaTradePercent = r.per;
                    }
                    if (r.particulars == "currLiaUnearned")
                    {
                        currLiaUnearnedCurrentYear = r.currAmount;
                        currLiaUnearnedPreviousYear = r.prevAmount;
                        currLiaUnearnedDiff = r.diff;
                        currLiaUnearnedPercent = r.per;
                    }
                    if (r.particulars == "currLiaClientCBU")
                    {
                        currLiaClientCBUCurrentYear = r.currAmount;
                        currLiaClientCBUPreviousYear = r.prevAmount;
                        currLiaClientCBUDiff = r.diff;
                        currLiaClientCBUPercent = r.per;
                    }
                    if (r.particulars == "currLiaTaxPayable")
                    {
                        currLiaTaxPayableCurrentYear = r.currAmount;
                        currLiaTaxPayablePreviousYear = r.prevAmount;
                        currLiaTaxPayableDiff = r.diff;
                        currLiaTaxPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaCGLIPayable")
                    {
                        currLiaCGLIPayableCurrentYear = r.currAmount;
                        currLiaCGLIPayablePreviousYear = r.prevAmount;
                        currLiaCGLIPayableDiff = r.diff;
                        currLiaCGLIPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaProvisional")
                    {
                        currLiaProvisionalCurrentYear = r.currAmount;
                        currLiaProvisionalPreviousYear = r.prevAmount;
                        currLiaProvisionalDiff = r.diff;
                        currLiaProvisionalPercent = r.per;
                    }
                    if (r.accntType == "Current Liabilities")
                    {
                        totalCurrLiaCurrYear += r.currAmount;
                        totalCurrLiaPrevYear += r.prevAmount;
                        totalCurrLiaDiff += r.diff;
                    }

                    //NONCURRENT LIABILITIES
                    if (r.particulars == "noncurrLiaLease")
                    {
                        noncurrLiaLeaseCurrentYear = r.currAmount;
                        noncurrLiaLeasePreviousYear = r.prevAmount;
                        noncurrLiaLeaseDiff = r.diff;
                        noncurrLiaLeasePercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaRetirement")
                    {
                        noncurrLiaRetirementCurrentYear = r.currAmount;
                        noncurrLiaRetirementPreviousYear = r.prevAmount;
                        noncurrLiaRetirementDiff = r.diff;
                        noncurrLiaRetirementPercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaDeferred")
                    {
                        noncurrLiaDeferredCurrentYear = r.currAmount;
                        noncurrLiaDeferredPreviousYear = r.prevAmount;
                        noncurrLiaDeferredDiff = r.diff;
                        noncurrLiaDeferredPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Liabilities")
                    {
                        totalNonCurrLiaCurrYear += r.currAmount;
                        totalNonCurrLiaPrevYear += r.prevAmount;
                        totalNonCurrLiaDiff += r.diff;
                    }

                    //FUND BALANCE
                    if (r.particulars == "fundRetained")
                    {
                        fundRetainedCurrentYear = r.currAmount;
                        fundRetainedPreviousYear = r.prevAmount;
                        fundRetainedDiff = r.diff;
                        fundRetainedPercent = r.per;
                    }
                    if (r.particulars == "fundCommulative")
                    {
                        fundCommulativeCurrentYear = r.currAmount;
                        fundCommulativePreviousYear = r.prevAmount;
                        fundCommulativeDiff = r.diff;
                        fundCommulativePercent = r.per;
                    }
                    if (r.particulars == "fundFairValue")
                    {
                        fundFairValueCurrentYear = r.currAmount;
                        fundFairValuePreviousYear = r.prevAmount;
                        fundFairValueDiff = r.diff;
                        fundFairValuePercent = r.per;
                    }
                    if (r.accntType == "Fund Balance")
                    {
                        totalFundCurrYear += r.currAmount;
                        totalFundPrevYear += r.prevAmount;
                        totalFundDiff += r.diff;
                    }
                }
                totalCurrAssetsPer = (totalCurrAssetsDiff / totalCurrAssetsPrevYear) * 100;
                totalNonCurrAssetsPer = (totalNonCurrAssetsDiff / totalNonCurrAssetsPrevYear) * 100;
                totalAssetsCurrYear = totalCurrAssetsCurrYear + totalNonCurrAssetsCurrYear;
                totalAssetsPrevYear = totalCurrAssetsPrevYear + totalNonCurrAssetsPrevYear;
                totalAssetsDiff = totalCurrAssetsDiff + totalNonCurrAssetsDiff;
                totalAssetsPer = (totalAssetsDiff / totalAssetsPrevYear) * 100;

                totalCurrLiaPer = (totalCurrLiaDiff / totalCurrLiaPrevYear) * 100;
                totalNonCurrLiaPer = (totalNonCurrLiaDiff / totalNonCurrLiaPrevYear) * 100;
                totalLiaCurrYear = totalCurrLiaCurrYear + totalNonCurrLiaCurrYear;
                totalLiaPrevYear = totalCurrLiaPrevYear + totalNonCurrLiaPrevYear;
                totalLiaDiff = totalCurrLiaDiff + totalNonCurrLiaDiff;
                totalLiaPer = (totalLiaDiff / totalLiaPrevYear) * 100;

                totalFundPer = (totalFundDiff / totalFundPrevYear) * 100;
                totalFundLiaCurrYear = totalLiaCurrYear + totalFundCurrYear;
                totalFundLiaPrevYear = totalLiaPrevYear + totalFundPrevYear;
                totalFundLiaDiff = totalLiaDiff + totalFundDiff;
                totalFundLiaPer = (totalFundLiaDiff / totalFundLiaPrevYear) * 100;
            }
            #endregion

            //INCOME STATEMENT
            #region
            var modelIS = GetIncomeStatement(month, year);
            var prevYearIS = year - 1;

            decimal revIntLoansCurrentYear = 0;
            decimal revIntLoansPreviousYear = 0;
            decimal revIntLoansDiff = 0;
            decimal revIntLoansPercent = 0;

            decimal revServiceIncomeCurrentYear = 0;
            decimal revServiceIncomePreviousYear = 0;
            decimal revServiceIncomeDiff = 0;
            decimal revServiceIncomePercent = 0;

            decimal revIntDepositsCurrentYear = 0;
            decimal revIntDepositsPreviousYear = 0;
            decimal revIntDepositsDiff = 0;
            decimal revIntDepositsPercent = 0;

            decimal revForexCurrentYear = 0;
            decimal revForexPreviousYear = 0;
            decimal revForexDiff = 0;
            decimal revForexPercent = 0;

            decimal revOtherCurrentYear = 0;
            decimal revOtherPreviousYear = 0;
            decimal revOtherDiff = 0;
            decimal revOtherPercent = 0;

            decimal revIntExpenseCBUCurrentYear = 0;
            decimal revIntExpenseCBUPreviousYear = 0;
            decimal revIntExpenseCBUDiff = 0;
            decimal revIntExpenseCBUPercent = 0;

            decimal totGrossIncomeCurrYear = 0;
            decimal totGrossIncomePrevYear = 0;
            decimal totGrossIncomeDiff = 0;
            decimal totGrossIncomePer = 0;

            decimal revIntExpenseCurrentYear = 0;
            decimal revIntExpensePreviousYear = 0;
            decimal revIntExpenseDiff = 0;
            decimal revIntExpensePercent = 0;

            decimal totFinancialCostCurrYear = 0;
            decimal totFinancialCostPrevYear = 0;
            decimal totFinancialCostDiff = 0;
            decimal totFinancialCostPer = 0;

            decimal totProvisionLoanCurrYear = 0;
            decimal totProvisionLoanPrevYear = 0;
            decimal totProvisionLoanDiff = 0;
            decimal totProvisionLoanPer = 0;

            decimal revProvisionCurrentYear = 0;
            decimal revProvisionPreviousYear = 0;
            decimal revProvisionDiff = 0;
            decimal revProvisionPercent = 0;

            decimal totNetMarginCurrYear = 0;
            decimal totNetMarginPrevYear = 0;
            decimal totNetMarginDiff = 0;
            decimal totNetMarginPer = 0;

            //EXPENSES
            decimal expPersonnelCurrentYear = 0;
            decimal expPersonnelPreviousYear = 0;
            decimal expPersonnelDiff = 0;
            decimal expPersonnelPercent = 0;

            decimal expOutstationCurrentYear = 0;
            decimal expOutstationPreviousYear = 0;
            decimal expOutstationDiff = 0;
            decimal expOutstationPercent = 0;

            decimal expFringeCurrentYear = 0;
            decimal expFringePreviousYear = 0;
            decimal expFringeDiff = 0;
            decimal expFringePercent = 0;

            decimal expRetirementCurrentYear = 0;
            decimal expRetirementPreviousYear = 0;
            decimal expRetirementDiff = 0;
            decimal expRetirementPercent = 0;

            decimal expOutsideCurrentYear = 0;
            decimal expOutsidePreviousYear = 0;
            decimal expOutsideDiff = 0;
            decimal expOutsidePercent = 0;

            decimal expProfFeesCurrentYear = 0;
            decimal expProfFeesPreviousYear = 0;
            decimal expProfFeesDiff = 0;
            decimal expProfFeesPercent = 0;

            decimal expMeetingsCurrentYear = 0;
            decimal expMeetingsPreviousYear = 0;
            decimal expMeetingsDiff = 0;
            decimal expMeetingsPercent = 0;

            decimal expClientCurrentYear = 0;
            decimal expClientPreviousYear = 0;
            decimal expClientDiff = 0;
            decimal expClientPercent = 0;

            decimal expLitigationCurrentYear = 0;
            decimal expLitigationPreviousYear = 0;
            decimal expLitigationDiff = 0;
            decimal expLitigationPercent = 0;

            decimal expSuppliesCurrentYear = 0;
            decimal expSuppliesPreviousYear = 0;
            decimal expSuppliesDiff = 0;
            decimal expSuppliesPercent = 0;

            decimal expUtilitiesCurrentYear = 0;
            decimal expUtilitiesPreviousYear = 0;
            decimal expUtilitiesDiff = 0;
            decimal expUtilitiesPercent = 0;

            decimal expCommunicationCurrentYear = 0;
            decimal expCommunicationPreviousYear = 0;
            decimal expCommunicationDiff = 0;
            decimal expCommunicationPercent = 0;

            decimal expRentalCurrentYear = 0;
            decimal expRentalPreviousYear = 0;
            decimal expRentalDiff = 0;
            decimal expRentalPercent = 0;

            decimal expInsuranceCurrentYear = 0;
            decimal expInsurancePreviousYear = 0;
            decimal expInsuranceDiff = 0;
            decimal expInsurancePercent = 0;

            decimal expTranspoCurrentYear = 0;
            decimal expTranspoPreviousYear = 0;
            decimal expTranspoDiff = 0;
            decimal expTranspoPercent = 0;

            decimal expTaxesCurrentYear = 0;
            decimal expTaxesPreviousYear = 0;
            decimal expTaxesDiff = 0;
            decimal expTaxesPercent = 0;

            decimal expRepairsCurrentYear = 0;
            decimal expRepairsPreviousYear = 0;
            decimal expRepairsDiff = 0;
            decimal expRepairsPercent = 0;

            decimal expDepreciationCurrentYear = 0;
            decimal expDepreciationPreviousYear = 0;
            decimal expDepreciationDiff = 0;
            decimal expDepreciationPercent = 0;

            decimal expProvisionCurrentYear = 0;
            decimal expProvisionPreviousYear = 0;
            decimal expProvisionDiff = 0;
            decimal expProvisionPercent = 0;

            decimal expOthersCurrentYear = 0;
            decimal expOthersPreviousYear = 0;
            decimal expOthersDiff = 0;
            decimal expOthersPercent = 0;

            //INCOME TAX

            decimal incomeTaxExpenseCurrentYear = 0;
            decimal incomeTaxExpensePreviousYear = 0;
            decimal incomeTaxExpenseDiff = 0;
            decimal incomeTaxExpensePercent = 0;

            decimal totExpensesCurrYear = 0;
            decimal totExpensesPrevYear = 0;
            decimal totExpensesDiff = 0;
            decimal totExpensesPer = 0;

            decimal totIncomeCurrYear = 0;
            decimal totIncomePrevYear = 0;
            decimal totIncomeDiff = 0;
            decimal totIncomePer = 0;

            decimal totIncomeTaxCurrYear = 0;
            decimal totIncomeTaxPrevYear = 0;
            decimal totIncomeTaxDiff = 0;
            decimal totIncomeTaxPer = 0;

            decimal totNetIncomeCurrYear = 0;
            decimal totNetIncomePrevYear = 0;
            decimal totNetIncomeDiff = 0;
            decimal totNetIncomePer = 0;

            if (modelIS.Count > 0)
            {
                foreach (var r in modelIS)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        revIntLoansCurrentYear = r.currAmount;
                        revIntLoansPreviousYear = r.prevAmount;
                        revIntLoansDiff = r.diff;
                        revIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        revServiceIncomeCurrentYear = r.currAmount;
                        revServiceIncomePreviousYear = r.prevAmount;
                        revServiceIncomeDiff = r.diff;
                        revServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        revIntDepositsCurrentYear = r.currAmount;
                        revIntDepositsPreviousYear = r.prevAmount;
                        revIntDepositsDiff = r.diff;
                        revIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        revForexCurrentYear = r.currAmount;
                        revForexPreviousYear = r.prevAmount;
                        revForexDiff = r.diff;
                        revForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        revOtherCurrentYear = r.currAmount;
                        revOtherPreviousYear = r.prevAmount;
                        revOtherDiff = r.diff;
                        revOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        totGrossIncomeCurrYear += r.currAmount;
                        totGrossIncomePrevYear += r.prevAmount;
                        totGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        revIntExpenseCurrentYear = r.currAmount;
                        revIntExpensePreviousYear = r.prevAmount;
                        revIntExpenseDiff = r.diff;
                        revIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        revIntExpenseCBUCurrentYear = r.currAmount;
                        revIntExpenseCBUPreviousYear = r.prevAmount;
                        revIntExpenseCBUDiff = r.diff;
                        revIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        totFinancialCostCurrYear += r.currAmount;
                        totFinancialCostPrevYear += r.prevAmount;
                        totFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        revProvisionCurrentYear = r.currAmount;
                        revProvisionPreviousYear = r.prevAmount;
                        revProvisionDiff = r.diff;
                        revProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        totProvisionLoanCurrYear += r.currAmount;
                        totProvisionLoanPrevYear += r.prevAmount;
                        totProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        expPersonnelCurrentYear = r.currAmount;
                        expPersonnelPreviousYear = r.prevAmount;
                        expPersonnelDiff = r.diff;
                        expPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        expOutstationCurrentYear = r.currAmount;
                        expOutstationPreviousYear = r.prevAmount;
                        expOutstationDiff = r.diff;
                        expOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        expFringeCurrentYear = r.currAmount;
                        expFringePreviousYear = r.prevAmount;
                        expFringeDiff = r.diff;
                        expFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        expRetirementCurrentYear = r.currAmount;
                        expRetirementPreviousYear = r.prevAmount;
                        expRetirementDiff = r.diff;
                        expRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        expOutsideCurrentYear = r.currAmount;
                        expOutsidePreviousYear = r.prevAmount;
                        expOutsideDiff = r.diff;
                        expOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        expProfFeesCurrentYear = r.currAmount;
                        expProfFeesPreviousYear = r.prevAmount;
                        expProfFeesDiff = r.diff;
                        expProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        expMeetingsCurrentYear = r.currAmount;
                        expMeetingsPreviousYear = r.prevAmount;
                        expMeetingsDiff = r.diff;
                        expMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        expClientCurrentYear = r.currAmount;
                        expClientPreviousYear = r.prevAmount;
                        expClientDiff = r.diff;
                        expClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        expLitigationCurrentYear = r.currAmount;
                        expLitigationPreviousYear = r.prevAmount;
                        expLitigationDiff = r.diff;
                        expLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        expSuppliesCurrentYear = r.currAmount;
                        expSuppliesPreviousYear = r.prevAmount;
                        expSuppliesDiff = r.diff;
                        expSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        expUtilitiesCurrentYear = r.currAmount;
                        expUtilitiesPreviousYear = r.prevAmount;
                        expUtilitiesDiff = r.diff;
                        expUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        expCommunicationCurrentYear = r.currAmount;
                        expCommunicationPreviousYear = r.prevAmount;
                        expCommunicationDiff = r.diff;
                        expCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        expRentalCurrentYear = r.currAmount;
                        expRentalPreviousYear = r.prevAmount;
                        expRentalDiff = r.diff;
                        expRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        expInsuranceCurrentYear = r.currAmount;
                        expInsurancePreviousYear = r.prevAmount;
                        expInsuranceDiff = r.diff;
                        expInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        expTranspoCurrentYear = r.currAmount;
                        expTranspoPreviousYear = r.prevAmount;
                        expTranspoDiff = r.diff;
                        expTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        expTaxesCurrentYear = r.currAmount;
                        expTaxesPreviousYear = r.prevAmount;
                        expTaxesDiff = r.diff;
                        expTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        expRepairsCurrentYear = r.currAmount;
                        expRepairsPreviousYear = r.prevAmount;
                        expRepairsDiff = r.diff;
                        expRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        expDepreciationCurrentYear = r.currAmount;
                        expDepreciationPreviousYear = r.prevAmount;
                        expDepreciationDiff = r.diff;
                        expDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        expProvisionCurrentYear = r.currAmount;
                        expProvisionPreviousYear = r.prevAmount;
                        expProvisionDiff = r.diff;
                        expProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        expOthersCurrentYear = r.currAmount;
                        expOthersPreviousYear = r.prevAmount;
                        expOthersDiff = r.diff;
                        expOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        totExpensesCurrYear += r.currAmount;
                        totExpensesPrevYear += r.prevAmount;
                        totExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        incomeTaxExpenseCurrentYear = r.currAmount;
                        incomeTaxExpensePreviousYear = r.prevAmount;
                        incomeTaxExpenseDiff = r.diff;
                        incomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        totIncomeTaxCurrYear += r.currAmount;
                        totIncomeTaxPrevYear += r.prevAmount;
                        totIncomeTaxDiff += r.diff;
                    }
                }
                totGrossIncomePer = (totGrossIncomeDiff / totGrossIncomePrevYear) * 100;
                totFinancialCostPer = (totFinancialCostDiff / totFinancialCostPrevYear) * 100;

                totNetMarginCurrYear = totGrossIncomeCurrYear - totFinancialCostCurrYear - totProvisionLoanCurrYear;
                totNetMarginPrevYear = totGrossIncomePrevYear - totFinancialCostPrevYear - totProvisionLoanPrevYear;
                totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                totNetMarginPer = (totNetMarginDiff / totNetMarginPrevYear) * 100;


                totExpensesPer = (totExpensesDiff / totExpensesPrevYear) * 100;
                //TOTAL INCOME
                totIncomeCurrYear = totNetMarginCurrYear - totExpensesCurrYear;
                totIncomePrevYear = totNetMarginPrevYear - totExpensesPrevYear;
                totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                totIncomePer = (totIncomeDiff / totIncomePrevYear * 100);

                //NET INCOME
                totNetIncomeCurrYear = totIncomeCurrYear - totIncomeTaxCurrYear;
                totNetIncomePrevYear = totIncomePrevYear - totIncomeTaxPrevYear;
                totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                totNetIncomePer = (totNetIncomeDiff / totNetIncomePrevYear) * 100;
            }
            #endregion

            //RETAINED EARNINGS
            #region

            var modelRE = GetRetainedEarnings(month, year);
            var prevYearRE = year - 1;

            decimal initBalanceCurrentYear = 0;
            decimal initBalancePreviousYear = 0;
            decimal initNetIncomeCurrentYear = 0;
            decimal initNetIncomePreviousYear = 0;
            decimal cumuBalanceCurrentYear = 0;
            decimal cumuBalancePreviousYear = 0;
            decimal cumuRemeasureCurrentYear = 0;
            decimal cumuRemeasurePreviousYear = 0;
            decimal fairBalanceCurrentYear = 0;
            decimal fairBalancePreviousYear = 0;
            decimal fairFairValueCurrentYear = 0;
            decimal fairFairValuePreviousYear = 0;

            decimal initTotalCurrentYear = 0;
            decimal initTotalPreviousYear = 0;

            decimal cumuTotalCurrentYear = 0;
            decimal cumuTotalPreviousYear = 0;

            decimal fairTotalCurrentYear = 0;
            decimal fairTotalPreviousYear = 0;

            decimal TotalCurrentYear = 0;
            decimal TotalPreviousYear = 0;

            if (modelRE.Count > 0)
            {
                foreach (var r in modelRE)
                {
                    if (r.particulars == "initBalance")
                    {
                        initBalanceCurrentYear = r.currAmount;
                        initBalancePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "initNetIncome")
                    {
                        initNetIncomeCurrentYear = r.currAmount;
                        initNetIncomePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "cumuBalance")
                    {
                        cumuBalanceCurrentYear = r.currAmount;
                        cumuBalancePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "cumuRemeasure")
                    {
                        cumuRemeasureCurrentYear = r.currAmount;
                        cumuRemeasurePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "fairBalance")
                    {
                        fairBalanceCurrentYear = r.currAmount;
                        fairBalancePreviousYear = r.prevAmount;
                    }
                    if (r.particulars == "fairFairValue")
                    {
                        fairFairValueCurrentYear = r.currAmount;
                        fairFairValuePreviousYear = r.prevAmount;
                    }
                    if (r.accntType == "Initial")
                    {
                        initTotalCurrentYear += r.currAmount;
                        initTotalPreviousYear += r.prevAmount;
                    }
                    if (r.accntType == "Cumulative")
                    {
                        cumuTotalCurrentYear += r.currAmount;
                        cumuTotalPreviousYear += r.prevAmount;
                    }
                    if (r.accntType == "FairValue")
                    {
                        fairTotalCurrentYear += r.currAmount;
                        fairTotalPreviousYear += r.prevAmount;
                    }
                }
                TotalCurrentYear = initTotalCurrentYear + cumuTotalCurrentYear + fairTotalCurrentYear;
                TotalPreviousYear = initTotalPreviousYear + cumuTotalPreviousYear + fairTotalPreviousYear;
                //INIT

            }

            #endregion

            //MONTHLY COMPARATIVE IS 
            #region
            var modelMIS = GetMonthlyIncomeStatement(month, year);
            var prevYearMIS = year - 1;
            var prevMonthMIS = "";
            if (month == "JANUARY")
            {
                prevMonthMIS = "DECEMBER";
            }
            else if (month == "FEBRUARY")
            {
                prevMonthMIS = "JANUARY";
            }
            else if (month == "MARCH")
            {
                prevMonthMIS = "FEBRUARY";
            }
            else if (month == "APRIL")
            {
                prevMonthMIS = "MARCH";
            }
            else if (month == "MAY")
            {
                prevMonthMIS = "APRIL";
            }
            else if (month == "JUNE")
            {
                prevMonthMIS = "MAY";
            }
            else if (month == "JULY")
            {
                prevMonthMIS = "JUNE";
            }
            else if (month == "AUGUST")
            {
                prevMonthMIS = "JULY";
            }
            else if (month == "SEPTEMBER")
            {
                prevMonthMIS = "AUGUST";
            }
            else if (month == "OCTOBER")
            {
                prevMonthMIS = "SEPTEMBER";
            }
            else if (month == "NOVEMBER")
            {
                prevMonthMIS = "OCTOBER";
            }
            else if (month == "DECEMBER")
            {
                prevMonthMIS = "NOVEMBER";
            }

            //REVENUE
            decimal misrevIntLoansCurrentYear = 0;
            decimal misrevIntLoansPreviousYear = 0;
            decimal misrevIntLoansDiff = 0;
            decimal misrevIntLoansPercent = 0;

            decimal misrevServiceIncomeCurrentYear = 0;
            decimal misrevServiceIncomePreviousYear = 0;
            decimal misrevServiceIncomeDiff = 0;
            decimal misrevServiceIncomePercent = 0;

            decimal misrevIntDepositsCurrentYear = 0;
            decimal misrevIntDepositsPreviousYear = 0;
            decimal misrevIntDepositsDiff = 0;
            decimal misrevIntDepositsPercent = 0;

            decimal misrevForexCurrentYear = 0;
            decimal misrevForexPreviousYear = 0;
            decimal misrevForexDiff = 0;
            decimal misrevForexPercent = 0;

            decimal misrevOtherCurrentYear = 0;
            decimal misrevOtherPreviousYear = 0;
            decimal misrevOtherDiff = 0;
            decimal misrevOtherPercent = 0;

            decimal misrevIntExpenseCBUCurrentYear = 0;
            decimal misrevIntExpenseCBUPreviousYear = 0;
            decimal misrevIntExpenseCBUDiff = 0;
            decimal misrevIntExpenseCBUPercent = 0;

            decimal mistotGrossIncomeCurrYear = 0;
            decimal mistotGrossIncomePrevYear = 0;
            decimal mistotGrossIncomeDiff = 0;
            decimal mistotGrossIncomePer = 0;

            decimal misrevIntExpenseCurrentYear = 0;
            decimal misrevIntExpensePreviousYear = 0;
            decimal misrevIntExpenseDiff = 0;
            decimal misrevIntExpensePercent = 0;

            decimal mistotFinancialCostCurrYear = 0;
            decimal mistotFinancialCostPrevYear = 0;
            decimal mistotFinancialCostDiff = 0;
            decimal mistotFinancialCostPer = 0;

            decimal mistotProvisionLoanCurrYear = 0;
            decimal mistotProvisionLoanPrevYear = 0;
            decimal mistotProvisionLoanDiff = 0;
            decimal mistotProvisionLoanPer = 0;

            decimal misrevProvisionCurrentYear = 0;
            decimal misrevProvisionPreviousYear = 0;
            decimal misrevProvisionDiff = 0;
            decimal misrevProvisionPercent = 0;

            decimal mistotNetMarginCurrYear = 0;
            decimal mistotNetMarginPrevYear = 0;
            decimal mistotNetMarginDiff = 0;
            decimal mistotNetMarginPer = 0;

            //EXPENSES
            decimal misexpPersonnelCurrentYear = 0;
            decimal misexpPersonnelPreviousYear = 0;
            decimal misexpPersonnelDiff = 0;
            decimal misexpPersonnelPercent = 0;

            decimal misexpOutstationCurrentYear = 0;
            decimal misexpOutstationPreviousYear = 0;
            decimal misexpOutstationDiff = 0;
            decimal misexpOutstationPercent = 0;

            decimal misexpFringeCurrentYear = 0;
            decimal misexpFringePreviousYear = 0;
            decimal misexpFringeDiff = 0;
            decimal misexpFringePercent = 0;

            decimal misexpRetirementCurrentYear = 0;
            decimal misexpRetirementPreviousYear = 0;
            decimal misexpRetirementDiff = 0;
            decimal misexpRetirementPercent = 0;

            decimal misexpOutsideCurrentYear = 0;
            decimal misexpOutsidePreviousYear = 0;
            decimal misexpOutsideDiff = 0;
            decimal misexpOutsidePercent = 0;

            decimal misexpProfFeesCurrentYear = 0;
            decimal misexpProfFeesPreviousYear = 0;
            decimal misexpProfFeesDiff = 0;
            decimal misexpProfFeesPercent = 0;

            decimal misexpMeetingsCurrentYear = 0;
            decimal misexpMeetingsPreviousYear = 0;
            decimal misexpMeetingsDiff = 0;
            decimal misexpMeetingsPercent = 0;

            decimal misexpClientCurrentYear = 0;
            decimal misexpClientPreviousYear = 0;
            decimal misexpClientDiff = 0;
            decimal misexpClientPercent = 0;

            decimal misexpLitigationCurrentYear = 0;
            decimal misexpLitigationPreviousYear = 0;
            decimal misexpLitigationDiff = 0;
            decimal misexpLitigationPercent = 0;

            decimal misexpSuppliesCurrentYear = 0;
            decimal misexpSuppliesPreviousYear = 0;
            decimal misexpSuppliesDiff = 0;
            decimal misexpSuppliesPercent = 0;

            decimal misexpUtilitiesCurrentYear = 0;
            decimal misexpUtilitiesPreviousYear = 0;
            decimal misexpUtilitiesDiff = 0;
            decimal misexpUtilitiesPercent = 0;

            decimal misexpCommunicationCurrentYear = 0;
            decimal misexpCommunicationPreviousYear = 0;
            decimal misexpCommunicationDiff = 0;
            decimal misexpCommunicationPercent = 0;

            decimal misexpRentalCurrentYear = 0;
            decimal misexpRentalPreviousYear = 0;
            decimal misexpRentalDiff = 0;
            decimal misexpRentalPercent = 0;

            decimal misexpInsuranceCurrentYear = 0;
            decimal misexpInsurancePreviousYear = 0;
            decimal misexpInsuranceDiff = 0;
            decimal misexpInsurancePercent = 0;

            decimal misexpTranspoCurrentYear = 0;
            decimal misexpTranspoPreviousYear = 0;
            decimal misexpTranspoDiff = 0;
            decimal misexpTranspoPercent = 0;

            decimal misexpTaxesCurrentYear = 0;
            decimal misexpTaxesPreviousYear = 0;
            decimal misexpTaxesDiff = 0;
            decimal misexpTaxesPercent = 0;

            decimal misexpRepairsCurrentYear = 0;
            decimal misexpRepairsPreviousYear = 0;
            decimal misexpRepairsDiff = 0;
            decimal misexpRepairsPercent = 0;

            decimal misexpDepreciationCurrentYear = 0;
            decimal misexpDepreciationPreviousYear = 0;
            decimal misexpDepreciationDiff = 0;
            decimal misexpDepreciationPercent = 0;

            decimal misexpProvisionCurrentYear = 0;
            decimal misexpProvisionPreviousYear = 0;
            decimal misexpProvisionDiff = 0;
            decimal misexpProvisionPercent = 0;

            decimal misexpOthersCurrentYear = 0;
            decimal misexpOthersPreviousYear = 0;
            decimal misexpOthersDiff = 0;
            decimal misexpOthersPercent = 0;

            //INCOME TAX

            decimal misincomeTaxExpenseCurrentYear = 0;
            decimal misincomeTaxExpensePreviousYear = 0;
            decimal misincomeTaxExpenseDiff = 0;
            decimal misincomeTaxExpensePercent = 0;

            decimal mistotExpensesCurrYear = 0;
            decimal mistotExpensesPrevYear = 0;
            decimal mistotExpensesDiff = 0;
            decimal mistotExpensesPer = 0;

            decimal mistotIncomeCurrYear = 0;
            decimal mistotIncomePrevYear = 0;
            decimal mistotIncomeDiff = 0;
            decimal mistotIncomePer = 0;

            decimal mistotIncomeTaxCurrYear = 0;
            decimal mistotIncomeTaxPrevYear = 0;
            decimal mistotIncomeTaxDiff = 0;
            decimal mistotIncomeTaxPer = 0;

            decimal mistotNetIncomeCurrYear = 0;
            decimal mistotNetIncomePrevYear = 0;
            decimal mistotNetIncomeDiff = 0;
            decimal mistotNetIncomePer = 0;

            if (modelMIS.Count > 0)
            {
                foreach (var r in modelMIS)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        misrevIntLoansCurrentYear = r.currAmount;
                        misrevIntLoansPreviousYear = r.prevAmount;
                        misrevIntLoansDiff = r.diff;
                        misrevIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        misrevServiceIncomeCurrentYear = r.currAmount;
                        misrevServiceIncomePreviousYear = r.prevAmount;
                        misrevServiceIncomeDiff = r.diff;
                        misrevServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        misrevIntDepositsCurrentYear = r.currAmount;
                        misrevIntDepositsPreviousYear = r.prevAmount;
                        misrevIntDepositsDiff = r.diff;
                        misrevIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        misrevForexCurrentYear = r.currAmount;
                        misrevForexPreviousYear = r.prevAmount;
                        misrevForexDiff = r.diff;
                        misrevForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        misrevOtherCurrentYear = r.currAmount;
                        misrevOtherPreviousYear = r.prevAmount;
                        misrevOtherDiff = r.diff;
                        misrevOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        mistotGrossIncomeCurrYear += r.currAmount;
                        mistotGrossIncomePrevYear += r.prevAmount;
                        mistotGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        misrevIntExpenseCurrentYear = r.currAmount;
                        misrevIntExpensePreviousYear = r.prevAmount;
                        misrevIntExpenseDiff = r.diff;
                        misrevIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        misrevIntExpenseCBUCurrentYear = r.currAmount;
                        misrevIntExpenseCBUPreviousYear = r.prevAmount;
                        misrevIntExpenseCBUDiff = r.diff;
                        misrevIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        mistotFinancialCostCurrYear += r.currAmount;
                        mistotFinancialCostPrevYear += r.prevAmount;
                        mistotFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        misrevProvisionCurrentYear = r.currAmount;
                        misrevProvisionPreviousYear = r.prevAmount;
                        misrevProvisionDiff = r.diff;
                        misrevProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        mistotProvisionLoanCurrYear += r.currAmount;
                        mistotProvisionLoanPrevYear += r.prevAmount;
                        mistotProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        misexpPersonnelCurrentYear = r.currAmount;
                        misexpPersonnelPreviousYear = r.prevAmount;
                        misexpPersonnelDiff = r.diff;
                        misexpPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        misexpOutstationCurrentYear = r.currAmount;
                        misexpOutstationPreviousYear = r.prevAmount;
                        misexpOutstationDiff = r.diff;
                        misexpOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        misexpFringeCurrentYear = r.currAmount;
                        misexpFringePreviousYear = r.prevAmount;
                        misexpFringeDiff = r.diff;
                        misexpFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        misexpRetirementCurrentYear = r.currAmount;
                        misexpRetirementPreviousYear = r.prevAmount;
                        misexpRetirementDiff = r.diff;
                        misexpRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        misexpOutsideCurrentYear = r.currAmount;
                        misexpOutsidePreviousYear = r.prevAmount;
                        misexpOutsideDiff = r.diff;
                        misexpOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        misexpProfFeesCurrentYear = r.currAmount;
                        misexpProfFeesPreviousYear = r.prevAmount;
                        misexpProfFeesDiff = r.diff;
                        misexpProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        misexpMeetingsCurrentYear = r.currAmount;
                        misexpMeetingsPreviousYear = r.prevAmount;
                        misexpMeetingsDiff = r.diff;
                        misexpMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        misexpClientCurrentYear = r.currAmount;
                        misexpClientPreviousYear = r.prevAmount;
                        misexpClientDiff = r.diff;
                        misexpClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        misexpLitigationCurrentYear = r.currAmount;
                        misexpLitigationPreviousYear = r.prevAmount;
                        misexpLitigationDiff = r.diff;
                        misexpLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        misexpSuppliesCurrentYear = r.currAmount;
                        misexpSuppliesPreviousYear = r.prevAmount;
                        misexpSuppliesDiff = r.diff;
                        misexpSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        misexpUtilitiesCurrentYear = r.currAmount;
                        misexpUtilitiesPreviousYear = r.prevAmount;
                        misexpUtilitiesDiff = r.diff;
                        misexpUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        misexpCommunicationCurrentYear = r.currAmount;
                        misexpCommunicationPreviousYear = r.prevAmount;
                        misexpCommunicationDiff = r.diff;
                        misexpCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        misexpRentalCurrentYear = r.currAmount;
                        misexpRentalPreviousYear = r.prevAmount;
                        misexpRentalDiff = r.diff;
                        misexpRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        misexpInsuranceCurrentYear = r.currAmount;
                        misexpInsurancePreviousYear = r.prevAmount;
                        misexpInsuranceDiff = r.diff;
                        misexpInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        misexpTranspoCurrentYear = r.currAmount;
                        misexpTranspoPreviousYear = r.prevAmount;
                        misexpTranspoDiff = r.diff;
                        misexpTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        misexpTaxesCurrentYear = r.currAmount;
                        misexpTaxesPreviousYear = r.prevAmount;
                        misexpTaxesDiff = r.diff;
                        misexpTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        misexpRepairsCurrentYear = r.currAmount;
                        misexpRepairsPreviousYear = r.prevAmount;
                        misexpRepairsDiff = r.diff;
                        misexpRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        misexpDepreciationCurrentYear = r.currAmount;
                        misexpDepreciationPreviousYear = r.prevAmount;
                        misexpDepreciationDiff = r.diff;
                        misexpDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        misexpProvisionCurrentYear = r.currAmount;
                        misexpProvisionPreviousYear = r.prevAmount;
                        misexpProvisionDiff = r.diff;
                        misexpProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        misexpOthersCurrentYear = r.currAmount;
                        misexpOthersPreviousYear = r.prevAmount;
                        misexpOthersDiff = r.diff;
                        misexpOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        mistotExpensesCurrYear += r.currAmount;
                        mistotExpensesPrevYear += r.prevAmount;
                        mistotExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        misincomeTaxExpenseCurrentYear = r.currAmount;
                        misincomeTaxExpensePreviousYear = r.prevAmount;
                        misincomeTaxExpenseDiff = r.diff;
                        misincomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        mistotIncomeTaxCurrYear += r.currAmount;
                        mistotIncomeTaxPrevYear += r.prevAmount;
                        mistotIncomeTaxDiff += r.diff;
                    }
                }
                mistotGrossIncomePer = (mistotGrossIncomeDiff / mistotGrossIncomePrevYear) * 100;
                mistotFinancialCostPer = (mistotFinancialCostDiff / mistotFinancialCostPrevYear) * 100;

                mistotNetMarginCurrYear = mistotGrossIncomeCurrYear - mistotFinancialCostCurrYear - mistotProvisionLoanCurrYear;
                mistotNetMarginPrevYear = mistotGrossIncomePrevYear - mistotFinancialCostPrevYear - mistotProvisionLoanPrevYear;
                mistotNetMarginDiff = mistotGrossIncomeDiff - mistotFinancialCostDiff - mistotProvisionLoanDiff;
                mistotNetMarginPer = (mistotNetMarginDiff / mistotNetMarginPrevYear) * 100;


                mistotExpensesPer = (mistotExpensesDiff / mistotExpensesPrevYear) * 100;
                //TOTAL INCOME
                mistotIncomeCurrYear = mistotNetMarginCurrYear - mistotExpensesCurrYear;
                mistotIncomePrevYear = mistotNetMarginPrevYear - mistotExpensesPrevYear;
                mistotIncomeDiff = mistotNetMarginDiff - mistotExpensesDiff;
                mistotIncomePer = (mistotIncomeDiff / mistotIncomePrevYear * 100);

                //NET INCOME
                mistotNetIncomeCurrYear = mistotIncomeCurrYear - mistotIncomeTaxCurrYear;
                mistotNetIncomePrevYear = mistotIncomePrevYear - mistotIncomeTaxPrevYear;
                mistotNetIncomeDiff = mistotIncomeDiff - mistotIncomeTaxDiff;
                mistotNetIncomePer = (mistotNetIncomeDiff / mistotNetIncomePrevYear) * 100;
            }
            #endregion

            //MONTHLY IS BUDGET
            #region
            var modelMB = GetMonthlyIncomeStatementVSBudget(month, year);
            var prevYearMB = year - 1;

            //REVENUE
            decimal mbrevIntLoansCurrentYear = 0;
            decimal mbrevIntLoansPreviousYear = 0;
            decimal mbrevIntLoansDiff = 0;
            decimal mbrevIntLoansPercent = 0;

            decimal mbrevServiceIncomeCurrentYear = 0;
            decimal mbrevServiceIncomePreviousYear = 0;
            decimal mbrevServiceIncomeDiff = 0;
            decimal mbrevServiceIncomePercent = 0;

            decimal mbrevIntDepositsCurrentYear = 0;
            decimal mbrevIntDepositsPreviousYear = 0;
            decimal mbrevIntDepositsDiff = 0;
            decimal mbrevIntDepositsPercent = 0;

            decimal mbrevForexCurrentYear = 0;
            decimal mbrevForexPreviousYear = 0;
            decimal mbrevForexDiff = 0;
            decimal mbrevForexPercent = 0;

            decimal mbrevOtherCurrentYear = 0;
            decimal mbrevOtherPreviousYear = 0;
            decimal mbrevOtherDiff = 0;
            decimal mbrevOtherPercent = 0;

            decimal mbrevIntExpenseCBUCurrentYear = 0;
            decimal mbrevIntExpenseCBUPreviousYear = 0;
            decimal mbrevIntExpenseCBUDiff = 0;
            decimal mbrevIntExpenseCBUPercent = 0;

            decimal mbtotGrossIncomeCurrYear = 0;
            decimal mbtotGrossIncomePrevYear = 0;
            decimal mbtotGrossIncomeDiff = 0;
            decimal mbtotGrossIncomePer = 0;

            decimal mbrevIntExpenseCurrentYear = 0;
            decimal mbrevIntExpensePreviousYear = 0;
            decimal mbrevIntExpenseDiff = 0;
            decimal mbrevIntExpensePercent = 0;

            decimal mbtotFinancialCostCurrYear = 0;
            decimal mbtotFinancialCostPrevYear = 0;
            decimal mbtotFinancialCostDiff = 0;
            decimal mbtotFinancialCostPer = 0;

            decimal mbtotProvisionLoanCurrYear = 0;
            decimal mbtotProvisionLoanPrevYear = 0;
            decimal mbtotProvisionLoanDiff = 0;
            decimal mbtotProvisionLoanPer = 0;

            decimal mbrevProvisionCurrentYear = 0;
            decimal mbrevProvisionPreviousYear = 0;
            decimal mbrevProvisionDiff = 0;
            decimal mbrevProvisionPercent = 0;

            decimal mbtotNetMarginCurrYear = 0;
            decimal mbtotNetMarginPrevYear = 0;
            decimal mbtotNetMarginDiff = 0;
            decimal mbtotNetMarginPer = 0;

            //EXPENSES
            decimal mbexpPersonnelCurrentYear = 0;
            decimal mbexpPersonnelPreviousYear = 0;
            decimal mbexpPersonnelDiff = 0;
            decimal mbexpPersonnelPercent = 0;

            decimal mbexpOutstationCurrentYear = 0;
            decimal mbexpOutstationPreviousYear = 0;
            decimal mbexpOutstationDiff = 0;
            decimal mbexpOutstationPercent = 0;

            decimal mbexpFringeCurrentYear = 0;
            decimal mbexpFringePreviousYear = 0;
            decimal mbexpFringeDiff = 0;
            decimal mbexpFringePercent = 0;

            decimal mbexpRetirementCurrentYear = 0;
            decimal mbexpRetirementPreviousYear = 0;
            decimal mbexpRetirementDiff = 0;
            decimal mbexpRetirementPercent = 0;

            decimal mbexpOutsideCurrentYear = 0;
            decimal mbexpOutsidePreviousYear = 0;
            decimal mbexpOutsideDiff = 0;
            decimal mbexpOutsidePercent = 0;

            decimal mbexpProfFeesCurrentYear = 0;
            decimal mbexpProfFeesPreviousYear = 0;
            decimal mbexpProfFeesDiff = 0;
            decimal mbexpProfFeesPercent = 0;

            decimal mbexpMeetingsCurrentYear = 0;
            decimal mbexpMeetingsPreviousYear = 0;
            decimal mbexpMeetingsDiff = 0;
            decimal mbexpMeetingsPercent = 0;

            decimal mbexpClientCurrentYear = 0;
            decimal mbexpClientPreviousYear = 0;
            decimal mbexpClientDiff = 0;
            decimal mbexpClientPercent = 0;

            decimal mbexpLitigationCurrentYear = 0;
            decimal mbexpLitigationPreviousYear = 0;
            decimal mbexpLitigationDiff = 0;
            decimal mbexpLitigationPercent = 0;

            decimal mbexpSuppliesCurrentYear = 0;
            decimal mbexpSuppliesPreviousYear = 0;
            decimal mbexpSuppliesDiff = 0;
            decimal mbexpSuppliesPercent = 0;

            decimal mbexpUtilitiesCurrentYear = 0;
            decimal mbexpUtilitiesPreviousYear = 0;
            decimal mbexpUtilitiesDiff = 0;
            decimal mbexpUtilitiesPercent = 0;

            decimal mbexpCommunicationCurrentYear = 0;
            decimal mbexpCommunicationPreviousYear = 0;
            decimal mbexpCommunicationDiff = 0;
            decimal mbexpCommunicationPercent = 0;

            decimal mbexpRentalCurrentYear = 0;
            decimal mbexpRentalPreviousYear = 0;
            decimal mbexpRentalDiff = 0;
            decimal mbexpRentalPercent = 0;

            decimal mbexpInsuranceCurrentYear = 0;
            decimal mbexpInsurancePreviousYear = 0;
            decimal mbexpInsuranceDiff = 0;
            decimal mbexpInsurancePercent = 0;

            decimal mbexpTranspoCurrentYear = 0;
            decimal mbexpTranspoPreviousYear = 0;
            decimal mbexpTranspoDiff = 0;
            decimal mbexpTranspoPercent = 0;

            decimal mbexpTaxesCurrentYear = 0;
            decimal mbexpTaxesPreviousYear = 0;
            decimal mbexpTaxesDiff = 0;
            decimal mbexpTaxesPercent = 0;

            decimal mbexpRepairsCurrentYear = 0;
            decimal mbexpRepairsPreviousYear = 0;
            decimal mbexpRepairsDiff = 0;
            decimal mbexpRepairsPercent = 0;

            decimal mbexpDepreciationCurrentYear = 0;
            decimal mbexpDepreciationPreviousYear = 0;
            decimal mbexpDepreciationDiff = 0;
            decimal mbexpDepreciationPercent = 0;

            decimal mbexpProvisionCurrentYear = 0;
            decimal mbexpProvisionPreviousYear = 0;
            decimal mbexpProvisionDiff = 0;
            decimal mbexpProvisionPercent = 0;

            decimal mbexpOthersCurrentYear = 0;
            decimal mbexpOthersPreviousYear = 0;
            decimal mbexpOthersDiff = 0;
            decimal mbexpOthersPercent = 0;

            //INCOME TAX

            decimal mbincomeTaxExpenseCurrentYear = 0;
            decimal mbincomeTaxExpensePreviousYear = 0;
            decimal mbincomeTaxExpenseDiff = 0;
            decimal mbincomeTaxExpensePercent = 0;

            decimal mbtotExpensesCurrYear = 0;
            decimal mbtotExpensesPrevYear = 0;
            decimal mbtotExpensesDiff = 0;
            decimal mbtotExpensesPer = 0;

            decimal mbtotIncomeCurrYear = 0;
            decimal mbtotIncomePrevYear = 0;
            decimal mbtotIncomeDiff = 0;
            decimal mbtotIncomePer = 0;

            decimal mbtotIncomeTaxCurrYear = 0;
            decimal mbtotIncomeTaxPrevYear = 0;
            decimal mbtotIncomeTaxDiff = 0;
            decimal mbtotIncomeTaxPer = 0;

            decimal mbtotNetIncomeCurrYear = 0;
            decimal mbtotNetIncomePrevYear = 0;
            decimal mbtotNetIncomeDiff = 0;
            decimal mbtotNetIncomePer = 0;

            if (modelMB.Count > 0)
            {
                foreach (var r in modelMB)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        mbrevIntLoansCurrentYear = r.budgetMonthly;
                        mbrevIntLoansPreviousYear = r.actual;
                        mbrevIntLoansDiff = r.diff;
                        mbrevIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        mbrevServiceIncomeCurrentYear = r.budgetMonthly;
                        mbrevServiceIncomePreviousYear = r.actual;
                        mbrevServiceIncomeDiff = r.diff;
                        mbrevServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        mbrevIntDepositsCurrentYear = r.budgetMonthly;
                        mbrevIntDepositsPreviousYear = r.actual;
                        mbrevIntDepositsDiff = r.diff;
                        mbrevIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        mbrevForexCurrentYear = r.budgetMonthly;
                        mbrevForexPreviousYear = r.actual;
                        mbrevForexDiff = r.diff;
                        mbrevForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        mbrevOtherCurrentYear = r.budgetMonthly;
                        mbrevOtherPreviousYear = r.actual;
                        mbrevOtherDiff = r.diff;
                        mbrevOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        mbtotGrossIncomeCurrYear += r.budgetMonthly;
                        mbtotGrossIncomePrevYear += r.actual;
                        mbtotGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        mbrevIntExpenseCurrentYear = r.budgetMonthly;
                        mbrevIntExpensePreviousYear = r.actual;
                        mbrevIntExpenseDiff = r.diff;
                        mbrevIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        mbrevIntExpenseCBUCurrentYear = r.budgetMonthly;
                        mbrevIntExpenseCBUPreviousYear = r.actual;
                        mbrevIntExpenseCBUDiff = r.diff;
                        mbrevIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        mbtotFinancialCostCurrYear += r.budgetMonthly;
                        mbtotFinancialCostPrevYear += r.actual;
                        mbtotFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        mbrevProvisionCurrentYear = r.budgetMonthly;
                        mbrevProvisionPreviousYear = r.actual;
                        mbrevProvisionDiff = r.diff;
                        mbrevProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        mbtotProvisionLoanCurrYear += r.budgetMonthly;
                        mbtotProvisionLoanPrevYear += r.actual;
                        mbtotProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        mbexpPersonnelCurrentYear = r.budgetMonthly;
                        mbexpPersonnelPreviousYear = r.actual;
                        mbexpPersonnelDiff = r.diff;
                        mbexpPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        mbexpOutstationCurrentYear = r.budgetMonthly;
                        mbexpOutstationPreviousYear = r.actual;
                        mbexpOutstationDiff = r.diff;
                        mbexpOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        mbexpFringeCurrentYear = r.budgetMonthly;
                        mbexpFringePreviousYear = r.actual;
                        mbexpFringeDiff = r.diff;
                        mbexpFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        mbexpRetirementCurrentYear = r.budgetMonthly;
                        mbexpRetirementPreviousYear = r.actual;
                        mbexpRetirementDiff = r.diff;
                        mbexpRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        mbexpOutsideCurrentYear = r.budgetMonthly;
                        mbexpOutsidePreviousYear = r.actual;
                        mbexpOutsideDiff = r.diff;
                        mbexpOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        mbexpProfFeesCurrentYear = r.budgetMonthly;
                        mbexpProfFeesPreviousYear = r.actual;
                        mbexpProfFeesDiff = r.diff;
                        mbexpProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        mbexpMeetingsCurrentYear = r.budgetMonthly;
                        mbexpMeetingsPreviousYear = r.actual;
                        mbexpMeetingsDiff = r.diff;
                        mbexpMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        mbexpClientCurrentYear = r.budgetMonthly;
                        mbexpClientPreviousYear = r.actual;
                        mbexpClientDiff = r.diff;
                        mbexpClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        mbexpLitigationCurrentYear = r.budgetMonthly;
                        mbexpLitigationPreviousYear = r.actual;
                        mbexpLitigationDiff = r.diff;
                        mbexpLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        mbexpSuppliesCurrentYear = r.budgetMonthly;
                        mbexpSuppliesPreviousYear = r.actual;
                        mbexpSuppliesDiff = r.diff;
                        mbexpSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        mbexpUtilitiesCurrentYear = r.budgetMonthly;
                        mbexpUtilitiesPreviousYear = r.actual;
                        mbexpUtilitiesDiff = r.diff;
                        mbexpUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        mbexpCommunicationCurrentYear = r.budgetMonthly;
                        mbexpCommunicationPreviousYear = r.actual;
                        mbexpCommunicationDiff = r.diff;
                        mbexpCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        mbexpRentalCurrentYear = r.budgetMonthly;
                        mbexpRentalPreviousYear = r.actual;
                        mbexpRentalDiff = r.diff;
                        mbexpRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        mbexpInsuranceCurrentYear = r.budgetMonthly;
                        mbexpInsurancePreviousYear = r.actual;
                        mbexpInsuranceDiff = r.diff;
                        mbexpInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        mbexpTranspoCurrentYear = r.budgetMonthly;
                        mbexpTranspoPreviousYear = r.actual;
                        mbexpTranspoDiff = r.diff;
                        mbexpTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        mbexpTaxesCurrentYear = r.budgetMonthly;
                        mbexpTaxesPreviousYear = r.actual;
                        mbexpTaxesDiff = r.diff;
                        mbexpTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        mbexpRepairsCurrentYear = r.budgetMonthly;
                        mbexpRepairsPreviousYear = r.actual;
                        mbexpRepairsDiff = r.diff;
                        mbexpRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        mbexpDepreciationCurrentYear = r.budgetMonthly;
                        mbexpDepreciationPreviousYear = r.actual;
                        mbexpDepreciationDiff = r.diff;
                        mbexpDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        mbexpProvisionCurrentYear = r.budgetMonthly;
                        mbexpProvisionPreviousYear = r.actual;
                        mbexpProvisionDiff = r.diff;
                        mbexpProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        mbexpOthersCurrentYear = r.budgetMonthly;
                        mbexpOthersPreviousYear = r.actual;
                        mbexpOthersDiff = r.diff;
                        mbexpOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        mbtotExpensesCurrYear += r.budgetMonthly;
                        mbtotExpensesPrevYear += r.actual;
                        mbtotExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        mbincomeTaxExpenseCurrentYear = r.budgetMonthly;
                        mbincomeTaxExpensePreviousYear = r.actual;
                        mbincomeTaxExpenseDiff = r.diff;
                        mbincomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        mbtotIncomeTaxCurrYear += r.budgetMonthly;
                        mbtotIncomeTaxPrevYear += r.actual;
                        mbtotIncomeTaxDiff += r.diff;
                    }
                }
                mbtotGrossIncomePer = (mbtotGrossIncomeDiff / mbtotGrossIncomeCurrYear) * 100;
                mbtotFinancialCostPer = (mbtotFinancialCostDiff / mbtotFinancialCostCurrYear) * 100;

                mbtotNetMarginCurrYear = mbtotGrossIncomeCurrYear - mbtotFinancialCostCurrYear - mbtotProvisionLoanCurrYear;
                mbtotNetMarginPrevYear = mbtotGrossIncomePrevYear - mbtotFinancialCostPrevYear - mbtotProvisionLoanPrevYear;
                //totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                mbtotNetMarginDiff = mbtotNetMarginPrevYear - mbtotNetMarginCurrYear;
                mbtotNetMarginPer = (mbtotNetMarginDiff / mbtotNetMarginCurrYear) * 100;


                mbtotExpensesPer = (mbtotExpensesDiff / mbtotExpensesCurrYear) * 100;
                //TOTAL INCOME
                mbtotIncomeCurrYear = mbtotNetMarginCurrYear - mbtotExpensesCurrYear;
                mbtotIncomePrevYear = mbtotNetMarginPrevYear - mbtotExpensesPrevYear;
                //totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                mbtotIncomeDiff = mbtotIncomePrevYear - mbtotIncomeCurrYear;
                mbtotIncomePer = (mbtotIncomeDiff / mbtotIncomeCurrYear * 100);

                //NET INCOME
                mbtotNetIncomeCurrYear = mbtotIncomeCurrYear - mbtotIncomeTaxCurrYear;
                mbtotNetIncomePrevYear = mbtotIncomePrevYear - mbtotIncomeTaxPrevYear;
                //totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                mbtotNetIncomeDiff = mbtotNetIncomePrevYear - mbtotNetIncomeCurrYear;
                mbtotNetIncomePer = (mbtotNetIncomeDiff / mbtotNetIncomeCurrYear) * 100;
            }
            #endregion

            //COMMULATIVE COMPARATIVE IS 
            #region
            var modelCB = GetCumulativeIncomeStatementVSBudget(month, year);
            var prevYearCB = year - 1;

            //REVENUE
            decimal cbrevIntLoansCurrentYear = 0;
            decimal cbrevIntLoansPreviousYear = 0;
            decimal cbrevIntLoansDiff = 0;
            decimal cbrevIntLoansPercent = 0;

            decimal cbrevServiceIncomeCurrentYear = 0;
            decimal cbrevServiceIncomePreviousYear = 0;
            decimal cbrevServiceIncomeDiff = 0;
            decimal cbrevServiceIncomePercent = 0;

            decimal cbrevIntDepositsCurrentYear = 0;
            decimal cbrevIntDepositsPreviousYear = 0;
            decimal cbrevIntDepositsDiff = 0;
            decimal cbrevIntDepositsPercent = 0;

            decimal cbrevForexCurrentYear = 0;
            decimal cbrevForexPreviousYear = 0;
            decimal cbrevForexDiff = 0;
            decimal cbrevForexPercent = 0;

            decimal cbrevOtherCurrentYear = 0;
            decimal cbrevOtherPreviousYear = 0;
            decimal cbrevOtherDiff = 0;
            decimal cbrevOtherPercent = 0;

            decimal cbrevIntExpenseCBUCurrentYear = 0;
            decimal cbrevIntExpenseCBUPreviousYear = 0;
            decimal cbrevIntExpenseCBUDiff = 0;
            decimal cbrevIntExpenseCBUPercent = 0;

            decimal cbtotGrossIncomeCurrYear = 0;
            decimal cbtotGrossIncomePrevYear = 0;
            decimal cbtotGrossIncomeDiff = 0;
            decimal cbtotGrossIncomePer = 0;

            decimal cbrevIntExpenseCurrentYear = 0;
            decimal cbrevIntExpensePreviousYear = 0;
            decimal cbrevIntExpenseDiff = 0;
            decimal cbrevIntExpensePercent = 0;

            decimal cbtotFinancialCostCurrYear = 0;
            decimal cbtotFinancialCostPrevYear = 0;
            decimal cbtotFinancialCostDiff = 0;
            decimal cbtotFinancialCostPer = 0;

            decimal cbtotProvisionLoanCurrYear = 0;
            decimal cbtotProvisionLoanPrevYear = 0;
            decimal cbtotProvisionLoanDiff = 0;
            decimal cbtotProvisionLoanPer = 0;

            decimal cbrevProvisionCurrentYear = 0;
            decimal cbrevProvisionPreviousYear = 0;
            decimal cbrevProvisionDiff = 0;
            decimal cbrevProvisionPercent = 0;

            decimal cbtotNetMarginCurrYear = 0;
            decimal cbtotNetMarginPrevYear = 0;
            decimal cbtotNetMarginDiff = 0;
            decimal cbtotNetMarginPer = 0;

            //EXPENSES
            decimal cbexpPersonnelCurrentYear = 0;
            decimal cbexpPersonnelPreviousYear = 0;
            decimal cbexpPersonnelDiff = 0;
            decimal cbexpPersonnelPercent = 0;

            decimal cbexpOutstationCurrentYear = 0;
            decimal cbexpOutstationPreviousYear = 0;
            decimal cbexpOutstationDiff = 0;
            decimal cbexpOutstationPercent = 0;

            decimal cbexpFringeCurrentYear = 0;
            decimal cbexpFringePreviousYear = 0;
            decimal cbexpFringeDiff = 0;
            decimal cbexpFringePercent = 0;

            decimal cbexpRetirementCurrentYear = 0;
            decimal cbexpRetirementPreviousYear = 0;
            decimal cbexpRetirementDiff = 0;
            decimal cbexpRetirementPercent = 0;

            decimal cbexpOutsideCurrentYear = 0;
            decimal cbexpOutsidePreviousYear = 0;
            decimal cbexpOutsideDiff = 0;
            decimal cbexpOutsidePercent = 0;

            decimal cbexpProfFeesCurrentYear = 0;
            decimal cbexpProfFeesPreviousYear = 0;
            decimal cbexpProfFeesDiff = 0;
            decimal cbexpProfFeesPercent = 0;

            decimal cbexpMeetingsCurrentYear = 0;
            decimal cbexpMeetingsPreviousYear = 0;
            decimal cbexpMeetingsDiff = 0;
            decimal cbexpMeetingsPercent = 0;

            decimal cbexpClientCurrentYear = 0;
            decimal cbexpClientPreviousYear = 0;
            decimal cbexpClientDiff = 0;
            decimal cbexpClientPercent = 0;

            decimal cbexpLitigationCurrentYear = 0;
            decimal cbexpLitigationPreviousYear = 0;
            decimal cbexpLitigationDiff = 0;
            decimal cbexpLitigationPercent = 0;

            decimal cbexpSuppliesCurrentYear = 0;
            decimal cbexpSuppliesPreviousYear = 0;
            decimal cbexpSuppliesDiff = 0;
            decimal cbexpSuppliesPercent = 0;

            decimal cbexpUtilitiesCurrentYear = 0;
            decimal cbexpUtilitiesPreviousYear = 0;
            decimal cbexpUtilitiesDiff = 0;
            decimal cbexpUtilitiesPercent = 0;

            decimal cbexpCommunicationCurrentYear = 0;
            decimal cbexpCommunicationPreviousYear = 0;
            decimal cbexpCommunicationDiff = 0;
            decimal cbexpCommunicationPercent = 0;

            decimal cbexpRentalCurrentYear = 0;
            decimal cbexpRentalPreviousYear = 0;
            decimal cbexpRentalDiff = 0;
            decimal cbexpRentalPercent = 0;

            decimal cbexpInsuranceCurrentYear = 0;
            decimal cbexpInsurancePreviousYear = 0;
            decimal cbexpInsuranceDiff = 0;
            decimal cbexpInsurancePercent = 0;

            decimal cbexpTranspoCurrentYear = 0;
            decimal cbexpTranspoPreviousYear = 0;
            decimal cbexpTranspoDiff = 0;
            decimal cbexpTranspoPercent = 0;

            decimal cbexpTaxesCurrentYear = 0;
            decimal cbexpTaxesPreviousYear = 0;
            decimal cbexpTaxesDiff = 0;
            decimal cbexpTaxesPercent = 0;

            decimal cbexpRepairsCurrentYear = 0;
            decimal cbexpRepairsPreviousYear = 0;
            decimal cbexpRepairsDiff = 0;
            decimal cbexpRepairsPercent = 0;

            decimal cbexpDepreciationCurrentYear = 0;
            decimal cbexpDepreciationPreviousYear = 0;
            decimal cbexpDepreciationDiff = 0;
            decimal cbexpDepreciationPercent = 0;

            decimal cbexpProvisionCurrentYear = 0;
            decimal cbexpProvisionPreviousYear = 0;
            decimal cbexpProvisionDiff = 0;
            decimal cbexpProvisionPercent = 0;

            decimal cbexpOthersCurrentYear = 0;
            decimal cbexpOthersPreviousYear = 0;
            decimal cbexpOthersDiff = 0;
            decimal cbexpOthersPercent = 0;

            //INCOME TAX

            decimal cbincomeTaxExpenseCurrentYear = 0;
            decimal cbincomeTaxExpensePreviousYear = 0;
            decimal cbincomeTaxExpenseDiff = 0;
            decimal cbincomeTaxExpensePercent = 0;

            decimal cbtotExpensesCurrYear = 0;
            decimal cbtotExpensesPrevYear = 0;
            decimal cbtotExpensesDiff = 0;
            decimal cbtotExpensesPer = 0;

            decimal cbtotIncomeCurrYear = 0;
            decimal cbtotIncomePrevYear = 0;
            decimal cbtotIncomeDiff = 0;
            decimal cbtotIncomePer = 0;

            decimal cbtotIncomeTaxCurrYear = 0;
            decimal cbtotIncomeTaxPrevYear = 0;
            decimal cbtotIncomeTaxDiff = 0;
            decimal cbtotIncomeTaxPer = 0;

            decimal cbtotNetIncomeCurrYear = 0;
            decimal cbtotNetIncomePrevYear = 0;
            decimal cbtotNetIncomeDiff = 0;
            decimal cbtotNetIncomePer = 0;

            if (modelCB.Count > 0)
            {
                foreach (var r in modelCB)
                {
                    if (r.particulars == "revIntLoans")
                    {
                        cbrevIntLoansCurrentYear = r.budgetCumulative;
                        cbrevIntLoansPreviousYear = r.actual;
                        cbrevIntLoansDiff = r.diff;
                        cbrevIntLoansPercent = r.per;
                    }
                    if (r.particulars == "revServiceIncome")
                    {
                        cbrevServiceIncomeCurrentYear = r.budgetCumulative;
                        cbrevServiceIncomePreviousYear = r.actual;
                        cbrevServiceIncomeDiff = r.diff;
                        cbrevServiceIncomePercent = r.per;
                    }
                    if (r.particulars == "revIntDeposits")
                    {
                        cbrevIntDepositsCurrentYear = r.budgetCumulative;
                        cbrevIntDepositsPreviousYear = r.actual;
                        cbrevIntDepositsDiff = r.diff;
                        cbrevIntDepositsPercent = r.per;
                    }
                    if (r.particulars == "revForex")
                    {
                        cbrevForexCurrentYear = r.budgetCumulative;
                        cbrevForexPreviousYear = r.actual;
                        cbrevForexDiff = r.diff;
                        cbrevForexPercent = r.per;
                    }
                    if (r.particulars == "revOther")
                    {
                        cbrevOtherCurrentYear = r.budgetCumulative;
                        cbrevOtherPreviousYear = r.actual;
                        cbrevOtherDiff = r.diff;
                        cbrevOtherPercent = r.per;
                    }
                    if (r.accntType == "GrossIncome")
                    {
                        cbtotGrossIncomeCurrYear += r.budgetCumulative;
                        cbtotGrossIncomePrevYear += r.actual;
                        cbtotGrossIncomeDiff += r.diff;
                    }

                    //FINANCIAL COST
                    if (r.particulars == "revIntExpense")
                    {
                        cbrevIntExpenseCurrentYear = r.budgetCumulative;
                        cbrevIntExpensePreviousYear = r.actual;
                        cbrevIntExpenseDiff = r.diff;
                        cbrevIntExpensePercent = r.per;
                    }
                    if (r.particulars == "revIntExpenseCBU")
                    {
                        cbrevIntExpenseCBUCurrentYear = r.budgetCumulative;
                        cbrevIntExpenseCBUPreviousYear = r.actual;
                        cbrevIntExpenseCBUDiff = r.diff;
                        cbrevIntExpenseCBUPercent = r.per;
                    }
                    if (r.accntType == "FinancialCost")
                    {
                        cbtotFinancialCostCurrYear += r.budgetCumulative;
                        cbtotFinancialCostPrevYear += r.actual;
                        cbtotFinancialCostDiff += r.diff;
                    }

                    //NET MARGIN
                    if (r.particulars == "revProvision")
                    {
                        cbrevProvisionCurrentYear = r.budgetCumulative;
                        cbrevProvisionPreviousYear = r.actual;
                        cbrevProvisionDiff = r.diff;
                        cbrevProvisionPercent = r.per;
                    }
                    if (r.accntType == "ProvisionLoan")
                    {
                        cbtotProvisionLoanCurrYear += r.budgetCumulative;
                        cbtotProvisionLoanPrevYear += r.actual;
                        cbtotProvisionLoanDiff += r.diff;
                    }

                    //EXPENSES
                    if (r.particulars == "expPersonnel")
                    {
                        cbexpPersonnelCurrentYear = r.budgetCumulative;
                        cbexpPersonnelPreviousYear = r.actual;
                        cbexpPersonnelDiff = r.diff;
                        cbexpPersonnelPercent = r.per;
                    }
                    if (r.particulars == "expOutstation")
                    {
                        cbexpOutstationCurrentYear = r.budgetCumulative;
                        cbexpOutstationPreviousYear = r.actual;
                        cbexpOutstationDiff = r.diff;
                        cbexpOutstationPercent = r.per;
                    }
                    if (r.particulars == "expFringe")
                    {
                        cbexpFringeCurrentYear = r.budgetCumulative;
                        cbexpFringePreviousYear = r.actual;
                        cbexpFringeDiff = r.diff;
                        cbexpFringePercent = r.per;
                    }
                    if (r.particulars == "expRetirement")
                    {
                        cbexpRetirementCurrentYear = r.budgetCumulative;
                        cbexpRetirementPreviousYear = r.actual;
                        cbexpRetirementDiff = r.diff;
                        cbexpRetirementPercent = r.per;
                    }
                    if (r.particulars == "expOutside")
                    {
                        cbexpOutsideCurrentYear = r.budgetCumulative;
                        cbexpOutsidePreviousYear = r.actual;
                        cbexpOutsideDiff = r.diff;
                        cbexpOutsidePercent = r.per;
                    }
                    if (r.particulars == "expProfFees")
                    {
                        cbexpProfFeesCurrentYear = r.budgetCumulative;
                        cbexpProfFeesPreviousYear = r.actual;
                        cbexpProfFeesDiff = r.diff;
                        cbexpProfFeesPercent = r.per;
                    }
                    if (r.particulars == "expMeetings")
                    {
                        cbexpMeetingsCurrentYear = r.budgetCumulative;
                        cbexpMeetingsPreviousYear = r.actual;
                        cbexpMeetingsDiff = r.diff;
                        cbexpMeetingsPercent = r.per;
                    }
                    if (r.particulars == "expClient")
                    {
                        cbexpClientCurrentYear = r.budgetCumulative;
                        cbexpClientPreviousYear = r.actual;
                        cbexpClientDiff = r.diff;
                        cbexpClientPercent = r.per;
                    }
                    if (r.particulars == "expLitigation")
                    {
                        cbexpLitigationCurrentYear = r.budgetCumulative;
                        cbexpLitigationPreviousYear = r.actual;
                        cbexpLitigationDiff = r.diff;
                        cbexpLitigationPercent = r.per;
                    }
                    if (r.particulars == "expSupplies")
                    {
                        cbexpSuppliesCurrentYear = r.budgetCumulative;
                        cbexpSuppliesPreviousYear = r.actual;
                        cbexpSuppliesDiff = r.diff;
                        cbexpSuppliesPercent = r.per;
                    }
                    if (r.particulars == "expUtilities")
                    {
                        cbexpUtilitiesCurrentYear = r.budgetCumulative;
                        cbexpUtilitiesPreviousYear = r.actual;
                        cbexpUtilitiesDiff = r.diff;
                        cbexpUtilitiesPercent = r.per;
                    }
                    if (r.particulars == "expCommunication")
                    {
                        cbexpCommunicationCurrentYear = r.budgetCumulative;
                        cbexpCommunicationPreviousYear = r.actual;
                        cbexpCommunicationDiff = r.diff;
                        cbexpCommunicationPercent = r.per;
                    }
                    if (r.particulars == "expRental")
                    {
                        cbexpRentalCurrentYear = r.budgetCumulative;
                        cbexpRentalPreviousYear = r.actual;
                        cbexpRentalDiff = r.diff;
                        cbexpRentalPercent = r.per;
                    }
                    if (r.particulars == "expInsurance")
                    {
                        cbexpInsuranceCurrentYear = r.budgetCumulative;
                        cbexpInsurancePreviousYear = r.actual;
                        cbexpInsuranceDiff = r.diff;
                        cbexpInsurancePercent = r.per;
                    }
                    if (r.particulars == "expTranspo")
                    {
                        cbexpTranspoCurrentYear = r.budgetCumulative;
                        cbexpTranspoPreviousYear = r.actual;
                        cbexpTranspoDiff = r.diff;
                        cbexpTranspoPercent = r.per;
                    }
                    if (r.particulars == "expTaxes")
                    {
                        cbexpTaxesCurrentYear = r.budgetCumulative;
                        cbexpTaxesPreviousYear = r.actual;
                        cbexpTaxesDiff = r.diff;
                        cbexpTaxesPercent = r.per;
                    }
                    if (r.particulars == "expRepairs")
                    {
                        cbexpRepairsCurrentYear = r.budgetCumulative;
                        cbexpRepairsPreviousYear = r.actual;
                        cbexpRepairsDiff = r.diff;
                        cbexpRepairsPercent = r.per;
                    }
                    if (r.particulars == "expDepreciation")
                    {
                        cbexpDepreciationCurrentYear = r.budgetCumulative;
                        cbexpDepreciationPreviousYear = r.actual;
                        cbexpDepreciationDiff = r.diff;
                        cbexpDepreciationPercent = r.per;
                    }
                    if (r.particulars == "expProvision")
                    {
                        cbexpProvisionCurrentYear = r.budgetCumulative;
                        cbexpProvisionPreviousYear = r.actual;
                        cbexpProvisionDiff = r.diff;
                        cbexpProvisionPercent = r.per;
                    }
                    if (r.particulars == "expOthers")
                    {
                        cbexpOthersCurrentYear = r.budgetCumulative;
                        cbexpOthersPreviousYear = r.actual;
                        cbexpOthersDiff = r.diff;
                        cbexpOthersPercent = r.per;
                    }

                    if (r.accntType == "Expenses")
                    {
                        cbtotExpensesCurrYear += r.budgetCumulative;
                        cbtotExpensesPrevYear += r.actual;
                        cbtotExpensesDiff += r.diff;
                    }

                    //INCOME TAX
                    if (r.particulars == "incomeTaxExpense")
                    {
                        cbincomeTaxExpenseCurrentYear = r.budgetCumulative;
                        cbincomeTaxExpensePreviousYear = r.actual;
                        cbincomeTaxExpenseDiff = r.diff;
                        cbincomeTaxExpensePercent = r.per;
                    }

                    if (r.accntType == "Income Tax")
                    {
                        cbtotIncomeTaxCurrYear += r.budgetCumulative;
                        cbtotIncomeTaxPrevYear += r.actual;
                        cbtotIncomeTaxDiff += r.diff;
                    }
                }
                cbtotGrossIncomePer = (cbtotGrossIncomeDiff / cbtotGrossIncomeCurrYear) * 100;
                cbtotFinancialCostPer = (totFinancialCostDiff / totFinancialCostCurrYear) * 100;

                cbtotNetMarginCurrYear = cbtotGrossIncomeCurrYear - cbtotFinancialCostCurrYear - cbtotProvisionLoanCurrYear;
                cbtotNetMarginPrevYear = cbtotGrossIncomePrevYear - cbtotFinancialCostPrevYear - cbtotProvisionLoanPrevYear;
                //totNetMarginDiff = totGrossIncomeDiff - totFinancialCostDiff - totProvisionLoanDiff;
                cbtotNetMarginDiff = cbtotNetMarginPrevYear - cbtotNetMarginCurrYear;
                cbtotNetMarginPer = (cbtotNetMarginDiff / cbtotNetMarginCurrYear) * 100;


                cbtotExpensesPer = (cbtotExpensesDiff / cbtotExpensesCurrYear) * 100;
                //TOTAL INCOME
                cbtotIncomeCurrYear = cbtotNetMarginCurrYear - cbtotExpensesCurrYear;
                cbtotIncomePrevYear = cbtotNetMarginPrevYear - cbtotExpensesPrevYear;
                //totIncomeDiff = totNetMarginDiff - totExpensesDiff;
                cbtotIncomeDiff = cbtotIncomePrevYear - cbtotIncomeCurrYear;
                cbtotIncomePer = (cbtotIncomeDiff / cbtotIncomeCurrYear * 100);

                //NET INCOME
                cbtotNetIncomeCurrYear = cbtotIncomeCurrYear - cbtotIncomeTaxCurrYear;
                cbtotNetIncomePrevYear = cbtotIncomePrevYear - cbtotIncomeTaxPrevYear;
                //totNetIncomeDiff = totIncomeDiff - totIncomeTaxDiff;
                cbtotNetIncomeDiff = cbtotNetIncomePrevYear - cbtotNetIncomeCurrYear;
                cbtotNetIncomePer = (cbtotNetIncomeDiff / cbtotNetIncomeCurrYear) * 100;
            }

            #endregion

            //BS BUDGET
            #region

            var modelBBS = GetBalanceSheetVSBudget(month, year);
            var prevYearBBS = year - 1;

            //ASSETS
            //CURRENT ASSETS
            decimal bbscurrAssetsCashCurrentYear = 0;
            decimal bbscurrAssetsCashPreviousYear = 0;
            decimal bbscurrAssetsDiff = 0;
            decimal bbscurrAssetsPercent = 0;

            decimal bbscurrAssetsLoanReceivableCurrentYear = 0;
            decimal bbscurrAssetsLoanReceivablePreviousYear = 0;
            decimal bbscurrAssetsLoanReceivableDiff = 0;
            decimal bbscurrAssetsLoanReceivablePercent = 0;

            decimal bbscurrAssetsOtherReceivableCurrentYear = 0;
            decimal bbscurrAssetsOtherReceivablePreviousYear = 0;
            decimal bbscurrAssetsOtherReceivableDiff = 0;
            decimal bbscurrAssetsOtherReceivablePercent = 0;

            decimal bbscurrAssetsOtherCurrentYear = 0;
            decimal bbscurrAssetsOtherPreviousYear = 0;
            decimal bbscurrAssetsOtherDiff = 0;
            decimal bbscurrAssetsOtherPercent = 0;

            decimal bbstotalCurrAssetsCurrYear = 0;
            decimal bbstotalCurrAssetsPrevYear = 0;
            decimal bbstotalCurrAssetsDiff = 0;
            decimal bbstotalCurrAssetsPer = 0;

            //NONCURRENT ASSETS
            decimal bbsnoncurrAssetsFinancialCurrentYear = 0;
            decimal bbsnoncurrAssetsFinancialPreviousYear = 0;
            decimal bbsnoncurrAssetsFinancialDiff = 0;
            decimal bbsnoncurrAssetsFinancialPercent = 0;

            decimal bbsnoncurrAssetsRestrictedCashCurrentYear = 0;
            decimal bbsnoncurrAssetsRestrictedCashPreviousYear = 0;
            decimal bbsnoncurrAssetsRestrictedCashDiff = 0;
            decimal bbsnoncurrAssetsRestrictedCashPercent = 0;

            decimal bbsnoncurrAssetsROUAssetCurrentYear = 0;
            decimal bbsnoncurrAssetsROUAssetPreviousYear = 0;
            decimal bbsnoncurrAssetsROUAssetDiff = 0;
            decimal bbsnoncurrAssetsROUAssetPercent = 0;

            decimal bbsnoncurrAssetsPropertyCurrentYear = 0;
            decimal bbsnoncurrAssetsPropertyPreviousYear = 0;
            decimal bbsnoncurrAssetsPropertyDiff = 0;
            decimal bbsnoncurrAssetsPropertyPercent = 0;

            decimal bbsnoncurrAssetsOtherCurrentYear = 0;
            decimal bbsnoncurrAssetsOtherPreviousYear = 0;
            decimal bbsnoncurrAssetsOtherDiff = 0;
            decimal bbsnoncurrAssetsOtherPercent = 0;

            decimal bbstotalNonCurrAssetsCurrYear = 0;
            decimal bbstotalNonCurrAssetsPrevYear = 0;
            decimal bbstotalNonCurrAssetsDiff = 0;
            decimal bbstotalNonCurrAssetsPer = 0;

            decimal bbstotalAssetsCurrYear = 0;
            decimal bbstotalAssetsPrevYear = 0;
            decimal bbstotalAssetsDiff = 0;
            decimal bbstotalAssetsPer = 0;

            //LIABILITIES
            //CURRENT LIA
            decimal bbscurrLiaTradeCurrentYear = 0;
            decimal bbscurrLiaTradePreviousYear = 0;
            decimal bbscurrLiaTradeDiff = 0;
            decimal bbscurrLiaTradePercent = 0;

            decimal bbscurrLiaUnearnedCurrentYear = 0;
            decimal bbscurrLiaUnearnedPreviousYear = 0;
            decimal bbscurrLiaUnearnedDiff = 0;
            decimal bbscurrLiaUnearnedPercent = 0;

            decimal bbscurrLiaClientCBUCurrentYear = 0;
            decimal bbscurrLiaClientCBUPreviousYear = 0;
            decimal bbscurrLiaClientCBUDiff = 0;
            decimal bbscurrLiaClientCBUPercent = 0;

            decimal bbscurrLiaTaxPayableCurrentYear = 0;
            decimal bbscurrLiaTaxPayablePreviousYear = 0;
            decimal bbscurrLiaTaxPayableDiff = 0;
            decimal bbscurrLiaTaxPayablePercent = 0;

            decimal bbscurrLiaCGLIPayableCurrentYear = 0;
            decimal bbscurrLiaCGLIPayablePreviousYear = 0;
            decimal bbscurrLiaCGLIPayableDiff = 0;
            decimal bbscurrLiaCGLIPayablePercent = 0;

            decimal bbscurrLiaProvisionalCurrentYear = 0;
            decimal bbscurrLiaProvisionalPreviousYear = 0;
            decimal bbscurrLiaProvisionalDiff = 0;
            decimal bbscurrLiaProvisionalPercent = 0;

            decimal bbstotalCurrLiaCurrYear = 0;
            decimal bbstotalCurrLiaPrevYear = 0;
            decimal bbstotalCurrLiaDiff = 0;
            decimal bbstotalCurrLiaPer = 0;

            //NONCURRENT LIA
            decimal bbsnoncurrLiaLeaseCurrentYear = 0;
            decimal bbsnoncurrLiaLeasePreviousYear = 0;
            decimal bbsnoncurrLiaLeaseDiff = 0;
            decimal bbsnoncurrLiaLeasePercent = 0;

            decimal bbsnoncurrLiaRetirementCurrentYear = 0;
            decimal bbsnoncurrLiaRetirementPreviousYear = 0;
            decimal bbsnoncurrLiaRetirementDiff = 0;
            decimal bbsnoncurrLiaRetirementPercent = 0;

            decimal bbsnoncurrLiaDeferredCurrentYear = 0;
            decimal bbsnoncurrLiaDeferredPreviousYear = 0;
            decimal bbsnoncurrLiaDeferredDiff = 0;
            decimal bbsnoncurrLiaDeferredPercent = 0;

            decimal bbstotalNonCurrLiaCurrYear = 0;
            decimal bbstotalNonCurrLiaPrevYear = 0;
            decimal bbstotalNonCurrLiaDiff = 0;
            decimal bbstotalNonCurrLiaPer = 0;

            decimal bbstotalLiaCurrYear = 0;
            decimal bbstotalLiaPrevYear = 0;
            decimal bbstotalLiaDiff = 0;
            decimal bbstotalLiaPer = 0;

            //FUND BALANCE
            decimal bbsfundRetainedCurrentYear = 0;
            decimal bbsfundRetainedPreviousYear = 0;
            decimal bbsfundRetainedDiff = 0;
            decimal bbsfundRetainedPercent = 0;

            decimal bbsfundCommulativeCurrentYear = 0;
            decimal bbsfundCommulativePreviousYear = 0;
            decimal bbsfundCommulativeDiff = 0;
            decimal bbsfundCommulativePercent = 0;

            decimal bbsfundFairValueCurrentYear = 0;
            decimal bbsfundFairValuePreviousYear = 0;
            decimal bbsfundFairValueDiff = 0;
            decimal bbsfundFairValuePercent = 0;

            decimal bbstotalFundCurrYear = 0;
            decimal bbstotalFundPrevYear = 0;
            decimal bbstotalFundDiff = 0;
            decimal bbstotalFundPer = 0;

            //TOTAL FUND BALANCE AND LIABILITIES
            decimal bbstotalFundLiaCurrYear = 0;
            decimal bbstotalFundLiaPrevYear = 0;
            decimal bbstotalFundLiaDiff = 0;
            decimal bbstotalFundLiaPer = 0;
            

            if (modelBBS.Count > 0)
            {
                foreach (var r in modelBBS)
                {
                    //CURRENT ASSERTS
                    if (r.particulars == "currAssetsCash")
                    {
                        bbscurrAssetsCashCurrentYear = r.budgetMonthly;
                        bbscurrAssetsCashPreviousYear = r.actual;
                        bbscurrAssetsDiff = r.diff;
                        bbscurrAssetsPercent = r.per;
                    }
                    if (r.particulars == "currAssetsLoanReceivable")
                    {
                        bbscurrAssetsLoanReceivableCurrentYear = r.budgetMonthly;
                        bbscurrAssetsLoanReceivablePreviousYear = r.actual;
                        bbscurrAssetsLoanReceivableDiff = r.diff;
                        bbscurrAssetsLoanReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOtherReceivable")
                    {
                        bbscurrAssetsOtherReceivableCurrentYear = r.budgetMonthly;
                        bbscurrAssetsOtherReceivablePreviousYear = r.actual;
                        bbscurrAssetsOtherReceivableDiff = r.diff;
                        bbscurrAssetsOtherReceivablePercent = r.per;
                    }
                    if (r.particulars == "currAssetsOther")
                    {
                        bbscurrAssetsOtherCurrentYear = r.budgetMonthly;
                        bbscurrAssetsOtherPreviousYear = r.actual;
                        bbscurrAssetsOtherDiff = r.diff;
                        bbscurrAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Current Assets")
                    {
                        bbstotalCurrAssetsCurrYear += r.budgetMonthly;
                        bbstotalCurrAssetsPrevYear += r.actual;
                        bbstotalCurrAssetsDiff += r.diff;
                    }

                    //NONCURRENT ASSETS
                    if (r.particulars == "noncurrAssetsFinancial")
                    {
                        bbsnoncurrAssetsFinancialCurrentYear = r.budgetMonthly;
                        bbsnoncurrAssetsFinancialPreviousYear = r.actual;
                        bbsnoncurrAssetsFinancialDiff = r.diff;
                        bbsnoncurrAssetsFinancialPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsRestrictedCash")
                    {
                        bbsnoncurrAssetsRestrictedCashCurrentYear = r.budgetMonthly;
                        bbsnoncurrAssetsRestrictedCashPreviousYear = r.actual;
                        bbsnoncurrAssetsRestrictedCashDiff = r.diff;
                        bbsnoncurrAssetsRestrictedCashPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsROUAsset")
                    {
                        bbsnoncurrAssetsROUAssetCurrentYear = r.budgetMonthly;
                        bbsnoncurrAssetsROUAssetPreviousYear = r.actual;
                        bbsnoncurrAssetsROUAssetDiff = r.diff;
                        bbsnoncurrAssetsROUAssetPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsProperty")
                    {
                        bbsnoncurrAssetsPropertyCurrentYear = r.budgetMonthly;
                        bbsnoncurrAssetsPropertyPreviousYear = r.actual;
                        bbsnoncurrAssetsPropertyDiff = r.diff;
                        bbsnoncurrAssetsPropertyPercent = r.per;
                    }
                    if (r.particulars == "noncurrAssetsOther")
                    {
                        bbsnoncurrAssetsOtherCurrentYear = r.budgetMonthly;
                        bbsnoncurrAssetsOtherPreviousYear = r.actual;
                        bbsnoncurrAssetsOtherDiff = r.diff;
                        bbsnoncurrAssetsOtherPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Assets")
                    {
                        bbstotalNonCurrAssetsCurrYear += r.budgetMonthly;
                        bbstotalNonCurrAssetsPrevYear += r.actual;
                        bbstotalNonCurrAssetsDiff += r.diff;
                    }

                    //CURRENT LIABILITIES
                    if (r.particulars == "currLiaTrade")
                    {
                        bbscurrLiaTradeCurrentYear = r.budgetMonthly;
                        bbscurrLiaTradePreviousYear = r.actual;
                        bbscurrLiaTradeDiff = r.diff;
                        bbscurrLiaTradePercent = r.per;
                    }
                    if (r.particulars == "currLiaUnearned")
                    {
                        bbscurrLiaUnearnedCurrentYear = r.budgetMonthly;
                        bbscurrLiaUnearnedPreviousYear = r.actual;
                        bbscurrLiaUnearnedDiff = r.diff;
                        bbscurrLiaUnearnedPercent = r.per;
                    }
                    if (r.particulars == "currLiaClientCBU")
                    {
                        bbscurrLiaClientCBUCurrentYear = r.budgetMonthly;
                        bbscurrLiaClientCBUPreviousYear = r.actual;
                        bbscurrLiaClientCBUDiff = r.diff;
                        bbscurrLiaClientCBUPercent = r.per;
                    }
                    if (r.particulars == "currLiaTaxPayable")
                    {
                        bbscurrLiaTaxPayableCurrentYear = r.budgetMonthly;
                        bbscurrLiaTaxPayablePreviousYear = r.actual;
                        bbscurrLiaTaxPayableDiff = r.diff;
                        bbscurrLiaTaxPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaCGLIPayable")
                    {
                        bbscurrLiaCGLIPayableCurrentYear = r.budgetMonthly;
                        bbscurrLiaCGLIPayablePreviousYear = r.actual;
                        bbscurrLiaCGLIPayableDiff = r.diff;
                        bbscurrLiaCGLIPayablePercent = r.per;
                    }
                    if (r.particulars == "currLiaProvisional")
                    {
                        bbscurrLiaProvisionalCurrentYear = r.budgetMonthly;
                        bbscurrLiaProvisionalPreviousYear = r.actual;
                        bbscurrLiaProvisionalDiff = r.diff;
                        bbscurrLiaProvisionalPercent = r.per;
                    }
                    if (r.accntType == "Current Liabilities")
                    {
                        bbstotalCurrLiaCurrYear += r.budgetMonthly;
                        bbstotalCurrLiaPrevYear += r.actual;
                        bbstotalCurrLiaDiff += r.diff;
                    }

                    //NONCURRENT LIABILITIES
                    if (r.particulars == "noncurrLiaLease")
                    {
                        bbsnoncurrLiaLeaseCurrentYear = r.budgetMonthly;
                        bbsnoncurrLiaLeasePreviousYear = r.actual;
                        bbsnoncurrLiaLeaseDiff = r.diff;
                        bbsnoncurrLiaLeasePercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaRetirement")
                    {
                        bbsnoncurrLiaRetirementCurrentYear = r.budgetMonthly;
                        bbsnoncurrLiaRetirementPreviousYear = r.actual;
                        bbsnoncurrLiaRetirementDiff = r.diff;
                        bbsnoncurrLiaRetirementPercent = r.per;
                    }
                    if (r.particulars == "noncurrLiaDeferred")
                    {
                        bbsnoncurrLiaDeferredCurrentYear = r.budgetMonthly;
                        bbsnoncurrLiaDeferredPreviousYear = r.actual;
                        bbsnoncurrLiaDeferredDiff = r.diff;
                        bbsnoncurrLiaDeferredPercent = r.per;
                    }
                    if (r.accntType == "Noncurrent Liabilities")
                    {
                        bbstotalNonCurrLiaCurrYear += r.budgetMonthly;
                        bbstotalNonCurrLiaPrevYear += r.actual;
                        bbstotalNonCurrLiaDiff += r.diff;
                    }

                    //FUND BALANCE
                    if (r.particulars == "fundRetained")
                    {
                        bbsfundRetainedCurrentYear = r.budgetMonthly;
                        bbsfundRetainedPreviousYear = r.actual;
                        bbsfundRetainedDiff = r.diff;
                        bbsfundRetainedPercent = r.per;
                    }
                    if (r.particulars == "fundCommulative")
                    {
                        bbsfundCommulativeCurrentYear = r.budgetMonthly;
                        bbsfundCommulativePreviousYear = r.actual;
                        bbsfundCommulativeDiff = r.diff;
                        bbsfundCommulativePercent = r.per;
                    }
                    if (r.particulars == "fundFairValue")
                    {
                        bbsfundFairValueCurrentYear = r.budgetMonthly;
                        bbsfundFairValuePreviousYear = r.actual;
                        bbsfundFairValueDiff = r.diff;
                        bbsfundFairValuePercent = r.per;
                    }
                    if (r.accntType == "Fund Balance")
                    {
                        bbstotalFundCurrYear += r.budgetMonthly;
                        bbstotalFundPrevYear += r.actual;
                        bbstotalFundDiff += r.diff;
                    }
                }
                bbstotalCurrAssetsPer = (bbstotalCurrAssetsDiff / bbstotalCurrAssetsPrevYear) * 100;
                bbstotalNonCurrAssetsPer = (bbstotalNonCurrAssetsDiff / bbstotalNonCurrAssetsPrevYear) * 100;
                bbstotalAssetsCurrYear = bbstotalCurrAssetsCurrYear + bbstotalNonCurrAssetsCurrYear;
                bbstotalAssetsPrevYear = bbstotalCurrAssetsPrevYear + bbstotalNonCurrAssetsPrevYear;
                bbstotalAssetsDiff = bbstotalCurrAssetsDiff + bbstotalNonCurrAssetsDiff;
                bbstotalAssetsPer = (bbstotalAssetsDiff / bbstotalAssetsCurrYear) * 100;

                bbstotalCurrLiaPer = (bbstotalCurrLiaDiff / bbstotalCurrLiaPrevYear) * 100;
                bbstotalNonCurrLiaPer = (bbstotalNonCurrLiaDiff / bbstotalNonCurrLiaPrevYear) * 100;
                bbstotalLiaCurrYear = bbstotalCurrLiaCurrYear + bbstotalNonCurrLiaCurrYear;
                bbstotalLiaPrevYear = bbstotalCurrLiaPrevYear + bbstotalNonCurrLiaPrevYear;
                bbstotalLiaDiff = bbstotalCurrLiaDiff + bbstotalNonCurrLiaDiff;
                bbstotalLiaPer = (bbstotalLiaDiff / bbstotalLiaCurrYear) * 100;

                bbstotalFundPer = (bbstotalFundDiff / bbstotalFundPrevYear) * 100;
                bbstotalFundLiaCurrYear = bbstotalLiaCurrYear + bbstotalFundCurrYear;
                bbstotalFundLiaPrevYear = bbstotalLiaPrevYear + bbstotalFundPrevYear;
                bbstotalFundLiaDiff = bbstotalLiaDiff + bbstotalFundDiff;
                bbstotalFundLiaPer = (bbstotalFundLiaDiff / bbstotalFundLiaCurrYear) * 100;
            }

            #endregion  

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("BS");
                ExcelWorksheet isWS = ep.Workbook.Worksheets.Add("IS");
                ExcelWorksheet reWS = ep.Workbook.Worksheets.Add("RE");
                ExcelWorksheet misWS = ep.Workbook.Worksheets.Add("Comparative Monthly IS");
                ExcelWorksheet mbWS = ep.Workbook.Worksheets.Add("Monthly IS vs Budget");
                ExcelWorksheet cbWS = ep.Workbook.Worksheets.Add("Compare Commu IS vs Budget");
                ExcelWorksheet bbsWS = ep.Workbook.Worksheets.Add("BS Actual vs Budget");

                //BALANCE SHEET
                #region
                //HEADER
                ew.Cells["A1:F1"].Merge = true;
                ew.Cells["A1:F1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A1:F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A1:F1"].Style.Font.Bold = true;

                ew.Cells["A2:F2"].Merge = true;
                ew.Cells["A2:F2"].Value = "(A Non-Stock, Non-Profit Organization)";
                ew.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A2:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A2:F2"].Style.Font.Bold = true;

                ew.Cells["A4:F4"].Merge = true;
                ew.Cells["A4:F4"].Value = "STATEMENT OF ASSETS, LIABILITIES AND FUND BALANCE";
                ew.Cells["A4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A4:F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A4:F4"].Style.Font.Bold = true;

                ew.Cells["A5:F5"].Merge = true;
                ew.Cells["A5:F5"].Value = "AS OF " + month + " " + year;
                ew.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A5:F5"].Style.Font.Bold = true;

                ew.Cells["A:F"].Style.Font.Name = "Century Gothic";
                ew.Cells["A1:F7"].Style.Font.Size = 12;
                ew.Cells["A8:F53"].Style.Font.Size = 10;

                //DETAILS
                ew.Cells["B7"].Value = "NOTE";
                ew.Cells["C7"].Value = year;
                ew.Cells["D7"].Value = prevYear;
                ew.Cells["E7"].Value = "INCREASE (DECREASE)";
                ew.Cells["E7"].Style.WrapText = true;
                ew.Cells["F7"].Value = "%";
                ew.Cells["A7:F7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["A7:F7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["A7:F7"].Style.Font.Bold = true;
                ew.Cells["B11:B53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B11:B53"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["B11:B53"].Style.Numberformat.Format = "0";

                ew.Cells["A8"].Value = "ASSETS";
                ew.Cells["A8"].Style.Font.Bold = true;

                ew.Cells["A10"].Value = "Current Assets";
                ew.Cells["A10"].Style.Font.Bold = true;

                ew.Cells["C11:D53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["F11:F53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C11:E53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* '-'??_);_(@_)";
                ew.Cells["F11:F53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                ew.Cells["A11"].Value = "Cash and Cash Equivalents";
                ew.Cells["B11"].Value = 1;
                ew.Cells["C11"].Value = currAssetsCashCurrentYear;
                ew.Cells["D11"].Value = currAssetsCashPreviousYear;
                ew.Cells["E11"].Value = currAssetsDiff;
                ew.Cells["F11"].Value = currAssetsPercent / 100;

                ew.Cells["A12"].Value = "Loans Receivable - Net";
                ew.Cells["B12"].Value = 2;
                ew.Cells["C12"].Value = currAssetsLoanReceivableCurrentYear;
                ew.Cells["D12"].Value = currAssetsLoanReceivablePreviousYear;
                ew.Cells["E12"].Value = currAssetsLoanReceivableDiff;
                ew.Cells["F12"].Value = currAssetsLoanReceivablePercent / 100;

                ew.Cells["A13"].Value = "Other receivables - net";
                ew.Cells["B13"].Value = 3;
                ew.Cells["C13"].Value = currAssetsOtherReceivableCurrentYear;
                ew.Cells["D13"].Value = currAssetsOtherReceivablePreviousYear;
                ew.Cells["E13"].Value = currAssetsOtherReceivableDiff;
                ew.Cells["F13"].Value = currAssetsOtherReceivablePercent / 100;

                ew.Cells["A14"].Value = "Other Current Assets";
                ew.Cells["B14"].Value = 4;
                ew.Cells["C14"].Value = currAssetsOtherCurrentYear;
                ew.Cells["D14"].Value = currAssetsOtherPreviousYear;
                ew.Cells["E14"].Value = currAssetsOtherDiff;
                ew.Cells["F14"].Value = currAssetsOtherPercent / 100;
                ew.Cells["A14:F14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A15"].Value = "     Total Current Assets";
                ew.Cells["B15"].Value = "";
                ew.Cells["C15"].Value = totalCurrAssetsCurrYear;
                ew.Cells["D15"].Value = totalCurrAssetsPrevYear;
                ew.Cells["E15"].Value = totalCurrAssetsDiff;
                ew.Cells["F15"].Value = totalCurrAssetsPer / 100;
                ew.Cells["A15:F15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT ASSETS
                ew.Cells["A17"].Value = "Noncurrent Assets";
                ew.Cells["A17"].Style.Font.Bold = true;

                ew.Cells["A18"].Value = "Financial Assets at FVOCI";
                ew.Cells["B18"].Value = 5;
                ew.Cells["C18"].Value = noncurrAssetsFinancialCurrentYear;
                ew.Cells["D18"].Value = noncurrAssetsFinancialPreviousYear;
                ew.Cells["E18"].Value = noncurrAssetsFinancialDiff;
                ew.Cells["F18"].Value = noncurrAssetsFinancialPercent / 100;

                ew.Cells["A19"].Value = "Restricted Cash";
                ew.Cells["B19"].Value = 6;
                ew.Cells["C19"].Value = noncurrAssetsRestrictedCashCurrentYear;
                ew.Cells["D19"].Value = noncurrAssetsRestrictedCashPreviousYear;
                ew.Cells["E19"].Value = noncurrAssetsRestrictedCashDiff;
                ew.Cells["F19"].Value = noncurrAssetsRestrictedCashPercent / 100;

                ew.Cells["A20"].Value = "ROU Asset";
                ew.Cells["B20"].Value = 7;
                ew.Cells["C20"].Value = noncurrAssetsROUAssetCurrentYear;
                ew.Cells["D20"].Value = noncurrAssetsROUAssetPreviousYear;
                ew.Cells["E20"].Value = noncurrAssetsROUAssetDiff;
                ew.Cells["F20"].Value = noncurrAssetsROUAssetPercent / 100;

                ew.Cells["A21"].Value = "Property and Equipment";
                ew.Cells["B21"].Value = 8;
                ew.Cells["C21"].Value = noncurrAssetsPropertyCurrentYear;
                ew.Cells["D21"].Value = noncurrAssetsPropertyPreviousYear;
                ew.Cells["E21"].Value = noncurrAssetsPropertyDiff;
                ew.Cells["F21"].Value = noncurrAssetsPropertyPercent / 100;

                ew.Cells["A22"].Value = "Other Noncurrent Assets";
                ew.Cells["B22"].Value = 9;
                ew.Cells["C22"].Value = noncurrAssetsOtherCurrentYear;
                ew.Cells["D22"].Value = noncurrAssetsOtherPreviousYear;
                ew.Cells["E22"].Value = noncurrAssetsOtherDiff;
                ew.Cells["F22"].Value = noncurrAssetsOtherPercent / 100;
                ew.Cells["A22:F22"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A23"].Value = "     Total Noncurrent Assets";
                ew.Cells["B23"].Value = "";
                ew.Cells["C23"].Value = totalNonCurrAssetsCurrYear;
                ew.Cells["D23"].Value = totalNonCurrAssetsPrevYear;
                ew.Cells["E23"].Value = totalNonCurrAssetsDiff;
                ew.Cells["F23"].Value = totalNonCurrAssetsPer / 100;
                ew.Cells["A23:F23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //TOTAL ASSETS
                ew.Cells["A25"].Value = "TOTAL ASSETS";
                ew.Cells["B25"].Value = "";
                ew.Cells["C25"].Value = Math.Round(totalAssetsCurrYear,0);
                ew.Cells["D25"].Value = Math.Round(totalAssetsPrevYear,0);
                ew.Cells["E25"].Value = Math.Round(totalAssetsDiff,0);
                ew.Cells["F25"].Value = totalAssetsPer / 100;
                ew.Cells["A25:F25"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A25:F25"].Style.Font.Bold = true;

                //LIABILITIES and FUND BALANCE
                ew.Cells["A28"].Value = "LIABILITIES AND FUND BALANCE";
                ew.Cells["A28"].Style.Font.Bold = true;

                ew.Cells["A30"].Value = "Current Liabilities";
                ew.Cells["A30"].Style.Font.Bold = true;

                //CURRENT LIABILITIES
                ew.Cells["A31"].Value = "Trade and Other Payables";
                ew.Cells["B31"].Value = 10;
                ew.Cells["C31"].Value = currLiaTradeCurrentYear;
                ew.Cells["D31"].Value = currLiaTradePreviousYear;
                ew.Cells["E31"].Value = currLiaTradeDiff;
                ew.Cells["F31"].Value = currLiaTradePercent / 100;

                ew.Cells["A32"].Value = "Unearned Interest Incomeh";
                ew.Cells["B32"].Value = 11;
                ew.Cells["C32"].Value = currLiaUnearnedCurrentYear;
                ew.Cells["D32"].Value = currLiaUnearnedPreviousYear;
                ew.Cells["E32"].Value = currLiaUnearnedDiff;
                ew.Cells["F32"].Value = currLiaUnearnedPercent / 100;

                ew.Cells["A33"].Value = "Clients' CBU";
                ew.Cells["B33"].Value = 12;
                ew.Cells["C33"].Value = currLiaClientCBUCurrentYear;
                ew.Cells["D33"].Value = currLiaClientCBUPreviousYear;
                ew.Cells["E33"].Value = currLiaClientCBUDiff;
                ew.Cells["F33"].Value = currLiaClientCBUPercent / 100;

                ew.Cells["A34"].Value = "Tax Payable";
                ew.Cells["B34"].Value = 13;
                ew.Cells["C34"].Value = currLiaTaxPayableCurrentYear;
                ew.Cells["D34"].Value = currLiaTaxPayablePreviousYear;
                ew.Cells["E34"].Value = currLiaTaxPayableDiff;
                ew.Cells["F34"].Value = currLiaTaxPayablePercent / 100;

                ew.Cells["A35"].Value = "MI/CGLI Payables";
                ew.Cells["B35"].Value = 14;
                ew.Cells["C35"].Value = currLiaCGLIPayableCurrentYear;
                ew.Cells["D35"].Value = currLiaCGLIPayablePreviousYear;
                ew.Cells["E35"].Value = currLiaCGLIPayableDiff;
                ew.Cells["F35"].Value = currLiaCGLIPayablePercent / 100;

                ew.Cells["A36"].Value = "Provision for contingenciess";
                ew.Cells["B36"].Value = 15;
                ew.Cells["C36"].Value = currLiaProvisionalCurrentYear;
                ew.Cells["D36"].Value = currLiaProvisionalPreviousYear;
                ew.Cells["E36"].Value = currLiaProvisionalDiff;
                ew.Cells["F36"].Value = currLiaProvisionalPercent / 100;
                ew.Cells["A36:F36"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A37"].Value = "     Total Current Liabilities";
                ew.Cells["B37"].Value = "";
                ew.Cells["C37"].Value = totalCurrLiaCurrYear;
                ew.Cells["D37"].Value = totalCurrLiaPrevYear;
                ew.Cells["E37"].Value = totalCurrLiaDiff;
                ew.Cells["F37"].Value = totalCurrLiaPer / 100;
                ew.Cells["A37:F37"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT LIABILITIES
                ew.Cells["A39"].Value = "Noncurrent Liabilities";
                ew.Cells["A39"].Style.Font.Bold = true;

                ew.Cells["A40"].Value = "Lease Liability";
                ew.Cells["B40"].Value = 16;
                ew.Cells["C40"].Value = noncurrLiaLeaseCurrentYear;
                ew.Cells["D40"].Value = noncurrLiaLeasePreviousYear;
                ew.Cells["E40"].Value = noncurrLiaLeaseDiff;
                ew.Cells["F40"].Value = noncurrLiaLeasePercent / 100;

                ew.Cells["A41"].Value = "Retirement Benefit Liability";
                ew.Cells["B41"].Value = 17;
                ew.Cells["C41"].Value = noncurrLiaRetirementCurrentYear;
                ew.Cells["D41"].Value = noncurrLiaRetirementPreviousYear;
                ew.Cells["E41"].Value = noncurrLiaRetirementDiff;
                ew.Cells["F41"].Value = noncurrLiaRetirementPercent / 100;

                ew.Cells["A42"].Value = "Deferred Tax Liability";
                ew.Cells["B42"].Value = 18;
                ew.Cells["C42"].Value = noncurrLiaDeferredCurrentYear;
                ew.Cells["D42"].Value = noncurrLiaDeferredPreviousYear;
                ew.Cells["E42"].Value = noncurrLiaDeferredDiff;
                ew.Cells["F42"].Value = noncurrLiaDeferredPercent / 100;
                ew.Cells["A42:F42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A43"].Value = "     Total Noncurrent Liabilities";
                ew.Cells["B43"].Value = "";
                ew.Cells["C43"].Value = totalNonCurrLiaCurrYear;
                ew.Cells["D43"].Value = totalNonCurrLiaPrevYear;
                ew.Cells["E43"].Value = totalNonCurrLiaDiff;
                ew.Cells["F43"].Value = totalNonCurrLiaPer / 100;
                ew.Cells["A43:F43"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A44"].Value = "     Total Liabilities";
                ew.Cells["B44"].Value = "";
                ew.Cells["C44"].Value = totalLiaCurrYear;
                ew.Cells["D44"].Value = totalLiaPrevYear;
                ew.Cells["E44"].Value = totalLiaDiff;
                ew.Cells["F44"].Value = totalLiaPer / 100;
                ew.Cells["A44:F44"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //FUND BALANCE
                ew.Cells["A46"].Value = "Fund Balance";
                ew.Cells["A46"].Style.Font.Bold = true;

                ew.Cells["A47"].Value = "Retained Earnings";
                ew.Cells["B47"].Value = 19;
                ew.Cells["C47"].Value = fundRetainedCurrentYear;
                ew.Cells["D47"].Value = fundRetainedPreviousYear;
                ew.Cells["E47"].Value = fundRetainedDiff;
                ew.Cells["F47"].Value = fundRetainedPercent / 100;

                ew.Cells["A48"].Value = "Cumulative remeasurement gains on";
                ew.Cells["A49"].Value = "     retirement benefit liability";
                ew.Cells["B49"].Value = 20;
                ew.Cells["C49"].Value = fundCommulativeCurrentYear;
                ew.Cells["D49"].Value = fundCommulativePreviousYear;
                ew.Cells["E49"].Value = fundCommulativeDiff;
                ew.Cells["F49"].Value = fundCommulativePercent / 100;

                ew.Cells["A50"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                ew.Cells["B50"].Value = 21;
                ew.Cells["C50"].Value = fundFairValueCurrentYear;
                ew.Cells["D50"].Value = fundFairValuePreviousYear;
                ew.Cells["E50"].Value = fundFairValueDiff;
                ew.Cells["F50"].Value = fundFairValuePercent / 100;
                ew.Cells["A50:F50"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A51"].Value = "     Total Fund Balance";
                ew.Cells["B51"].Value = "";
                ew.Cells["C51"].Value = totalFundCurrYear;
                ew.Cells["D51"].Value = totalFundPrevYear;
                ew.Cells["E51"].Value = totalFundDiff;
                ew.Cells["F51"].Value = totalFundPer / 100;
                ew.Cells["A51:F51"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["A53"].Value = "TOTAL LIABILITIES AND FUND BALANCE";
                ew.Cells["B53"].Value = "";
                ew.Cells["C53"].Value = totalAssetsCurrYear;
                ew.Cells["D53"].Value = totalAssetsPrevYear;
                ew.Cells["E53"].Value = totalAssetsDiff;
                ew.Cells["F53"].Value = totalAssetsPer / 100;
                //ew.Cells["C53"].Value = totalFundLiaCurrYear;
                //ew.Cells["D53"].Value = totalFundLiaPrevYear;
                //ew.Cells["E53"].Value = totalFundLiaDiff;
                //ew.Cells["F53"].Value = totalFundLiaPer / 100;
                ew.Cells["A53:F53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["A53:F53"].Style.Font.Bold = true;

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();
                ew.Column(6).AutoFit();

                ew.View.FreezePanes(8, 2);

                #endregion

                //INCOME STATEMENT
                #region
                isWS.Cells["A1:F1"].Merge = true;
                isWS.Cells["A1:F1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                isWS.Cells["A1:F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["A1:F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                isWS.Cells["A1:F1"].Style.Font.Bold = true;

                isWS.Cells["A2:F2"].Merge = true;
                isWS.Cells["A2:F2"].Value = "(A Non-Stock, Non-Profit Organization)";
                isWS.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["A2:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                isWS.Cells["A2:F2"].Style.Font.Bold = true;

                isWS.Cells["A4:F4"].Merge = true;
                isWS.Cells["A4:F4"].Value = "STATEMENT OF COMPREHENSIVE INCOME";
                isWS.Cells["A4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["A4:F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                isWS.Cells["A4:F4"].Style.Font.Bold = true;

                isWS.Cells["A5:F5"].Merge = true;
                isWS.Cells["A5:F5"].Value = "AS OF " + month + " " + year;
                isWS.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                isWS.Cells["A5:F5"].Style.Font.Bold = true;

                isWS.Cells["A:F"].Style.Font.Name = "Century Gothic";
                isWS.Cells["A1:F8"].Style.Font.Size = 12;
                isWS.Cells["A8:F53"].Style.Font.Size = 10;

                //DETAILS
                isWS.Cells["B8"].Value = "NOTES";
                isWS.Cells["C8"].Value = year;
                isWS.Cells["D8"].Value = prevYearIS;
                isWS.Cells["E8"].Value = "INCREASE (DECREASE)";
                isWS.Cells["E8"].Style.WrapText = true;
                isWS.Cells["F8"].Value = "%";
                isWS.Cells["A8:F8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["A8:F8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                isWS.Cells["A8:F8"].Style.Font.Bold = true;

                isWS.Cells["A9"].Value = "REVENUE";
                isWS.Cells["A9"].Style.Font.Bold = true;

                isWS.Cells["C10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                isWS.Cells["F10:F53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["C10:E53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                isWS.Cells["F10:F53"].Style.Numberformat.Format = "0.00%_);(0.00%)";
                isWS.Cells["B10:B53"].Style.Numberformat.Format = "0";
                isWS.Cells["B10:B53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                isWS.Cells["B10:B53"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                isWS.Cells["A10"].Value = "Interest on Loans";
                isWS.Cells["B10"].Value = 22;
                isWS.Cells["C10"].Value = revIntLoansCurrentYear;
                isWS.Cells["D10"].Value = revIntLoansPreviousYear;
                isWS.Cells["E10"].Value = revIntLoansDiff;
                isWS.Cells["F10"].Value = revIntLoansPercent / 100;

                isWS.Cells["A11"].Value = "Service Income";
                isWS.Cells["B11"].Value = 23;
                isWS.Cells["C11"].Value = revServiceIncomeCurrentYear;
                isWS.Cells["D11"].Value = revServiceIncomePreviousYear;
                isWS.Cells["E11"].Value = revServiceIncomeDiff;
                isWS.Cells["F11"].Value = revServiceIncomePercent / 100;

                isWS.Cells["A12"].Value = "Interest on Deposit and Placements";
                isWS.Cells["B12"].Value = 24;
                isWS.Cells["C12"].Value = revIntDepositsCurrentYear;
                isWS.Cells["D12"].Value = revIntDepositsPreviousYear;
                isWS.Cells["E12"].Value = revIntDepositsDiff;
                isWS.Cells["F12"].Value = revIntDepositsPercent / 100;

                isWS.Cells["A13"].Value = "Forex Gain or Loss";
                isWS.Cells["B13"].Value = 25;
                isWS.Cells["C13"].Value = revForexCurrentYear;
                isWS.Cells["D13"].Value = revForexPreviousYear;
                isWS.Cells["E13"].Value = revForexDiff;
                isWS.Cells["F13"].Value = revForexPercent / 100;

                isWS.Cells["A14"].Value = "Other Income";
                isWS.Cells["B14"].Value = 26;
                isWS.Cells["C14"].Value = revOtherCurrentYear;
                isWS.Cells["D14"].Value = revOtherPreviousYear;
                isWS.Cells["E14"].Value = revOtherDiff;
                isWS.Cells["F14"].Value = revOtherPercent / 100;
                isWS.Cells["A14:F14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                isWS.Cells["A15"].Value = "     Total Gross Income";
                isWS.Cells["B15"].Value = "";
                isWS.Cells["C15"].Value = totGrossIncomeCurrYear;
                isWS.Cells["D15"].Value = totGrossIncomePrevYear;
                isWS.Cells["E15"].Value = totGrossIncomeDiff;
                isWS.Cells["F15"].Value = totGrossIncomePer / 100;
                isWS.Cells["A15:F15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                isWS.Cells["A17"].Value = "Interest Expense";
                isWS.Cells["C17"].Value = 27;
                isWS.Cells["D17"].Value = revIntExpenseCurrentYear;
                isWS.Cells["D17"].Value = revIntExpensePreviousYear;
                isWS.Cells["E17"].Value = revIntExpenseDiff;
                isWS.Cells["F17"].Value = revIntExpensePercent / 100;

                isWS.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                isWS.Cells["B18"].Value = 28;
                isWS.Cells["C18"].Value = revIntExpenseCBUCurrentYear;
                isWS.Cells["D18"].Value = revIntExpenseCBUPreviousYear;
                isWS.Cells["E18"].Value = revIntExpenseCBUDiff;
                isWS.Cells["F18"].Value = revIntExpenseCBUPercent / 100;
                isWS.Cells["A18:F18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                isWS.Cells["A19"].Value = "     Total Financial Costs";
                isWS.Cells["B19"].Value = "";
                isWS.Cells["C19"].Value = totFinancialCostCurrYear;
                isWS.Cells["D19"].Value = totFinancialCostPrevYear;
                isWS.Cells["E19"].Value = totFinancialCostDiff;
                isWS.Cells["F19"].Value = totFinancialCostPer / 100;
                isWS.Cells["A19:F19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                isWS.Cells["A21"].Value = "Provision for Loan Losses";
                isWS.Cells["B21"].Value = 29;
                isWS.Cells["C21"].Value = revProvisionCurrentYear;
                isWS.Cells["D21"].Value = revProvisionPreviousYear;
                isWS.Cells["E21"].Value = revProvisionDiff;
                isWS.Cells["F21"].Value = revProvisionPercent / 100;
                isWS.Cells["A21:F21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                isWS.Cells["A23"].Value = "     Net Margin";
                isWS.Cells["B23"].Value = "";
                isWS.Cells["C23"].Value = totNetMarginCurrYear;
                isWS.Cells["D23"].Value = totNetMarginPrevYear;
                isWS.Cells["E23"].Value = totNetMarginDiff;
                isWS.Cells["F23"].Value = totNetMarginPer / 100;
                isWS.Cells["A23:F23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                isWS.Cells["A25"].Value = "EXPENSES";
                isWS.Cells["A25:F25"].Style.Font.Bold = true;


                isWS.Cells["A26"].Value = "Personnel costs";
                isWS.Cells["B26"].Value = 30;
                isWS.Cells["C26"].Value = expPersonnelCurrentYear;
                isWS.Cells["D26"].Value = expPersonnelPreviousYear;
                isWS.Cells["E26"].Value = expPersonnelDiff;
                isWS.Cells["F26"].Value = expPersonnelPercent / 100;

                isWS.Cells["A27"].Value = "Outstation";
                isWS.Cells["B27"].Value = 31;
                isWS.Cells["C27"].Value = expOutstationCurrentYear;
                isWS.Cells["D27"].Value = expOutstationPreviousYear;
                isWS.Cells["E27"].Value = expOutstationDiff;
                isWS.Cells["F27"].Value = expOutstationPercent / 100;

                isWS.Cells["A28"].Value = "Fringe Benefit";
                isWS.Cells["B28"].Value = 32;
                isWS.Cells["C28"].Value = expFringeCurrentYear;
                isWS.Cells["D28"].Value = expFringePreviousYear;
                isWS.Cells["E28"].Value = expFringeDiff;
                isWS.Cells["F28"].Value = expFringePercent / 100;

                isWS.Cells["A29"].Value = "Retirement";
                isWS.Cells["B29"].Value = 33;
                isWS.Cells["C29"].Value = expRetirementCurrentYear;
                isWS.Cells["D29"].Value = expRetirementPreviousYear;
                isWS.Cells["E29"].Value = expRetirementDiff;
                isWS.Cells["F29"].Value = expRetirementPercent / 100;

                isWS.Cells["A30"].Value = "Outside Services";
                isWS.Cells["B30"].Value = 34;
                isWS.Cells["C30"].Value = expOutsideCurrentYear;
                isWS.Cells["D30"].Value = expOutsidePreviousYear;
                isWS.Cells["E30"].Value = expOutsideDiff;
                isWS.Cells["F30"].Value = expOutsidePercent / 100;

                isWS.Cells["A31"].Value = "Professional fees";
                isWS.Cells["B31"].Value = 35;
                isWS.Cells["C31"].Value = expProfFeesCurrentYear;
                isWS.Cells["D31"].Value = expProfFeesPreviousYear;
                isWS.Cells["E31"].Value = expProfFeesDiff;
                isWS.Cells["F31"].Value = expProfFeesPercent / 100;

                isWS.Cells["A32"].Value = "Meetings, Training and Conferences";
                isWS.Cells["B32"].Value = 36;
                isWS.Cells["C32"].Value = expMeetingsCurrentYear;
                isWS.Cells["D32"].Value = expMeetingsPreviousYear;
                isWS.Cells["E32"].Value = expMeetingsDiff;
                isWS.Cells["F32"].Value = expMeetingsPercent / 100;

                isWS.Cells["A33"].Value = "Client and Community Services";
                isWS.Cells["B33"].Value = 37;
                isWS.Cells["C33"].Value = expClientCurrentYear;
                isWS.Cells["D33"].Value = expClientPreviousYear;
                isWS.Cells["E33"].Value = expClientDiff;
                isWS.Cells["F33"].Value = expClientPercent / 100;

                isWS.Cells["A34"].Value = "Litigation";
                isWS.Cells["B34"].Value = 38;
                isWS.Cells["C34"].Value = expLitigationCurrentYear;
                isWS.Cells["D34"].Value = expLitigationPreviousYear;
                isWS.Cells["E34"].Value = expLitigationDiff;
                isWS.Cells["F34"].Value = expLitigationPercent / 100;

                isWS.Cells["A35"].Value = "Supplies";
                isWS.Cells["B35"].Value = 39;
                isWS.Cells["C35"].Value = expSuppliesCurrentYear;
                isWS.Cells["D35"].Value = expSuppliesPreviousYear;
                isWS.Cells["E35"].Value = expSuppliesDiff;
                isWS.Cells["F35"].Value = expSuppliesPercent / 100;

                isWS.Cells["A36"].Value = "Utilities";
                isWS.Cells["B36"].Value = 40;
                isWS.Cells["C36"].Value = expUtilitiesCurrentYear;
                isWS.Cells["D36"].Value = expUtilitiesPreviousYear;
                isWS.Cells["E36"].Value = expUtilitiesDiff;
                isWS.Cells["F36"].Value = expUtilitiesPercent / 100;

                isWS.Cells["A37"].Value = "Communication and Courier";
                isWS.Cells["B37"].Value = 41;
                isWS.Cells["C37"].Value = expCommunicationCurrentYear;
                isWS.Cells["D37"].Value = expCommunicationPreviousYear;
                isWS.Cells["E37"].Value = expCommunicationDiff;
                isWS.Cells["F37"].Value = expCommunicationPercent / 100;


                isWS.Cells["A38"].Value = "Rental";
                isWS.Cells["B38"].Value = 42;
                isWS.Cells["C38"].Value = expRentalCurrentYear;
                isWS.Cells["D38"].Value = expRentalPreviousYear;
                isWS.Cells["E38"].Value = expRentalDiff;
                isWS.Cells["F38"].Value = expRentalPercent / 100;

                isWS.Cells["A39"].Value = "Insurance";
                isWS.Cells["B39"].Value = 43;
                isWS.Cells["C39"].Value = expInsuranceCurrentYear;
                isWS.Cells["D39"].Value = expInsurancePreviousYear;
                isWS.Cells["E39"].Value = expInsuranceDiff;
                isWS.Cells["F39"].Value = expInsurancePercent / 100;

                isWS.Cells["A40"].Value = "Transportation and Travel";
                isWS.Cells["B40"].Value = 44;
                isWS.Cells["C40"].Value = expTranspoCurrentYear;
                isWS.Cells["D40"].Value = expTranspoPreviousYear;
                isWS.Cells["E40"].Value = expTranspoDiff;
                isWS.Cells["F40"].Value = expTranspoPercent / 100;

                isWS.Cells["A41"].Value = "Taxes and Licenses";
                isWS.Cells["B41"].Value = 45;
                isWS.Cells["C41"].Value = expTaxesCurrentYear;
                isWS.Cells["D41"].Value = expTaxesPreviousYear;
                isWS.Cells["E41"].Value = expTaxesDiff;
                isWS.Cells["F41"].Value = expTaxesPercent / 100;

                isWS.Cells["A42"].Value = "Repairs and Maintenance";
                isWS.Cells["B42"].Value = 46;
                isWS.Cells["C42"].Value = expRepairsCurrentYear;
                isWS.Cells["D42"].Value = expRepairsPreviousYear;
                isWS.Cells["E42"].Value = expRepairsDiff;
                isWS.Cells["F42"].Value = expRepairsPercent / 100;

                isWS.Cells["A43"].Value = "Depreciation and Amortization";
                isWS.Cells["B43"].Value = 47;
                isWS.Cells["C43"].Value = expDepreciationCurrentYear;
                isWS.Cells["D43"].Value = expDepreciationPreviousYear;
                isWS.Cells["E43"].Value = expDepreciationDiff;
                isWS.Cells["F43"].Value = expDepreciationPercent / 100;

                isWS.Cells["A44"].Value = "Provision for Other Losses";
                isWS.Cells["B44"].Value = 48;
                isWS.Cells["C44"].Value = expProvisionCurrentYear;
                isWS.Cells["D44"].Value = expProvisionPreviousYear;
                isWS.Cells["E44"].Value = expProvisionDiff;
                isWS.Cells["F44"].Value = expProvisionPercent / 100;

                isWS.Cells["A45"].Value = "Others";
                isWS.Cells["B45"].Value = 49;
                isWS.Cells["C45"].Value = expOthersCurrentYear;
                isWS.Cells["D45"].Value = expOthersPreviousYear;
                isWS.Cells["E45"].Value = expOthersDiff;
                isWS.Cells["F45"].Value = expOthersPercent / 100;
                isWS.Cells["A45:F45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                isWS.Cells["A46"].Value = "     Total Expenses";
                isWS.Cells["B46"].Value = "";
                isWS.Cells["C46"].Value = totExpensesCurrYear;
                isWS.Cells["D46"].Value = totExpensesPrevYear;
                isWS.Cells["E46"].Value = totExpensesDiff;
                isWS.Cells["F46"].Value = totExpensesPer / 100;
                isWS.Cells["A46:F46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                isWS.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                isWS.Cells["B48"].Value = "";
                isWS.Cells["C48"].Value = totIncomeCurrYear;
                isWS.Cells["D48"].Value = totIncomePrevYear;
                isWS.Cells["E48"].Value = totIncomeDiff;
                isWS.Cells["F48"].Value = totIncomePer / 100;
                isWS.Cells["A48:F48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                isWS.Cells["A48:F48"].Style.Font.Bold = true;

                isWS.Cells["A50"].Value = "INCOME TAX EXPENSE";
                isWS.Cells["B50"].Value = 50;
                isWS.Cells["C50"].Value = incomeTaxExpenseCurrentYear;
                isWS.Cells["D50"].Value = incomeTaxExpensePreviousYear;
                isWS.Cells["E50"].Value = incomeTaxExpenseDiff;
                isWS.Cells["F50"].Value = incomeTaxExpensePercent / 100;
                isWS.Cells["A50:F50"].Style.Font.Bold = true;

                isWS.Cells["A53"].Value = "NET  INCOME (LOSS)";
                isWS.Cells["B53"].Value = "";
                isWS.Cells["C53"].Value = totNetIncomeCurrYear;
                isWS.Cells["D53"].Value = totNetIncomePrevYear;
                isWS.Cells["E53"].Value = totNetIncomeDiff;
                isWS.Cells["F53"].Value = totNetIncomePer / 100;
                isWS.Cells["A53:F53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                isWS.Cells["A53:F53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                isWS.Cells["A53:F53"].Style.Font.Bold = true;

                isWS.Column(1).AutoFit();
                isWS.Column(2).AutoFit();
                isWS.Column(3).AutoFit();
                isWS.Column(4).AutoFit();
                isWS.Column(5).AutoFit();
                isWS.Column(6).AutoFit();

                isWS.View.FreezePanes(8, 2);
                #endregion

                //RETAINED EARNINGS
                #region
                //HEADER
                reWS.Cells["A1:D1"].Merge = true;
                reWS.Cells["A1:D1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                reWS.Cells["A1:D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["A1:D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["A1:D1"].Style.Font.Bold = true;

                reWS.Cells["A2:D2"].Merge = true;
                reWS.Cells["A2:D2"].Value = "(A Non-Stock, Non-Profit Organization)";
                reWS.Cells["A2:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["A2:D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["A2:D2"].Style.Font.Bold = true;

                reWS.Cells["A4:D4"].Merge = true;
                reWS.Cells["A4:D4"].Value = "STATEMENT OF CHANGES IN FUND BALANCE";
                reWS.Cells["A4:D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["A4:D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["A4:D4"].Style.Font.Bold = true;

                reWS.Cells["A5:D5"].Merge = true;
                reWS.Cells["A5:D5"].Value = "FOR THE YEAR ENDED " + month + " " + year;
                reWS.Cells["A5:D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["A5:D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["A5:D5"].Style.Font.Bold = true;

                reWS.Cells["A6:D6"].Merge = true;
                reWS.Cells["A6:D6"].Value = "(With Comparative Figures for " + prevYear + ")";
                reWS.Cells["A6:D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["A6:D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["A6:D6"].Style.Font.Bold = true;

                reWS.Cells["A:D"].Style.Font.Name = "Century Gothic";
                reWS.Cells["A1:D7"].Style.Font.Size = 12;
                reWS.Cells["A8:D41"].Style.Font.Size = 10;

                //DETAILS
                reWS.Cells["B8"].Value = "NOTE";
                reWS.Cells["C8"].Value = year;
                reWS.Cells["D8"].Value = prevYear;

                reWS.Cells["A8:D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["A8:D8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["A8:D8"].Style.Font.Bold = true;
                reWS.Cells["B11:B41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                reWS.Cells["B11:B41"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                reWS.Cells["B11:B41"].Style.Numberformat.Format = "0";

                reWS.Cells["A10"].Value = "Initial Contribution and Retained Earnings (Deficit)";
                reWS.Cells["A10"].Style.Font.Bold = true;

                reWS.Cells["C11:D41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                reWS.Cells["C11:D41"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";

                reWS.Cells["A11"].Value = "Balance at Beginning of Year";
                reWS.Cells["B11"].Value = "";
                reWS.Cells["C11"].Value = initBalanceCurrentYear;
                reWS.Cells["D11"].Value = initBalancePreviousYear;

                reWS.Cells["A12"].Value = "Net Income (Loss)";
                reWS.Cells["B12"].Value = "";
                reWS.Cells["C12"].Value = initNetIncomeCurrentYear;
                reWS.Cells["D12"].Value = initNetIncomePreviousYear;
                reWS.Cells["A12:D12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                reWS.Cells["A13"].Value = "Balance at End of Year";
                reWS.Cells["B13"].Value = "";
                reWS.Cells["C13"].Value = initTotalCurrentYear;
                reWS.Cells["D13"].Value = initTotalPreviousYear;
                reWS.Cells["A13:D13"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                reWS.Cells["A15"].Value = "Cumulative Remeasurement Gains on Retirement Benefit Liability";
                reWS.Cells["A15"].Style.Font.Bold = true;

                reWS.Cells["A16"].Value = "Balance of Beginning of Year";
                reWS.Cells["B16"].Value = "";
                reWS.Cells["C16"].Value = cumuBalanceCurrentYear;
                reWS.Cells["D16"].Value = cumuBalancePreviousYear;

                reWS.Cells["A17"].Value = "Remeasurement Gains (Losses) for the Year";
                reWS.Cells["B17"].Value = "";
                reWS.Cells["C17"].Value = cumuRemeasureCurrentYear;
                reWS.Cells["D17"].Value = cumuRemeasurePreviousYear;
                reWS.Cells["A17:D17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                reWS.Cells["A18"].Value = "Balance at End of Year";
                reWS.Cells["B18"].Value = "";
                reWS.Cells["C18"].Value = cumuTotalCurrentYear;
                reWS.Cells["D18"].Value = cumuTotalPreviousYear;
                reWS.Cells["A18:D18"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                reWS.Cells["A20"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                reWS.Cells["A20"].Style.Font.Bold = true;

                reWS.Cells["A21"].Value = "Balance of Beginning of Year";
                reWS.Cells["B21"].Value = "";
                reWS.Cells["C21"].Value = fairBalanceCurrentYear;
                reWS.Cells["D21"].Value = fairBalancePreviousYear;

                reWS.Cells["A22"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                reWS.Cells["B22"].Value = "";
                reWS.Cells["C22"].Value = fairFairValueCurrentYear;
                reWS.Cells["D22"].Value = fairFairValuePreviousYear;
                reWS.Cells["A22:D22"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                reWS.Cells["A23"].Value = "Balance at End of Year";
                reWS.Cells["B23"].Value = "";
                reWS.Cells["C23"].Value = fairTotalCurrentYear;
                reWS.Cells["D23"].Value = fairTotalPreviousYear;
                reWS.Cells["A23:D23"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                reWS.Cells["A25"].Value = "";
                reWS.Cells["B25"].Value = "";
                reWS.Cells["C25"].Value = TotalCurrentYear;
                reWS.Cells["D25"].Value = TotalPreviousYear;
                reWS.Cells["A25:D25"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                reWS.Cells["A25"].Style.Font.Bold = true;

                reWS.Column(1).AutoFit();
                reWS.Column(2).AutoFit();
                reWS.Column(3).AutoFit();
                reWS.Column(4).AutoFit();

                reWS.View.FreezePanes(8, 2);
                #endregion

                //MONTHY IS COMPARATIVE
                #region
                //HEADER
                misWS.Cells["A1:E1"].Merge = true;
                misWS.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                misWS.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                misWS.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                misWS.Cells["A1:E1"].Style.Font.Bold = true;

                misWS.Cells["A2:E2"].Merge = true;
                misWS.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                misWS.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                misWS.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                misWS.Cells["A2:E2"].Style.Font.Bold = true;

                misWS.Cells["A4:E4"].Merge = true;
                misWS.Cells["A4:E4"].Value = "COMPARATIVE MONTHLY INCOME STATEMENT";
                misWS.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                misWS.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                misWS.Cells["A4:E4"].Style.Font.Bold = true;

                misWS.Cells["A5:E5"].Merge = true;
                misWS.Cells["A5:E5"].Value = "FOR THE MONTH ENDED " + month + " " + year;
                misWS.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                misWS.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                misWS.Cells["A5:E5"].Style.Font.Bold = true;

                misWS.Cells["A:E"].Style.Font.Name = "Century Gothic";
                misWS.Cells["A1:E8"].Style.Font.Size = 12;
                misWS.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                misWS.Cells["B8"].Value = month.ToUpper();
                misWS.Cells["C8"].Value = prevMonthMIS.ToUpper();
                misWS.Cells["D8"].Value = "INCREASE (DECREASE)";
                misWS.Cells["D8"].Style.WrapText = true;
                misWS.Cells["E8"].Value = "%";
                misWS.Cells["A8:E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                misWS.Cells["A8:E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                misWS.Cells["A8:E8"].Style.Font.Bold = true;

                misWS.Cells["A9"].Value = "REVENUE";
                misWS.Cells["A9"].Style.Font.Bold = true;

                misWS.Cells["B10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                misWS.Cells["E10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                misWS.Cells["B10:D53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                misWS.Cells["E10:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                misWS.Cells["A10"].Value = "Interest on Loans";
                misWS.Cells["B10"].Value = misrevIntLoansCurrentYear;
                misWS.Cells["C10"].Value = misrevIntLoansPreviousYear;
                misWS.Cells["D10"].Value = misrevIntLoansDiff;
                misWS.Cells["E10"].Value = misrevIntLoansPercent / 100;

                misWS.Cells["A11"].Value = "Service Income";
                misWS.Cells["B11"].Value = misrevServiceIncomeCurrentYear;
                misWS.Cells["C11"].Value = misrevServiceIncomePreviousYear;
                misWS.Cells["D11"].Value = misrevServiceIncomeDiff;
                misWS.Cells["E11"].Value = misrevServiceIncomePercent / 100;

                misWS.Cells["A12"].Value = "Interest on Deposit and Placements";
                misWS.Cells["B12"].Value = misrevIntDepositsCurrentYear;
                misWS.Cells["C12"].Value = misrevIntDepositsPreviousYear;
                misWS.Cells["D12"].Value = misrevIntDepositsDiff;
                misWS.Cells["E12"].Value = misrevIntDepositsPercent / 100;

                misWS.Cells["A13"].Value = "Forex Gain or Loss";
                misWS.Cells["B13"].Value = misrevForexCurrentYear;
                misWS.Cells["C13"].Value = misrevForexPreviousYear;
                misWS.Cells["D13"].Value = misrevForexDiff;
                misWS.Cells["E13"].Value = misrevForexPercent / 100;

                misWS.Cells["A14"].Value = "Other Income";
                misWS.Cells["B14"].Value = misrevOtherCurrentYear;
                misWS.Cells["C14"].Value = misrevOtherPreviousYear;
                misWS.Cells["D14"].Value = misrevOtherDiff;
                misWS.Cells["E14"].Value = misrevOtherPercent / 100;
                misWS.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                misWS.Cells["A15"].Value = "     Total Gross Income";
                misWS.Cells["B15"].Value = mistotGrossIncomeCurrYear;
                misWS.Cells["C15"].Value = mistotGrossIncomePrevYear;
                misWS.Cells["D15"].Value = mistotGrossIncomeDiff;
                misWS.Cells["E15"].Value = mistotGrossIncomePer / 100;
                misWS.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                misWS.Cells["A17"].Value = "Interest Expense";
                misWS.Cells["B17"].Value = misrevIntExpenseCurrentYear;
                misWS.Cells["C17"].Value = misrevIntExpensePreviousYear;
                misWS.Cells["D17"].Value = misrevIntExpenseDiff;
                misWS.Cells["E17"].Value = misrevIntExpensePercent / 100;

                misWS.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                misWS.Cells["B18"].Value = misrevIntExpenseCBUCurrentYear;
                misWS.Cells["C18"].Value = misrevIntExpenseCBUPreviousYear;
                misWS.Cells["D18"].Value = misrevIntExpenseCBUDiff;
                misWS.Cells["E18"].Value = misrevIntExpenseCBUPercent / 100;
                misWS.Cells["A18:E18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                misWS.Cells["A19"].Value = "     Total Financial Costs";
                misWS.Cells["B19"].Value = mistotFinancialCostCurrYear;
                misWS.Cells["C19"].Value = mistotFinancialCostPrevYear;
                misWS.Cells["D19"].Value = mistotFinancialCostDiff;
                misWS.Cells["E19"].Value = mistotFinancialCostPer / 100;
                misWS.Cells["A19:E19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                misWS.Cells["A21"].Value = "Provision for Loan Losses";
                misWS.Cells["B21"].Value = misrevProvisionCurrentYear;
                misWS.Cells["C21"].Value = misrevProvisionPreviousYear;
                misWS.Cells["D21"].Value = misrevProvisionDiff;
                misWS.Cells["E21"].Value = misrevProvisionPercent / 100;
                misWS.Cells["A21:E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                misWS.Cells["A23"].Value = "     Net Margin";
                misWS.Cells["B23"].Value = mistotNetMarginCurrYear;
                misWS.Cells["C23"].Value = mistotNetMarginPrevYear;
                misWS.Cells["D23"].Value = mistotNetMarginDiff;
                misWS.Cells["E23"].Value = mistotNetMarginPer / 100;
                misWS.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                misWS.Cells["A25"].Value = "EXPENSES";
                misWS.Cells["A25:E25"].Style.Font.Bold = true;


                misWS.Cells["A26"].Value = "Personnel Costs";
                misWS.Cells["B26"].Value = misexpPersonnelCurrentYear;
                misWS.Cells["C26"].Value = misexpPersonnelPreviousYear;
                misWS.Cells["D26"].Value = misexpPersonnelDiff;
                misWS.Cells["E26"].Value = misexpPersonnelPercent / 100;

                misWS.Cells["A27"].Value = "Outstation";
                misWS.Cells["B27"].Value = misexpOutstationCurrentYear;
                misWS.Cells["C27"].Value = misexpOutstationPreviousYear;
                misWS.Cells["D27"].Value = misexpOutstationDiff;
                misWS.Cells["E27"].Value = misexpOutstationPercent / 100;

                misWS.Cells["A28"].Value = "Fringe Benefit";
                misWS.Cells["B28"].Value = misexpFringeCurrentYear;
                misWS.Cells["C28"].Value = misexpFringePreviousYear;
                misWS.Cells["D28"].Value = misexpFringeDiff;
                misWS.Cells["E28"].Value = misexpFringePercent / 100;

                misWS.Cells["A29"].Value = "Retirement";
                misWS.Cells["B29"].Value = misexpRetirementCurrentYear;
                misWS.Cells["C29"].Value = misexpRetirementPreviousYear;
                misWS.Cells["D29"].Value = misexpRetirementDiff;
                misWS.Cells["E29"].Value = misexpRetirementPercent / 100;

                misWS.Cells["A30"].Value = "Outside Services";
                misWS.Cells["B30"].Value = misexpOutsideCurrentYear;
                misWS.Cells["C30"].Value = misexpOutsidePreviousYear;
                misWS.Cells["D30"].Value = misexpOutsideDiff;
                misWS.Cells["E30"].Value = misexpOutsidePercent / 100;

                misWS.Cells["A31"].Value = "Professional fees";
                misWS.Cells["B31"].Value = misexpProfFeesCurrentYear;
                misWS.Cells["C31"].Value = misexpProfFeesPreviousYear;
                misWS.Cells["D31"].Value = misexpProfFeesDiff;
                misWS.Cells["E31"].Value = misexpProfFeesPercent / 100;

                misWS.Cells["A32"].Value = "Meetings, Training and Conferences";
                misWS.Cells["B32"].Value = misexpMeetingsCurrentYear;
                misWS.Cells["C32"].Value = misexpMeetingsPreviousYear;
                misWS.Cells["D32"].Value = misexpMeetingsDiff;
                misWS.Cells["E32"].Value = misexpMeetingsPercent / 100;

                misWS.Cells["A33"].Value = "Client and Community Services";
                misWS.Cells["B33"].Value = misexpClientCurrentYear;
                misWS.Cells["C33"].Value = misexpClientPreviousYear;
                misWS.Cells["D33"].Value = misexpClientDiff;
                misWS.Cells["E33"].Value = misexpClientPercent / 100;

                misWS.Cells["A34"].Value = "Litigation";
                misWS.Cells["B34"].Value = misexpLitigationCurrentYear;
                misWS.Cells["C34"].Value = misexpLitigationPreviousYear;
                misWS.Cells["D34"].Value = misexpLitigationDiff;
                misWS.Cells["E34"].Value = misexpLitigationPercent / 100;

                misWS.Cells["A35"].Value = "Supplies";
                misWS.Cells["B35"].Value = misexpSuppliesCurrentYear;
                misWS.Cells["C35"].Value = misexpSuppliesPreviousYear;
                misWS.Cells["D35"].Value = misexpSuppliesDiff;
                misWS.Cells["E35"].Value = misexpSuppliesPercent / 100;

                misWS.Cells["A36"].Value = "Utilities";
                misWS.Cells["B36"].Value = misexpUtilitiesCurrentYear;
                misWS.Cells["C36"].Value = misexpUtilitiesPreviousYear;
                misWS.Cells["D36"].Value = misexpUtilitiesDiff;
                misWS.Cells["E36"].Value = misexpUtilitiesPercent / 100;

                misWS.Cells["A37"].Value = "Communication and Courier";
                misWS.Cells["B37"].Value = misexpCommunicationCurrentYear;
                misWS.Cells["C37"].Value = misexpCommunicationPreviousYear;
                misWS.Cells["D37"].Value = misexpCommunicationDiff;
                misWS.Cells["E37"].Value = misexpCommunicationPercent / 100;


                misWS.Cells["A38"].Value = "Rental";
                misWS.Cells["B38"].Value = misexpRentalCurrentYear;
                misWS.Cells["C38"].Value = misexpRentalPreviousYear;
                misWS.Cells["D38"].Value = misexpRentalDiff;
                misWS.Cells["E38"].Value = misexpRentalPercent / 100;

                misWS.Cells["A39"].Value = "Insurance";
                misWS.Cells["B39"].Value = misexpInsuranceCurrentYear;
                misWS.Cells["C39"].Value = misexpInsurancePreviousYear;
                misWS.Cells["D39"].Value = misexpInsuranceDiff;
                misWS.Cells["E39"].Value = misexpInsurancePercent / 100;

                misWS.Cells["A40"].Value = "Transportation and Travel";
                misWS.Cells["B40"].Value = misexpTranspoCurrentYear;
                misWS.Cells["C40"].Value = misexpTranspoPreviousYear;
                misWS.Cells["D40"].Value = misexpTranspoDiff;
                misWS.Cells["E40"].Value = misexpTranspoPercent / 100;

                misWS.Cells["A41"].Value = "Taxes and Licenses";
                misWS.Cells["B41"].Value = misexpTaxesCurrentYear;
                misWS.Cells["C41"].Value = misexpTaxesPreviousYear;
                misWS.Cells["D41"].Value = misexpTaxesDiff;
                misWS.Cells["E41"].Value = misexpTaxesPercent / 100;

                misWS.Cells["A42"].Value = "Repairs and Maintenance";
                misWS.Cells["B42"].Value = misexpRepairsCurrentYear;
                misWS.Cells["C42"].Value = misexpRepairsPreviousYear;
                misWS.Cells["D42"].Value = misexpRepairsDiff;
                misWS.Cells["E42"].Value = misexpRepairsPercent / 100;

                misWS.Cells["A43"].Value = "Depreciation and Amortization";
                misWS.Cells["B43"].Value = misexpDepreciationCurrentYear;
                misWS.Cells["C43"].Value = misexpDepreciationPreviousYear;
                misWS.Cells["D43"].Value = misexpDepreciationDiff;
                misWS.Cells["E43"].Value = misexpDepreciationPercent / 100;

                misWS.Cells["A44"].Value = "Provision for Other Losses";
                misWS.Cells["B44"].Value = misexpProvisionCurrentYear;
                misWS.Cells["C44"].Value = misexpProvisionPreviousYear;
                misWS.Cells["D44"].Value = misexpProvisionDiff;
                misWS.Cells["E44"].Value = misexpProvisionPercent / 100;

                misWS.Cells["A45"].Value = "Others";
                misWS.Cells["B45"].Value = misexpOthersCurrentYear;
                misWS.Cells["C45"].Value = misexpOthersPreviousYear;
                misWS.Cells["D45"].Value = misexpOthersDiff;
                misWS.Cells["E45"].Value = misexpOthersPercent / 100;
                misWS.Cells["A45:E45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                misWS.Cells["A46"].Value = "     Total Expenses";
                misWS.Cells["B46"].Value = mistotExpensesCurrYear;
                misWS.Cells["C46"].Value = mistotExpensesPrevYear;
                misWS.Cells["D46"].Value = mistotExpensesDiff;
                misWS.Cells["E46"].Value = mistotExpensesPer / 100;
                misWS.Cells["A46:E46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                misWS.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                misWS.Cells["B48"].Value = mistotIncomeCurrYear;
                misWS.Cells["C48"].Value = mistotIncomePrevYear;
                misWS.Cells["D48"].Value = mistotIncomeDiff;
                misWS.Cells["E48"].Value = mistotIncomePer / 100;
                misWS.Cells["A48:E48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                misWS.Cells["A48:E48"].Style.Font.Bold = true;

                misWS.Cells["A50"].Value = "INCOME TAX EXPENSE";
                misWS.Cells["B50"].Value = misincomeTaxExpenseCurrentYear;
                misWS.Cells["C50"].Value = misincomeTaxExpensePreviousYear;
                misWS.Cells["D50"].Value = misincomeTaxExpenseDiff;
                misWS.Cells["E50"].Value = misincomeTaxExpensePercent / 100;
                misWS.Cells["A50:E50"].Style.Font.Bold = true;

                misWS.Cells["A53"].Value = "NET  INCOME (LOSS)";
                misWS.Cells["B53"].Value = mistotNetIncomeCurrYear;
                misWS.Cells["C53"].Value = mistotNetIncomePrevYear;
                misWS.Cells["D53"].Value = mistotNetIncomeDiff;
                misWS.Cells["E53"].Value = mistotNetIncomePer / 100;
                misWS.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                misWS.Cells["A53:E53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                misWS.Cells["A53:E53"].Style.Font.Bold = true;

                misWS.Column(1).AutoFit();
                misWS.Column(2).AutoFit();
                misWS.Column(3).AutoFit();
                misWS.Column(4).AutoFit();
                misWS.Column(5).AutoFit();

                misWS.View.FreezePanes(8, 2);
                #endregion

                //MONTHLY IS BUDGET
                #region
                mbWS.Cells["A1:E1"].Merge = true;
                mbWS.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                mbWS.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                mbWS.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                mbWS.Cells["A1:E1"].Style.Font.Bold = true;

                mbWS.Cells["A2:E2"].Merge = true;
                mbWS.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                mbWS.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                mbWS.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                mbWS.Cells["A2:E2"].Style.Font.Bold = true;

                mbWS.Cells["A4:E4"].Merge = true;
                mbWS.Cells["A4:E4"].Value = "COMPARATIVE MONTHLY INCOME STATEMENT-ACTUAL VS BUDGET";
                mbWS.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                mbWS.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                mbWS.Cells["A4:E4"].Style.Font.Bold = true;

                mbWS.Cells["A5:E5"].Merge = true;
                mbWS.Cells["A5:E5"].Value = "FOR THE MONTH ENDED " + month + " " + year;
                mbWS.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                mbWS.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                mbWS.Cells["A5:E5"].Style.Font.Bold = true;

                mbWS.Cells["A:E"].Style.Font.Name = "Century Gothic";
                mbWS.Cells["A1:E8"].Style.Font.Size = 12;
                mbWS.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                mbWS.Cells["B8"].Value = "BUDGET";
                mbWS.Cells["C8"].Value = "ACTUAL";
                mbWS.Cells["D8"].Value = "VARIANCE";
                mbWS.Cells["E8"].Value = "%";
                mbWS.Cells["A8:E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                mbWS.Cells["A8:E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                mbWS.Cells["A8:E8"].Style.Font.Bold = true;

                mbWS.Cells["A9"].Value = "REVENUE";
                mbWS.Cells["A9"].Style.Font.Bold = true;

                mbWS.Cells["B10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                mbWS.Cells["E10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                mbWS.Cells["B10:D53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                mbWS.Cells["E10:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                mbWS.Cells["A10"].Value = "Interest on Loans";
                mbWS.Cells["B10"].Value = mbrevIntLoansCurrentYear;
                mbWS.Cells["C10"].Value = mbrevIntLoansPreviousYear;
                mbWS.Cells["D10"].Value = mbrevIntLoansDiff;
                mbWS.Cells["E10"].Value = mbrevIntLoansPercent / 100;

                mbWS.Cells["A11"].Value = "Service Income";
                mbWS.Cells["B11"].Value = mbrevServiceIncomeCurrentYear;
                mbWS.Cells["C11"].Value = mbrevServiceIncomePreviousYear;
                mbWS.Cells["D11"].Value = mbrevServiceIncomeDiff;
                mbWS.Cells["E11"].Value = mbrevServiceIncomePercent / 100;

                mbWS.Cells["A12"].Value = "Interest on Deposit and Placements";
                mbWS.Cells["B12"].Value = mbrevIntDepositsCurrentYear;
                mbWS.Cells["C12"].Value = mbrevIntDepositsPreviousYear;
                mbWS.Cells["D12"].Value = mbrevIntDepositsDiff;
                mbWS.Cells["E12"].Value = mbrevIntDepositsPercent / 100;

                mbWS.Cells["A13"].Value = "Forex Gain or Loss";
                mbWS.Cells["B13"].Value = mbrevForexCurrentYear;
                mbWS.Cells["C13"].Value = mbrevForexPreviousYear;
                mbWS.Cells["D13"].Value = mbrevForexDiff;
                mbWS.Cells["E13"].Value = mbrevForexPercent / 100;

                mbWS.Cells["A14"].Value = "Other Income";
                mbWS.Cells["B14"].Value = mbrevOtherCurrentYear;
                mbWS.Cells["C14"].Value = mbrevOtherPreviousYear;
                mbWS.Cells["D14"].Value = mbrevOtherDiff;
                mbWS.Cells["E14"].Value = mbrevOtherPercent / 100;
                mbWS.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                mbWS.Cells["A15"].Value = "     Total Gross Income";
                mbWS.Cells["B15"].Value = mbtotGrossIncomeCurrYear;
                mbWS.Cells["C15"].Value = mbtotGrossIncomePrevYear;
                mbWS.Cells["D15"].Value = mbtotGrossIncomeDiff;
                mbWS.Cells["E15"].Value = mbtotGrossIncomePer / 100;
                mbWS.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                mbWS.Cells["A17"].Value = "Interest Expense";
                mbWS.Cells["B17"].Value = mbrevIntExpenseCurrentYear;
                mbWS.Cells["C17"].Value = mbrevIntExpensePreviousYear;
                mbWS.Cells["D17"].Value = mbrevIntExpenseDiff;
                mbWS.Cells["E17"].Value = mbrevIntExpensePercent / 100;

                mbWS.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                mbWS.Cells["B18"].Value = mbrevIntExpenseCBUCurrentYear;
                mbWS.Cells["C18"].Value = mbrevIntExpenseCBUPreviousYear;
                mbWS.Cells["D18"].Value = mbrevIntExpenseCBUDiff;
                mbWS.Cells["E18"].Value = mbrevIntExpenseCBUPercent / 100;
                mbWS.Cells["A18:E18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                mbWS.Cells["A19"].Value = "     Total Financial Costs";
                mbWS.Cells["B19"].Value = mbtotFinancialCostCurrYear;
                mbWS.Cells["C19"].Value = mbtotFinancialCostPrevYear;
                mbWS.Cells["D19"].Value = mbtotFinancialCostDiff;
                mbWS.Cells["E19"].Value = mbtotFinancialCostPer / 100;
                mbWS.Cells["A19:E19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                mbWS.Cells["A21"].Value = "Provision for Loan Losses";
                mbWS.Cells["B21"].Value = mbrevProvisionCurrentYear;
                mbWS.Cells["C21"].Value = mbrevProvisionPreviousYear;
                mbWS.Cells["D21"].Value = mbrevProvisionDiff;
                mbWS.Cells["E21"].Value = mbrevProvisionPercent / 100;
                mbWS.Cells["A21:E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                mbWS.Cells["A23"].Value = "     Net Margin";
                mbWS.Cells["B23"].Value = mbtotNetMarginCurrYear;
                mbWS.Cells["C23"].Value = mbtotNetMarginPrevYear;
                mbWS.Cells["D23"].Value = mbtotNetMarginDiff;
                mbWS.Cells["E23"].Value = mbtotNetMarginPer / 100;
                mbWS.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                mbWS.Cells["A25"].Value = "EXPENSES";
                mbWS.Cells["A25:E25"].Style.Font.Bold = true;


                mbWS.Cells["A26"].Value = "Personnel Costs";
                mbWS.Cells["B26"].Value = mbexpPersonnelCurrentYear;
                mbWS.Cells["C26"].Value = mbexpPersonnelPreviousYear;
                mbWS.Cells["D26"].Value = mbexpPersonnelDiff;
                mbWS.Cells["E26"].Value = mbexpPersonnelPercent / 100;

                mbWS.Cells["A27"].Value = "Outstation";
                mbWS.Cells["B27"].Value = mbexpOutstationCurrentYear;
                mbWS.Cells["C27"].Value = mbexpOutstationPreviousYear;
                mbWS.Cells["D27"].Value = mbexpOutstationDiff;
                mbWS.Cells["E27"].Value = mbexpOutstationPercent / 100;

                mbWS.Cells["A28"].Value = "Fringe Benefit";
                mbWS.Cells["B28"].Value = mbexpFringeCurrentYear;
                mbWS.Cells["C28"].Value = mbexpFringePreviousYear;
                mbWS.Cells["D28"].Value = mbexpFringeDiff;
                mbWS.Cells["E28"].Value = mbexpFringePercent / 100;

                mbWS.Cells["A29"].Value = "Retirement";
                mbWS.Cells["B29"].Value = mbexpRetirementCurrentYear;
                mbWS.Cells["C29"].Value = mbexpRetirementPreviousYear;
                mbWS.Cells["D29"].Value = mbexpRetirementDiff;
                mbWS.Cells["E29"].Value = mbexpRetirementPercent / 100;

                mbWS.Cells["A30"].Value = "Outside Services";
                mbWS.Cells["B30"].Value = mbexpOutsideCurrentYear;
                mbWS.Cells["C30"].Value = mbexpOutsidePreviousYear;
                mbWS.Cells["D30"].Value = mbexpOutsideDiff;
                mbWS.Cells["E30"].Value = mbexpOutsidePercent / 100;

                mbWS.Cells["A31"].Value = "Professional fees";
                mbWS.Cells["B31"].Value = mbexpProfFeesCurrentYear;
                mbWS.Cells["C31"].Value = mbexpProfFeesPreviousYear;
                mbWS.Cells["D31"].Value = mbexpProfFeesDiff;
                mbWS.Cells["E31"].Value = mbexpProfFeesPercent / 100;

                mbWS.Cells["A32"].Value = "Meetings, Training and Conferences";
                mbWS.Cells["B32"].Value = mbexpMeetingsCurrentYear;
                mbWS.Cells["C32"].Value = mbexpMeetingsPreviousYear;
                mbWS.Cells["D32"].Value = mbexpMeetingsDiff;
                mbWS.Cells["E32"].Value = mbexpMeetingsPercent / 100;

                mbWS.Cells["A33"].Value = "Client and Community Services";
                mbWS.Cells["B33"].Value = mbexpClientCurrentYear;
                mbWS.Cells["C33"].Value = mbexpClientPreviousYear;
                mbWS.Cells["D33"].Value = mbexpClientDiff;
                mbWS.Cells["E33"].Value = mbexpClientPercent / 100;

                mbWS.Cells["A34"].Value = "Litigation";
                mbWS.Cells["B34"].Value = mbexpLitigationCurrentYear;
                mbWS.Cells["C34"].Value = mbexpLitigationPreviousYear;
                mbWS.Cells["D34"].Value = mbexpLitigationDiff;
                mbWS.Cells["E34"].Value = mbexpLitigationPercent / 100;

                mbWS.Cells["A35"].Value = "Supplies";
                mbWS.Cells["B35"].Value = mbexpSuppliesCurrentYear;
                mbWS.Cells["C35"].Value = mbexpSuppliesPreviousYear;
                mbWS.Cells["D35"].Value = mbexpSuppliesDiff;
                mbWS.Cells["E35"].Value = mbexpSuppliesPercent / 100;

                mbWS.Cells["A36"].Value = "Utilities";
                mbWS.Cells["B36"].Value = mbexpUtilitiesCurrentYear;
                mbWS.Cells["C36"].Value = mbexpUtilitiesPreviousYear;
                mbWS.Cells["D36"].Value = mbexpUtilitiesDiff;
                mbWS.Cells["E36"].Value = mbexpUtilitiesPercent / 100;

                mbWS.Cells["A37"].Value = "Communication and Courier";
                mbWS.Cells["B37"].Value = mbexpCommunicationCurrentYear;
                mbWS.Cells["C37"].Value = mbexpCommunicationPreviousYear;
                mbWS.Cells["D37"].Value = mbexpCommunicationDiff;
                mbWS.Cells["E37"].Value = mbexpCommunicationPercent / 100;


                mbWS.Cells["A38"].Value = "Rental";
                mbWS.Cells["B38"].Value = mbexpRentalCurrentYear;
                mbWS.Cells["C38"].Value = mbexpRentalPreviousYear;
                mbWS.Cells["D38"].Value = mbexpRentalDiff;
                mbWS.Cells["E38"].Value = mbexpRentalPercent / 100;

                mbWS.Cells["A39"].Value = "Insurance";
                mbWS.Cells["B39"].Value = mbexpInsuranceCurrentYear;
                mbWS.Cells["C39"].Value = mbexpInsurancePreviousYear;
                mbWS.Cells["D39"].Value = mbexpInsuranceDiff;
                mbWS.Cells["E39"].Value = mbexpInsurancePercent / 100;

                mbWS.Cells["A40"].Value = "Transportation and Travel";
                mbWS.Cells["B40"].Value = mbexpTranspoCurrentYear;
                mbWS.Cells["C40"].Value = mbexpTranspoPreviousYear;
                mbWS.Cells["D40"].Value = mbexpTranspoDiff;
                mbWS.Cells["E40"].Value = mbexpTranspoPercent / 100;

                mbWS.Cells["A41"].Value = "Taxes and Licenses";
                mbWS.Cells["B41"].Value = mbexpTaxesCurrentYear;
                mbWS.Cells["C41"].Value = mbexpTaxesPreviousYear;
                mbWS.Cells["D41"].Value = mbexpTaxesDiff;
                mbWS.Cells["E41"].Value = mbexpTaxesPercent / 100;

                mbWS.Cells["A42"].Value = "Repairs and Maintenance";
                mbWS.Cells["B42"].Value = mbexpRepairsCurrentYear;
                mbWS.Cells["C42"].Value = mbexpRepairsPreviousYear;
                mbWS.Cells["D42"].Value = mbexpRepairsDiff;
                mbWS.Cells["E42"].Value = mbexpRepairsPercent / 100;

                mbWS.Cells["A43"].Value = "Depreciation and Amortization";
                mbWS.Cells["B43"].Value = mbexpDepreciationCurrentYear;
                mbWS.Cells["C43"].Value = mbexpDepreciationPreviousYear;
                mbWS.Cells["D43"].Value = mbexpDepreciationDiff;
                mbWS.Cells["E43"].Value = mbexpDepreciationPercent / 100;

                mbWS.Cells["A44"].Value = "Provision for Other Losses";
                mbWS.Cells["B44"].Value = mbexpProvisionCurrentYear;
                mbWS.Cells["C44"].Value = mbexpProvisionPreviousYear;
                mbWS.Cells["D44"].Value = mbexpProvisionDiff;
                mbWS.Cells["E44"].Value = mbexpProvisionPercent / 100;

                mbWS.Cells["A45"].Value = "Others";
                mbWS.Cells["B45"].Value = mbexpOthersCurrentYear;
                mbWS.Cells["C45"].Value = mbexpOthersPreviousYear;
                mbWS.Cells["D45"].Value = mbexpOthersDiff;
                mbWS.Cells["E45"].Value = mbexpOthersPercent / 100;
                mbWS.Cells["A45:E45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                mbWS.Cells["A46"].Value = "     Total Expenses";
                mbWS.Cells["B46"].Value = mbtotExpensesCurrYear;
                mbWS.Cells["C46"].Value = mbtotExpensesPrevYear;
                mbWS.Cells["D46"].Value = mbtotExpensesDiff;
                mbWS.Cells["E46"].Value = mbtotExpensesPer / 100;
                mbWS.Cells["A46:E46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                mbWS.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                mbWS.Cells["B48"].Value = mbtotIncomeCurrYear;
                mbWS.Cells["C48"].Value = mbtotIncomePrevYear;
                mbWS.Cells["D48"].Value = mbtotIncomeDiff;
                mbWS.Cells["E48"].Value = mbtotIncomePer / 100;
                mbWS.Cells["A48:E48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                mbWS.Cells["A48:E48"].Style.Font.Bold = true;

                mbWS.Cells["A50"].Value = "INCOME TAX EXPENSE";
                mbWS.Cells["B50"].Value = mbincomeTaxExpenseCurrentYear;
                mbWS.Cells["C50"].Value = mbincomeTaxExpensePreviousYear;
                mbWS.Cells["D50"].Value = mbincomeTaxExpenseDiff;
                mbWS.Cells["E50"].Value = mbincomeTaxExpensePercent / 100;
                mbWS.Cells["A50:E50"].Style.Font.Bold = true;

                mbWS.Cells["A53"].Value = "NET  INCOME (LOSS)";
                mbWS.Cells["B53"].Value = mbtotNetIncomeCurrYear;
                mbWS.Cells["C53"].Value = mbtotNetIncomePrevYear;
                mbWS.Cells["D53"].Value = mbtotNetIncomeDiff;
                mbWS.Cells["E53"].Value = mbtotNetIncomePer / 100;
                mbWS.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                mbWS.Cells["A53:E53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                mbWS.Cells["A53:E53"].Style.Font.Bold = true;

                mbWS.Column(1).AutoFit();
                mbWS.Column(2).AutoFit();
                mbWS.Column(3).AutoFit();
                mbWS.Column(4).AutoFit();
                mbWS.Column(5).AutoFit();

                mbWS.View.FreezePanes(8, 2);
                #endregion

                //COMMULATIVE IS BUDGET
                #region
                //HEADER
                cbWS.Cells["A1:E1"].Merge = true;
                cbWS.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                cbWS.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cbWS.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cbWS.Cells["A1:E1"].Style.Font.Bold = true;

                cbWS.Cells["A2:E2"].Merge = true;
                cbWS.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                cbWS.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cbWS.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cbWS.Cells["A2:E2"].Style.Font.Bold = true;

                cbWS.Cells["A4:E4"].Merge = true;
                cbWS.Cells["A4:E4"].Value = "COMPARATIVE STATEMENT OF COMPREHENSIVE INCOME";
                cbWS.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cbWS.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cbWS.Cells["A4:E4"].Style.Font.Bold = true;

                cbWS.Cells["A5:E5"].Merge = true;
                cbWS.Cells["A5:E5"].Value = "FOR THE MONTH ENDED " + month + " " + year;
                cbWS.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cbWS.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cbWS.Cells["A5:E5"].Style.Font.Bold = true;

                cbWS.Cells["A:E"].Style.Font.Name = "Century Gothic";
                cbWS.Cells["A1:E8"].Style.Font.Size = 12;
                cbWS.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                cbWS.Cells["B8"].Value = "BUDGET";
                cbWS.Cells["C8"].Value = "ACTUAL";
                cbWS.Cells["D8"].Value = "VARIANCE";
                cbWS.Cells["E8"].Value = "%";
                cbWS.Cells["A8:E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cbWS.Cells["A8:E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cbWS.Cells["A8:E8"].Style.Font.Bold = true;

                cbWS.Cells["A9"].Value = "REVENUE";
                cbWS.Cells["A9"].Style.Font.Bold = true;

                cbWS.Cells["B10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                cbWS.Cells["E10:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cbWS.Cells["B10:D53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                cbWS.Cells["E10:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                cbWS.Cells["A10"].Value = "Interest on Loans";
                cbWS.Cells["B10"].Value = cbrevIntLoansCurrentYear;
                cbWS.Cells["C10"].Value = cbrevIntLoansPreviousYear;
                cbWS.Cells["D10"].Value = cbrevIntLoansDiff;
                cbWS.Cells["E10"].Value = cbrevIntLoansPercent / 100;

                cbWS.Cells["A11"].Value = "Service Income";
                cbWS.Cells["B11"].Value = cbrevServiceIncomeCurrentYear;
                cbWS.Cells["C11"].Value = cbrevServiceIncomePreviousYear;
                cbWS.Cells["D11"].Value = cbrevServiceIncomeDiff;
                cbWS.Cells["E11"].Value = cbrevServiceIncomePercent / 100;

                cbWS.Cells["A12"].Value = "Interest on Deposit and Placements";
                cbWS.Cells["B12"].Value = cbrevIntDepositsCurrentYear;
                cbWS.Cells["C12"].Value = cbrevIntDepositsPreviousYear;
                cbWS.Cells["D12"].Value = cbrevIntDepositsDiff;
                cbWS.Cells["E12"].Value = cbrevIntDepositsPercent / 100;

                cbWS.Cells["A13"].Value = "Forex Gain or Loss";
                cbWS.Cells["B13"].Value = cbrevForexCurrentYear;
                cbWS.Cells["C13"].Value = cbrevForexPreviousYear;
                cbWS.Cells["D13"].Value = cbrevForexDiff;
                cbWS.Cells["E13"].Value = cbrevForexPercent / 100;

                cbWS.Cells["A14"].Value = "Other Income";
                cbWS.Cells["B14"].Value = cbrevOtherCurrentYear;
                cbWS.Cells["C14"].Value = cbrevOtherPreviousYear;
                cbWS.Cells["D14"].Value = cbrevOtherDiff;
                cbWS.Cells["E14"].Value = cbrevOtherPercent / 100;
                cbWS.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                cbWS.Cells["A15"].Value = "     Total Gross Income";
                cbWS.Cells["B15"].Value = cbtotGrossIncomeCurrYear;
                cbWS.Cells["C15"].Value = cbtotGrossIncomePrevYear;
                cbWS.Cells["D15"].Value = cbtotGrossIncomeDiff;
                cbWS.Cells["E15"].Value = cbtotGrossIncomePer / 100;
                cbWS.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                cbWS.Cells["A17"].Value = "Interest Expense";
                cbWS.Cells["B17"].Value = cbrevIntExpenseCurrentYear;
                cbWS.Cells["C17"].Value = cbrevIntExpensePreviousYear;
                cbWS.Cells["D17"].Value = cbrevIntExpenseDiff;
                cbWS.Cells["E17"].Value = cbrevIntExpensePercent / 100;

                cbWS.Cells["A18"].Value = "Interest Expense on Clients' CBU";
                cbWS.Cells["B18"].Value = cbrevIntExpenseCBUCurrentYear;
                cbWS.Cells["C18"].Value = cbrevIntExpenseCBUPreviousYear;
                cbWS.Cells["D18"].Value = cbrevIntExpenseCBUDiff;
                cbWS.Cells["E18"].Value = cbrevIntExpenseCBUPercent / 100;
                cbWS.Cells["A18:E18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                cbWS.Cells["A19"].Value = "     Total Financial Costs";
                cbWS.Cells["B19"].Value = cbtotFinancialCostCurrYear;
                cbWS.Cells["C19"].Value = cbtotFinancialCostPrevYear;
                cbWS.Cells["D19"].Value = cbtotFinancialCostDiff;
                cbWS.Cells["E19"].Value = cbtotFinancialCostPer / 100;
                cbWS.Cells["A19:E19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                cbWS.Cells["A21"].Value = "Provision for Loan Losses";
                cbWS.Cells["B21"].Value = cbrevProvisionCurrentYear;
                cbWS.Cells["C21"].Value = cbrevProvisionPreviousYear;
                cbWS.Cells["D21"].Value = cbrevProvisionDiff;
                cbWS.Cells["E21"].Value = cbrevProvisionPercent / 100;
                cbWS.Cells["A21:E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                cbWS.Cells["A23"].Value = "     Net Margin";
                cbWS.Cells["B23"].Value = cbtotNetMarginCurrYear;
                cbWS.Cells["C23"].Value = cbtotNetMarginPrevYear;
                cbWS.Cells["D23"].Value = cbtotNetMarginDiff;
                cbWS.Cells["E23"].Value = cbtotNetMarginPer / 100;
                cbWS.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //EXPENSES
                cbWS.Cells["A25"].Value = "EXPENSES";
                cbWS.Cells["A25:E25"].Style.Font.Bold = true;


                cbWS.Cells["A26"].Value = "Personnel Costs";
                cbWS.Cells["B26"].Value = cbexpPersonnelCurrentYear;
                cbWS.Cells["C26"].Value = cbexpPersonnelPreviousYear;
                cbWS.Cells["D26"].Value = cbexpPersonnelDiff;
                cbWS.Cells["E26"].Value = cbexpPersonnelPercent / 100;

                cbWS.Cells["A27"].Value = "Outstation";
                cbWS.Cells["B27"].Value = cbexpOutstationCurrentYear;
                cbWS.Cells["C27"].Value = cbexpOutstationPreviousYear;
                cbWS.Cells["D27"].Value = cbexpOutstationDiff;
                cbWS.Cells["E27"].Value = cbexpOutstationPercent / 100;

                cbWS.Cells["A28"].Value = "Fringe Benefit";
                cbWS.Cells["B28"].Value = cbexpFringeCurrentYear;
                cbWS.Cells["C28"].Value = cbexpFringePreviousYear;
                cbWS.Cells["D28"].Value = cbexpFringeDiff;
                cbWS.Cells["E28"].Value = cbexpFringePercent / 100;

                cbWS.Cells["A29"].Value = "Retirement";
                cbWS.Cells["B29"].Value = cbexpRetirementCurrentYear;
                cbWS.Cells["C29"].Value = cbexpRetirementPreviousYear;
                cbWS.Cells["D29"].Value = cbexpRetirementDiff;
                cbWS.Cells["E29"].Value = cbexpRetirementPercent / 100;

                cbWS.Cells["A30"].Value = "Outside Services";
                cbWS.Cells["B30"].Value = cbexpOutsideCurrentYear;
                cbWS.Cells["C30"].Value = cbexpOutsidePreviousYear;
                cbWS.Cells["D30"].Value = cbexpOutsideDiff;
                cbWS.Cells["E30"].Value = cbexpOutsidePercent / 100;

                cbWS.Cells["A31"].Value = "Professional fees";
                cbWS.Cells["B31"].Value = cbexpProfFeesCurrentYear;
                cbWS.Cells["C31"].Value = cbexpProfFeesPreviousYear;
                cbWS.Cells["D31"].Value = cbexpProfFeesDiff;
                cbWS.Cells["E31"].Value = cbexpProfFeesPercent / 100;

                cbWS.Cells["A32"].Value = "Meetings, Training and Conferences";
                cbWS.Cells["B32"].Value = cbexpMeetingsCurrentYear;
                cbWS.Cells["C32"].Value = cbexpMeetingsPreviousYear;
                cbWS.Cells["D32"].Value = cbexpMeetingsDiff;
                cbWS.Cells["E32"].Value = cbexpMeetingsPercent / 100;

                cbWS.Cells["A33"].Value = "Client and Community Services";
                cbWS.Cells["B33"].Value = cbexpClientCurrentYear;
                cbWS.Cells["C33"].Value = cbexpClientPreviousYear;
                cbWS.Cells["D33"].Value = cbexpClientDiff;
                cbWS.Cells["E33"].Value = cbexpClientPercent / 100;

                cbWS.Cells["A34"].Value = "Litigation";
                cbWS.Cells["B34"].Value = cbexpLitigationCurrentYear;
                cbWS.Cells["C34"].Value = cbexpLitigationPreviousYear;
                cbWS.Cells["D34"].Value = cbexpLitigationDiff;
                cbWS.Cells["E34"].Value = cbexpLitigationPercent / 100;

                cbWS.Cells["A35"].Value = "Supplies";
                cbWS.Cells["B35"].Value = cbexpSuppliesCurrentYear;
                cbWS.Cells["C35"].Value = cbexpSuppliesPreviousYear;
                cbWS.Cells["D35"].Value = cbexpSuppliesDiff;
                cbWS.Cells["E35"].Value = cbexpSuppliesPercent / 100;

                cbWS.Cells["A36"].Value = "Utilities";
                cbWS.Cells["B36"].Value = cbexpUtilitiesCurrentYear;
                cbWS.Cells["C36"].Value = cbexpUtilitiesPreviousYear;
                cbWS.Cells["D36"].Value = cbexpUtilitiesDiff;
                cbWS.Cells["E36"].Value = cbexpUtilitiesPercent / 100;

                cbWS.Cells["A37"].Value = "Communication and Courier";
                cbWS.Cells["B37"].Value = cbexpCommunicationCurrentYear;
                cbWS.Cells["C37"].Value = cbexpCommunicationPreviousYear;
                cbWS.Cells["D37"].Value = cbexpCommunicationDiff;
                cbWS.Cells["E37"].Value = cbexpCommunicationPercent / 100;


                cbWS.Cells["A38"].Value = "Rental";
                cbWS.Cells["B38"].Value = cbexpRentalCurrentYear;
                cbWS.Cells["C38"].Value = cbexpRentalPreviousYear;
                cbWS.Cells["D38"].Value = cbexpRentalDiff;
                cbWS.Cells["E38"].Value = cbexpRentalPercent / 100;

                cbWS.Cells["A39"].Value = "Insurance";
                cbWS.Cells["B39"].Value = cbexpInsuranceCurrentYear;
                cbWS.Cells["C39"].Value = cbexpInsurancePreviousYear;
                cbWS.Cells["D39"].Value = cbexpInsuranceDiff;
                cbWS.Cells["E39"].Value = cbexpInsurancePercent / 100;

                cbWS.Cells["A40"].Value = "Transportation and Travel";
                cbWS.Cells["B40"].Value = cbexpTranspoCurrentYear;
                cbWS.Cells["C40"].Value = cbexpTranspoPreviousYear;
                cbWS.Cells["D40"].Value = cbexpTranspoDiff;
                cbWS.Cells["E40"].Value = cbexpTranspoPercent / 100;

                cbWS.Cells["A41"].Value = "Taxes and Licenses";
                cbWS.Cells["B41"].Value = cbexpTaxesCurrentYear;
                cbWS.Cells["C41"].Value = cbexpTaxesPreviousYear;
                cbWS.Cells["D41"].Value = cbexpTaxesDiff;
                cbWS.Cells["E41"].Value = cbexpTaxesPercent / 100;

                cbWS.Cells["A42"].Value = "Repairs and Maintenance";
                cbWS.Cells["B42"].Value = cbexpRepairsCurrentYear;
                cbWS.Cells["C42"].Value = cbexpRepairsPreviousYear;
                cbWS.Cells["D42"].Value = cbexpRepairsDiff;
                cbWS.Cells["E42"].Value = cbexpRepairsPercent / 100;

                cbWS.Cells["A43"].Value = "Depreciation and Amortization";
                cbWS.Cells["B43"].Value = cbexpDepreciationCurrentYear;
                cbWS.Cells["C43"].Value = cbexpDepreciationPreviousYear;
                cbWS.Cells["D43"].Value = cbexpDepreciationDiff;
                cbWS.Cells["E43"].Value = cbexpDepreciationPercent / 100;

                cbWS.Cells["A44"].Value = "Provision for Other Losses";
                cbWS.Cells["B44"].Value = cbexpProvisionCurrentYear;
                cbWS.Cells["C44"].Value = cbexpProvisionPreviousYear;
                cbWS.Cells["D44"].Value = cbexpProvisionDiff;
                cbWS.Cells["E44"].Value = cbexpProvisionPercent / 100;

                cbWS.Cells["A45"].Value = "Others";
                cbWS.Cells["B45"].Value = cbexpOthersCurrentYear;
                cbWS.Cells["C45"].Value = cbexpOthersPreviousYear;
                cbWS.Cells["D45"].Value = cbexpOthersDiff;
                cbWS.Cells["E45"].Value = cbexpOthersPercent / 100;
                cbWS.Cells["A45:E45"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                cbWS.Cells["A46"].Value = "     Total Expenses";
                cbWS.Cells["B46"].Value = cbtotExpensesCurrYear;
                cbWS.Cells["C46"].Value = cbtotExpensesPrevYear;
                cbWS.Cells["D46"].Value = cbtotExpensesDiff;
                cbWS.Cells["E46"].Value = cbtotExpensesPer / 100;
                cbWS.Cells["A46:E46"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                cbWS.Cells["A48"].Value = "INCOME (LOSS) BEFORE TAX";
                cbWS.Cells["B48"].Value = cbtotIncomeCurrYear;
                cbWS.Cells["C48"].Value = cbtotIncomePrevYear;
                cbWS.Cells["D48"].Value = cbtotIncomeDiff;
                cbWS.Cells["E48"].Value = cbtotIncomePer / 100;
                cbWS.Cells["A48:E48"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                cbWS.Cells["A48:E48"].Style.Font.Bold = true;

                cbWS.Cells["A50"].Value = "INCOME TAX EXPENSE";
                cbWS.Cells["B50"].Value = cbincomeTaxExpenseCurrentYear;
                cbWS.Cells["C50"].Value = cbincomeTaxExpensePreviousYear;
                cbWS.Cells["D50"].Value = cbincomeTaxExpenseDiff;
                cbWS.Cells["E50"].Value = cbincomeTaxExpensePercent / 100;
                cbWS.Cells["A50:E50"].Style.Font.Bold = true;

                cbWS.Cells["A53"].Value = "NET  INCOME (LOSS)";
                cbWS.Cells["B53"].Value = cbtotNetIncomeCurrYear;
                cbWS.Cells["C53"].Value = cbtotNetIncomePrevYear;
                cbWS.Cells["D53"].Value = cbtotNetIncomeDiff;
                cbWS.Cells["E53"].Value = cbtotNetIncomePer / 100;
                cbWS.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                cbWS.Cells["A53:E53"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                cbWS.Cells["A53:E53"].Style.Font.Bold = true;

                cbWS.Column(1).AutoFit();
                cbWS.Column(2).AutoFit();
                cbWS.Column(3).AutoFit();
                cbWS.Column(4).AutoFit();
                cbWS.Column(5).AutoFit();

                cbWS.View.FreezePanes(8, 2);
                #endregion

                //BS BUDGET
                #region
                bbsWS.Cells["A1:E1"].Merge = true;
                bbsWS.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                bbsWS.Cells["A1:E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                bbsWS.Cells["A1:E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                bbsWS.Cells["A1:E1"].Style.Font.Bold = true;

                bbsWS.Cells["A2:E2"].Merge = true;
                bbsWS.Cells["A2:E2"].Value = "(A Non-Stock, Non-Profit Organization)";
                bbsWS.Cells["A2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                bbsWS.Cells["A2:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                bbsWS.Cells["A2:E2"].Style.Font.Bold = true;

                bbsWS.Cells["A4:E4"].Merge = true;
                bbsWS.Cells["A4:E4"].Value = "STATEMENT OF ASSETS, LIABILITIES AND FUND BALANCE";
                bbsWS.Cells["A4:E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                bbsWS.Cells["A4:E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                bbsWS.Cells["A4:E4"].Style.Font.Bold = true;

                bbsWS.Cells["A5:E5"].Merge = true;
                bbsWS.Cells["A5:E5"].Value = "AS OF " + month + " " + year;
                bbsWS.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                bbsWS.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                bbsWS.Cells["A5:E5"].Style.Font.Bold = true;

                bbsWS.Cells["A:E"].Style.Font.Name = "Century Gothic";
                bbsWS.Cells["A1:E7"].Style.Font.Size = 12;
                bbsWS.Cells["A8:E53"].Style.Font.Size = 10;

                //DETAILS
                //bbsWS.Cells["B7"].Value = "NOTE";
                bbsWS.Cells["B7"].Value = "BUDGET";
                bbsWS.Cells["C7"].Value = "ACTUAL";
                bbsWS.Cells["D7"].Value = "INCREASE (DECREASE)";
                bbsWS.Cells["D7"].Style.WrapText = true;
                bbsWS.Cells["E7"].Value = "%";
                bbsWS.Cells["A7:E7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                bbsWS.Cells["A7:E7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                bbsWS.Cells["A7:E7"].Style.Font.Bold = true;
                //bbsWS.Cells["B11:B53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //bbsWS.Cells["B11:B53"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //bbsWS.Cells["B11:B53"].Style.Numberformat.Format = "0";

                bbsWS.Cells["A8"].Value = "ASSETS";
                bbsWS.Cells["A8"].Style.Font.Bold = true;

                bbsWS.Cells["A10"].Value = "Current Assets";
                bbsWS.Cells["A10"].Style.Font.Bold = true;

                bbsWS.Cells["B11:D53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                bbsWS.Cells["E11:E53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                bbsWS.Cells["C11:E53"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* '-'??_);_(@_)";
                bbsWS.Cells["E11:E53"].Style.Numberformat.Format = "0.00%_);(0.00%)";

                bbsWS.Cells["A11"].Value = "Cash and Cash Equivalents";
                bbsWS.Cells["B11"].Value = bbscurrAssetsCashCurrentYear;
                bbsWS.Cells["C11"].Value = bbscurrAssetsCashPreviousYear;
                bbsWS.Cells["D11"].Value = bbscurrAssetsDiff;
                bbsWS.Cells["E11"].Value = bbscurrAssetsPercent / 100;

                bbsWS.Cells["A12"].Value = "Loans Receivable - Net";
                bbsWS.Cells["B12"].Value = bbscurrAssetsLoanReceivableCurrentYear;
                bbsWS.Cells["C12"].Value = bbscurrAssetsLoanReceivablePreviousYear;
                bbsWS.Cells["D12"].Value = bbscurrAssetsLoanReceivableDiff;
                bbsWS.Cells["E12"].Value = bbscurrAssetsLoanReceivablePercent / 100;

                bbsWS.Cells["A13"].Value = "Other receivables - net";
                bbsWS.Cells["B13"].Value = bbscurrAssetsOtherReceivableCurrentYear;
                bbsWS.Cells["C13"].Value = bbscurrAssetsOtherReceivablePreviousYear;
                bbsWS.Cells["D13"].Value = bbscurrAssetsOtherReceivableDiff;
                bbsWS.Cells["E13"].Value = bbscurrAssetsOtherReceivablePercent / 100;

                bbsWS.Cells["A14"].Value = "Other Current Assets";
                bbsWS.Cells["B14"].Value = bbscurrAssetsOtherCurrentYear;
                bbsWS.Cells["C14"].Value = bbscurrAssetsOtherPreviousYear;
                bbsWS.Cells["D14"].Value = bbscurrAssetsOtherDiff;
                bbsWS.Cells["E14"].Value = bbscurrAssetsOtherPercent / 100;
                bbsWS.Cells["A14:E14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A15"].Value = "     Total Current Assets";
                bbsWS.Cells["B15"].Value = bbstotalCurrAssetsCurrYear;
                bbsWS.Cells["C15"].Value = bbstotalCurrAssetsPrevYear;
                bbsWS.Cells["D15"].Value = bbstotalCurrAssetsDiff;
                bbsWS.Cells["E15"].Value = bbstotalCurrAssetsPer / 100;
                bbsWS.Cells["A15:E15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT ASSETS
                bbsWS.Cells["A17"].Value = "Noncurrent Assets";
                bbsWS.Cells["A17"].Style.Font.Bold = true;

                bbsWS.Cells["A18"].Value = "Financial Assets at FVOCI";
                bbsWS.Cells["B18"].Value = bbsnoncurrAssetsFinancialCurrentYear;
                bbsWS.Cells["C18"].Value = bbsnoncurrAssetsFinancialPreviousYear;
                bbsWS.Cells["D18"].Value = bbsnoncurrAssetsFinancialDiff;
                bbsWS.Cells["E18"].Value = bbsnoncurrAssetsFinancialPercent / 100;

                bbsWS.Cells["A19"].Value = "Restricted Cash";
                bbsWS.Cells["B19"].Value = bbsnoncurrAssetsRestrictedCashCurrentYear;
                bbsWS.Cells["C19"].Value = bbsnoncurrAssetsRestrictedCashPreviousYear;
                bbsWS.Cells["D19"].Value = bbsnoncurrAssetsRestrictedCashDiff;
                bbsWS.Cells["E19"].Value = bbsnoncurrAssetsRestrictedCashPercent / 100;

                bbsWS.Cells["A20"].Value = "ROU Asset";
                bbsWS.Cells["B20"].Value = bbsnoncurrAssetsROUAssetCurrentYear;
                bbsWS.Cells["C20"].Value = bbsnoncurrAssetsROUAssetPreviousYear;
                bbsWS.Cells["D20"].Value = bbsnoncurrAssetsROUAssetDiff;
                bbsWS.Cells["E20"].Value = bbsnoncurrAssetsROUAssetPercent / 100;

                bbsWS.Cells["A21"].Value = "Property and Equipment";
                bbsWS.Cells["B21"].Value = bbsnoncurrAssetsPropertyCurrentYear;
                bbsWS.Cells["C21"].Value = bbsnoncurrAssetsPropertyPreviousYear;
                bbsWS.Cells["D21"].Value = bbsnoncurrAssetsPropertyDiff;
                bbsWS.Cells["E21"].Value = bbsnoncurrAssetsPropertyPercent / 100;

                bbsWS.Cells["A22"].Value = "Other Noncurrent Assets";
                bbsWS.Cells["B22"].Value = bbsnoncurrAssetsOtherCurrentYear;
                bbsWS.Cells["C22"].Value = bbsnoncurrAssetsOtherPreviousYear;
                bbsWS.Cells["D22"].Value = bbsnoncurrAssetsOtherDiff;
                bbsWS.Cells["E22"].Value = bbsnoncurrAssetsOtherPercent / 100;
                bbsWS.Cells["A22:E22"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A23"].Value = "     Total Noncurrent Assets";
                bbsWS.Cells["B23"].Value = bbstotalNonCurrAssetsCurrYear;
                bbsWS.Cells["C23"].Value = bbstotalNonCurrAssetsPrevYear;
                bbsWS.Cells["D23"].Value = bbstotalNonCurrAssetsDiff;
                bbsWS.Cells["E23"].Value = bbstotalNonCurrAssetsPer / 100;
                bbsWS.Cells["A23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //TOTAL ASSETS
                bbsWS.Cells["A25"].Value = "TOTAL ASSETS";
                bbsWS.Cells["B25"].Value = Math.Round(bbstotalAssetsCurrYear, 0);
                bbsWS.Cells["C25"].Value = Math.Round(bbstotalAssetsPrevYear, 0);
                bbsWS.Cells["D25"].Value = Math.Round(bbstotalAssetsDiff, 0);
                bbsWS.Cells["E25"].Value = bbstotalAssetsPer / 100;
                bbsWS.Cells["A25:E25"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                bbsWS.Cells["A25:E25"].Style.Font.Bold = true;

                //LIABILITIES and FUND BALANCE
                bbsWS.Cells["A28"].Value = "LIABILITIES AND FUND BALANCE";
                bbsWS.Cells["A28"].Style.Font.Bold = true;

                bbsWS.Cells["A30"].Value = "Current Liabilities";
                bbsWS.Cells["A30"].Style.Font.Bold = true;

                //CURRENT LIABILITIES
                bbsWS.Cells["A31"].Value = "Trade and Other Payables";
                bbsWS.Cells["B31"].Value = bbscurrLiaTradeCurrentYear;
                bbsWS.Cells["C31"].Value = bbscurrLiaTradePreviousYear;
                bbsWS.Cells["D31"].Value = bbscurrLiaTradeDiff;
                bbsWS.Cells["E31"].Value = bbscurrLiaTradePercent / 100;

                bbsWS.Cells["A32"].Value = "Unearned Interest Income";
                bbsWS.Cells["B32"].Value = bbscurrLiaUnearnedCurrentYear;
                bbsWS.Cells["C32"].Value = bbscurrLiaUnearnedPreviousYear;
                bbsWS.Cells["D32"].Value = bbscurrLiaUnearnedDiff;
                bbsWS.Cells["E32"].Value = bbscurrLiaUnearnedPercent / 100;

                bbsWS.Cells["A33"].Value = "Clients' CBU";
                bbsWS.Cells["B33"].Value = bbscurrLiaClientCBUCurrentYear;
                bbsWS.Cells["C33"].Value = bbscurrLiaClientCBUPreviousYear;
                bbsWS.Cells["D33"].Value = bbscurrLiaClientCBUDiff;
                bbsWS.Cells["E33"].Value = bbscurrLiaClientCBUPercent / 100;

                bbsWS.Cells["A34"].Value = "Tax Payable";
                bbsWS.Cells["B34"].Value = bbscurrLiaTaxPayableCurrentYear;
                bbsWS.Cells["C34"].Value = bbscurrLiaTaxPayablePreviousYear;
                bbsWS.Cells["D34"].Value = bbscurrLiaTaxPayableDiff;
                bbsWS.Cells["E34"].Value = bbscurrLiaTaxPayablePercent / 100;

                bbsWS.Cells["A35"].Value = "MI/CGLI Payables";
                bbsWS.Cells["B35"].Value = bbscurrLiaCGLIPayableCurrentYear;
                bbsWS.Cells["C35"].Value = bbscurrLiaCGLIPayablePreviousYear;
                bbsWS.Cells["D35"].Value = bbscurrLiaCGLIPayableDiff;
                bbsWS.Cells["E35"].Value = bbscurrLiaCGLIPayablePercent / 100;

                bbsWS.Cells["A36"].Value = "Provision for contingenciess";
                bbsWS.Cells["B36"].Value = bbscurrLiaProvisionalCurrentYear;
                bbsWS.Cells["C36"].Value = bbscurrLiaProvisionalPreviousYear;
                bbsWS.Cells["D36"].Value = bbscurrLiaProvisionalDiff;
                bbsWS.Cells["E36"].Value = bbscurrLiaProvisionalPercent / 100;
                bbsWS.Cells["A36:E36"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A37"].Value = "     Total Current Liabilities";
                bbsWS.Cells["B37"].Value = bbstotalCurrLiaCurrYear;
                bbsWS.Cells["C37"].Value = bbstotalCurrLiaPrevYear;
                bbsWS.Cells["D37"].Value = bbstotalCurrLiaDiff;
                bbsWS.Cells["E37"].Value = bbstotalCurrLiaPer / 100;
                bbsWS.Cells["A37:E37"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //NONCURRENT LIABILITIES
                bbsWS.Cells["A39"].Value = "Noncurrent Liabilities";
                bbsWS.Cells["A39"].Style.Font.Bold = true;

                bbsWS.Cells["A40"].Value = "Lease Liability";
                bbsWS.Cells["B40"].Value = bbsnoncurrLiaLeaseCurrentYear;
                bbsWS.Cells["C40"].Value = bbsnoncurrLiaLeasePreviousYear;
                bbsWS.Cells["D40"].Value = bbsnoncurrLiaLeaseDiff;
                bbsWS.Cells["E40"].Value = bbsnoncurrLiaLeasePercent / 100;

                bbsWS.Cells["A41"].Value = "Retirement Benefit Liability";
                bbsWS.Cells["B41"].Value = bbsnoncurrLiaRetirementCurrentYear;
                bbsWS.Cells["C41"].Value = bbsnoncurrLiaRetirementPreviousYear;
                bbsWS.Cells["D41"].Value = bbsnoncurrLiaRetirementDiff;
                bbsWS.Cells["E41"].Value = bbsnoncurrLiaRetirementPercent / 100;

                bbsWS.Cells["A42"].Value = "Deferred Tax Liability";
                bbsWS.Cells["B42"].Value = bbsnoncurrLiaDeferredCurrentYear;
                bbsWS.Cells["C42"].Value = bbsnoncurrLiaDeferredPreviousYear;
                bbsWS.Cells["D42"].Value = bbsnoncurrLiaDeferredDiff;
                bbsWS.Cells["E42"].Value = bbsnoncurrLiaDeferredPercent / 100;
                bbsWS.Cells["A42:E42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A43"].Value = "     Total Noncurrent Liabilities";
                bbsWS.Cells["B43"].Value = bbstotalNonCurrLiaCurrYear;
                bbsWS.Cells["C43"].Value = bbstotalNonCurrLiaPrevYear;
                bbsWS.Cells["D43"].Value = bbstotalNonCurrLiaDiff;
                bbsWS.Cells["E43"].Value = bbstotalNonCurrLiaPer / 100;
                bbsWS.Cells["A43:E43"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A44"].Value = "     Total Liabilities";
                bbsWS.Cells["B44"].Value = bbstotalLiaCurrYear;
                bbsWS.Cells["C44"].Value = bbstotalLiaPrevYear;
                bbsWS.Cells["D44"].Value = bbstotalLiaDiff;
                bbsWS.Cells["E44"].Value = bbstotalLiaPer / 100;
                bbsWS.Cells["A44:E44"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                //FUND BALANCE
                bbsWS.Cells["A46"].Value = "Fund Balance";
                bbsWS.Cells["A46"].Style.Font.Bold = true;

                bbsWS.Cells["A47"].Value = "Retained Earnings";
                bbsWS.Cells["B47"].Value = bbsfundRetainedCurrentYear;
                bbsWS.Cells["C47"].Value = bbsfundRetainedPreviousYear;
                bbsWS.Cells["D47"].Value = bbsfundRetainedDiff;
                bbsWS.Cells["E47"].Value = bbsfundRetainedPercent / 100;

                bbsWS.Cells["A48"].Value = "Cumulative remeasurement gains on";
                bbsWS.Cells["A49"].Value = "     retirement benefit liability";
                bbsWS.Cells["B49"].Value = bbsfundCommulativeCurrentYear;
                bbsWS.Cells["C49"].Value = bbsfundCommulativePreviousYear;
                bbsWS.Cells["D49"].Value = bbsfundCommulativeDiff;
                bbsWS.Cells["E49"].Value = bbsfundCommulativePercent / 100;

                bbsWS.Cells["A50"].Value = "Fair Value Reserve on FVTOCI Financial Assets";
                bbsWS.Cells["B50"].Value = bbsfundFairValueCurrentYear;
                bbsWS.Cells["C50"].Value = bbsfundFairValuePreviousYear;
                bbsWS.Cells["D50"].Value = bbsfundFairValueDiff;
                bbsWS.Cells["E50"].Value = bbsfundFairValuePercent / 100;
                bbsWS.Cells["A50:E50"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A51"].Value = "     Total Fund Balance";
                bbsWS.Cells["B51"].Value = bbstotalFundCurrYear;
                bbsWS.Cells["C51"].Value = bbstotalFundPrevYear;
                bbsWS.Cells["D51"].Value = bbstotalFundDiff;
                bbsWS.Cells["E51"].Value = bbstotalFundPer / 100;
                bbsWS.Cells["A51:E51"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                bbsWS.Cells["A53"].Value = "TOTAL LIABILITIES AND FUND BALANCE";
                bbsWS.Cells["B53"].Value = Math.Round(bbstotalFundLiaCurrYear, 0);
                bbsWS.Cells["C53"].Value = Math.Round(bbstotalFundLiaPrevYear, 0);
                bbsWS.Cells["D53"].Value = Math.Round(bbstotalFundLiaDiff, 0);
                bbsWS.Cells["E53"].Value = bbstotalFundLiaPer / 100;
                bbsWS.Cells["A53:E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                bbsWS.Cells["A53:E53"].Style.Font.Bold = true;

                bbsWS.Column(1).AutoFit();
                bbsWS.Column(2).AutoFit();
                bbsWS.Column(3).AutoFit();
                bbsWS.Column(4).AutoFit();
                bbsWS.Column(5).AutoFit();

                bbsWS.View.FreezePanes(8, 2);
                #endregion

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "FS " + month + year + ".xlsx";

                return fileStreamResult;
            }
        }

        [HttpGet]
        public IActionResult ExcelNotes(string month, int year)
        {

            var model = _notesController.GetAllNotes(month, year);
            var nIncluded = _notesController.GetNotesIncluded(month, year);

            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("NOTES TO FS");
                #region
                ew.Cells.Style.Font.Name = "Century Gothic";
                ew.Cells.Style.Font.Size = 10;
                
                ew.Cells["A1:D1"].Merge = true;
                ew.Cells["A1:D1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.(A MICROFINANCE NGO)";
                ew.Cells["A1:D1"].Style.Font.Bold = true;

                ew.Cells["A2:D2"].Merge = true;
                ew.Cells["A2:D2"].Value = "Notes to Financial Statements";
                ew.Cells["A2:D2"].Style.Font.Bold = true;

                ew.Cells["A3:D3"].Merge = true;
                ew.Cells["A3:D3"].Value = month + " " + year;
                ew.Cells["A3:D3"].Style.Font.Bold = true;

                ew.Cells["A5"].Value = "1";
                ew.Cells["A5"].Style.Font.Bold = true;

                ew.Cells["B5"].Value = "Cash and cash equivalents is composed of the following";
                ew.Cells["C6"].Value = year;
                ew.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["C6"].Style.Font.Bold = true;

                ew.Cells["D6"].Value = year - 1;
                ew.Cells["D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["D6"].Style.Font.Bold = true;

                ew.Cells["E6"].Value = "INCREASE (DECREASE)";
                ew.Cells["E6"].Style.WrapText = true;
                ew.Cells["E6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["E6"].Style.Font.Bold = true;

                ew.Cells["F6"].Value = "%";
                ew.Cells["F6"].Style.Font.Bold = true;
                #endregion

                //NOTES 1
                #region 
                ///NOTES 1
                int rowCount1 = 7;
                decimal totalCashCurr = 0;
                decimal totalCashPrev = 0;
                decimal totalCashDiff = 0;
                decimal totalCashPer = 0;
                foreach (var cash in model.notes1)
                {
                    var part1 = "";
                    if (cash.Particulars == "CashInBank")
                    {
                        part1 = "Cash in bank";
                    }
                    else if (cash.Particulars == "PettyCash")
                    {
                        part1 = "Petty cash fund";
                    }
                    else if (cash.Particulars == "ShortTerm")
                    {
                        part1 = "Short-Term Placements";
                    }
                    ew.Cells["B" + rowCount1].Value = part1;
                    ew.Cells["C" + rowCount1].Value = Math.Round(cash.currAmount,0);
                    ew.Cells["D" + rowCount1].Value = Math.Round(cash.prevAmount,0);
                    ew.Cells["E" + rowCount1].Value = Math.Round(cash.diff,0);
                    ew.Cells["F" + rowCount1].Value = cash.per + " %";

                    ew.Cells["C" + rowCount1].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                    ew.Cells["D" + rowCount1].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                    ew.Cells["E" + rowCount1].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                    ew.Cells["F" + rowCount1].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";

                    ew.Cells["F" + rowCount1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ew.Cells["F" + rowCount1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    rowCount1 += 1;
                    totalCashCurr += cash.currAmount;
                    totalCashPrev += cash.prevAmount;
                    totalCashDiff += cash.diff;
                    
                }
                totalCashPer = (totalCashDiff / totalCashPrev) * 100;
                ew.Cells["B10"].Value = "Cash and Cash equivalents";
                ew.Cells["C10"].Value = Math.Round(totalCashCurr,0);
                ew.Cells["D10"].Value = Math.Round(totalCashPrev,0);
                ew.Cells["E10"].Value = Math.Round(totalCashDiff,0);
                ew.Cells["F10"].Value = Math.Round(totalCashPer,2) + " %";

                ew.Cells["C10"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["D10"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["E10"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["F10"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";

                ew.Cells["F10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["B10"].Style.Font.Bold = true;
                ew.Cells["C10"].Style.Font.Bold = true;
                ew.Cells["D10"].Style.Font.Bold = true;
                ew.Cells["E10"].Style.Font.Bold = true;
                ew.Cells["F10"].Style.Font.Bold = true;

                ew.Cells["C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["E9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["C10"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D10"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E10"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F10"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                #endregion

                //NOTES 1.1
                #region
                ew.Cells["B12"].Value = "Breakdown as follows:";
                ew.Cells["B12"].Style.Font.Bold = true;

                ew.Cells["B14"].Value = "CASH IN BANK";
                ew.Cells["B14"].Style.Font.Bold = true;
                ew.Cells["B14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["B14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                ew.Cells["C15"].Value = "HEAD OFFICE";
                ew.Cells["C15"].Style.Font.Bold = true;
                ew.Cells["C15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                ew.Cells["D15"].Value = "BRANCHES";
                ew.Cells["D15"].Style.Font.Bold = true;
                ew.Cells["D15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                ew.Cells["E15"].Value = "TOTAL";
                ew.Cells["E15"].Style.Font.Bold = true;
                ew.Cells["E15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                decimal totalCibHo = 0;
                decimal totalCibBranch = 0;
                int rowCountcib = 16;
                foreach (var cib in model.cib)
                {
                    ew.Cells["B" + rowCountcib].Value = cib.bank;
                    ew.Cells["C" + rowCountcib].Value = Math.Round(cib.ho,0);
                    ew.Cells["D" + rowCountcib].Value = Math.Round(cib.branches, 0);
                    ew.Cells["E" + rowCountcib].Value = Math.Round(cib.ho,0) + Math.Round(cib.branches,0);

                    ew.Cells["C" + rowCountcib].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["D" + rowCountcib].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["E" + rowCountcib].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                    rowCountcib += 1;
                    totalCibHo += cib.ho;
                    totalCibBranch += cib.branches;
                }

                ew.Cells["B28"].Value = "Cash in Bank";
                ew.Cells["C28"].Value = Math.Round(totalCibHo,0);
                ew.Cells["D28"].Value = Math.Round(totalCibBranch,0);
                ew.Cells["E28"].Value = Math.Round(totalCibHo + totalCibBranch,0);

                ew.Cells["B29"].Value = "% over total funds";
                ew.Cells["C29"].Value = Math.Round((totalCibHo/ (totalCibHo + totalCibBranch)) * 100,2) + "%";
                ew.Cells["D29"].Value = Math.Round((totalCibBranch / (totalCibHo + totalCibBranch)),2) * 100 + "%";
                ew.Cells["E29"].Value = 100 + "%";

                ew.Cells["C28"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["D28"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";
                ew.Cells["E28"].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)";

                ew.Cells["B28"].Style.Font.Bold = true;
                ew.Cells["C28"].Style.Font.Bold = true;
                ew.Cells["D28"].Style.Font.Bold = true;
                ew.Cells["E28"].Style.Font.Bold = true;
                ew.Cells["B29"].Style.Font.Bold = true;
                ew.Cells["C29"].Style.Font.Bold = true;
                ew.Cells["D29"].Style.Font.Bold = true;
                ew.Cells["E29"].Style.Font.Bold = true;

                ew.Cells["C29"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["D29"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ew.Cells["E29"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                ew.Cells["C27"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D27"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["E27"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["C28"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D28"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E28"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["C29"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D29"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E29"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;


                ew.Cells["B31"].Value = "SHOR-TERM PLACEMENTS";
                ew.Cells["B12"].Style.Font.Bold = true;

                int rowCountst = 32;
                decimal totalSt = 0;
                foreach (var st in model.notes1_1)
                {
                    ew.Cells["B" + rowCountst].Value = st.particulars;
                    ew.Cells["C" + rowCountst].Value = Math.Round(st.amount,0);
                    ew.Cells["C" + rowCountst].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    rowCountst += 1;
                    totalSt += st.amount;
                }
                ew.Cells["B34"].Value = "Total";
                ew.Cells["C34"].Value = Math.Round(totalSt,0);
                ew.Cells["C34"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["B34"].Style.Font.Bold = true;
                ew.Cells["C34"].Style.Font.Bold = true;
                ew.Cells["C33"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["C34"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                #endregion

                //NOTES 2
                #region
                ew.Cells["A36"].Value = "2";
                ew.Cells["A36"].Style.Font.Bold = true;
                ew.Cells["B36"].Value = "Loan receivable - net";
                ew.Cells["B36"].Style.Font.Bold = true;
                ew.Cells["C38"].Value = "Loans Receivable";
                ew.Cells["C38"].Style.Font.Bold = true;
                ew.Cells["D38"].Value = "Allowance for Probable Loan Losses";
                ew.Cells["D38"].Style.WrapText = true;
                ew.Cells["D38"].Style.Font.Bold = true;
                ew.Cells["E38"].Value = "Net Receivable";
                ew.Cells["E38"].Style.Font.Bold = true;
                ew.Cells["F38"].Value = "%";
                ew.Cells["F38"].Style.Font.Bold = true;
                ew.Cells["C38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C38"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["D38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D38"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["E38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E38"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["F38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F38"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                decimal totalLR = 0;
                decimal totalLLR = 0;
                decimal totalNR = 0;

                int rowCountN2 = 39;
                foreach (var notes2 in model.notes2)
                {
                    totalLR += notes2.LoansReceivable;
                    totalLLR += notes2.LoansLossReserve;
                    totalNR += notes2.NetReceivable;

                    ew.Cells["B" + rowCountN2].Value = notes2.particulars;
                    ew.Cells["C" + rowCountN2].Value = Math.Round(notes2.LoansReceivable,2);
                    ew.Cells["D" + rowCountN2].Value = Math.Round(notes2.LoansLossReserve, 2);
                    ew.Cells["E" + rowCountN2].Value = Math.Round(notes2.NetReceivable, 2);
                    ew.Cells["F" + rowCountN2].Value = Math.Round((notes2.NetReceivable/totalNR)*100, 2) + "%";
                    ew.Cells["F" + rowCountN2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ew.Cells["F" + rowCountN2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    ew.Cells["C" + rowCountN2].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["D" + rowCountN2].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["E" + rowCountN2].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                    rowCountN2 += 1;
                }

                ew.Cells["B43"].Value = "Total";
                ew.Cells["B43"].Style.Font.Bold = true;
                ew.Cells["C43"].Value = Math.Round(totalLR, 0);
                ew.Cells["C43"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["C43"].Style.Font.Bold = true;

                ew.Cells["D43"].Value = Math.Round(totalLLR, 0);
                ew.Cells["D43"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["D43"].Style.Font.Bold = true;

                ew.Cells["E43"].Value = Math.Round(totalNR, 0);
                ew.Cells["E43"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["E43"].Style.Font.Bold = true;

                ew.Cells["F43"].Value = "100%";
                ew.Cells["F43"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F43"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["F43"].Style.Font.Bold = true;

                ew.Cells["C42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["E42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F42"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["C43"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D43"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E43"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F43"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                foreach (var remarks in nIncluded)
                {
                    if (remarks.id == 2)
                    {
                        ew.Cells["B45:F45"].Value = remarks.remarks;
                        ew.Cells["B45:F45"].Merge = true;
                    }
                }

                #endregion

                //NOTES 3
                #region
                ew.Cells["A48"].Value = "3";
                ew.Cells["A48"].Style.Font.Bold = true;
                ew.Cells["B48:F48"].Value = "Other receivables  consist of Advances to Officers, employees and suppliers.";
                ew.Cells["B48:F48"].Merge = true;
                ew.Cells["C49"].Value = "Other Receivable";
                ew.Cells["C49"].Style.WrapText = true;
                ew.Cells["C49"].Style.Font.Bold = true;
                ew.Cells["D49"].Value = "Allowance for Probable Losses";
                ew.Cells["D49"].Style.WrapText = true;
                ew.Cells["D49"].Style.Font.Bold = true;
                ew.Cells["E49"].Value = "Other Receivables - Net";
                ew.Cells["E49"].Style.Font.Bold = true;
                
                ew.Cells["C49"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C49"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["D49"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D49"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["E49"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E49"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["F49"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F49"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                decimal totalOR = 0;
                decimal totalPL = 0;
                decimal totalON = 0;

                int rowCountN3 = 50;
                foreach (var notes3 in model.notes3)
                {
                    totalOR += notes3.otherReceivables;
                    totalPL += notes3.allowanceProbableCause;
                    totalON += notes3.net;

                    ew.Cells["B" + rowCountN3].Value = notes3.particulars;
                    ew.Cells["C" + rowCountN3].Value = Math.Round(notes3.otherReceivables, 2);
                    ew.Cells["D" + rowCountN3].Value = Math.Round(notes3.allowanceProbableCause, 2);
                    ew.Cells["E" + rowCountN3].Value = Math.Round(notes3.net, 2);

                    ew.Cells["C" + rowCountN3].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["D" + rowCountN3].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["E" + rowCountN3].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                    rowCountN3 += 1;
                }

               
                ew.Cells["C54"].Value = Math.Round(totalOR, 0);
                ew.Cells["C54"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["C54"].Style.Font.Bold = true;

                ew.Cells["D54"].Value = Math.Round(totalPL, 0);
                ew.Cells["D54"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["D54"].Style.Font.Bold = true;

                ew.Cells["E54"].Value = Math.Round(totalNR, 0);
                ew.Cells["E54"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["E54"].Style.Font.Bold = true;

                ew.Cells["C53"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D53"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["E53"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ew.Cells["C54"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D54"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E54"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                foreach (var remarks in nIncluded)
                {
                    if (remarks.id == 3)
                    {
                        ew.Cells["B57:F57"].Value = remarks.remarks;
                        ew.Cells["B57:F57"].Merge = true;
                    }
                }
                #endregion

                //NOTES 4
                #region
                ew.Cells["A59"].Value = "4";
                ew.Cells["A59"].Style.Font.Bold = true;

                ew.Cells["B59"].Value = "Other current assets consist of the ff.:";
                ew.Cells["C60"].Value = year;
                ew.Cells["C60"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C60"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["C60"].Style.Font.Bold = true;

                ew.Cells["D60"].Value = year - 1;
                ew.Cells["D60"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D60"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["D60"].Style.Font.Bold = true;

                ew.Cells["E60"].Value = "INCREASE (DECREASE)";
                ew.Cells["E60"].Style.WrapText = true;
                ew.Cells["E60"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E60"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["E60"].Style.Font.Bold = true;

                ew.Cells["F60"].Value = "%";
                ew.Cells["F60"].Style.Font.Bold = true;

                decimal totalOtherCurr = 0;
                decimal totalOtherPrev = 0;
                decimal totalOtherDiff = 0;
                decimal totalOtherPer = 0;

                var rowCount4 = 61;
                foreach (var notes4 in model.notes4)
                {
                    var part1 = "";
                    if (notes4.Particulars == "prePayments")
                    {
                        part1 = "Prepayments";
                    }
                    else if (notes4.Particulars == "unissuedOfficeSupplies")
                    {
                        part1 = "Unissued Office Supplies";
                    }
                    else if (notes4.Particulars == "otherAssets")
                    {
                        part1 = "Other Assets";
                    }
                    ew.Cells["B" + rowCount4].Value = part1;
                    ew.Cells["C" + rowCount4].Value = Math.Round(notes4.currAmount, 0);
                    ew.Cells["D" + rowCount4].Value = Math.Round(notes4.prevAmount, 0);
                    ew.Cells["E" + rowCount4].Value = Math.Round(notes4.diff, 0);
                    ew.Cells["F" + rowCount4].Value = notes4.per + " %";

                    ew.Cells["C" + rowCount4].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["D" + rowCount4].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["E" + rowCount4].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["F" + rowCount4].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                    ew.Cells["F" + rowCount4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ew.Cells["F" + rowCount4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    rowCount4 += 1;
                    totalOtherCurr += notes4.currAmount;
                    totalOtherPrev += notes4.prevAmount;
                    totalOtherDiff += notes4.diff;

                }
                totalOtherPer = (totalOtherDiff / totalOtherPrev) * 100;
                ew.Cells["B64"].Value = "Other Current Assets";
                ew.Cells["C64"].Value = Math.Round(totalOtherCurr, 0);
                ew.Cells["D64"].Value = Math.Round(totalOtherPrev, 0);
                ew.Cells["E64"].Value = Math.Round(totalOtherDiff, 0);
                ew.Cells["F64"].Value = Math.Round(totalOtherPer, 2) + " %";

                ew.Cells["C64"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["D64"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["E64"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["F64"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                ew.Cells["F64"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F64"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["B64"].Style.Font.Bold = true;
                ew.Cells["C64"].Style.Font.Bold = true;
                ew.Cells["D64"].Style.Font.Bold = true;
                ew.Cells["E64"].Style.Font.Bold = true;
                ew.Cells["F64"].Style.Font.Bold = true;

                ew.Cells["C63"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D63"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["E63"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F63"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["C64"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D64"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E64"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F64"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                foreach (var remarks in nIncluded)
                {
                    if (remarks.id == 4)
                    {
                        ew.Cells["B66:F66"].Value = remarks.remarks;
                        ew.Cells["B66:F66"].Merge = true;
                    }
                }

                #endregion

                //NOTES 5
                #region
                ew.Cells["A69"].Value = "5";
                ew.Cells["A69"].Style.Font.Bold = true;

                ew.Cells["B69"].Value = "Financial assets at FVOCI comprised of:";
                ew.Cells["C70"].Value = year;
                ew.Cells["C70"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C70"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["C70"].Style.Font.Bold = true;

                ew.Cells["D70"].Value = year - 1;
                ew.Cells["D70"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D70"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["D70"].Style.Font.Bold = true;

                ew.Cells["E70"].Value = "INCREASE (DECREASE)";
                ew.Cells["E70"].Style.WrapText = true;
                ew.Cells["E70"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E70"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["E70"].Style.Font.Bold = true;

                ew.Cells["F70"].Value = "%";
                ew.Cells["F70"].Style.Font.Bold = true;

                decimal totalFVOCICurr = 0;
                decimal totalFVOCIPrev = 0;
                decimal totalFVOCIDiff = 0;
                decimal totalFVOCIPer = 0;

                var rowCount5 = 71;
                foreach (var notes5 in model.notes5)
                {
                    var part1 = "";
                    if (notes5.Particulars == "smc")
                    {
                        part1 = "SMC Preferred Shares";
                    }
                    else if (notes5.Particulars == "pinoyAko")
                    {
                        part1 = "Pinoy Ako";
                    }
                    else if (notes5.Particulars == "mes")
                    {
                        part1 = "Formerly MES";
                    }
                    else if (notes5.Particulars == "append")
                    {
                        part1 = "APPEND";
                    }
                    ew.Cells["B" + rowCount5].Value = part1;
                    ew.Cells["C" + rowCount5].Value = Math.Round(notes5.currAmount, 0);
                    ew.Cells["D" + rowCount5].Value = Math.Round(notes5.prevAmount, 0);
                    ew.Cells["E" + rowCount5].Value = Math.Round(notes5.diff, 0);
                    ew.Cells["F" + rowCount5].Value = notes5.per + " %";

                    ew.Cells["C" + rowCount5].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["D" + rowCount5].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["E" + rowCount5].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                    ew.Cells["F" + rowCount5].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                    ew.Cells["F" + rowCount5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ew.Cells["F" + rowCount5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    rowCount5 += 1;
                    totalFVOCICurr += notes5.currAmount;
                    totalFVOCIPrev += notes5.prevAmount;
                    totalFVOCIDiff += notes5.diff;

                }
                totalFVOCIPer = (totalFVOCIDiff / totalFVOCIPrev) * 100;
                ew.Cells["B75"].Value = "";
                ew.Cells["C75"].Value = Math.Round(totalFVOCICurr, 0);
                ew.Cells["D75"].Value = Math.Round(totalFVOCIPrev, 0);
                ew.Cells["E75"].Value = Math.Round(totalFVOCIDiff, 0);
                ew.Cells["F75"].Value = Math.Round(totalFVOCIPer, 2) + " %";

                ew.Cells["C75"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["D75"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["E75"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["F75"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";

                ew.Cells["F75"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F75"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["B75"].Style.Font.Bold = true;
                ew.Cells["C75"].Style.Font.Bold = true;
                ew.Cells["D75"].Style.Font.Bold = true;
                ew.Cells["E75"].Style.Font.Bold = true;
                ew.Cells["F75"].Style.Font.Bold = true;

                ew.Cells["C74"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D74"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["E74"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F74"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["C75"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["D75"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["E75"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F75"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                foreach (var remarks in nIncluded)
                {
                    if (remarks.id == 5)
                    {
                        ew.Cells["B77:F77"].Value = remarks.remarks;
                        ew.Cells["B77:F77"].Merge = true;
                    }
                }

                #endregion

                //NOTES 5.1
                #region
                ew.Cells["B79"].Value = "Breakdown as folloes:";
                ew.Cells["B79"].Style.Font.Bold = true;
                ew.Cells["B79"].Style.Font.Italic = true;
                ew.Cells["B79"].Style.Font.UnderLine = true;

                ew.Cells["B80"].Value = "SMC PREFERRED SHARES";
                ew.Cells["B80"].Style.Font.Bold = true;

                ew.Cells["C81"].Value = "DATE ISSUED";
                ew.Cells["C81"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["C81"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["C81"].Style.Font.Bold = true;

                ew.Cells["D81"].Value = "NO OF. SHARES";
                ew.Cells["D81"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["D81"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["D81"].Style.Font.Bold = true;

                var cRate = year - 1;
                ew.Cells["E81"].Value = "CLOSING RATE 12/31/" +  cRate.ToString();
                ew.Cells["E81"].Style.WrapText = true;
                ew.Cells["E81"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["E81"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["E81"].Style.Font.Bold = true;

                ew.Cells["F81"].Value = "MARKET VALUE";
                ew.Cells["F81"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells["F81"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ew.Cells["F81"].Style.Font.Bold = true;

                decimal totalMarketValueSMC = 0;
                var rowCount5_1 = 82;
                foreach (var item in model.notes5_1)
                {
                    if (item.entity == "SMC PREFERRED SHARES")
                    {
                        ew.Cells["B" + rowCount5_1].Value = item.particulars;
                        ew.Cells["C" + rowCount5_1].Value = item.dateIssued;
                        ew.Cells["C" + rowCount5_1].Style.Numberformat.Format = "mm/dd/yyyy";
                        ew.Cells["D" + rowCount5_1].Value = item.noShares;
                        ew.Cells["D" + rowCount5_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["E" + rowCount5_1].Value = item.closingRate;
                        ew.Cells["E" + rowCount5_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["F" + rowCount5_1].Value = item.marketValue;
                        ew.Cells["F" + rowCount5_1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ew.Cells["F" + rowCount5_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        totalMarketValueSMC += item.marketValue;

                        rowCount5_1 += 1;
                        ew.Cells["B" + rowCount5_1].Value = "TOTAL";
                        ew.Cells["B" + rowCount5_1].Style.Font.Bold = true;
                        ew.Cells["F" + rowCount5_1].Value = totalMarketValueSMC;
                        ew.Cells["F" + rowCount5_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["F" + rowCount5_1].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                        ew.Cells["F" + rowCount5_1].Style.Font.Bold = true;
                    }
                }

                ew.Cells["B85"].Value = "PINOY AKO";
                ew.Cells["B85"].Style.Font.Bold = true;

                decimal totalMarketValuePA = 0;
                var rowCount5_1_1 = 86;
                foreach (var item in model.notes5_1)
                {
                    if (item.entity == "PINOY AKO")
                    {
                        ew.Cells["B" + rowCount5_1_1].Value = item.particulars;
                        ew.Cells["C" + rowCount5_1_1].Value = item.dateIssued;
                        ew.Cells["C" + rowCount5_1_1].Style.Numberformat.Format = "mm/dd/yyyy";
                        ew.Cells["D" + rowCount5_1_1].Value = item.noShares;
                        ew.Cells["D" + rowCount5_1_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["E" + rowCount5_1_1].Value = item.closingRate;
                        ew.Cells["E" + rowCount5_1_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["F" + rowCount5_1_1].Value = item.marketValue;
                        ew.Cells["F" + rowCount5_1_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        totalMarketValuePA += item.marketValue;

                        rowCount5_1_1 += 1;
                    }
                }
                ew.Cells["B90"].Value = "TOTAL";
                ew.Cells["B90"].Style.Font.Bold = true;
                ew.Cells["F90"].Value = totalMarketValuePA;
                ew.Cells["F90"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["F89"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F90"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F90"].Style.Font.Bold = true;


                ew.Cells["B92"].Value = "FORMERLY MES";
                ew.Cells["B92"].Style.Font.Bold = true;

                decimal totalMarketValueMES = 0;
                var rowCount5_1_2 = 93;
                foreach (var item in model.notes5_1)
                {
                    if (item.entity == "FORMERLY MES")
                    {
                        ew.Cells["B" + rowCount5_1_2].Value = item.particulars;
                        ew.Cells["C" + rowCount5_1_2].Value = item.dateIssued;
                        ew.Cells["C" + rowCount5_1_2].Style.Numberformat.Format = "mm/dd/yyyy";
                        ew.Cells["D" + rowCount5_1_2].Value = item.noShares;
                        ew.Cells["D" + rowCount5_1_2].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["E" + rowCount5_1_2].Value = item.closingRate;
                        ew.Cells["E" + rowCount5_1_2].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["F" + rowCount5_1_2].Value = item.marketValue;
                        ew.Cells["F" + rowCount5_1_2].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        totalMarketValueMES += item.marketValue;

                        rowCount5_1_2 += 1;
                    }
                }
                ew.Cells["B112"].Value = "TOTAL";
                ew.Cells["B112"].Style.Font.Bold = true;
                ew.Cells["F112"].Value = totalMarketValueMES;
                ew.Cells["F112"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["F111"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F112"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F112"].Style.Font.Bold = true;


                ew.Cells["B114"].Value = "APPEND";
                ew.Cells["B114"].Style.Font.Bold = true;

                decimal totalMarketValueAPPEND = 0;
                var rowCount5_1_3 = 115;
                foreach (var item in model.notes5_1)
                {
                    if (item.entity == "APPEND")
                    {
                        ew.Cells["B" + rowCount5_1_3].Value = item.particulars;
                        ew.Cells["C" + rowCount5_1_3].Value = item.dateIssued;
                        ew.Cells["C" + rowCount5_1_3].Style.Numberformat.Format = "mm/dd/yyyy";
                        ew.Cells["D" + rowCount5_1_3].Value = item.noShares;
                        ew.Cells["D" + rowCount5_1_3].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["E" + rowCount5_1_3].Value = item.closingRate;
                        ew.Cells["E" + rowCount5_1_3].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        ew.Cells["F" + rowCount5_1_3].Value = item.marketValue;
                        ew.Cells["F" + rowCount5_1_3].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        totalMarketValueAPPEND += item.marketValue;

                        rowCount5_1_3 += 1;
                    }
                }
                ew.Cells["B117"].Value = "TOTAL";
                ew.Cells["B117"].Style.Font.Bold = true;
                ew.Cells["F117"].Value = totalMarketValueAPPEND;
                ew.Cells["F117"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["F116"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["F117"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                ew.Cells["F117"].Style.Font.Bold = true;

                #endregion

                //NOTES 6
                #region

                ew.Cells["A120"].Value = "6";
                ew.Cells["A120"].Style.Font.Bold = true;

                ew.Cells["B120"].Value = " Restricted Cash as follows:";
                ew.Cells["B121"].Value = "HEAD OFFICE";
                ew.Cells["B121"].Style.Font.Bold = true;

                decimal totalHO = 0;
                var rowCount6 = 122;
                foreach (var item in model.notes6)
                {
                    if (item.description == "HO")
                    {
                        ew.Cells["B" + rowCount6].Value = item.particulars;
                        ew.Cells["B" + rowCount6 + ":C" + rowCount6].Merge = true;
                        ew.Cells["D" + rowCount6].Value = item.amount;
                        ew.Cells["D" + rowCount6].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        rowCount6 += 1;
                        totalHO += item.amount;
                    }
                    
                }

                ew.Cells["B131"].Value = "Sub-Total";
                ew.Cells["B131"].Style.Font.Bold = true;
                ew.Cells["D131"].Value = totalHO;
                ew.Cells["D131"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["D131"].Style.Font.Bold = true;
                ew.Cells["D130"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D131"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;


                ew.Cells["B132"].Value = "SHORT-TERM PLACEMENTS";
                ew.Cells["B132"].Style.Font.Bold = true;

                decimal totalST = 0;
                var rowCount6_1 = 133;
                foreach (var item in model.notes6)
                {
                    if (item.description == "SHORTTERM")
                    {
                        ew.Cells["B" + rowCount6_1].Value = item.particulars;
                        ew.Cells["B" + rowCount6_1 + ":C" + rowCount6_1].Merge = true;
                        ew.Cells["D" + rowCount6_1].Value = item.amount;
                        ew.Cells["D" + rowCount6_1].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                        rowCount6_1 += 1;
                        totalST += item.amount;
                    }
                    
                }

                ew.Cells["B134"].Value = "Sub-Total";
                ew.Cells["B134"].Style.Font.Bold = true;
                ew.Cells["D134"].Value = totalST;
                ew.Cells["D134"].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* -??_);_(@_)";
                ew.Cells["D134"].Style.Font.Bold = true;
                ew.Cells["D133"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ew.Cells["D134"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;

                #endregion

                ew.Column(1).AutoFit();
                ew.Column(2).AutoFit();
                ew.Column(3).AutoFit();
                ew.Column(4).AutoFit();
                ew.Column(5).AutoFit();
                ew.Column(6).AutoFit();

                //ew.View.FreezePanes(8, 2);

                
             

                MemoryStream stream = new MemoryStream();
                ep.SaveAs(stream);
                stream.Position = 0;
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "FS NOTES " + month + year + ".xlsx";

                return fileStreamResult;
            }
        }
    }

}