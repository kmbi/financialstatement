﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using fs.Models;
using Microsoft.AspNetCore.Authorization;
using System.Data.SqlClient;
using System.Data;

namespace fs.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        string conString = SQLConString.consTring();

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public List<dashboardAll> GetDashboard()
        {
            List<dashboardAll> viewModel = new List<dashboardAll>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboard";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboardAll
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            amount = reader.GetDecimal(2),
                            particulars = reader.GetString(3)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<dashboard> GetDashboardPAR()
        {
            List<dashboard> viewModel = new List<dashboard>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboardGetPAR";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboard
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            forTheMonth = reader.GetDecimal(2)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<dashboard> GetDashboardDisbursement()
        {
            List<dashboard> viewModel = new List<dashboard>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboardGetDisbursements";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboard
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            forTheMonth = reader.GetDecimal(2)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<dashboardAll> GetDashboardIncome()
        {
            List<dashboardAll> viewModel = new List<dashboardAll>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboardIncome";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboardAll
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            amount = reader.GetDecimal(2),
                            particulars = reader.GetString(3)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<dashboardAll> GetDashboardIncomeDESC()
        {
            List<dashboardAll> viewModel = new List<dashboardAll>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboardIncomeDESC";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboardAll
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            amount = reader.GetDecimal(2),
                            particulars = reader.GetString(3)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<dashboardAll> GetDashboardNetIncome()
        {
            List<dashboardAll> viewModel = new List<dashboardAll>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboardNetIncome";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboardAll
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            amount = reader.GetDecimal(2),
                            particulars = reader.GetString(3)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<dashboardAll> GetDashboardNetIncomeDESC()
        {
            List<dashboardAll> viewModel = new List<dashboardAll>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spDashboardNetIncomeDESC";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@month", month));
                //cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new dashboardAll
                        {
                            month = reader.GetString(0).Trim(),
                            year = reader.GetInt32(1).ToString().Trim(),
                            amount = reader.GetDecimal(2),
                            particulars = reader.GetString(3)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
