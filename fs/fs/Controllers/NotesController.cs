﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using fs.Models;
using fs.Models.IRepo;
using Microsoft.AspNetCore.Mvc;

namespace fs.Controllers
{
    public class NotesController : Controller
    {
        string conString = SQLConString.consTring();
        private iUserManager _userManager;

        public NotesController(iUserManager userManager)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            
        }

        public List<notes> GetNotes(string month, int year)
        {
            List<notes> viewModel = new List<notes>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select * from notes where id not in (select notesId from fsNotes where month = '" + month + "' and year = '" + year + "')";
                SqlCommand cmd = new SqlCommand(sql, con);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new notes
                        {
                            id = reader.GetInt32(0),
                            description = reader.GetString(1)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        [HttpGet]
        public bool CheckNotesIfExist(string month, int year, int notesId)
        {
            bool result;
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "Select * from fsNotes where month = '" + month + "' and year = '" + year + "' and notesId = '" + notesId + "'";
                SqlCommand cmd = new SqlCommand(sql, con);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = true;
                }
                else {
                    result = false;
                }
                con.Close();
            }
            return result;
        }

        public IActionResult GetFSNotes(string month, int year)
        {
            List<fsNotesViewModel> viewModel = new List<fsNotesViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "select fsNotes.*,notes.description from fsNotes " +
                            " inner join notes on fsNotes.notesId = notes.id where fsNotes.month = '" + month + "' and fsNotes.year = '" + year + "' order by fsNotes.notesId";
                SqlCommand cmd = new SqlCommand(sql, con);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new fsNotesViewModel
                        {
                            id = reader.GetInt32(0),
                            notesId = reader.GetInt32(1),
                            remarks = reader.GetString(2),
                            month = reader.GetString(3),
                            year = reader.GetInt32(4),
                            description = reader.GetString(8)
                        });
                    }
                }
                con.Close();
            }
            return Json(new { data = viewModel });
            //return viewModel;
        }

        [HttpPost]
        public void Insert(fsNotes data)
        {
            string empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "INSERT INTO fsNotes(notesId,remarks,month,year,createdBy,createdAt,updatedAt)";
                sql += "VALUES('" + data.notesId + "','" +
                    data.remarks.Replace("'","") + "','" +
                    data.month + "','" +
                    data.year + "','" +
                    empCode + "','" +
                    DateTime.Now + "','" +
                    DateTime.Now + "')";
                SqlCommand cmd = new SqlCommand(sql, con);

                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpPost]
        public void Update(fsNotes data)
        {
            string empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "UPDATE fsNotes SET remarks = '" + data.remarks.Replace("'", "") + "'" +
                    ",updatedAt='" + DateTime.Now + "'" +
                    " WHERE month = '" + data.month + "' and year = '" + data.year + "' and notesId = '" + data.notesId + "'";
                SqlCommand cmd = new SqlCommand(sql, con);

                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        [HttpDelete]
        public void Delete(int id)
        {
            string empCode = _userManager.GetCurrentUser(this.HttpContext).employeeCode.ToString();

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE FROM fsNotes where id = " + id + "";
                SqlCommand cmd = new SqlCommand(sql, con);

                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public List<notesCashViewModel> GetNotesCash(string month, int year)
        {
            List<notesCashViewModel> viewModel = new List<notesCashViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "spNotes1";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new notesCashViewModel
                        {
                            particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
           return viewModel;
        }

        public List<cibViewModel> GetNotesCIB(string month, int year)
        {
            List<cibViewModel> viewModel = new List<cibViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select id, bank, ROUND(ho,0) as ho, ROUND(branches,0) as branches,month, year, uploadedBy, uploadedDate from cib where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new cibViewModel
                        {
                            id = reader.GetInt32(0),
                            bank = reader.GetString(1),
                            ho = reader.GetDecimal(2),
                            branches = reader.GetDecimal(3),
                            month = reader.GetString(4),
                            year = reader.GetInt32(5).ToString()
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public List<notesIncluded> GetNotesIncluded(string month, int year)
        {
            List<notesIncluded> viewModel = new List<notesIncluded>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotesIncluded";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        viewModel.Add(new notesIncluded
                        {
                            id = reader.GetInt32(0),
                            description = reader.GetString(1),
                            wTable = reader.GetBoolean(2),
                            remarks = reader.GetString(3),
                            included = reader.GetInt32(4)
                        });
                    }
                }
                con.Close();
            }
            //return Json(new { data = viewModel });
            return viewModel;
        }

        public notesViewModel GetAllNotes(string month, int year)
        {
            notesViewModel notesViewModel = new notesViewModel();

            //NOTES 1
            List<notes1> notes1 = new List<notes1>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes1";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes1.Add(new notes1
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //CIB
            List<cibViewModel> cibViewModel = new List<cibViewModel>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                //38 columns
                string sql = "Select * from cib where month = '" + month + "' and year = '" + year + "'";
                SqlCommand cmd = new SqlCommand(sql, con);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cibViewModel.Add(new cibViewModel
                        {
                            id = reader.GetInt32(0),
                            bank = reader.GetString(1),
                            ho = reader.GetDecimal(2),
                            branches = reader.GetDecimal(3),
                            month = reader.GetString(4),
                            year = reader.GetInt32(5).ToString()
                        });
                    }
                }
                con.Close();
            }

            //NOTES 1.1
            List<notes1_1> notes1_1 = new List<notes1_1>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes1_1";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes1_1.Add(new notes1_1
                        {
                            particulars = reader.GetString(1),
                            amount = reader.GetDecimal(2)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 2
            List<notes2> notes2 = new List<notes2>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes2";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes2.Add(new notes2
                        {
                            particulars = reader.GetString(0),
                            LoansReceivable = reader.GetDecimal(1),
                            LoansLossReserve = reader.GetDecimal(2),
                            NetReceivable = reader.GetDecimal(3)
                        });
                    }
                }
                con.Close();
            }


            //NOTES 3
            List<notes3> notes3 = new List<notes3>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes3";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes3.Add(new notes3
                        {
                            particulars = reader.GetString(0),
                            otherReceivables = reader.GetDecimal(1),
                            allowanceProbableCause = reader.GetDecimal(2),
                            net = reader.GetDecimal(3)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 4
            List<notes4> notes4 = new List<notes4>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes4";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes4.Add(new notes4
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(2),
                            prevAmount = reader.GetDecimal(3),
                            diff = reader.GetDecimal(4),
                            per = reader.GetDecimal(5)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 5
            List<notes5> notes5 = new List<notes5>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes5";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes5.Add(new notes5
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 5.1
            List<notes5_1> notes5_1 = new List<notes5_1>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes5_1";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var dateIssued = "";
                        var s = reader.GetDateTime(3).ToString();
                        if (reader.GetDateTime(3).ToShortDateString() == "01/01/1900")
                        {
                            dateIssued = "";
                        }
                        else
                        {
                            dateIssued = reader.GetDateTime(3).ToString("MMM dd, yyyy");
                        }
                        notes5_1.Add(new notes5_1
                        {
                            particulars = reader.GetString(1),
                            entity = reader.GetString(2),
                            dateIssued = dateIssued,
                            noShares = reader.GetInt32(4),
                            closingRate = reader.GetDecimal(5),
                            marketValue = reader.GetDecimal(6)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 6
            List<notes6> notes6 = new List<notes6>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes6";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes6.Add(new notes6
                        {
                            particulars = reader.GetString(1),
                            amount = reader.GetDecimal(2),
                            description = reader.GetString(3)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 8
            List<notes8> notes8 = new List<notes8>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes8";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes8.Add(new notes8
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 9
            List<notes9> notes9 = new List<notes9>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes9";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes9.Add(new notes9
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 10
            List<notes10> notes10 = new List<notes10>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes10";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes10.Add(new notes10
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 12
            List<notes12> notes12 = new List<notes12>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes12";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes12.Add(new notes12
                        {
                            particulars = reader.GetString(0),
                            CBU = reader.GetDecimal(1)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 14
            List<notes14> notes14 = new List<notes14>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes14";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes14.Add(new notes14
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 23
            List<notes23> notes23 = new List<notes23>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes23";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes23.Add(new notes23
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 28
            List<notes28> notes28 = new List<notes28>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes28";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes28.Add(new notes28
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 30
            List<notes30> notes30 = new List<notes30>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes30";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes30.Add(new notes30
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 36
            List<notes36> notes36 = new List<notes36>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes36";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes36.Add(new notes36
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 39
            List<notes39> notes39 = new List<notes39>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes39";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes39.Add(new notes39
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 41
            List<notes41> notes41 = new List<notes41>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes41";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes41.Add(new notes41
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 44
            List<notes44> notes44 = new List<notes44>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes44";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes44.Add(new notes44
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 45
            List<notes45> notes45 = new List<notes45>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes45";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes45.Add(new notes45
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            //NOTES 49
            List<notes49> notes49 = new List<notes49>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "spNotes49";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@month", month));
                cmd.Parameters.Add(new SqlParameter("@year", year));

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notes49.Add(new notes49
                        {
                            Particulars = reader.GetString(0),
                            currAmount = reader.GetDecimal(1),
                            prevAmount = reader.GetDecimal(2),
                            diff = reader.GetDecimal(3),
                            per = reader.GetDecimal(4)
                        });
                    }
                }
                con.Close();
            }

            notesViewModel.notes1 = notes1;
            notesViewModel.cib = cibViewModel;
            notesViewModel.notes1_1 = notes1_1;
            notesViewModel.notes2 = notes2;
            notesViewModel.notes3 = notes3;
            notesViewModel.notes4 = notes4;
            notesViewModel.notes5 = notes5;
            notesViewModel.notes5_1 = notes5_1;
            notesViewModel.notes6 = notes6;
            notesViewModel.notes8 = notes8;
            notesViewModel.notes9 = notes9;
            notesViewModel.notes10 = notes10;
            notesViewModel.notes12 = notes12;
            notesViewModel.notes14 = notes14;
            notesViewModel.notes23 = notes23;
            notesViewModel.notes28 = notes28;
            notesViewModel.notes30 = notes30;
            notesViewModel.notes36 = notes36;
            notesViewModel.notes39 = notes39;
            notesViewModel.notes41 = notes41;
            notesViewModel.notes44 = notes44;
            notesViewModel.notes45 = notes45;
            notesViewModel.notes49 = notes49;

            return notesViewModel;
        }
    }
}