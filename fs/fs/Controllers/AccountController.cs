﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using fs.Models;
using fs.Models.IRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fs.Controllers
{
    public class AccountController : Controller
    {
        string conString = SQLConString.consTring();

        private iAccountRepository _repoAccount;
        private iUserManager _userManager;

        public AccountController(iAccountRepository repoAccount, iUserManager userManager)
        {
            _repoAccount = repoAccount;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public IActionResult Login(LoginViewModel lv, string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Email/Password not found");
                return View(lv);
            }

            var user = _userManager.Validate(lv.email);

            if (user.employeeCode != null)
            {
                var model = _repoAccount.login(lv.email);
                var hashedPassword = model.password;
                bool validPassword = BCrypt.Net.BCrypt.Verify(lv.password, hashedPassword);

                if (validPassword)
                {
                    _userManager.SignIn(this.HttpContext, user, false);

                    var d = User.Identity.IsAuthenticated;
                    Console.Write("auth: " + d);
                    //if (string.IsNullOrEmpty(lv.ReturnUrl))
                    //    return RedirectToAction("Index", "Home");

                    //return View(lv.ReturnUrl);

                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    else
                        return RedirectToAction("Index", "Home");
                }
            }

            ModelState.AddModelError("Error", "Email/Password not found");
            return View(lv);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            _userManager.SignOut(this.HttpContext);
            return this.RedirectToAction("Login", "Account");
        }

        public IActionResult Users()
        {
            return View();
        }

        public IActionResult GetUsers()
        {
            //GET systemUsers with access
            List<systemUsersViewModel> viewModel = new List<systemUsersViewModel>();


            //GET ALL USERS
            var model = _repoAccount.getAllUsers();
            foreach (var item in model)
            {
                using (SqlConnection con = new SqlConnection(conString))
                {
                    con.Open();
                    string sql = "SELECT * FROM systemUsers where empCode = '" + item.employeeCode + "'";
                    SqlCommand cmd = new SqlCommand(sql, con);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            viewModel.Add(new systemUsersViewModel
                            {
                                empCode = reader.GetString(1),
                                email = reader.GetString(2),
                                lastName = item.name.last,
                                firstName = item.name.first,
                                middleName = item.name.middle,
                                department = item.employment.department,
                                haveAccess = true
                            });
                        }
                    }

                    con.Close();
                }

            }
            return Json(new { data = viewModel });
        }

        public IActionResult GetAllUsers()
        {

            //GET systemUsers with access
            List<systemUsersViewModel> viewModel = new List<systemUsersViewModel>();

            //GET ALL USERS
            var model = _repoAccount.getAllUsers();

            foreach (var item in model)
            {
                using (SqlConnection con = new SqlConnection(conString))
                {
                    con.Open();
                    string sql = "SELECT * FROM systemUsers where empCode = '" + item.employeeCode + "'";
                    SqlCommand cmd = new SqlCommand(sql, con);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        //while (reader.Read())
                        //{
                        //    viewModel.Add(new systemUsersViewModel
                        //    {
                        //        empCode = reader.GetString(1),
                        //        email = reader.GetString(2),
                        //        lastName = item.name.last,
                        //        firstName = item.name.first,
                        //        middleName = item.name.middle,
                        //        department = item.employment.department,
                        //        haveAccess = true
                        //    });
                        //}
                    }
                    else
                    {
                        viewModel.Add(new systemUsersViewModel
                        {
                            empCode = item.employeeCode,
                            email = item.email,
                            lastName = item.name.last,
                            firstName = item.name.first,
                            middleName = item.name.middle,
                            department = item.employment.department,
                            haveAccess = true
                        });
                    }

                    con.Close();
                }

            }
            //return model;
            return Json(new { data = viewModel });
        }

        public void Insert(string empCode, string email)
        {
            
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "INSERT INTO systemUsers (empCode, email) VALUES ('" + empCode + "', '" + email + "')";
                SqlCommand cmd = new SqlCommand(sql, con);

                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void Delete(string empCode)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "DELETE from systemUsers where empCode = '" + empCode + "'";
                SqlCommand cmd = new SqlCommand(sql, con);

                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

    }
}