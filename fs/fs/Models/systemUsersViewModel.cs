﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class systemUsersViewModel
    {
        public string empCode { get; set; }
        public string email { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string department { get; set; }
        public bool haveAccess { get; set; }
    }
}
