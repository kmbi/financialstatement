﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class dashboard
    {
        public string month { get; set; }
        public string year { get; set; }
        public decimal forTheMonth { get; set; }
    }

    public class dashboardAll
    {
        public string month { get; set; }
        public string year { get; set; }
        public decimal amount { get; set; }
        public string particulars { get; set; }
    }
}
