﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class fsNotes
    {
        public int id { get; set; }
        public int notesId { get; set; }
        public string remarks { get; set; }
        public string month { get; set; }
        public int year { get; set; }
        public string createdBy { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class fsNotesViewModel
    {
        public int id { get; set; }
        public int notesId { get; set; }
        public string remarks { get; set; }
        public string description { get; set; }
        public string month { get; set; }
        public int year { get; set; }
        public string createdBy { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
