﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class notesIncluded
    {
        public int id { get; set; }
        public string description { get; set; }
        public bool wTable { get; set; }
        public string remarks { get; set; }
        public int included { get; set; }
    }
}
