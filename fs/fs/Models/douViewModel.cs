﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class douViewModel
    {
        public int id { get; set; }
        public string particulars { get; set; }
        public decimal forTheDay { get; set; }
        public decimal cumulative { get; set; }
        public decimal forTheMonth { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
