﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class cibViewModel
    {
        public int id { get; set; }
        public string bank { get; set; }
        public decimal ho { get; set; }
        public decimal branches { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
