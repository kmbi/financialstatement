﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class notesViewModel
    {
        public List<notes1> notes1 {get; set;}
        public List<cibViewModel> cib { get; set; }
        public List<notes1_1> notes1_1 { get; set; }
        public List<notes2> notes2 { get; set; }
        public List<notes3> notes3 { get; set; }
        public List<notes4> notes4 { get; set; }
        public List<notes5> notes5 { get; set; }
        public List<notes5_1> notes5_1 { get; set; }
        public List<notes6> notes6 { get; set; }
        public List<notes8> notes8 { get; set; }
        public List<notes9> notes9 { get; set; }
        public List<notes10> notes10 { get; set; }
        public List<notes12> notes12 { get; set; }
        public List<notes14> notes14 { get; set; }
        public List<notes23> notes23 { get; set; }
        public List<notes28> notes28 { get; set; }
        public List<notes30> notes30 { get; set; }
        public List<notes36> notes36 { get; set; }
        public List<notes39> notes39 { get; set; }
        public List<notes41> notes41 { get; set; }
        public List<notes44> notes44 { get; set; }
        public List<notes45> notes45 { get; set; }
        public List<notes49> notes49 { get; set; }
    }

    public class notes1
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes1_1
    {
        public string particulars { get; set; }
        public decimal amount { get; set; }
    }
    public class notes2
    {
        public string particulars { get; set; }
        public decimal LoansReceivable { get; set; }
        public decimal LoansLossReserve { get; set; }
        public decimal NetReceivable { get; set; }
    }
   
    public class notes3
    {
        public string particulars { get; set; }
        public decimal otherReceivables { get; set; }
        public decimal allowanceProbableCause { get; set; }
        public decimal net { get; set; }
    }
    public class notes4
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes5
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes5_1
    {
        public string particulars { get; set; }
        public string entity { get; set; }
        public string dateIssued { get; set; }
        public int noShares { get; set; }
        public decimal closingRate { get; set; }
        public decimal marketValue { get; set; }
    }
    public class notes6
    {
        public string particulars { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
    }
    public class notes8
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes9
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes10
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes12
    {
        public string particulars { get; set; }
        public decimal CBU { get; set; }
    }
    public class notes14
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes23
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes28
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes30
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes36
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes39
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes41
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes44
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes45
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
    public class notes49
    {
        public string Particulars { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
    }
}
