﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class incomeStatementViewModel
    {
        public int id { get; set; }
        public string accntTitle { get; set; }
        public decimal m1 { get; set; }
        public decimal m2 { get; set; }
        public decimal m3 { get; set; }
        public decimal m4 { get; set; }
        public decimal m5 { get; set; }
        public decimal m6 { get; set; }
        public decimal m7 { get; set; }
        public decimal m8 { get; set; }
        public decimal m9 { get; set; }
        public decimal m10 { get; set; }
        public decimal m11 { get; set; }
        public decimal m12 { get; set; }
        public string year { get; set; }
    }
}
