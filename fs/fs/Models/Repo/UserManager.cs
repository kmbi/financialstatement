﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using MongoDB.Driver;
using fs;
using fs.Models;
using fs.Models.IRepo;

namespace fs.Models.Repo
{
    public class UserManager : AppDbContext, iUserManager
    {
        iAccountRepository _user;
        public UserManager(iAccountRepository user, DbContextOptions<AppDbContext> options) : base(options)
        {
            _user = user;
        }

        public users GetCurrentUser(HttpContext httpContext)
        {
            string currentUserId = this.GetCurrentUserId(httpContext).ToString();

            if (currentUserId == null)
                return null;
            //int currUser = int.Parse(currentUserId);
            //return users.Find(currUser);

            //return user.Find(currUser);
            return user.Find(u => u.email == currentUserId).FirstOrDefault();
        }

        public string GetCurrentUserId(HttpContext httpContext)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
                return null;

            Claim claim = httpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (claim == null)
                return null;

            string currentUserId = claim.Value;

            //if (!int.TryParse(claim.Value, out currentUserId))
            //    return -1;

            return currentUserId;
        }

        public async void SignIn(HttpContext httpContext, users user, bool isPersistent = false)
        {
            ClaimsIdentity identity = new ClaimsIdentity(this.GetUserClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await httpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties() { IsPersistent = isPersistent }
            );
            httpContext.User = principal;

        }

        public async void SignOut(HttpContext httpContext)
        {
            await httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public users userDetails(HttpContext httpContext)
        {
            var email = GetCurrentUser(httpContext).email;
            var user = _user.login(email);
            return user;

        }

        public users Validate(string email)
        {
            var user = _user.login(email);

            return user;
        }

        private IEnumerable<Claim> GetUserClaims(users user)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Sid, user.employeeCode.ToString()));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.email.ToString()));
            claims.Add(new Claim(ClaimTypes.GivenName, user.name.first.ToString()));
            claims.Add(new Claim(ClaimTypes.Email, user.name.middle.ToString()));
            claims.Add(new Claim(ClaimTypes.Surname, user.name.last.ToString()));
            return claims;
        }
    }
}
