﻿
using fs.Models.IRepo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models.Repo
{
    public class AccountRepository : AppDbContext, iAccountRepository
    {
        string conString = SQLConString.consTring();
        // private readonly AppDbContext _db;
        public AccountRepository(DbContextOptions<AppDbContext> options) : base(options)
        {
            //_db = db;
        }

        public void changePassword(string slug, string password)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", slug);
            var s = user.Find(filter1).FirstOrDefault();

            var update = Builders<users>.Update.Set("password", password)
           .Set("updatedAt", DateTime.Now);
            user.UpdateOne(filter1, update);
        }

        public List<users> getAllUsers()
        {
            //return user.Find(new BsonDocument()).ToList();
            var sort = Builders<users>.Sort.Ascending(e => e.name.last).Ascending(e => e.name.first).Ascending(e => e.name.middle);
            var filter1 = Builders<users>.Filter.Eq("employment.branch", "head-office-imdptlz");
            var filter2 = Builders<users>.Filter.Ne("employment.status", "RESIGNED");
            var filter = Builders<users>.Filter.And(filter1, filter2);
            return user.Find(filter).Sort(sort).ToList();
        }

        public string getEmail(string empCode)
        {
            var filter = Builders<users>.Filter.Eq("employeeCode", empCode.Trim());
            return user.Find(filter).FirstOrDefault().email;
        }

        public string getPassword(string slug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", slug);
            return  user.Find(filter1).FirstOrDefault().password;
        }

        public users login(string email)
        {
            var users = user.Find(u => u.email == email).FirstOrDefault();
            //return user.Find(u => u.email == email).FirstOrDefault();
            users result = new users();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                string sql = "SELECT * FROM systemUsers where email = '" + email + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = users;
                }

                con.Close();
            }

            return result;
        }
        
    }
}
