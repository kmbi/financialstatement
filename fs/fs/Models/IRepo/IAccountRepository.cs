﻿
using fs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models.IRepo
{
    public interface iAccountRepository
    {
        users login(string email);
        void changePassword(string slug, string password);
        string getPassword(string slug);
        string getEmail(string empCode);

        List<users> getAllUsers();
    }
}
