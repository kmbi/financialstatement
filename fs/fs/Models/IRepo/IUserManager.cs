﻿
using fs.Models;
using Microsoft.AspNetCore.Http;

namespace fs.Models.IRepo
{
    public interface iUserManager
    {
        users Validate(string username);
        void SignIn(HttpContext httpContext, users user, bool isPersistent = false);
        void SignOut(HttpContext httpContext);
        string GetCurrentUserId(HttpContext httpContext);
        users GetCurrentUser(HttpContext httpContext);
       // registration userDetails(HttpContext httpContext);
    }
}
