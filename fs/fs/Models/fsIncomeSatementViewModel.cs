﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class fsIncomeStatementViewModel
    {
        public string particulars { get; set; }
        public string accntType { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
        public string month { get; set; }
        public int currYear { get; set; }
        public int prevYear { get; set; }
    }

    public class fsIncomeStatementMonthlyViewModel
    {
        public string particulars { get; set; }
        public string accntType { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
        public string currMonth { get; set; }
        public string prevMonth { get; set; }
        public int currYear { get; set; }
        public int prevYear { get; set; }
    }

    public class fsMonthlyISvsBudgetViewModel
    {
        public string particulars { get; set; }
        public string accntType { get; set; }
        public decimal budgetMonthly { get; set; }
        public decimal budgetCumulative { get; set; }
        public decimal actual { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
    public class fsMonthlyBSvsBudgetViewModel
    {
        public string particulars { get; set; }
        public string accntType { get; set; }
        public decimal budgetMonthly { get; set; }
        public decimal budgetCumulative { get; set; }
        public decimal actual { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
