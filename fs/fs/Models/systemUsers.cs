﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class systemUsers
    {
        public int id { get; set; }
        public string empCode { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
