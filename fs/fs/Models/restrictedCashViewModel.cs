﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class restrictedCashViewModel
    {
        public int id { get; set; }
        public string particulars { get; set; }
        public decimal amount { get; set; }
        public string description { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
