﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class fsBalanceSheetViewModel
    {
        public string particulars { get; set; }
        public string accntType { get; set; }
        public decimal currAmount { get; set; }
        public decimal prevAmount { get; set; }
        public decimal diff { get; set; }
        public decimal per { get; set; }
        public string month { get; set; }
        public int currYear { get; set; }
        public int prevYear { get; set; }
    }
}
