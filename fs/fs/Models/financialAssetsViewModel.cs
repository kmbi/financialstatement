﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class financialAssetsViewModel
    {
        public int id { get; set; }
        public string particulars { get; set; }
        public string entity { get; set; }
        public string dateIssued { get; set; }
        public Int32 noShares { get; set; }
        public decimal closingRate { get; set; }
        public decimal marketValue { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
