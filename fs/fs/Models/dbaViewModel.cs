﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class dbaViewModel
    {
        public int id { get; set; }
        public string accntNo { get; set; }
        public string accntTitle { get; set; }
        public string accntType { get; set; }
        public decimal consolidated { get; set; }
        public decimal headOffice { get; set; }
        public decimal northLuzon { get; set; }
        public decimal southLuzon { get; set; }
        public decimal visayas { get; set; }
        public decimal mindanao { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
