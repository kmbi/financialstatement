﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs.Models
{
    public class shortTermViewModel
    {
        public int id { get; set; }
        public string particulars { get; set; }
        public decimal amount { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
