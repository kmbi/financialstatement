﻿using fs.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fs
{
    public class AppDbContext : IdentityDbContext<IdentityUser>
    {
        private readonly IMongoDatabase _db;
        public static IMongoClient _client;

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            _db = Connect();
        }

        public IMongoDatabase Connect()
        {
            var client = new MongoClient("mongodb+srv://dbdev:8yWRKkJuhx3bngxx@version3-mj3ao.mongodb.net/test");
            //var client = new MongoClient("mongodb://localhost:27017/kmbi");
            var database = client.GetDatabase("kmbi");

            return database;
        }

        public IMongoCollection<users> user => _db.GetCollection<users>("users");
    }
}
